import http from "k6/http";
import { check } from "k6";
import { randomString } from "https://jslib.k6.io/k6-utils/1.2.0/index.js";
import { uuidv4 } from "https://jslib.k6.io/k6-utils/1.4.0/index.js";

export const options = {
  stages: [
    { duration: "1m", target: 200 },
    { duration: "2m", target: 200 },
    { duration: "30s", target: 0 },
  ],
};

export default function () {
  const username = `user_${randomString(8)}`;

  const payload = {
    authentication: {
      data: {
        username: username,
        password:
                "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3",
      },
      type: "basic",
    },
    username: username,
    groups: [],
  };

  const headers = {
    accept: "*/*",
    "X-Correlation-Id": "f08f278e-f6f5-4706-ab09-b230612d9cc8",
    "Accept-Language": "en-US",
    "X-Started": "2024-11-09T18:01:03.748Z",
    "X-Context-Category": "default",
    "X-Context-Id": "none",
    "X-Client-Id": "dafae812-6a07-4e4f-8ac5-d5a81bb32cab",
    "X-Client-Name": "swagger",
    "X-Client-Version": "0.3.6",
    "X-Client-Variant": "dev",
    "X-Created": "2024-11-09T18:01:03.748Z",
    "Content-Type": "application/json",
  };

  const response = http.post(
          `http://localhost:3000/api/v1/user/${uuidv4()}`,
          JSON.stringify(payload),
          { headers: headers }
  );

  const isExpectedStatus = check(response, {
    "status is 200, 201 or 409": (r) => [200, 201, 409].includes(r.status),
  });

  if (!isExpectedStatus) {
    console.log(`
      Unexpected response:
      Status: ${response.status}
      Body: ${response.body}
      Username: ${username}
      Time: ${new Date().toISOString()}
    `);
  }

  // Optional: Track success rates for each status code
  if (response.status === 200) {
    // Successful request
  } else if (response.status === 201) {
    // Created successfully
  } else if (response.status === 409) {
    // Conflict - probably duplicate username
  }
}
