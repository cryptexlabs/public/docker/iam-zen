import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { APP_FILTER } from '@nestjs/core';
import {
  AppHttpExceptionFilter,
  ForwardedUriMiddleware,
  HealthzService,
} from '@cryptexlabs/codex-nodejs-common';
import { appConfig, baseLogger, setupProviders } from './setup';
import { Locale } from '@cryptexlabs/codex-data-model';
import { appProviders } from './app/app.providers';
import { authenticationProviders } from './authentication/authentication.providers';
import { groupProviders } from './group/group.providers';
import { permissionProviders } from './permission/permission.providers';
import { policyProviders } from './policy/policy.providers';
import { roleProviders } from './role/role.providers';
import { userProviders } from './user/user.providers';
import { AppController } from './app/app.controller';
import { GroupController } from './group/group.controller';
import { PermissionController } from './permission/permission.controller';
import { PolicyController } from './policy/policy.controller';
import { RoleController } from './role/role.controller';
import { UserController } from './user/user.controller';
import { HealthzController } from './healthz.controller';
import { EnvController } from './env.controller';
import { RootController } from './root.controller';
import { LoadConfigService } from './load-config.service';
import { OpenTelemetryModuleImport } from './telemetry';
import { LoggerModule } from 'nestjs-pino';
import { pinoLoggerConfig } from './logger.config';
import { IdentityLinkController } from './user/identity-link/identity-link.controller';
import { messageBrokerProviders } from './message-broker';
import { RedisModule } from './redis.module';
import { HealthzRefreshService } from './healthz.refresh.service';
import { AuthfModule } from './authentication/authf/authf.module';

const appModuleImports = [];

const defaultLocale = new Locale(
  appConfig.fallbackLanguage,
  appConfig.fallbackCountry,
);

if (appConfig.telemetry.metrics.enabled) {
  appModuleImports.push(OpenTelemetryModuleImport);
}

if (appConfig.loggerType.includes('pino')) {
  appModuleImports.push(
    LoggerModule.forRoot({
      ...pinoLoggerConfig(appConfig),
      exclude: [
        {
          method: RequestMethod.GET,
          path: '/healthz',
        },
        {
          method: RequestMethod.GET,
          path: appConfig.telemetry.metrics.path,
        },
      ],
    }),
  );
}

const appFilter = new AppHttpExceptionFilter(
  defaultLocale,
  baseLogger,
  appConfig,
);

@Module({
  imports: [
    ...appModuleImports,
    RedisModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', '..', '..', 'ui', 'build'),
      serveRoot: process.env.UI_PREFIX || '',
    }),
    AuthfModule,
  ],
  controllers: [
    RootController,
    AppController,
    GroupController,
    PermissionController,
    PolicyController,
    RoleController,
    UserController,
    IdentityLinkController,
    HealthzController,
    EnvController,
  ],
  providers: [
    LoadConfigService,
    {
      provide: Locale,
      useValue: defaultLocale,
    },
    HealthzService,
    HealthzRefreshService,
    ...appProviders,
    ...authenticationProviders,
    ...groupProviders,
    ...permissionProviders,
    ...policyProviders,
    ...roleProviders,
    ...userProviders,
    ...messageBrokerProviders,
    {
      provide: APP_FILTER,
      useValue: appFilter,
    },
    ...setupProviders,
  ],
})
export class MainModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(ForwardedUriMiddleware).forRoutes('/');
    // consumer.apply(ApiHeadersValidationMiddleware).forRoutes("/");
  }
}
