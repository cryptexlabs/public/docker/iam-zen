import { Config } from './config';
import { baseDir, serverDir } from './root';
import {
  ContextBuilder,
  CustomLogger,
  HealthzService,
  ServiceClient,
} from '@cryptexlabs/codex-nodejs-common';
import { Logger as CustomPinoLogger, PinoLogger } from 'nestjs-pino';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import { i18nData } from './locale/locales';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { DatabaseTypeEnum } from './data/database-type.enum';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';
import { pinoLoggerConfig } from './logger.config';
import { LoggerService } from '@nestjs/common/services/logger.service';
import { Logger } from '@nestjs/common';

const appConfig = new Config(
  baseDir,
  serverDir,
  '509F31E4-0EC2-4A17-9005-86E46B171409',
  'localhost',
);

let baseLoggerImpl: LoggerService = new Logger();

if (appConfig.loggerType.includes('pino')) {
  const pinoLogger = new PinoLogger(pinoLoggerConfig(appConfig));
  baseLoggerImpl = new CustomPinoLogger(pinoLogger, {
    renameContext: null,
  });
}

const baseLogger = new CustomLogger(
  'info',
  appConfig.logLevels,
  false,
  baseLoggerImpl,
);
const contextBuilder = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext('default', null),
).setI18nData(i18nData);

const setupProviders: any[] = [
  {
    provide: 'CONTEXT_BUILDER',
    useValue: contextBuilder,
  },
  {
    provide: 'CONFIG',
    useValue: appConfig,
  },
  {
    provide: 'LOGGER',
    useValue: baseLogger,
  },
  {
    provide: 'HEALTHZ_SERVICE',
    useClass: HealthzService,
  },
];

let elasticsearchService = null;

if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  elasticsearchService = new ElasticsearchService({
    node: appConfig.elasticsearch.url,
  });
  setupProviders.push({
    provide: 'ELASTICSEARCH_SERVICE',
    useValue: elasticsearchService,
  });
  setupProviders.push(ElasticsearchPinger);
}

export {
  appConfig,
  baseLogger,
  contextBuilder,
  setupProviders,
  elasticsearchService,
};
