import {
  ContextBuilder,
  RestResponse,
  ServiceClient,
} from '@cryptexlabs/codex-nodejs-common';
import { HttpStatus } from '@nestjs/common';
import { appConfig, baseLogger } from '../setup';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import { i18nData } from '../locale/locales';

const exampleContext = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext('default', null),
)
  .setI18nData(i18nData)
  .build()
  .getResult();

export class ExampleRestResponse extends RestResponse<any> {
  constructor(status: HttpStatus, type: string, data: any) {
    super(exampleContext, status, type, data);
  }
}
