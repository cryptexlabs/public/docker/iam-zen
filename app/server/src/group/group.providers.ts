import { GroupService } from './group.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { GroupDefaultLoaderService } from './group.default-loader.service';
import { GroupDataServiceInterface } from './group.data.service.interface';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { Config } from '../config';
import { GroupElasticsearchDataService } from './group.elasticsearch.data.service';
import { GroupCacheService } from './group.cache.service';
import { GroupDataService } from './group.data.service';
import { UserDataServiceInterface } from '../user/persistence/user.data.service.interface';

const providers = [];

if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    GroupElasticsearchDataService,
    GroupCacheService,
    GroupDataService,
    {
      provide: 'GROUP_PERSISTENCE_SERVICE',
      useFactory: async (service: GroupElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [GroupElasticsearchDataService],
    },
    {
      provide: 'GROUP_DATA_SERVICE',
      useFactory: async (
        persistenceService: GroupDataServiceInterface,
        config: Config,
        dataService: GroupDataService,
      ) => {
        if (config.redis.enabled) {
          return dataService;
        } else {
          return persistenceService;
        }
      },
      inject: ['GROUP_PERSISTENCE_SERVICE', 'CONFIG', GroupDataService],
    },
  );
}

export const groupProviders = [
  ...providers,
  GroupService,
  {
    provide: 'USER_GROUPS_PROVIDER',
    useClass: GroupService,
  },
  {
    provide: 'GROUP_SERVICE',
    useClass: GroupService,
  },
  {
    provide: GroupDefaultLoaderService,
    useFactory: async (
      groupDataService: GroupDataServiceInterface,
      config: Config,
      contextBuilder: ContextBuilder,
    ) => {
      const service = new GroupDefaultLoaderService(
        groupDataService,
        config,
        contextBuilder,
      );
      if (config.autoLoadConfig) {
        if (
          config.environmentName === 'docker' ||
          config.environmentName === 'localhost'
        ) {
          new Promise((resolve) => setTimeout(resolve, 2000)).then(() =>
            service.load(),
          );
        } else {
          service.load().then();
        }
      }
      return service;
    },
    inject: ['GROUP_DATA_SERVICE', 'CONFIG', 'CONTEXT_BUILDER'],
  },
];
