import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { GroupUpdateInterface } from './group.update.interface';

@Injectable()
export class GroupUpdateValidationPipe implements PipeTransform {
  private _schema: Joi.ObjectSchema<GroupUpdateInterface>;
  constructor() {
    this._schema = Joi.object({
      name: Joi.string().alphanum().required(),
      description: Joi.string().required().allow(''),
      policies: Joi.array()
        .items(Joi.string().uuid({ version: 'uuidv4' }))
        .optional(),
    });
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this._schema.validate(value.data);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
