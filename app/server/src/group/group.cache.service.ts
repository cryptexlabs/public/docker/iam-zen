import {
  Context,
  PaginatedResults,
  StringUtil,
} from '@cryptexlabs/codex-nodejs-common';
import { Inject, Injectable } from '@nestjs/common';
import { QueryOptionsInterface } from 'src/query.options.interface';
import { RoleInterface } from 'src/role/role.interface';
import { GroupDataServiceInterface } from './group.data.service.interface';
import { GroupInterface } from './group.interface';
import { GroupPatchInterface } from './group.patch.interface';
import { GroupUpdateInterface } from './group.update.interface';
import { RedisClientType } from 'redis';
import * as NodeCache from 'node-cache';

@Injectable()
export class GroupCacheService implements GroupDataServiceInterface {
  private readonly GROUPS_BY_IDS_PREFIX = 'iam-zen:groups:by-ids';
  private readonly GROUPS_BY_IDS_TTL = 15 * 60;

  private readonly _memoryCache: NodeCache;

  constructor(@Inject('REDIS') private readonly redis: RedisClientType) {
    this._memoryCache = new NodeCache();
  }

  private _getGroupsByIdsKey(groupIds: string[]): string {
    const sortedGroupIds = [...groupIds].sort();
    const hashedGroupIds = StringUtil.insecureMd5(sortedGroupIds.join(','));
    return `${this.GROUPS_BY_IDS_PREFIX}:${hashedGroupIds}`;
  }

  public async getGroupsByIds(
    context: Context,
    groupIds: string[],
  ): Promise<GroupInterface[] | null> {
    const key = this._getGroupsByIdsKey(groupIds);

    const memoryGroups = this._memoryCache.get(key);
    if (memoryGroups) {
      return memoryGroups;
    }

    const json = await this.redis.get(key);
    if (json) {
      const parsedGroups = JSON.parse(json);

      this._memoryCache.set(key, parsedGroups, this.GROUPS_BY_IDS_TTL);

      return parsedGroups;
    }

    return null;
  }

  public async saveGroupsByIds(
    context: Context,
    groups: GroupInterface[],
  ): Promise<void> {
    const groupIds = groups.map((group) => group.id);
    const key = this._getGroupsByIdsKey(groupIds);

    this._memoryCache.set(key, groups, this.GROUPS_BY_IDS_TTL);

    await this.redis.set(key, JSON.stringify(groups), {
      EX: this.GROUPS_BY_IDS_TTL,
    });
  }

  public async saveGroupsFromConfig(
    context: Context,
    groups: GroupInterface[],
  ): Promise<GroupInterface[]> {
    throw new Error('Method not implemented.');
  }
  public async getGroupWithId(
    context: Context,
    groupId: string,
  ): Promise<GroupInterface> {
    throw new Error('Method not implemented.');
  }
  public async getGroupWithName(
    context: Context,
    name: string,
  ): Promise<GroupInterface> {
    throw new Error('Method not implemented.');
  }
  public async getGroups(
    context: Context,
    page: number,
    pageSize: number,
    options: QueryOptionsInterface,
  ): Promise<PaginatedResults<GroupInterface>> {
    throw new Error('Method not implemented.');
  }
  public async deleteGroup(
    context: Context,
    groupId: string,
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async deleteGroups(
    context: Context,
    groupIds: string[],
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async saveGroup(
    context: Context,
    groupId: string,
    groupUpdate: GroupUpdateInterface,
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async patchGroup(
    context: Context,
    groupId: string,
    groupPatch: GroupPatchInterface,
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async addPolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async removePolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async getGroupsForPolicy(
    context: Context,
    policyId: string,
  ): Promise<GroupInterface[]> {
    throw new Error('Method not implemented.');
  }
  public async getGroupsForPolicies(
    context: Context,
    policyIds: string[],
  ): Promise<RoleInterface[]> {
    throw new Error('Method not implemented.');
  }
}
