export interface GroupPolicyInterface {
  id: string;
  name: string;
  description: string;
  immutable: boolean;
}
