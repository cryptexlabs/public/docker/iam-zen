import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { Inject, Injectable } from '@nestjs/common';
import { QueryOptionsInterface } from 'src/query.options.interface';
import { RoleInterface } from 'src/role/role.interface';
import { GroupDataServiceInterface } from './group.data.service.interface';
import { GroupInterface } from './group.interface';
import { GroupPatchInterface } from './group.patch.interface';
import { GroupUpdateInterface } from './group.update.interface';
import { GroupCacheService } from './group.cache.service';

@Injectable()
export class GroupDataService implements GroupDataServiceInterface {
  constructor(
    @Inject('GROUP_PERSISTENCE_SERVICE')
    private readonly persistenceService: GroupDataServiceInterface,
    private readonly cacheService: GroupCacheService,
  ) {}

  public async getGroupsByIds(
    context: Context,
    groupIds: string[],
  ): Promise<GroupInterface[]> {
    const cachedGroups = await this.cacheService.getGroupsByIds(
      context,
      groupIds,
    );
    if (cachedGroups) {
      context.logger.debug(
        'GroupDataService.getGroupsByIds: returning cached groups',
        groupIds,
      );
      return cachedGroups;
    }

    const persistedGroups = await this.persistenceService.getGroupsByIds(
      context,
      groupIds,
    );
    await this.cacheService.saveGroupsByIds(context, persistedGroups);
    return persistedGroups;
  }
  public async saveGroupsFromConfig(
    context: Context,
    groups: GroupInterface[],
  ): Promise<GroupInterface[]> {
    return this.persistenceService.saveGroupsFromConfig(context, groups);
  }
  public async getGroupWithId(
    context: Context,
    groupId: string,
  ): Promise<GroupInterface> {
    return this.persistenceService.getGroupWithId(context, groupId);
  }
  public async getGroupWithName(
    context: Context,
    name: string,
  ): Promise<GroupInterface> {
    return this.persistenceService.getGroupWithName(context, name);
  }
  public async getGroups(
    context: Context,
    page: number,
    pageSize: number,
    options: QueryOptionsInterface,
  ): Promise<PaginatedResults<GroupInterface>> {
    return this.persistenceService.getGroups(context, page, pageSize, options);
  }
  public async deleteGroup(
    context: Context,
    groupId: string,
  ): Promise<boolean> {
    return this.persistenceService.deleteGroup(context, groupId);
  }
  public async deleteGroups(
    context: Context,
    groupIds: string[],
  ): Promise<boolean> {
    return this.persistenceService.deleteGroups(context, groupIds);
  }
  public async saveGroup(
    context: Context,
    groupId: string,
    groupUpdate: GroupUpdateInterface,
  ): Promise<boolean> {
    return this.persistenceService.saveGroup(context, groupId, groupUpdate);
  }
  public async patchGroup(
    context: Context,
    groupId: string,
    groupPatch: GroupPatchInterface,
  ): Promise<boolean> {
    return this.persistenceService.patchGroup(context, groupId, groupPatch);
  }
  public async addPolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<boolean> {
    return this.persistenceService.addPolicies(context, groupId, policyIds);
  }
  public async removePolicies(
    context: Context,
    groupId: string,
    policyIds: string[],
  ): Promise<boolean> {
    return this.persistenceService.removePolicies(context, groupId, policyIds);
  }
  public async getGroupsForPolicy(
    context: Context,
    policyId: string,
  ): Promise<GroupInterface[]> {
    return this.persistenceService.getGroupsForPolicy(context, policyId);
  }
  public async getGroupsForPolicies(
    context: Context,
    policyIds: string[],
  ): Promise<RoleInterface[]> {
    return this.persistenceService.getGroupsForPolicies(context, policyIds);
  }
}
