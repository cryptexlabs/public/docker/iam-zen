import { GroupInterface } from './group.interface';

export type GroupPersistedInterface = GroupInterface;
