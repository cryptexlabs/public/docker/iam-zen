export class GroupUtil {
  public static getUniquePolicyIdsForGroups(groups: { policies?: string[] }[]) {
    const uniquePolicyIds = [];
    for (const group of groups) {
      for (const policyId of group.policies || []) {
        if (!uniquePolicyIds.includes(policyId)) {
          uniquePolicyIds.push(policyId);
        }
      }
    }
    return uniquePolicyIds;
  }
}
