export interface RolePermissionInterface {
  id: string;
  name: string;
  value: string;
  description: string;
}

export interface RolePermissionDetailInterface extends RolePermissionInterface {
  fromPolicies: {
    id: string;
    name: string;
  }[];
}
