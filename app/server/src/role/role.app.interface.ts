export interface RoleAppInterface {
  id: string;
  name: string;
  roles: string[];
}

export interface RoleAppDetailInterface {
  id: string;
  name: string;
  description: string;
  immutable: boolean;
}
