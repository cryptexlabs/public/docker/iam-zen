export class RoleConfigInterface {
  name: string;
  id: string;
  description?: string;
  policies?: string[];
}
