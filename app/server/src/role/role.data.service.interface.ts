import { RolePersistedInterface } from './role.persisted.interface';
import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { RoleUpdateInterface } from './role.update.interface';
import { RolePatchInterface } from './role.patch.interface';
import { QueryOptionsInterface } from '../query.options.interface';
import { RoleInterface } from './role.interface';

export interface RoleDataServiceInterface {
  getRolesByIds(
    context: Context,
    roleIds: string[],
  ): Promise<RolePersistedInterface[]>;
  saveRolesFromConfig(
    context: Context,
    roles: RolePersistedInterface[],
  ): Promise<RolePersistedInterface[]>;
  getRoleWithId(
    context: Context,
    roleId: string,
  ): Promise<RolePersistedInterface | null>;

  getRoleWithName(
    context: Context,
    name: string,
  ): Promise<RolePersistedInterface | null>;

  getRoles(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<RolePersistedInterface>>;

  deleteRole(context: Context, roleId: string): Promise<boolean>;

  deleteRoles(context: Context, roleIds: string[]): Promise<boolean>;

  saveRole(
    context: Context,
    roleId: string,
    roleUpdate: RoleUpdateInterface,
  ): Promise<boolean>;

  patchRole(
    context: Context,
    roleId: string,
    rolePatch: RolePatchInterface,
  ): Promise<boolean>;

  addPolicies(
    context: Context,
    roleId: string,
    policyIds: string[],
  ): Promise<boolean>;

  removePolicies(
    context: Context,
    roleId: string,
    policyIds: string[],
  ): Promise<boolean>;

  getRolesForPolicy(
    context: Context,
    policyId: string,
  ): Promise<RoleInterface[]>;

  getRolesForPolicies(
    context: Context,
    policyIds: string[],
  ): Promise<RoleInterface[]>;
}
