import { Context } from '@cryptexlabs/codex-nodejs-common';
import { RoleAppDetailInterface, RoleAppInterface } from './role.app.interface';

export interface RoleAppsProviderInterface {
  getAppsForRoles(
    context: Context,
    roleIds: string[],
  ): Promise<RoleAppInterface[]>;

  getAppsForRole(
    context: Context,
    roleId: string,
  ): Promise<RoleAppDetailInterface[]>;
}
