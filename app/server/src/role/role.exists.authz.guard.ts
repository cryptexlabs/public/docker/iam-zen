import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class RoleExistsAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);

    return (
      util.isAuthorized(AuthzNamespace, {
        action: 'get',
        object: 'role',
        objectId: 'any',
      }) ||
      (util.isAuthorized(AuthzNamespace, {
        action: 'list',
        object: 'role',
        objectId: '',
      }) &&
        !util.isAuthorized(AuthzNamespace, {
          action: 'any',
          object: 'role',
          objectId: '',
        })) ||
      (util.isAuthorized(AuthzNamespace, {
        action: 'create',
        object: 'role',
        objectId: '',
      }) &&
        !util.isAuthorized(AuthzNamespace, {
          action: 'any',
          object: 'role',
          objectId: '',
        }))
    );
  }
}
