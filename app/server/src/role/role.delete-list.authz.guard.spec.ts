import { RoleDeleteListAuthzGuard } from './role.delete-list.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(RoleDeleteListAuthzGuard.name, () => {
  it('Should allow a super admin to delete a role', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to delete any object to delete the role', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:delete`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to delete a specific role to delete the specific role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to a specific role to delete the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to delete any role to delete the role', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::role:any:delete`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['4d2114ca-24e2-43e5-bddb-d9a6688b8340'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to delete a different specific role to delete the specific role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['3630a7ed-fef5-4345-9947-c54e8d15f954'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to a different specific role to delete the role', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::role:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          query: {
            ids: ['3630a7ed-fef5-4345-9947-c54e8d15f954'],
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new RoleDeleteListAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
