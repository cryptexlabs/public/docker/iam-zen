export interface RoleInterface {
  id: string;
  name: string;
  description: string;
  policies: string[];
}
