import { RoleInListDto } from './role.in-list.dto';

export const roleUpdateExample = {
  name: 'Widget Role 7',
  description: 'Role 7 for widget app',
  policies: ['ec570e6e-caaf-43f6-ac6b-cea8b7949124'],
};

export const rolePatchExample = {
  name: 'Widget Role 7',
  description: 'Role 7 for widget app',
};

export const exampleRole = new RoleInListDto(
  {
    id: '6fc9ded0-ea0c-444b-a4f4-c52d290638dd',
    ...roleUpdateExample,
  },
  2,
  true,
);
