export enum KafkaTopicsEnum {
  USER_DELETED = 'com.cryptexlabs.iam-zen.user.deleted',
  USER_CREATED = 'com.cryptexlabs.iam-zen.user.created',
}
