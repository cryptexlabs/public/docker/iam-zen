import { dirname } from 'path';

const rootDir = __dirname;
const serverDir = dirname(dirname(rootDir));
const appDir = dirname(serverDir);
const baseDir = dirname(appDir);
export { rootDir, baseDir, appDir, serverDir };
