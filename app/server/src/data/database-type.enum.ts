export enum DatabaseTypeEnum {
  ELASTICSEARCH = 'elasticsearch',
  MYSQL = 'mysql',
  SQLITE = 'sqlite',
}
