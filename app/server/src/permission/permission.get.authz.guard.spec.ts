import { PermissionGetAuthzGuard } from './permission.get.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(PermissionGetAuthzGuard.name, () => {
  it('Should allow a super admin to get a permission', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            permissionId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          body: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get any object to get the permission', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::any:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            permissionId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get a specific permission to get the specific permission', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::permission:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            permissionId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to a specific permission to get the permission', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::permission:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            permissionId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to get any permission to get the permission', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::permission:any:get`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            permissionId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to get a different specific permission to get the specific permission', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::permission:4d2114ca-24e2-43e5-bddb-d9a6688b8340:get`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            permissionId: '3630a7ed-fef5-4345-9947-c54e8d15f954',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to a different specific permission to get the permission', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::permission:4d2114ca-24e2-43e5-bddb-d9a6688b8340:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            permissionId: '3630a7ed-fef5-4345-9947-c54e8d15f954',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new PermissionGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
