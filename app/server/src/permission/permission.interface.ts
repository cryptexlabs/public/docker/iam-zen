export interface PermissionInterface {
  id: string;
  value: string;
  description?: string;
  name: string;
}
