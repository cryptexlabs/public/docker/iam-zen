import { PermissionInterface } from './permission.interface';
import { StringUtil } from '@cryptexlabs/codex-nodejs-common';

export class PermissionUtil {
  static getPermissionsHash(permissions: PermissionInterface[]): string {
    const sortedPermissions = [...permissions];

    // Sort by id so that the hash is always the same if the permissions are the same
    sortedPermissions.sort((a, b) => (a.id > b.id ? 1 : -1));

    // We only are concerned with changes in permission values. Not names or descriptions
    const permissionValues = sortedPermissions.map(
      (permission) => permission.value,
    );

    return StringUtil.insecureMd5(JSON.stringify(permissionValues));
  }
  static mapSelfToUserId(permissions: PermissionInterface[], userId: string) {
    return permissions.map((item) => ({
      ...item,
      value: item.value.replace(/:self:/g, `:${userId}:`),
    }));
  }
}
