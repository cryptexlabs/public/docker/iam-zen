import { PermissionService } from './permission.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Config } from '../config';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { PermissionDefaultLoaderService } from './permission.default-loader.service';
import { PermissionDataServiceInterface } from './permission.data.service.interface';
import { PermissionElasticsearchDataService } from './permission.elasticsearch.data.service';
import { PermissionDataService } from './permission.data.service';
import { PermissionCacheService } from './permission.cache.service';

const providers = [];
if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    PermissionElasticsearchDataService,
    PermissionDataService,
    PermissionCacheService,
    {
      provide: 'PERMISSION_PERSISTENCE_SERVICE',
      useFactory: async (service: PermissionElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [PermissionElasticsearchDataService],
    },
    {
      provide: 'PERMISSION_DATA_SERVICE',
      useFactory: async (
        persistenceService: PermissionDataServiceInterface,
        config: Config,
        dataService: PermissionDataService,
      ) => {
        if (config.redis.enabled) {
          return dataService;
        } else {
          return persistenceService;
        }
      },
      inject: [
        'PERMISSION_PERSISTENCE_SERVICE',
        'CONFIG',
        PermissionDataService,
      ],
    },
  );
}

export const permissionProviders = [
  ...providers,
  PermissionService,
  {
    provide: 'PERMISSIONS_SERVICE',
    useClass: PermissionService,
  },
  {
    provide: 'USER_PERMISSIONS_PROVIDER',
    useClass: PermissionService,
  },
  {
    provide: 'APP_PERMISSIONS_PROVIDER',
    useClass: PermissionService,
  },
  {
    provide: 'POLICY_PERMISSIONS_PROVIDER',
    useClass: PermissionService,
  },
  {
    provide: PermissionDefaultLoaderService,
    useFactory: async (
      permissionDataService: PermissionDataServiceInterface,
      config: Config,
      contextBuilder: ContextBuilder,
    ) => {
      const service = new PermissionDefaultLoaderService(
        permissionDataService,
        config,
        contextBuilder,
      );
      if (config.autoLoadConfig) {
        if (
          config.environmentName === 'docker' ||
          config.environmentName === 'localhost'
        ) {
          new Promise((resolve) => setTimeout(resolve, 2000)).then(() =>
            service.load(),
          );
        } else {
          service.load().then();
        }
      }
      return service;
    },
    inject: ['PERMISSION_DATA_SERVICE', 'CONFIG', 'CONTEXT_BUILDER'],
  },
];
