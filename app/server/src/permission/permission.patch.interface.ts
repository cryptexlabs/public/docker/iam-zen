export interface PermissionPatchInterface {
  name?: string;
  description?: string;
  value?: string;
}
