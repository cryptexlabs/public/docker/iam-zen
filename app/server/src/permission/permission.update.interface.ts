export interface PermissionUpdateInterface {
  name: string;
  description: string;
  value: string;
  policies?: string[];
}
