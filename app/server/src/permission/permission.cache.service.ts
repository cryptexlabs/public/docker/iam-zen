import {
  Context,
  PaginatedResults,
  StringUtil,
} from '@cryptexlabs/codex-nodejs-common';
import { Inject, Injectable } from '@nestjs/common';
import { QueryOptionsInterface } from 'src/query.options.interface';
import { PermissionDataServiceInterface } from './permission.data.service.interface';
import { PermissionInterface } from './permission.interface';
import { PermissionPatchInterface } from './permission.patch.interface';
import { PermissionUpdateInterface } from './permission.update.interface';
import { RedisClientType } from 'redis';
import * as NodeCache from 'node-cache';

@Injectable()
export class PermissionCacheService implements PermissionDataServiceInterface {
  private readonly PERMISSIONS_BY_IDS_PREFIX = 'iam-zen:permissions:by-ids';
  private readonly PERMISSIONS_BY_IDS_TTL = 15 * 60;

  private readonly _memoryCache: NodeCache;

  constructor(@Inject('REDIS') private readonly redis: RedisClientType) {
    this._memoryCache = new NodeCache();
  }

  private _getPermissionsByIdsKey(permissionIds: string[]): string {
    const sortedPermissionIds = [...permissionIds].sort();
    const hashedPermissionIds = StringUtil.insecureMd5(
      sortedPermissionIds.join(','),
    );
    return `${this.PERMISSIONS_BY_IDS_PREFIX}:${hashedPermissionIds}`;
  }

  public async getPermissionsByIds(
    context: Context,
    permissionIds: string[],
  ): Promise<PermissionInterface[]> {
    const key = this._getPermissionsByIdsKey(permissionIds);

    const memoryPermissions = this._memoryCache.get(key);
    if (memoryPermissions) {
      return memoryPermissions;
    }

    const json = await this.redis.get(key);
    if (json) {
      const parsedPermissions = JSON.parse(json);

      this._memoryCache.set(
        key,
        parsedPermissions,
        this.PERMISSIONS_BY_IDS_TTL,
      );

      return parsedPermissions;
    }

    return null;
  }

  public async savePermissionsByIds(
    context: Context,
    permissions: PermissionInterface[],
  ): Promise<void> {
    const permissionIds = permissions.map((permission) => permission.id);
    const key = this._getPermissionsByIdsKey(permissionIds);

    this._memoryCache.set(key, permissions, this.PERMISSIONS_BY_IDS_TTL);

    await this.redis.set(key, JSON.stringify(permissions), {
      EX: this.PERMISSIONS_BY_IDS_TTL,
    });
  }

  public async savePermissionsFromConfig(
    context: Context,
    policies: PermissionInterface[],
  ): Promise<PermissionInterface[]> {
    throw new Error('Method not implemented.');
  }
  public async getPermissionById(
    context: Context,
    permissionId: string,
  ): Promise<PermissionInterface> {
    throw new Error('Method not implemented.');
  }
  public async getPermissionWithName(
    context: Context,
    name: string,
  ): Promise<PermissionInterface> {
    throw new Error('Method not implemented.');
  }
  public async getPermissions(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<PermissionInterface>> {
    throw new Error('Method not implemented.');
  }
  public async deletePermission(
    context: Context,
    permissionId: string,
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async deletePermissions(
    context: Context,
    permissionIds: string[],
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async savePermission(
    context: Context,
    permissionId: string,
    permissionUpdate: PermissionUpdateInterface,
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async patchPermission(
    context: Context,
    permissionId: string,
    permissionPatch: PermissionPatchInterface,
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
}
