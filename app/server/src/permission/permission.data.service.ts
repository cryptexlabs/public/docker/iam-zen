import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { Inject, Injectable } from '@nestjs/common';
import { QueryOptionsInterface } from 'src/query.options.interface';
import { PermissionDataServiceInterface } from './permission.data.service.interface';
import { PermissionInterface } from './permission.interface';
import { PermissionPatchInterface } from './permission.patch.interface';
import { PermissionUpdateInterface } from './permission.update.interface';
import { PermissionCacheService } from './permission.cache.service';

@Injectable()
export class PermissionDataService implements PermissionDataServiceInterface {
  constructor(
    @Inject('PERMISSION_PERSISTENCE_SERVICE')
    private readonly persistenceService: PermissionDataServiceInterface,
    private readonly cacheService: PermissionCacheService,
  ) {}

  public async getPermissionsByIds(
    context: Context,
    permissionIds: string[],
  ): Promise<PermissionInterface[]> {
    const cachedPermissions = await this.cacheService.getPermissionsByIds(
      context,
      permissionIds,
    );
    if (cachedPermissions) {
      context.logger.debug(
        `PermissionDataService:getPermissionsByIds: returning cached permissions`,
        permissionIds,
      );
      return cachedPermissions;
    }

    const persistedPermissions =
      await this.persistenceService.getPermissionsByIds(context, permissionIds);
    await this.cacheService.savePermissionsByIds(context, persistedPermissions);
    return persistedPermissions;
  }

  public async savePermissionsFromConfig(
    context: Context,
    policies: PermissionInterface[],
  ): Promise<PermissionInterface[]> {
    return this.persistenceService.savePermissionsFromConfig(context, policies);
  }
  public async getPermissionById(
    context: Context,
    permissionId: string,
  ): Promise<PermissionInterface> {
    return this.persistenceService.getPermissionById(context, permissionId);
  }
  public async getPermissionWithName(
    context: Context,
    name: string,
  ): Promise<PermissionInterface> {
    return this.persistenceService.getPermissionWithName(context, name);
  }
  public async getPermissions(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<PermissionInterface>> {
    return this.persistenceService.getPermissions(
      context,
      page,
      pageSize,
      options,
    );
  }
  public async deletePermission(
    context: Context,
    permissionId: string,
  ): Promise<boolean> {
    return this.persistenceService.deletePermission(context, permissionId);
  }
  public async deletePermissions(
    context: Context,
    permissionIds: string[],
  ): Promise<boolean> {
    return this.persistenceService.deletePermissions(context, permissionIds);
  }
  public async savePermission(
    context: Context,
    permissionId: string,
    permissionUpdate: PermissionUpdateInterface,
  ): Promise<boolean> {
    return this.persistenceService.savePermission(
      context,
      permissionId,
      permissionUpdate,
    );
  }
  public async patchPermission(
    context: Context,
    permissionId: string,
    permissionPatch: PermissionPatchInterface,
  ): Promise<boolean> {
    return this.persistenceService.patchPermission(
      context,
      permissionId,
      permissionPatch,
    );
  }
}
