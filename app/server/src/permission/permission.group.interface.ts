export interface PermissionGroupInterface {
  id: string;
  name: string;
  description: string;
}
