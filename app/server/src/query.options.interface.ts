export enum OrderDirectionEnum {
  ASC = 'asc',
  DESC = 'desc',
}

export interface QueryOptionsInterface {
  searchName?: string;
  search?: string;
  orderBy?: string;
  orderDirection?: OrderDirectionEnum;
}
