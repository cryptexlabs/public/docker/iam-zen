import {
  Controller,
  Get,
  HttpStatus,
  Inject,
  InternalServerErrorException,
} from '@nestjs/common';
import {
  HealthzResponse,
  HealthzService,
} from '@cryptexlabs/codex-nodejs-common';
import { Locale } from '@cryptexlabs/codex-data-model';
import { Config } from './config';
import { UserDataServiceInterface } from './user/persistence/user.data.service.interface';

@Controller('healthz')
export class HealthzController {
  constructor(
    private readonly fallbackLocale: Locale,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('HEALTHZ_SERVICE') private readonly healthzService: HealthzService,
  ) {}

  @Get()
  public async root() {
    if (await this.healthzService.isHealthy()) {
      return new HealthzResponse(
        HttpStatus.OK,
        this.fallbackLocale,
        this.config,
      );
    } else {
      throw new InternalServerErrorException(`Service not healthy`);
    }
  }
}
