import { Inject, Injectable } from '@nestjs/common';
import { Config } from '../config';
import { ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { PolicyDataServiceInterface } from './policy.data.service.interface';
import { PolicyPersistedInterface } from './policy.persisted.interface';

@Injectable()
export class PolicyDefaultLoaderService {
  constructor(
    @Inject('POLICY_DATA_SERVICE')
    private readonly policyDataService: PolicyDataServiceInterface,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {}

  public async load() {
    const persistedPolicies: PolicyPersistedInterface[] =
      this.config.defaultPolicies.map((configPolicy) => {
        return {
          name: configPolicy.name,
          id: configPolicy.id,
          description: configPolicy.description || '',
          permissions: configPolicy.permissions,
        };
      });

    const context = this.contextBuilder.build().getResult();

    await this.policyDataService.savePoliciesFromConfig(
      context,
      persistedPolicies,
    );
  }
}
