export interface PolicyRoleInterface {
  id: string;
  name: string;
  description: string;
  immutable: boolean;
}
