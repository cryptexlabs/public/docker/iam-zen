import {
  PolicyPermissionDetailedInterface,
  PolicyPermissionInterface,
} from './policy.permission.interface';
import { Context } from '@cryptexlabs/codex-nodejs-common';

export interface PolicyPermissionsProviderInterface {
  getPermissionsForPoliciesByPermissionIds(
    context: Context,
    permissionIds: string[],
  ): Promise<PolicyPermissionInterface[]>;

  getPermissionsForPolicy(
    context: Context,
    policyId: string,
    permissionIds: string[],
  ): Promise<PolicyPermissionDetailedInterface[]>;
}
