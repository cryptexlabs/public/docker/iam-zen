export interface PolicyPermissionInterface {
  id: string;
  name: string;
  value: string;
  description: string;
}

export interface PolicyPermissionDetailedInterface
  extends PolicyPermissionInterface {
  immutable: boolean;
}
