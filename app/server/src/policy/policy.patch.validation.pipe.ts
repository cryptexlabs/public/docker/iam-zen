import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { PolicyPatchInterface } from './policy.patch.interface';

@Injectable()
export class PolicyPatchValidationPipe implements PipeTransform {
  private _schema: Joi.ObjectSchema<PolicyPatchInterface>;
  constructor() {
    this._schema = Joi.object({
      name: Joi.string().alphanum().optional(),
      description: Joi.string().optional(),
    });
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this._schema.validate(value.data);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
