import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AuthzNamespace } from '../constants';
import { HttpAuthzAttachObjectsGuardUtil } from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class PolicyAttachPermissionsAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzAttachObjectsGuardUtil(context);
    return util.isAuthorized(
      'policy',
      util.params.policyId,
      'permission',
      util.body,
      AuthzNamespace.object,
    );
  }
}
