import {
  ArgumentMetadata,
  HttpException,
  HttpStatus,
  Injectable,
  PipeTransform,
} from '@nestjs/common';

@Injectable()
export class OrderDirectionValidationPipe implements PipeTransform {
  public transform(value: any, metadata: ArgumentMetadata): any {
    if (value && !['asc', 'desc'].includes(value)) {
      throw new HttpException(
        'Invalid sortDirection. Must be "asc" or "desc"',
        HttpStatus.BAD_REQUEST,
      );
    }

    return value;
  }
}
