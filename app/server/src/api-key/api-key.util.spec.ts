import { ApiKeyUtil } from './api-key.util';

describe('ApiKeyUtil', () => {
  it('should generate a random ID string', () => {
    const firstId = ApiKeyUtil.randomIdString(21);
    const secondId = ApiKeyUtil.randomIdString(21);
    expect(firstId.length).toBe(21);
    expect(secondId.length).toBe(21);
    expect(firstId).not.toBe(secondId);
  });

  it('should generate a random secret', () => {
    const firstSecret = ApiKeyUtil.randomSecretString(19);
    const secondSecret = ApiKeyUtil.randomSecretString(19);
    expect(firstSecret.length).toBe(38);
    expect(secondSecret.length).toBe(38);
    expect(firstSecret).not.toBe(secondSecret);
  });

  it('should generate an api key and secret pair', () => {
    const pair1 = ApiKeyUtil.generateApiAuth();
    const pair2 = ApiKeyUtil.generateApiAuth();
    expect(pair1.apiKey.length).toBe(20);
    expect(pair1.secret.length).toBe(40);
    expect(pair2.apiKey.length).toBe(20);
    expect(pair2.secret.length).toBe(40);
    expect(pair1.apiKey).not.toBe(pair2.apiKey);
    expect(pair1.secret).not.toBe(pair2.secret);
  });
});
