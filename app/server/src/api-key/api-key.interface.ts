import { ApiKeyTypeEnum } from './api-key.type.enum';

export interface ApiKeyInterface {
  type: ApiKeyTypeEnum;
  id: string;
  description: string;
  data: unknown;
}
