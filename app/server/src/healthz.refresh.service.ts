import { Inject, Injectable } from '@nestjs/common';
import {
  Context,
  ContextBuilder,
  HealthzComponentEnum,
  HealthzService,
} from '@cryptexlabs/codex-nodejs-common';
import { Config } from './config';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';
import { ElasticsearchService } from '@nestjs/elasticsearch';

@Injectable()
export class HealthzRefreshService {
  constructor(
    @Inject('HEALTHZ_SERVICE') private readonly healthzService: HealthzService,

    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,

    @Inject('CONFIG') private readonly config: Config,

    private readonly elasticsearchService: ElasticsearchService,
  ) {}

  public async startHealthzWatcher(): Promise<void> {
    const context = this.contextBuilder.build().getResult();

    await this._updateHealthz(context);
    setInterval(async () => {
      await this._updateHealthz(context);
    }, this.config.componentHealthRefreshInterval * 1000);
  }

  private async _updateHealthz(context: Context): Promise<void> {
    await this._updateHealthzStatusForDb(context);
  }

  private async _updateHealthzStatusForDb(context: Context): Promise<void> {
    context.logger.verbose(
      `Refreshing healthz status for ${this.config.dbType} db`,
    );

    try {
      if (this.config.dbType.includes('elasticsearch')) {
        const result = await this.elasticsearchService.ping();
        if (result) {
          await this.healthzService.makeHealthy(
            HealthzComponentEnum.DATABASE_ELASTICSEARCH,
          );
        } else {
          await this.healthzService.makeUnhealthy(
            HealthzComponentEnum.DATABASE_ELASTICSEARCH,
          );
        }
      }
    } catch (e) {
      await this.healthzService.makeUnhealthy(
        HealthzComponentEnum.DATABASE_ELASTICSEARCH,
      );
    }
  }
}
