import { Telemetry } from './telemetry';
import { NestFactory } from '@nestjs/core';
import { MainModule } from './main.module';
import * as packageJSON from '../package.json';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { appConfig, baseLogger } from './setup';
import { LoadConfigService } from './load-config.service';
import { DatabaseTypeEnum } from './data/database-type.enum';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import * as process from 'process';
import { ConsoleLogger } from '@nestjs/common';
import {
  HttpStatusInterceptor,
  KafkaService,
} from '@cryptexlabs/codex-nodejs-common';
import { KafkaTopicsEnum } from './kafka.topics.enum';
import { HealthzRefreshService } from './healthz.refresh.service';

const asciiText = `
    ____   ___       __  ___     _____            
   /  _/  /   |     /  |/  /    /__  /  ___  ____ 
   / /   / /| |    / /|_/ /       / /  / _ \\/ __ \\
 _/ /   / ___ |   / /  / /       / /__/  __/ / / /
/___/  /_/  |_|  /_/  /_/       /____/\\___/_/ /_/ 
                                                 
****************************************************
*                                                  *
*     IDENTITY AND ACCESS MANAGEMENT SERVER        *
*                                                  *
****************************************************             

`;
async function rest() {
  if (
    appConfig.telemetry.metrics.enabled ||
    appConfig.telemetry.traces.enabled
  ) {
    const telemetry = new Telemetry();
    await telemetry.start();
  }
  const config = appConfig;
  const app = await NestFactory.create(MainModule, {
    logger: console,
  });
  const server = app.getHttpServer();
  server.keepAliveTimeout = config.httpKeepAliveTimeout;
  server.setTimeout(config.httpRequestTimeout);
  app.useLogger(new ConsoleLogger());
  app.useGlobalInterceptors(new HttpStatusInterceptor());

  const docOptions = new DocumentBuilder()
    .setTitle(packageJSON.name)
    .setDescription(packageJSON.description)
    .setVersion(packageJSON.version)
    .addBearerAuth({ in: 'header', type: 'http' })
    .build();

  const document = SwaggerModule.createDocument(app, docOptions);
  SwaggerModule.setup(
    `${config.docsPrefix}/${config.apiVersion}${config.docsPath}`,
    app,
    document,
    {
      swaggerOptions: {
        docExpansion: false,
      },
    },
  );

  const healthzRefresh = app.get<HealthzRefreshService>(HealthzRefreshService);
  healthzRefresh.startHealthzWatcher().then();

  app.enableCors({
    origin: true,
    methods: '*',
    allowedHeaders: '*',
    credentials: true,
  });

  if (appConfig.kafka.enabled) {
    const kafka = app.get<KafkaService>(KafkaService);
    await kafka.initializeProducer();
    await kafka.connect();
    await kafka.ensureTopicsExist(Object.values(KafkaTopicsEnum), false);
    baseLogger.info('Kafka producer initialized');
  }

  await app.listen(config.httpPort, () => {
    console.log(asciiText);
    if (['docker', 'localhost'].includes(appConfig.environmentName)) {
      const port = appConfig.externalHttpPort;
      console.log(`Swagger docs available on http://localhost:${port}/${config.docsPrefix}/${config.apiVersion}${config.docsPath}
      `);
    }
  });
}

const loadConfig = async () => {
  const app = await NestFactory.createApplicationContext(MainModule);

  const loadConfigService = app.get(LoadConfigService);

  console.log(asciiText);

  await loadConfigService.loadConfig();

  process.exit(0);
};

const waitReady = async () => {
  if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
    await new Promise((resolve) => {
      const interval = setInterval(async () => {
        try {
          const elasticsearchService = new ElasticsearchService({
            node: appConfig.elasticsearch.url,
          });

          const ping = await elasticsearchService.ping();

          if (ping) {
            clearInterval(interval);
            resolve(undefined);
          }
        } catch (e) {
          baseLogger.debug('Elasticsearch still not healthy');
        }
      }, 1000);
    });
  }

  console.log(asciiText);

  process.exit(0);
};

const mode = process.env.MODE || 'rest';

const modeMap = {
  rest,
  loadConfig,
  waitReady,
};

modeMap[mode]();
