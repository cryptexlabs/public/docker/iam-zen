import { UserService } from './user.service';
import { appConfig, elasticsearchService } from '../setup';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { UserElasticsearchDataService } from './persistence/user.elasticsearch.data.service';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { IdentityLinkService } from './identity-link/identity-link.service';
import { IdentityLinkElasticsearchDataService } from './identity-link/identity-link.elasticsearch.data.service';
import { UserDefaultLoaderService } from './persistence/user.default-loader.service';
import { UserDataServiceInterface } from './persistence/user.data.service.interface';
import { Config } from '../config';
import { UserPermissionsProviderInterface } from './permission/user.permissions-provider.interface';
import { AuthenticationServiceInterface } from '../authentication/authentication.service.interface';
import { ContextBuilder, CustomLogger } from '@cryptexlabs/codex-nodejs-common';

import { UserLoginExtraService } from './identity-link/user.login.extra.service';
import { UserEventBrokerService } from './user.event.broker.service';
import { UserDeviceElasticsearchDataService } from './persistence/user.device.elasticsearch.data.service';
import { UserDataService } from './persistence/user.data.service';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';
import { UserCacheService } from './persistence/user.cache.service';
import { UserHttpService } from './user.http.service';
import { UserProductivePersistenceCacheService } from './persistence/user.productive.persistence.cache.service';
import { IdentityLinkDataService } from './identity-link/identity-link.data.service';
import { IdentityLinkDataServiceInterface } from './identity-link/identity-link.data.service.interface';

const providers = [];

if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  providers.push(
    {
      provide: ElasticsearchService,
      useValue: elasticsearchService,
    },
    IdentityLinkElasticsearchDataService,
    {
      provide: 'USER_PERSISTENCE_SERVICE',
      useFactory: async (
        logger: CustomLogger,
        es: ElasticsearchService,
        esp: ElasticsearchPinger,
        config: Config,
      ) => {
        const service = new UserElasticsearchDataService(logger, es, esp);
        await service.initializeIndex();
        return new UserProductivePersistenceCacheService(service, config);
      },
      inject: ['LOGGER', ElasticsearchService, ElasticsearchPinger, 'CONFIG'],
    },
    UserDeviceElasticsearchDataService,
    {
      provide: 'USER_DEVICE_DATA_SERVICE',
      useFactory: async (service: UserDeviceElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [UserDeviceElasticsearchDataService],
    },
    UserDataService,
    UserCacheService,
    IdentityLinkDataService,
    {
      provide: 'USER_DATA_SERVICE',
      useFactory: async (
        persistenceService: UserDataServiceInterface,
        config: Config,
        dataService: UserDataService,
      ) => {
        if (config.redis.enabled) {
          return dataService;
        } else {
          return persistenceService;
        }
      },
      inject: ['USER_PERSISTENCE_SERVICE', 'CONFIG', UserDataService],
    },
    {
      provide: 'IDENTITY_LINK_PERSISTENCE_SERVICE',
      useFactory: async (service: IdentityLinkElasticsearchDataService) => {
        await service.initializeIndex();
        return service;
      },
      inject: [IdentityLinkElasticsearchDataService],
    },
    {
      provide: 'IDENTITY_LINK_DATA_SERVICE',
      useFactory: async (
        persistenceService: IdentityLinkDataServiceInterface,
        config: Config,
        dataService: IdentityLinkDataService,
      ) => {
        if (config.redis.enabled) {
          return dataService;
        } else {
          return persistenceService;
        }
      },
      inject: [
        'IDENTITY_LINK_PERSISTENCE_SERVICE',
        'CONFIG',
        IdentityLinkDataService,
      ],
    },
  );
}

export const userProviders = [
  ...providers,
  UserService,
  UserEventBrokerService,
  {
    provide: UserDefaultLoaderService,
    useFactory: async (
      userDataService: UserDataServiceInterface,
      config: Config,
      userPermissionsProvider: UserPermissionsProviderInterface,
      authenticationService: AuthenticationServiceInterface,
      contextBuilder: ContextBuilder,
      identityLinkService: IdentityLinkService,
      loginExtraService: UserLoginExtraService,
    ) => {
      const userDefaultService = new UserDefaultLoaderService(
        userDataService,
        config,
        userPermissionsProvider,
        authenticationService,
        contextBuilder,
        identityLinkService,
        loginExtraService,
      );

      if (config.autoLoadConfig) {
        if (
          config.environmentName === 'docker' ||
          config.environmentName === 'localhost'
        ) {
          new Promise((resolve) => setTimeout(resolve, 2000)).then(() =>
            userDefaultService.load(),
          );
        } else {
          userDefaultService.load().then();
        }
      }
      return userDefaultService;
    },
    inject: [
      'USER_DATA_SERVICE',
      'CONFIG',
      'USER_PERMISSIONS_PROVIDER',
      'AUTHENTICATION_SERVICE',
      'CONTEXT_BUILDER',
      IdentityLinkService,
      UserLoginExtraService,
    ],
  },
  IdentityLinkService,
  UserLoginExtraService,
  UserHttpService,
];
