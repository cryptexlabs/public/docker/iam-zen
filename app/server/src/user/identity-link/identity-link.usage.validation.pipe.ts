import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';

@Injectable()
export class IdentityLinkUsageValidationPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (!Object.values(IdentityLinkUsageEnum).includes(value)) {
      throw new BadRequestException(
        `Invalid link usage: ${value}. Valid: "${Object.values(
          IdentityLinkUsageEnum,
        ).join('","')}"`,
      );
    }
    return value;
  }
}
