import { Inject, Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import {
  Context,
  CustomLogger,
  ElasticsearchInitializer,
} from '@cryptexlabs/codex-nodejs-common';
import { IdentityLinkDataServiceInterface } from './identity-link.data.service.interface';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';
import { IdentityLinkInterface } from './identity-link.interface';

@Injectable()
export class IdentityLinkElasticsearchDataService
  implements IdentityLinkDataServiceInterface
{
  private static get DEFAULT_INDEX() {
    return 'identity_link';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
    elasticsearchPinger: ElasticsearchPinger,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        usage: {
          type: 'keyword',
        },
        linkType: {
          type: 'keyword',
        },
        linkId: {
          type: 'keyword',
        },
        userId: {
          type: 'text',
          fields: {
            keyword: {
              type: 'keyword',
            },
          },
        },
      },
      elasticsearchPinger,
    );
  }

  public async initializeIndex() {
    await this.elasticsearchInitializer.initializeIndex();
  }

  public async deleteIdentityLinksForUser(
    context: Context,
    userId: string,
  ): Promise<void> {
    await this.elasticsearchService.deleteByQuery({
      index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            filter: [
              {
                term: {
                  'userId.keyword': userId,
                },
              },
            ],
          },
        },
      },
    });
  }

  public async getUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
  ): Promise<string | null> {
    const response = await this.elasticsearchService.search({
      index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            filter: [
              {
                term: {
                  usage,
                },
              },
              {
                term: {
                  linkType,
                },
              },
              {
                term: {
                  linkId,
                },
              },
            ],
          },
        },
      },
    });

    if ((response.hits.total as any).value === 0) {
      return null;
    }

    return (response.hits.hits[0] as any)._source.userId;
  }

  public async searchUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkId: string,
  ): Promise<string | null> {
    const response = await this.elasticsearchService.search({
      index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            filter: [
              {
                term: {
                  usage,
                },
              },
              {
                term: {
                  linkId,
                },
              },
            ],
          },
        },
      },
    });

    if ((response.hits.total as any).value === 0) {
      return null;
    }

    return (response.hits.hits[0] as any)._source.userId;
  }

  public async saveIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void> {
    const doc = {
      usage,
      linkType,
      linkId,
      userId,
    };

    await this.elasticsearchService.update({
      index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      id: `${usage}-${linkType}-${linkId}`,
      body: {
        doc,
        doc_as_upsert: true,
      },
    });
  }

  public async getIdentityLinksForUser(
    context: Context,
    userId: string,
  ): Promise<IdentityLinkInterface[]> {
    const response = await this.elasticsearchService.search({
      index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      query: {
        bool: {
          filter: [
            {
              term: {
                'userId.keyword': userId,
              },
            },
          ],
        },
      },
    });

    if ((response.hits.total as any).value === 0) {
      return [];
    }

    return response.hits.hits.map((hit) => {
      const type = (hit._source as any).linkType;
      const linkId = (hit._source as any).linkId;
      let data: any;
      if (type === IdentityLinkTypeEnum.AUTHF) {
        data = {
          authenticationId: linkId,
        };
      }
      return {
        type,
        usage: (hit._source as any).usage,
        data,
        linkId,
      };
    });
  }

  public async createIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void> {
    await this.elasticsearchService.index({
      index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
      id: `${usage}-${linkType}-${linkId}`,
      body: {
        usage,
        linkType,
        linkId,
        userId,
      },
    });
  }

  public async createIdentityLinks(
    context: Context,
    links: { userId: string; link: IdentityLinkInterface }[],
  ): Promise<void> {
    const body = links.flatMap((link) => [
      {
        index: {
          _index: IdentityLinkElasticsearchDataService.DEFAULT_INDEX,
          _id: `${link.link.usage}-${link.link.type}-${link.link.linkId}`,
        },
      },
      {
        usage: link.link.usage,
        linkType: link.link.type,
        linkId: link.link.linkId,
        userId: link.userId,
      },
    ]);

    await this.elasticsearchService.bulk({ body });
  }
}
