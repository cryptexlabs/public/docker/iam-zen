import { IdentityLinkInterface } from './identity-link.interface';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';

export interface SamsungExternalIdIdentityLinkInterface
  extends IdentityLinkInterface {
  type: IdentityLinkTypeEnum.SAMSUNG;
  usage: IdentityLinkUsageEnum.EXTERNAL_ID;
  data: {
    username: string;
  };
}
