import { Inject, Injectable } from '@nestjs/common';
import { IdentityLinkDataServiceInterface } from './identity-link.data.service.interface';
import { IdentityLinkInterface } from './identity-link.interface';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';
import { Context, ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { Config } from '../../config';

@Injectable()
export class IdentityLinkDataService
  implements IdentityLinkDataServiceInterface
{
  private _batchCreateIdentityLinks: {
    userId: string;
    link: IdentityLinkInterface;
  }[] = [];

  constructor(
    @Inject('IDENTITY_LINK_PERSISTENCE_SERVICE')
    private readonly persistenceService: IdentityLinkDataServiceInterface,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
    @Inject('CONFIG') private readonly config: Config,
  ) {
    this._startBatchProcessor().then();
  }

  private async _startBatchProcessor() {
    const context = this.contextBuilder.build().getResult();

    setInterval(async () => {
      try {
        if (this._batchCreateIdentityLinks.length > 0) {
          context.logger.debug(
            `Creating ${this._batchCreateIdentityLinks.length} identity links`,
            this._batchCreateIdentityLinks,
          );
          const links = [...this._batchCreateIdentityLinks];
          this._batchCreateIdentityLinks = [];
          await this.persistenceService.createIdentityLinks(context, links);
        }
      } catch (e) {
        context.logger.error(e);
      }
    }, this.config.createIdentityLinkInterval);
  }

  public async deleteIdentityLinksForUser(
    context: Context,
    userId: string,
  ): Promise<void> {
    return await this.persistenceService.deleteIdentityLinksForUser(
      context,
      userId,
    );
  }

  public async getIdentityLinksForUser(
    context: Context,
    userId: string,
  ): Promise<IdentityLinkInterface[]> {
    return await this.persistenceService.getIdentityLinksForUser(
      context,
      userId,
    );
  }

  public async getUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
  ): Promise<string | null> {
    return await this.persistenceService.getUserIdForIdentityLink(
      usage,
      linkType,
      linkId,
    );
  }

  public async saveIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void> {
    return await this.persistenceService.saveIdentityLink(
      usage,
      linkType,
      linkId,
      userId,
    );
  }

  public async searchUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkId: string,
  ): Promise<string | null> {
    return await this.persistenceService.searchUserIdForIdentityLink(
      usage,
      linkId,
    );
  }

  public async createIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void> {
    this._batchCreateIdentityLinks.push({
      userId,
      link: {
        usage,
        linkId,
        type: linkType,
        data: undefined,
      },
    });
  }

  public async createIdentityLinks(
    context: Context,
    links: { userId: string; link: IdentityLinkInterface }[],
  ): Promise<void> {
    throw new Error('Method not implemented.');
  }
}
