import { Inject, Injectable } from '@nestjs/common';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';
import { IdentityLinkDataServiceInterface } from './identity-link.data.service.interface';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { IdentityLinkInterface } from './identity-link.interface';

@Injectable()
export class IdentityLinkService {
  constructor(
    @Inject('IDENTITY_LINK_DATA_SERVICE')
    private readonly identityLinkDataService: IdentityLinkDataServiceInterface,
  ) {}

  public async getUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
  ): Promise<string | null> {
    return this.identityLinkDataService.getUserIdForIdentityLink(
      usage,
      linkType,
      linkId,
    );
  }

  public async getIdentityLinksForUser(
    context: Context,
    userId: string,
  ): Promise<IdentityLinkInterface[]> {
    return this.identityLinkDataService.getIdentityLinksForUser(
      context,
      userId,
    );
  }

  public async deleteIdentityLinksForUser(
    context: Context,
    userId: string,
  ): Promise<void> {
    return this.identityLinkDataService.deleteIdentityLinksForUser(
      context,
      userId,
    );
  }

  public async saveIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void> {
    await this.identityLinkDataService.saveIdentityLink(
      usage,
      linkType,
      linkId,
      userId,
    );
  }

  public async createIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void> {
    await this.identityLinkDataService.createIdentityLink(
      usage,
      linkType,
      linkId,
      userId,
    );
  }
}
