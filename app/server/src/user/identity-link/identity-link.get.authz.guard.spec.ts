import { ExecutionContext } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { IdentityLinkGetAuthzGuard } from './identity-link.get.authz.guard';
import { AuthzNamespace } from '../../constants';

describe(IdentityLinkGetAuthzGuard.name, () => {
  it('Should allow super admin to get an identity link', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::any:any:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new IdentityLinkGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to empty object identity link to get the link', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::identity-link::any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new IdentityLinkGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any identity link to get the link', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::identity-link:any:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new IdentityLinkGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should now allow someone without permission to get an identity link', () => {
    const token = jwt.sign(
      {
        scopes: [`${AuthzNamespace.object}:::some:object:any`],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new IdentityLinkGetAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
