import { IdentityLinkUsageEnum } from './identity-link-usage.enum';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { IdentityLinkInterface } from './identity-link.interface';

export interface IdentityLinkDataServiceInterface {
  getIdentityLinksForUser(
    context: Context,
    userId: string,
  ): Promise<IdentityLinkInterface[]>;

  getUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
  ): Promise<string | null>;

  deleteIdentityLinksForUser(context: Context, userId: string): Promise<void>;

  searchUserIdForIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkId: string,
  ): Promise<string | null>;

  saveIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void>;

  createIdentityLink(
    usage: IdentityLinkUsageEnum,
    linkType: IdentityLinkTypeEnum,
    linkId: string,
    userId: string,
  ): Promise<void>;

  createIdentityLinks(
    context: Context,
    links: { userId: string; link: IdentityLinkInterface }[],
  ): Promise<void>;
}
