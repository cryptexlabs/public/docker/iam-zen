export enum IdentityLinkTypeEnum {
  AUTHF = 'authf',
  AWS_COGNITO = 'aws-cognito',
  SAMSUNG = 'samsung',
  LOCAL_AUTH = 'local-auth',
}
