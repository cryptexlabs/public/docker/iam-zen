import {
  Controller,
  Get,
  Headers,
  HttpStatus,
  Inject,
  Query,
  UseGuards,
} from '@nestjs/common';
import { appConfig } from '../../setup';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ApiMetaHeaders,
  ContextBuilder,
  NotEmptyPipe,
  RestResponse,
  Uuidv4ValidationPipe,
} from '@cryptexlabs/codex-nodejs-common';
import { ExampleRestResponse } from '../../example/example.rest.response';
import { UserGetAuthzGuard } from '../read/user.get.authz.guard';
import { ApiMetaHeadersInterface } from '@cryptexlabs/codex-data-model';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';
import { IdentityLinkService } from './identity-link.service';
import { IdentityLinkUsageEnum } from './identity-link-usage.enum';
import { IdentityLinkTypeValidationPipe } from './identity-link.type.validation.pipe';
import { IdentityLinkUsageValidationPipe } from './identity-link.usage.validation.pipe';
import { IdentityLinkGetAuthzGuard } from './identity-link.get.authz.guard';

@Controller(`/${appConfig.appPrefix}/${appConfig.apiVersion}/identity-link`)
@ApiTags('identity-link')
@ApiMetaHeaders()
export class IdentityLinkController {
  constructor(
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
    private readonly identityLinkService: IdentityLinkService,
  ) {}

  @Get('any/user-id')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get identity link user ID',
  })
  @ApiQuery({
    name: 'type',
    example: IdentityLinkTypeEnum.AUTHF,
    required: true,
  })
  @ApiQuery({
    name: 'usage',
    example: IdentityLinkUsageEnum.LOGIN,
    required: true,
  })
  @ApiQuery({
    name: 'link-id',
    example: '7f3cf4b1-f482-408b-80a0-d6dd7191bf59',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.identity-link',
        '278c6ec0-777c-43ad-83db-18b7cdfe2314',
      ),
    },
    status: HttpStatus.OK,
  })
  @UseGuards(IdentityLinkGetAuthzGuard)
  public async getIdentityLink(
    @Headers() headers: ApiMetaHeadersInterface,
    @Query('type', IdentityLinkTypeValidationPipe) type: IdentityLinkTypeEnum,
    @Query('usage', IdentityLinkUsageValidationPipe)
    usage: IdentityLinkUsageEnum,
    @Query('link-id', NotEmptyPipe) linkId: string,
  ): Promise<RestResponse<string | null>> {
    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    const userId = await this.identityLinkService.getUserIdForIdentityLink(
      usage,
      type,
      linkId,
    );

    return new RestResponse<string | null>(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.identity-link',
      userId,
    );
  }
}
