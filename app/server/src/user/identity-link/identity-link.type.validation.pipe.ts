import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { IdentityLinkTypeEnum } from './identity-link-type.enum';

@Injectable()
export class IdentityLinkTypeValidationPipe implements PipeTransform {
  transform(value: any, metadata: ArgumentMetadata) {
    if (!Object.values(IdentityLinkTypeEnum).includes(value)) {
      throw new BadRequestException(
        `Invalid link type: ${value}. Valid: "${Object.values(
          IdentityLinkTypeEnum,
        ).join('","')}"`,
      );
    }
    return value;
  }
}
