import {
  BadRequestException,
  forwardRef,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { UserDataServiceInterface } from './persistence/user.data.service.interface';
import {
  AuthenticateTypeEnum,
  AuthenticateTypes,
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  BasicAuthAuthenticateInterface,
  CreateTokenResponseDataInterface,
  RefreshAuthenticateInterface,
  StatelessAuthenticateTypes,
  TokenPairInterface,
} from '@cryptexlabs/authf-data-model';
import { AuthenticationServiceInterface } from '../authentication/authentication.service.interface';
import { Config } from '../config';
import { AuthenticationServiceTypeEnum } from '../authentication/authentication-service.type.enum';
import { AuthfIdentityLinkInterface } from '../authentication/authf.identity-link.interface';
import { v4 as uuidv4 } from 'uuid';
import { IdentityLinkTypeEnum } from './identity-link/identity-link-type.enum';
import { IdentityLinkUsageEnum } from './identity-link/identity-link-usage.enum';
import { IdentityLinkInterface } from './identity-link/identity-link.interface';
import { AuthenticationServiceFactory } from '../authentication/authentication-service-factory';
import { UserCreateInterface } from './create/user.create.interface';
import {
  ArrayUtil,
  Context,
  FriendlyHttpException,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { IdentityLinkService } from './identity-link/identity-link.service';
import { UserPermissionsProviderInterface } from './permission/user.permissions-provider.interface';
import { UserPersistedInterface } from './persistence/user.persisted.interface';
import { UserDto } from './user.dto';
import { UserInListDto } from './read/user.in-list.dto';
import { UserGroupsProviderInterface } from './group/user.groups.provider.interface';
import { PermissionUtil } from '../permission/permission.util';
import { TokenUtil } from '../token/token.util';
import { UserUpdateInterface } from './update/user.update.interface';
import { UserPatchInterface } from './update/user.patch.interface';
import { ImmutableObjectError } from '../error/immutable-object.error';
import { LocalesEnum } from '../locale/enum';
import { GroupService } from '../group/group.service';
import { UserLoginExtraService } from './identity-link/user.login.extra.service';
import { ExtraUtil } from './identity-link/extra.util';
import { QueryOptionsInterface } from '../query.options.interface';
import { PolicyService } from '../policy/policy.service';
import { GroupUtil } from '../group/group.util';
import { UserExportedDto } from './read/user.exported.dto';
import { UserEventBrokerService } from './user.event.broker.service';
import * as crypto from 'crypto';
import { UserDeviceDataServiceInterface } from './persistence/user.device.data.service.interface';
import { generateUsername } from 'unique-username-generator';
import * as jwt from 'jsonwebtoken';
import { BasicAuthAuthenticationProviderInterface } from '@cryptexlabs/authf-data-model/src/authentication/provider/basic/basic-auth.authentication-provider.interface';
import { BasicAuthAuthenticationProviderDataInterface } from '@cryptexlabs/authf-data-model/src/authentication/provider/basic/basic-auth.authentication-provider.data.interface';

@Injectable()
export class UserService {
  constructor(
    @Inject('CONFIG') private readonly config: Config,
    @Inject('USER_DATA_SERVICE')
    private readonly userDataService: UserDataServiceInterface,
    @Inject('USER_DEVICE_DATA_SERVICE')
    private readonly userDeviceDataService: UserDeviceDataServiceInterface,
    @Inject('USER_PERMISSIONS_PROVIDER')
    private readonly permissionsProvider: UserPermissionsProviderInterface,
    @Inject('USER_GROUPS_PROVIDER')
    private readonly userGroupsProvider: UserGroupsProviderInterface,
    private readonly identityLinkService: IdentityLinkService,
    @Inject('AUTHENTICATION_SERVICE')
    private readonly authenticationService: AuthenticationServiceInterface,
    private readonly authenticationServiceFactory: AuthenticationServiceFactory,
    @Inject(forwardRef(() => 'GROUP_SERVICE'))
    private readonly groupService: GroupService,
    private readonly loginExtraService: UserLoginExtraService,
    @Inject('POLICY_SERVICE') private readonly policyService: PolicyService,
    private readonly userEventBrokerService: UserEventBrokerService,
  ) {}

  public async loginUserWithId(
    context: Context,
    userId: string,
    userLogin: AuthenticateTypes,
  ) {
    if (userLogin.type === AuthenticateTypeEnum.REFRESH) {
      return this.loginUserWithRefresh(context, userLogin);
    }
    const user = await this.userDataService.getUserWithId(userId);

    if (!user) {
      throw new HttpException(
        `User with id ${userId} not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    if (user.blacklisted) {
      throw new UnauthorizedException();
    }

    return this._loginUser(context, user, userLogin);
  }

  public async loginUserWithRefresh(
    context: Context,
    userLogin: RefreshAuthenticateInterface,
  ) {
    const token = userLogin.data.token;
    if (!token) {
      throw new HttpException(`Invalid refresh token`, HttpStatus.BAD_REQUEST);
    }

    const decodedRefreshToken = jwt.decode(token) as unknown as any;

    const loginIdentityLinkId = decodedRefreshToken.sub;

    const userId = await this.identityLinkService.getUserIdForIdentityLink(
      IdentityLinkUsageEnum.LOGIN,
      this.authenticationService.getIdentityLinkType(),
      loginIdentityLinkId,
    );
    if (!userId) {
      throw new InternalServerErrorException(
        `No login identity link found for login identity link ID: ${loginIdentityLinkId}`,
      );
    }

    const user = await this.userDataService.getUserWithId(userId);

    if (!user) {
      throw new NotFoundException();
    }

    if (user.blacklisted) {
      throw new UnauthorizedException();
    }

    return this.authenticationService.loginUserWithRefreshToken(
      context,
      userLogin,
    );
  }

  public async getUserWithIdOrName(
    userId: string,
    username: string,
  ): Promise<UserPersistedInterface | null> {
    return await this.userDataService.getUserWithIdOrName(userId, username);
  }

  public async loginUserWithUsername(
    context: Context,
    username: string,
    userLogin: BasicAuthAuthenticateInterface,
  ) {
    const user = await this.userDataService.getUserWithUsername(username);

    if (!user) {
      throw new HttpException(
        `User with username ${username} not found`,
        HttpStatus.NOT_FOUND,
      );
    }

    if (user.blacklisted) {
      throw new UnauthorizedException();
    }

    return this._loginUser(context, user, userLogin as AuthenticateTypes);
  }

  public async loginUserWithDeviceId(context: Context, deviceId: string) {
    let userId = await this.userDeviceDataService.getUserIdWithDeviceId(
      deviceId,
    );
    let user: UserPersistedInterface;

    if (!userId) {
      userId = uuidv4();
      // Create user and add the device id

      const username = generateUsername();
      user = await this.createUser(context, userId, {
        username,
        authentication: {
          type: AuthenticationProviderTypeEnum.BASIC,
          data: {
            username,
            // Generate a 30 character random password that will never be used
            password: crypto.randomBytes(30).toString('hex').slice(0, 30),
          },
        },
      });
      await this.userDeviceDataService.saveUserDevice(userId, deviceId);
    } else {
      user = await this.userDataService.getUserWithId(userId);
    }

    if (!user) {
      throw new NotFoundException();
    }

    if (user.blacklisted) {
      throw new UnauthorizedException();
    }

    // Make a token
    const loginIdentityLink = user.identityLinks.find(
      (link) => link.usage === IdentityLinkUsageEnum.LOGIN,
    );
    const tokenData = await this.refreshUserPermissionsWithPersistedUser(
      context,
      null,
      user,
      loginIdentityLink,
    );

    return {
      ...tokenData,
      userId,
    };
  }

  public async loginWithAuthenticationVerification(
    context: Context,
    userLogin: StatelessAuthenticateTypes,
  ): Promise<any> {
    const validationResult =
      await this.authenticationService.validateAuthentication(
        context,
        userLogin,
      );

    // Check if user is already registered
    let existingUser = await this.userDataService.getUserWithUsername(
      validationResult.user.username,
    );
    if (!existingUser) {
      let authentication;
      if (userLogin.type === AuthenticateTypeEnum.SAMSUNG_ANY) {
        authentication = {
          type: AuthenticationProviderTypeEnum.SAMSUNG,
          data: {
            userId: userLogin.data.userId,
            clientId: userLogin.data.clientId,
          },
        };
      } else {
        throw new HttpException(
          `Authentication type: ${userLogin.type} not supported for authentication verification`,
          HttpStatus.NOT_IMPLEMENTED,
        );
      }

      const createUser: UserCreateInterface = {
        username: validationResult.user.username,
        authentication,
      };
      const authenticationId = uuidv4();

      await this.createUser(context, null, createUser, { authenticationId });

      do {
        existingUser = await this.userDataService.getUserWithUsername(
          validationResult.user.username,
        );
        if (!existingUser) {
          context.logger.verbose(`Waiting for user persistence #1`);
          await new Promise((resolve) => setTimeout(resolve, 100));
        }
      } while (!existingUser);
    } else {
      if (existingUser.blacklisted) {
        throw new UnauthorizedException();
      }
    }

    // Sometimes login data is not immediately available for first time logins
    let loginData: any;
    do {
      loginData = await this._loginUser(context, existingUser, userLogin);
      if (!loginData) {
        context.logger.verbose(`Waiting for user persistence #2`);
        await new Promise((resolve) => setTimeout(resolve, 100));
      }
    } while (!loginData);

    await this._saveExternalIdIdentityLink(userLogin, existingUser.id);

    // will need the user id because they logged in without it and probably do not know what it is
    loginData.userId = existingUser.id;

    return loginData;
  }

  private async _saveExternalIdIdentityLink(
    userLogin: StatelessAuthenticateTypes,
    userId: string,
  ): Promise<void> {
    if (userLogin.type === AuthenticateTypeEnum.SAMSUNG_ANY) {
      await this.identityLinkService.saveIdentityLink(
        IdentityLinkUsageEnum.EXTERNAL_ID,
        IdentityLinkTypeEnum.SAMSUNG,
        userLogin.data.userId,
        userId,
      );
    }

    // Other login types
  }

  private async _loginUser(
    context: Context,
    user: UserPersistedInterface,
    userLogin: AuthenticateTypes,
  ) {
    let loginIdentityLink;

    if (userLogin.type === AuthenticateTypeEnum.REFRESH) {
      throw new BadRequestException(
        `Invalid login type: ${AuthenticateTypeEnum.REFRESH}`,
      );
    } else {
      loginIdentityLink = user.identityLinks.find(
        (link) => link.usage === IdentityLinkUsageEnum.LOGIN,
      );
    }

    if (!loginIdentityLink) {
      throw new HttpException('Not authorized', HttpStatus.UNAUTHORIZED);
    }

    const authenticationService =
      this.authenticationServiceFactory.getAuthenticationServiceForLoginIdentityLinkType(
        loginIdentityLink.type,
      );
    if (!authenticationService) {
      throw new HttpException('Not authorized', HttpStatus.UNAUTHORIZED);
    }

    const tokenData = await this.refreshUserPermissionsWithPersistedUser(
      context,
      userLogin,
      user,
      loginIdentityLink,
    );

    if (!tokenData) {
      return await authenticationService.loginUser(
        context,
        user,
        loginIdentityLink,
        userLogin,
      );
    } else {
      return tokenData;
    }
  }

  public async getTokenForNewUser(
    context: Context,
    userId: string,
  ): Promise<TokenPairInterface> {
    const dbPermissions =
      await this.permissionsProvider.getPermissionsForNewUser(context);

    const payload: AuthenticationInterface = {
      providers: [],
      token: {
        expirationPolicy: this.config.tokenExpirationPolicy,
        subject: userId,
        body: {
          scopes: PermissionUtil.mapSelfToUserId(dbPermissions, userId).map(
            (permission) => permission.value,
          ),
        },
      },
    };

    const linkTypeMap = {
      [AuthenticationServiceTypeEnum.AUTHF]: IdentityLinkTypeEnum.AUTHF,
      [AuthenticationServiceTypeEnum.LOCAL]: IdentityLinkTypeEnum.LOCAL_AUTH,
    };
    const identityLinkType =
      linkTypeMap[this.config.authenticationProviderType];

    const authenticationService =
      this.authenticationServiceFactory.getAuthenticationServiceForLoginIdentityLinkType(
        identityLinkType,
      );

    const createTokenResponse = await authenticationService.getToken(
      context,
      payload,
      null,
      'any',
    );
    return createTokenResponse.token;
  }

  public async createUser(
    context: Context,
    userId: string | null,
    newUser: UserCreateInterface,
    options?: { authenticationId?: string; returnNullOnConflict?: boolean },
  ): Promise<UserPersistedInterface> {
    if (this.config.maxUsers) {
      const userCount = await this.userDataService.getUserCount(context);
      if (userCount >= this.config.maxUsers) {
        throw new FriendlyHttpException(
          `Maximum number of users ${this.config.maxUsers} has been reached. Currently there are ${userCount} users`,
          context,
          `Maximum number of users has been reached`,
          HttpStatus.FORBIDDEN,
          '',
        );
      }
    }

    if (!userId && !newUser.username) {
      throw new HttpException(
        `Must provide either a userId or a username`,
        HttpStatus.BAD_REQUEST,
      );
    }

    let user;
    if (userId && newUser.username) {
      user = await this.userDataService.getUserWithIdOrName(
        userId,
        newUser.username,
      );
      if (user) {
        if (user.blacklisted) {
          throw new UnauthorizedException();
        }
        if (options?.returnNullOnConflict) {
          return null;
        }
        throw new HttpException(
          `User with username: ${newUser.username} or user Id: ${userId} already exists`,
          HttpStatus.CONFLICT,
        );
      }
    } else if (newUser.username) {
      user = await this.userDataService.getUserWithUsername(newUser.username);
      if (user) {
        if (user.blacklisted) {
          throw new UnauthorizedException();
        }
        if (options?.returnNullOnConflict) {
          return null;
        }
        throw new HttpException(
          `User with username: ${newUser.username} already exists`,
          HttpStatus.CONFLICT,
        );
      }
    } else if (userId) {
      user = await this.userDataService.getUserWithId(userId);
      if (user) {
        if (user.blacklisted) {
          throw new UnauthorizedException();
        }
        if (options?.returnNullOnConflict) {
          return null;
        }
        throw new HttpException(
          `User with id: ${userId} already exists`,
          HttpStatus.CONFLICT,
        );
      }
    }

    let loginIdentityLink: IdentityLinkInterface;
    const useUserId = userId || uuidv4();

    if (
      this.config.authenticationProviderType ===
      AuthenticationServiceTypeEnum.AUTHF
    ) {
      const hardCodeMap = {
        // UserId : Authentication ID
        '278c6ec0-777c-43ad-83db-18b7cdfe2314':
          '1cffcc7d-1cfd-4bf4-96a4-0318b7bd8a58',
      };

      let authenticationId: string = options?.authenticationId || uuidv4();
      if (hardCodeMap[useUserId]) {
        authenticationId = hardCodeMap[useUserId];
      }

      loginIdentityLink = {
        usage: IdentityLinkUsageEnum.LOGIN,
        type: IdentityLinkTypeEnum.AUTHF,
        linkId: authenticationId,
        data: {
          // Hard code this authentication id to user id for testing
          authenticationId,
        },
      } as AuthfIdentityLinkInterface;
    }

    if (!loginIdentityLink) {
      throw new HttpException(
        `Unsupported authentication provider type: ${this.config.authenticationProviderType}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    const permissions = await this.permissionsProvider.getPermissionsForNewUser(
      context,
    );
    const tokenExpirationPolicy = this.config.tokenExpirationPolicy;

    const defaultUserAttributes = this.config.newUserDefaultAttributes;
    const defaultIdentityLinks = defaultUserAttributes.identityLinks
      ? defaultUserAttributes.identityLinks
      : [];

    if (newUser.groups) {
      const notAllowedGroups = newUser.groups.filter(
        (g) =>
          !(this.config.newUserDefaultAttributes.allowedGroups || []).includes(
            g,
          ),
      );
      if (notAllowedGroups.length > 0) {
        throw new FriendlyHttpException(
          `Not allowed to join groups: ${JSON.stringify(notAllowedGroups)}`,
          context,
          `You are not allowed to join these groups during registration: ${notAllowedGroups.join(
            ', ',
          )}`,
          HttpStatus.FORBIDDEN,
          '',
        );
      }
    }

    const loginExtra = await this.loginExtraService.getLoginExtra(useUserId);
    const persistUser: UserPersistedInterface = {
      id: useUserId,
      identityLinks: [loginIdentityLink, ...defaultIdentityLinks],
      username: newUser.username || null,
      groups: ArrayUtil.unique([
        ...(defaultUserAttributes.groups || []),
        ...(newUser.groups || []),
      ]),
      blacklisted: undefined,
      hash: {
        permissions: PermissionUtil.getPermissionsHash(permissions),
        tokenExpirationPolicy: TokenUtil.getTokenExpirationPolicyHash(
          this.config.tokenExpirationPolicy,
        ),
        extra: ExtraUtil.getExtraHash(loginExtra),
      },
    };

    await Promise.all([
      this.userDataService.createUser(context, persistUser) as any,
      this.authenticationService.createUser(
        context,
        useUserId,
        loginIdentityLink,
        newUser,
        PermissionUtil.mapSelfToUserId(permissions, useUserId),
        tokenExpirationPolicy,
      ),
      this.userEventBrokerService.userCreated(context, useUserId),
    ]);

    if (
      this.config.authenticationProviderType ===
      AuthenticationServiceTypeEnum.AUTHF
    ) {
      await this.identityLinkService.createIdentityLink(
        IdentityLinkUsageEnum.LOGIN,
        IdentityLinkTypeEnum.AUTHF,
        (loginIdentityLink as AuthfIdentityLinkInterface).data.authenticationId,
        useUserId,
      );
    }

    return persistUser;
  }

  public async refreshUserPermissions(
    context: Context,
    userId: string,
    identityLink: IdentityLinkInterface,
    waitForUserToExist = false,
  ): Promise<void | CreateTokenResponseDataInterface> {
    let persistedUser;

    while (
      !(persistedUser = await this.userDataService.getUserWithId(userId)) &&
      waitForUserToExist
    ) {
      context.logger.verbose(`Waiting for user persistence #3`);
      await new Promise((resolve) => setTimeout(resolve, 20));
    }
    return await this.refreshUserPermissionsWithPersistedUser(
      context,
      null,
      persistedUser,
      identityLink,
    );
  }

  public async getUsers(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserInListDto>> {
    const result = await this.userDataService.getUsers(
      context,
      page,
      pageSize,
      options,
    );
    const users = result.results.map((persistedUser) => {
      const immutable = !!this.config.defaultUsers.find(
        (configUser) => configUser.id === persistedUser.id,
      );
      return new UserInListDto(persistedUser, immutable);
    });

    return {
      total: result.total,
      results: users,
    };
  }

  public async refreshUserPermissionsWithPersistedUser(
    context: Context,
    userLogin: AuthenticateTypes | null,
    persistedUser: UserPersistedInterface,
    identityLink: IdentityLinkInterface,
  ): Promise<void | CreateTokenResponseDataInterface> {
    const currentPermissions =
      await this.permissionsProvider.getPermissionsForUser(
        context,
        persistedUser,
      );
    const currentExtra = await this.loginExtraService.getLoginExtra(
      persistedUser.id,
    );
    const currentExtraHash = ExtraUtil.getExtraHash(currentExtra);
    const currentPermissionsHash =
      PermissionUtil.getPermissionsHash(currentPermissions);
    const tokenExpirationPolicy = this.config.tokenExpirationPolicy;
    const currentTokenExpirationPolicyHash =
      TokenUtil.getTokenExpirationPolicyHash(tokenExpirationPolicy);
    let tokenData;
    if (
      persistedUser.hash.permissions !== currentPermissionsHash ||
      persistedUser.hash.tokenExpirationPolicy !==
        currentTokenExpirationPolicyHash
    ) {
      if (persistedUser.hash.permissions !== currentPermissionsHash) {
        context.logger.debug(
          `persisted permissions hash did not match current permissions hash`,
          {
            persistedHash: persistedUser.hash.permissions,
            currentHash: currentPermissionsHash,
            currentPermissions,
          },
        );
      }

      if (
        persistedUser.hash.tokenExpirationPolicy !==
        currentTokenExpirationPolicyHash
      ) {
        context.logger.debug(
          `persisted token expiration policy hash did not match current token expiration policy hash`,
          {
            persistedHash: persistedUser.hash.tokenExpirationPolicy,
            currentHash: currentTokenExpirationPolicyHash,
            tokenExpirationPolicy,
          },
        );
      }

      tokenData = await this.authenticationService.updateTokenPayload(
        context,
        userLogin,
        persistedUser.id,
        PermissionUtil.mapSelfToUserId(currentPermissions, persistedUser.id),
        tokenExpirationPolicy,
        [identityLink],
        true,
      );
      let succeeded = false;
      while (!succeeded) {
        try {
          await this.userDataService.updateTokenHashes(
            context,
            persistedUser.id,
            currentPermissionsHash,
            currentTokenExpirationPolicyHash,
            currentExtraHash,
          );
          succeeded = true;
        } catch (e) {
          context.logger.verbose(`Failing to update token hash #1. retrying`);
          await new Promise((resolve) => setTimeout(resolve, 100));
        }
      }
    }

    if (persistedUser.hash.extra !== currentExtraHash) {
      context.logger.debug(
        `persisted extra hash did not match current extra hash`,
        {
          persistedHash: persistedUser.hash.extra,
          currentHash: currentExtraHash,
          currentExtra,
        },
      );

      const loginIdentityLink = persistedUser.identityLinks.find(
        (findLink) => findLink.usage === IdentityLinkUsageEnum.LOGIN,
      );

      if (!loginIdentityLink) {
        return;
      }

      tokenData = await this.authenticationService.updateUserAuthentication(
        context,
        userLogin,
        loginIdentityLink,
        PermissionUtil.mapSelfToUserId(currentPermissions, persistedUser.id),
        this.config.tokenExpirationPolicy,
        persistedUser.id,
      );
      let succeeded = false;
      while (!succeeded) {
        try {
          await this.userDataService.updateTokenHashes(
            context,
            persistedUser.id,
            currentPermissionsHash,
            currentTokenExpirationPolicyHash,
            currentExtraHash,
          );
          succeeded = true;
        } catch (e) {
          context.logger.verbose(`Failed to update token hash #2. Retrying`);
          await new Promise((resolve) => setTimeout(resolve, 100));
        }
      }
    }
    return tokenData;
  }

  public async getUserByIdExists(
    context: Context,
    userId: string,
  ): Promise<boolean> {
    const user = await this.userDataService.getUserWithId(userId);
    return !!user;
  }

  public async getUserById(context: Context, userId: string): Promise<UserDto> {
    const user = await this.userDataService.getUserWithId(userId);
    if (!user) {
      throw new HttpException('User does not exist', HttpStatus.NOT_FOUND);
    }
    const immutable = !!this.config.defaultUsers.find(
      (configUser) => configUser.id === user.id,
    );

    const groups = await this.userGroupsProvider.getGroupsForUser(
      context,
      user.groups,
    );

    const groupsWithPolicies = await this.groupService.getGroupsByIds(
      context,
      groups.map((group) => group.id),
    );

    const policyIds = GroupUtil.getUniquePolicyIdsForGroups(groupsWithPolicies);

    const policies = await this.policyService.getPoliciesByIds(
      context,
      policyIds,
    );

    const userPolicies = policies.map((policy) => {
      const fromGroups = [];
      const policyIds = policies.map((policy) => policy.id);
      for (const group of groups) {
        const intersect = group.policies.filter((policyId) =>
          policyIds.includes(policyId),
        );
        if (intersect.length > 0) {
          fromGroups.push({
            id: group.id,
            name: group.name,
          });
        }
      }

      return {
        id: policy.id,
        name: policy.name,
        description: policy.description,
        fromGroups,
      };
    });

    const permissions = await this.permissionsProvider.getPermissionsForUser(
      context,
      user,
    );

    const userPermissions = permissions.map((permission) => {
      const fromPolicies = [];
      for (const policy of policies) {
        if (policy.permissions.includes(permission.id)) {
          fromPolicies.push({
            id: policy.id,
            name: policy.name,
          });
        }
      }

      const fromGroups = [];
      const policyIds = fromPolicies.map((policy) => policy.id);
      for (const group of groups) {
        const intersect = group.policies.filter((policyId) =>
          policyIds.includes(policyId),
        );
        if (intersect.length > 0) {
          fromGroups.push({
            id: group.id,
            name: group.name,
          });
        }
      }

      return {
        id: permission.id,
        name: permission.name,
        value: permission.value,
        description: permission.description,
        fromPolicies,
        fromGroups,
      };
    });

    const groupsForResponse = groups.map((group) => ({
      id: group.id,
      name: group.name,
      description: group.description,
    }));

    const identityLinks: IdentityLinkInterface[] =
      await this.identityLinkService.getIdentityLinksForUser(context, userId);

    return new UserDto(
      user,
      groupsForResponse,
      userPolicies,
      userPermissions,
      immutable,
      identityLinks,
    );
  }

  public async deleteUser(context: Context, userId: string): Promise<void> {
    this._immutabilityCheck(context, userId);
    const user = await this.userDataService.getUserWithId(userId);
    if (!user) {
      throw new NotFoundException();
    }
    const loginIdentityLinks = user.identityLinks.filter(
      (link) => link.usage === IdentityLinkUsageEnum.LOGIN,
    );

    for (const loginIdentityLink of loginIdentityLinks) {
      const authenticationService =
        this.authenticationServiceFactory.getAuthenticationServiceForLoginIdentityLinkType(
          loginIdentityLink.type,
        );

      try {
        await authenticationService.deleteAuthentication(
          context,
          (loginIdentityLink as AuthfIdentityLinkInterface).data
            .authenticationId,
        );
      } catch (e) {
        context.logger.error(`error deleting authentication: ${e}`);
      }
    }

    await this.userDataService.deleteUser(context, userId);
    await this.identityLinkService.deleteIdentityLinksForUser(context, userId);
    await this.userEventBrokerService.userDeleted(context, userId);
  }

  public async deleteUsers(context: Context, userIds: string[]): Promise<void> {
    for (const userId of userIds) {
      this._immutabilityCheck(context, userId);
    }

    const didModify = await this.userDataService.deleteUsers(context, userIds);
    if (!didModify) {
      throw new NotFoundException();
    }

    for (const userId of userIds) {
      await this.userEventBrokerService.userDeleted(context, userId);
    }
  }

  public async updateUser(
    context: Context,
    userId: string,
    userUpdate: UserUpdateInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, userId);
    const didModify = await this.userDataService.updateUser(
      context,
      userId,
      userUpdate,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async patchUser(
    context: Context,
    userId: string,
    userPatch: UserPatchInterface,
  ): Promise<void> {
    this._immutabilityCheck(context, userId);
    const didModify = await this.userDataService.patchUser(
      context,
      userId,
      userPatch,
    );

    if (!didModify) {
      throw new NotFoundException();
    }

    const identityLinks =
      await this.identityLinkService.getIdentityLinksForUser(context, userId);
    const loginIdentityLink = identityLinks.find(
      (link) => link.usage === IdentityLinkUsageEnum.LOGIN,
    );

    const authenticationService =
      this.authenticationServiceFactory.getAuthenticationServiceForLoginIdentityLinkType(
        loginIdentityLink.type,
      );

    const persistedUser = await this.userDataService.getUserWithId(userId);

    const currentPermissions =
      await this.permissionsProvider.getPermissionsForUser(
        context,
        persistedUser,
      );

    await authenticationService.updateUserAuthentication(
      context,
      {
        type: AuthenticateTypeEnum.BASIC,
        data: {
          username: userPatch.username,
          password: userPatch.password,
        },
      },
      loginIdentityLink,
      PermissionUtil.mapSelfToUserId(currentPermissions, persistedUser.id),
      this.config.tokenExpirationPolicy,
      userId,
      {
        type: AuthenticationProviderTypeEnum.BASIC,
        data: {
          username: userPatch.username,
          password: userPatch.password,
        },
      },
    );
  }

  public async addGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<void> {
    const uniqueGroups = ArrayUtil.unique(groupIds);

    const foundGroups = await this.groupService.getGroupsByIds(
      context,
      uniqueGroups,
    );

    if (foundGroups.length !== uniqueGroups.length) {
      throw new HttpException(`Invalid groups`, HttpStatus.BAD_REQUEST);
    }

    const didModify = await this.userDataService.addGroups(
      context,
      userId,
      uniqueGroups,
    );
    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async removeGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<void> {
    const immutableUser = this.config.defaultUsers.find(
      (findUser) => findUser.id === userId,
    );

    if (immutableUser) {
      if (ArrayUtil.intersection(groupIds, immutableUser.groups).length > 0) {
        const locale =
          groupIds.length === 1
            ? LocalesEnum.ERROR_IMMUTABLE_USER_GROUP_DELETE_SINGLE
            : LocalesEnum.ERROR_IMMUTABLE_USER_GROUP_DELETE_MULTIPLE;
        throw new ImmutableObjectError(context, new Error().stack, locale);
      }
    }

    const didModify = await this.userDataService.removeGroups(
      context,
      userId,
      groupIds,
    );

    if (!didModify) {
      throw new NotFoundException();
    }
  }

  public async existsByUsername(
    context: Context,
    username: string,
  ): Promise<boolean> {
    const user = await this.userDataService.getUserWithUsername(username);
    return !!user;
  }

  public async export(
    context: Context,
    all: boolean,
    userIds: string[],
  ): Promise<UserExportedDto[]> {
    if (all) {
      let page = 1;
      const pageSize = 100;
      const collection = [];
      let keepGoing = true;
      do {
        const usersResult = await this.userDataService.getUsers(
          context,
          page,
          pageSize,
        );

        if (usersResult.results.length > 0) {
          collection.push(...usersResult.results);
          page++;
        } else {
          keepGoing = false;
        }
      } while (keepGoing);

      return collection.map((user) => new UserExportedDto(user));
    }

    if (userIds.length === 0) {
      return [];
    }

    const users = await this.userDataService.getUsersWithIds(userIds);

    return users.map((user) => new UserExportedDto(user));
  }

  private _immutabilityCheck(context: Context, userId: string) {
    const immutableUser = this.config.defaultUsers.find(
      (findUser) => findUser.id === userId,
    );
    if (immutableUser) {
      throw new ImmutableObjectError(
        context,
        new Error().stack,
        LocalesEnum.ERROR_IMMUTABLE_USER,
      );
    }
  }

  public async getUsersForGroup(
    context: Context,
    groupId: string,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserInListDto>> {
    const result = await this.userDataService.getUsersForGroups(
      context,
      [groupId],
      page,
      pageSize,
      options,
    );
    const users = result.results.map((persistedUser) => {
      const immutable = !!this.config.defaultUsers.find(
        (configUser) => configUser.id === persistedUser.id,
      );
      return new UserInListDto(persistedUser, immutable);
    });

    return {
      total: result.total,
      results: users,
    };
  }

  public async getUsersForPolicy(
    context: Context,
    policyId: string,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserInListDto>> {
    const groups = await this.groupService.getGroupsForPolicyId(
      context,
      policyId,
    );

    const result = await this.userDataService.getUsersForGroups(
      context,
      groups.map((g) => g.id),
      page,
      pageSize,
      options,
    );
    const users = result.results.map((persistedUser) => {
      const immutable = !!this.config.defaultUsers.find(
        (configUser) => configUser.id === persistedUser.id,
      );
      return new UserInListDto(persistedUser, immutable);
    });

    return {
      total: result.total,
      results: users,
    };
  }

  public async getUsersForPermission(
    context: Context,
    permissionId: string,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserInListDto>> {
    const policies = await this.policyService.getPoliciesForPermission(
      context,
      permissionId,
    );

    const groups = await this.groupService.getGroupsByPoliciesForPermissions(
      context,
      policies.map((p) => p.id),
    );

    const result = await this.userDataService.getUsersForGroups(
      context,
      groups.map((g) => g.id),
      page,
      pageSize,
      options,
    );

    const users = result.results.map((persistedUser) => {
      const immutable = !!this.config.defaultUsers.find(
        (configUser) => configUser.id === persistedUser.id,
      );
      return new UserInListDto(persistedUser, immutable);
    });

    return {
      total: result.total,
      results: users,
    };
  }

  public async updateBlacklistedStatus(
    context: Context,
    userId: string,
    blacklisted: boolean,
  ): Promise<void> {
    await this.userDataService.updateBlacklistedStatus(
      context,
      userId,
      blacklisted,
    );

    const links = await this.identityLinkService.getIdentityLinksForUser(
      context,
      userId,
    );
    const loginIdentityLink = links.find(
      (link) => link.usage === IdentityLinkUsageEnum.LOGIN,
    );

    const authenticationService =
      this.authenticationServiceFactory.getAuthenticationServiceForLoginIdentityLinkType(
        loginIdentityLink.type,
      );

    await authenticationService.updateBlacklistStatus(
      context,
      blacklisted,
      loginIdentityLink,
    );
  }
}
