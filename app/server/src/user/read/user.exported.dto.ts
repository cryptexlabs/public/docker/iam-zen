import { UserPersistedInterface } from '../persistence/user.persisted.interface';

export class UserExportedDto {
  public readonly id: string;
  public readonly username: string;

  constructor(user: UserPersistedInterface) {
    this.id = user.id;
    this.username = user.username;
  }
}
