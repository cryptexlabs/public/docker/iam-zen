export interface UserExportRequestInterface {
  all: boolean;
  userIds: string[];
}
