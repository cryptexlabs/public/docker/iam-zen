import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  Header,
  Headers,
  HttpCode,
  HttpException,
  HttpStatus,
  Inject,
  NotImplementedException,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Put,
  Query,
  Req,
  Response,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import {
  ExampleAuthTokenPairResponse,
  ExampleBasicAuth,
  ExampleDeviceAuth,
  exampleNewBasicAuthUser,
  examplePatchUser,
  ExampleRefreshAuth,
  ExampleSamsungAuth,
  exampleUpdateUser,
  exampleUser,
  exampleUserId,
  exampleUsers,
} from './example';
import { UserService } from './user.service';
import { UserCreateInterface } from './create/user.create.interface';
import {
  ApiMetaHeaders,
  ApiPagination,
  ContextBuilder,
  NotEmptyPipe,
  RestPaginatedResponse,
  RestResponse,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { LocalesEnum } from '../locale/enum';
import {
  AuthenticateTypeEnum,
  AuthenticateTypes,
  AuthfMetaTypeEnum,
  BasicAuthAuthenticateInterface,
  RefreshAuthenticateInterface,
  SamsungAnyAuthenticateInterface,
  SamsungLegacyAuthenticateInterface,
} from '@cryptexlabs/authf-data-model';
import { UserRegisterValidationPipe } from './register/user.register.validation.pipe';
import { UserRegisterWithUsernameValidationPipe } from './register/user.register.with-username.validation.pipe';
import { ExampleSimpleResponse } from '../example/example.simple.response';
import { ExampleRestResponse } from '../example/example.rest.response';
import { UserUpdateInterface } from './update/user.update.interface';
import { UserPatchInterface } from './update/user.patch.interface';
import { appConfig } from '../setup';
import { UserModifyGroupsGuard } from './group/user.modify.groups.guard';
import { UserListAuthzGuard } from './read/user.list.authz.guard';
import { UserUpdateAuthzGuard } from './update/user.update.authz.guard';
import { UserDeleteAuthzGuard } from './delete/user.delete.authz.guard';
import { UserGetAuthzGuard } from './read/user.get.authz.guard';
import { UserCreateAuthzGuard } from './create/user.create.authz.guard';
import {
  ApiMetaHeadersInterface,
  MessageInterface,
} from '@cryptexlabs/codex-data-model';
import { UserInListDto } from './read/user.in-list.dto';
import { UserPatchValidationPipe } from './update/user.patch.validation.pipe';
import { UserUpdateValidationPipe } from './update/user.update.validation.pipe';
import { Uuidv4ArrayValidationPipe } from '@cryptexlabs/codex-nodejs-common/lib/src/pipe/uuidv4.array.validation.pipe';
import { OrderDirectionEnum } from '../query.options.interface';
import { OrderDirectionValidationPipe } from '../order.direction.validation.pipe';
import { UserExportRequestInterface } from './read/user.export.request.interface';
import { UserDeviceAuthenticateInterface } from './device/user.device.authenticate.interface';
import { Config } from '../config';
import { UserHttpService } from './user.http.service';
import { UserUpdateBlacklistedStatusRequestInterface } from './update/user.update.blacklisted-status.request.interface';
import { UserUpdateBlacklistedStatusAuthzGuard } from './update/user.update.blacklisted-status.authz.guard';

@Controller(`/${appConfig.appPrefix}/${appConfig.apiVersion}/user`)
@ApiTags('user')
@ApiMetaHeaders()
export class UserController {
  constructor(
    private readonly userService: UserService,
    private readonly userHttpService: UserHttpService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
    @Inject('CONFIG') private readonly config: Config,
  ) {}

  @Post(':userId/token')
  @ApiOperation({
    summary: 'Create a new token for a user (Login)',
  })
  @ApiBody({
    examples: {
      'Basic Auth': {
        value: new ExampleBasicAuth(),
      },
      Refresh: {
        value: new ExampleRefreshAuth(),
      },
    },
    schema: {},
    required: true,
  })
  @ApiParam({
    name: 'userId',
    example: exampleUserId,
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleAuthTokenPairResponse(),
    },
    status: HttpStatus.OK,
  })
  public async login(
    @Headers() headers,
    @Body(NotEmptyPipe) body: BasicAuthAuthenticateInterface,
    @Param('userId', NotEmptyPipe) userId: string,
    @Req() req: any,
  ) {
    const requestContext = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();
    if (userId === 'any') {
      return this.loginWithUsername(headers, body, req);
    }

    const result = await this.userService.loginUserWithId(
      requestContext,
      userId,
      body as AuthenticateTypes,
    );
    if (!result) {
      return;
    }
    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.CREATED,
      AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
      result,
    );
  }

  @Post('any/token')
  @ApiOperation({
    summary: 'Create a new token for a user (Login)',
  })
  @ApiBody({
    examples: {
      'Basic Auth': {
        value: new ExampleBasicAuth(),
      },
      Refresh: {
        value: new ExampleRefreshAuth(),
      },
      Samsung: {
        value: new ExampleSamsungAuth(),
      },
      Device: appConfig.loginWithDeviceEnabled
        ? {
            value: new ExampleDeviceAuth(),
          }
        : undefined,
    },
    schema: {},
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleAuthTokenPairResponse(),
    },
    status: HttpStatus.OK,
  })
  public async loginWithUsername(
    @Headers() headers,
    @Body()
    body:
      | BasicAuthAuthenticateInterface
      | RefreshAuthenticateInterface
      | SamsungLegacyAuthenticateInterface
      | SamsungAnyAuthenticateInterface
      | UserDeviceAuthenticateInterface,
    @Req() request: any,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    let result;

    if (body.type === AuthenticateTypeEnum.BASIC) {
      result = await this.userService.loginUserWithUsername(
        context,
        body.data.username.toLowerCase(),
        body,
      );
    } else if (body.type === AuthenticateTypeEnum.REFRESH) {
      result = await this.userService.loginUserWithRefresh(context, body);
    } else if (body.type === AuthenticateTypeEnum.SAMSUNG_LEGACY) {
      throw new HttpException(
        `type: ${AuthenticateTypeEnum.SAMSUNG_LEGACY} is not implemented. Come back next year`,
        HttpStatus.NOT_IMPLEMENTED,
      );
    } else if (body.type === AuthenticateTypeEnum.SAMSUNG_ANY) {
      if (this.config.httpRequestTimeout < 5000) {
        request.setTimeout(5000);
      }

      result = await this.userService.loginWithAuthenticationVerification(
        context,
        body as any,
      );
    } else if (body.type === 'device') {
      if (this.config.loginWithDeviceEnabled) {
        result = await this.userService.loginUserWithDeviceId(
          context,
          body.data.deviceId,
        );
      } else {
        throw new NotImplementedException(
          'Login with device Id not enabled on this server',
        );
      }
    } else {
      throw new BadRequestException(
        `Invalid authentication type: '${(body as AuthenticateTypes).type}'`,
      );
    }

    if (!result) {
      return;
    }

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.CREATED,
      AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
      result,
    );
  }

  @Post(':userId')
  @ApiOperation({
    summary: 'Create new user (Register)',
  })
  @ApiParam({
    name: 'userId',
    example: exampleUserId,
    required: true,
  })
  @ApiBody({
    schema: {
      example: exampleNewBasicAuthUser,
    },
    required: true,
  })
  @UseGuards(UserCreateAuthzGuard)
  public async register(
    @Headers() headers,
    @Body(NotEmptyPipe, UserRegisterValidationPipe) body: UserCreateInterface,
    @Param('userId', NotEmptyPipe) userId: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const data = await this.userHttpService.createUser(context, userId, {
      ...body,
      username: body.username.toLowerCase(),
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      data.status,
      'cryptexlabs.iam-zen.authentication.token-pair',
      {
        token: data.token,
      },
    );
  }

  @Post()
  @ApiOperation({
    summary: 'Create new user (Register) with username',
  })
  @ApiBody({
    schema: {
      example: exampleNewBasicAuthUser,
    },
    required: true,
  })
  @UseGuards(UserCreateAuthzGuard)
  public async registerWithUsername(
    @Headers() headers,
    @Body(NotEmptyPipe, UserRegisterWithUsernameValidationPipe)
    body: UserCreateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.createUser(context, null, {
      ...body,
      username: body.username.toLowerCase(),
    });

    return new SimpleHttpResponse(
      this.contextBuilder
        .build()
        .setMetaFromHeadersForNewMessage(headers)
        .getResult(),
      HttpStatus.CREATED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get list of users',
  })
  @ApiPagination()
  @ApiQuery({
    name: 'orderBy',
    required: false,
    schema: {
      example: 'name',
    },
  })
  @ApiQuery({
    name: 'orderDirection',
    required: false,
    schema: {
      example: 'asc',
    },
  })
  @ApiQuery({
    name: 'searchName',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.user.list',
        exampleUsers,
      ),
    },
  })
  @UseGuards(UserListAuthzGuard)
  public async getUsers(
    @Query('page', NotEmptyPipe, ParseIntPipe) page: number,
    @Query('pageSize', NotEmptyPipe, ParseIntPipe) pageSize: number,
    @Query('searchName') searchName: string,
    @Query('search') search: string,
    @Query('orderBy') orderBy: string,
    @Query('orderDirection', OrderDirectionValidationPipe)
    orderDirection: OrderDirectionEnum,
    @Headers() headers: ApiMetaHeadersInterface,
  ): Promise<MessageInterface<UserInListDto[]>> {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const result = await this.userService.getUsers(context, page, pageSize, {
      searchName,
      search,
      orderBy,
      orderDirection,
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestPaginatedResponse(
      responseContext,
      HttpStatus.OK,
      result.total,
      'cryptexlabs.iam-zen.user.list',
      result.results,
    );
  }

  @Get(':userId/exists')
  @ApiOperation({
    summary: 'Check if user with ID exists',
  })
  @ApiParam({
    name: 'userId',
    example: '278c6ec0-777c-43ad-83db-18b7cdfe2314',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.user.exists',
        true,
      ),
    },
    status: HttpStatus.OK,
  })
  public async getUserWithIdExists(
    @Param('userId', NotEmptyPipe) userId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const exists = await this.userService.getUserByIdExists(context, userId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.user.exists',
      exists,
    );
  }

  @Get(':userId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Get a user by user ID',
  })
  @ApiParam({
    name: 'userId',
    example: '278c6ec0-777c-43ad-83db-18b7cdfe2314',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.user',
        exampleUser,
      ),
    },
    status: HttpStatus.OK,
  })
  @UseGuards(UserGetAuthzGuard)
  public async getUser(
    @Param('userId', NotEmptyPipe) userId: string,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const user = await this.userService.getUserById(context, userId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.user',
      user,
    );
  }

  @Delete(':userId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete user by user ID',
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(UserDeleteAuthzGuard)
  public async deleteUser(
    @Param('userId', NotEmptyPipe) userId: string,
    @Headers() headers,
    @Req() request: any,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    if (this.config.httpRequestTimeout < 20000) {
      request.setTimeout(20000);
    }

    await this.userService.deleteUser(context, userId);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete()
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Delete users by user ID',
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @ApiQuery({
    name: 'ids',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(UserDeleteAuthzGuard)
  public async deleteUsers(
    @Query('ids', NotEmptyPipe) userIds: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const useIds = Array.isArray(userIds) ? userIds : [userIds];

    await this.userService.deleteUsers(context, useIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Put(':userId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Update user by user ID',
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiBody({
    schema: {
      example: exampleUpdateUser,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(UserUpdateAuthzGuard)
  public async updateUser(
    @Param('userId', NotEmptyPipe) userId: string,
    @Headers() headers,
    @Body(NotEmptyPipe, UserUpdateValidationPipe)
    userUpdate: UserUpdateInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.updateUser(context, userId, userUpdate);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Patch(':userId')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Partially update user by user ID',
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiBody({
    schema: {
      example: examplePatchUser,
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(UserUpdateAuthzGuard)
  public async patchUser(
    @Param('userId', NotEmptyPipe) userId: string,
    @Body(NotEmptyPipe, UserPatchValidationPipe) userPatch: UserPatchInterface,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.patchUser(context, userId, {
      ...userPatch,
      username: userPatch?.username.toLowerCase(),
    });

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Post(':userId/group')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Add user to groups',
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiBody({
    schema: {
      example: ['613b6c12-d9c1-47cf-b401-0b5b03ef33d0'],
    },
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(new UserModifyGroupsGuard('create'))
  public async addGroups(
    @Param('userId', NotEmptyPipe) userId: string,
    @Body(NotEmptyPipe, Uuidv4ArrayValidationPipe) groups: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.addGroups(context, userId, groups);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete(':userId/group')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Remove user from groups',
  })
  @ApiParam({
    name: 'userId',
    example: '16b327c5-af12-4f02-a772-005c6b92ba6d',
    required: true,
  })
  @ApiQuery({
    name: 'ids',
    example: [
      '613b6c12-d9c1-47cf-b401-0b5b03ef33d0',
      'cf24881f-0983-40e2-a6d1-2e560863e89e',
    ],
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.ACCEPTED,
  })
  @HttpCode(HttpStatus.ACCEPTED)
  @UseGuards(new UserModifyGroupsGuard('delete'))
  public async removeGroups(
    @Param('userId', NotEmptyPipe) userId: string,
    @Query('ids', NotEmptyPipe) ids: string[],
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const groupIds = typeof ids === 'string' ? [ids] : ids;

    await this.userService.removeGroups(context, userId, groupIds);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }

  @Get('any/username-exists')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Checks if username exists',
  })
  @ApiQuery({
    name: 'username',
    example: 'johndoe',
    required: true,
  })
  @ApiResponse({
    schema: {
      example: new ExampleRestResponse(
        HttpStatus.OK,
        'cryptexlabs.iam-zen.user.username.exists',
        true,
      ),
    },
  })
  @UseGuards(UserCreateAuthzGuard)
  public async getUserNameExists(
    @Headers() headers,
    @Query('username') username: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const exists = await this.userService.existsByUsername(context, username);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      'cryptexlabs.iam-zen.user.username.exists',
      exists,
    );
  }

  @Post('any/export')
  @ApiBearerAuth()
  @ApiOperation({
    summary: 'Exports users',
    description:
      "If 'all' is true then will select all the users regardless of the list provided. If 'all' is false then will only export the list of user ids provided",
  })
  @ApiBody({
    schema: {},
    examples: {
      Some: {
        value: {
          all: false,
          userIds: ['278c6ec0-777c-43ad-83db-18b7cdfe2314'],
        },
      },
      All: {
        value: {
          all: true,
          userIds: [],
        },
      },
    },
    required: true,
  })
  @UseGuards(UserListAuthzGuard)
  @Header('Content-Type', 'text/csv')
  public async export(
    @Headers() headers,
    @Body() body: UserExportRequestInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const exportedUsers = await this.userService.export(
      context,
      body.all,
      body.userIds,
    );

    const csvRows = exportedUsers.map((user) => `${user.id},${user.username}`);

    return csvRows.join('\n') + '\n';
  }

  @Put(':userId/blacklisted')
  @ApiBearerAuth()
  @ApiOperation({
    summary: "Updates a user's blacklisted status",
  })
  @ApiBody({
    schema: {
      example: {
        blacklisted: true,
      },
    },
  })
  @ApiParam({
    name: 'userId',
    example: exampleUserId,
  })
  @UseGuards(UserUpdateBlacklistedStatusAuthzGuard)
  public async updateBlacklistedStatus(
    @Param('userId', NotEmptyPipe) userId: string,
    @Headers() headers: ApiMetaHeadersInterface,
    @Body() body: UserUpdateBlacklistedStatusRequestInterface,
  ) {
    const requestContext = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    await this.userService.updateBlacklistedStatus(
      requestContext,
      userId,
      body.blacklisted,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.ACCEPTED,
      LocalesEnum.SUCCESS,
    );
  }
}
