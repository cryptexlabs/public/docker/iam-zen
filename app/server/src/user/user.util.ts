import { AuthenticationTypes } from '@cryptexlabs/authf-data-model';
import { StringUtil } from '@cryptexlabs/codex-nodejs-common';

export class UserUtil {
  static getAuthenticationHash(authentication: AuthenticationTypes): string {
    return StringUtil.sha512(JSON.stringify(authentication || ''));
  }
}
