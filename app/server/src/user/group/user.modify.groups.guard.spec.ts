import { ExecutionContext } from '@nestjs/common';
import { UserDeleteAuthzGuard } from '../delete/user.delete.authz.guard';

describe('UserDeleteGroupsGuard', function () {
  it('should allow deletion for admin user', () => {
    const guard = new UserDeleteAuthzGuard();
    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization:
              'Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNzhjNmVjMC03NzdjLTQzYWQtODNkYi0xOGI3Y2RmZTIzMTQiLCJ0eXBlIjoiYWNjZXNzIiwic2NvcGVzIjpbImlhbS16ZW46Ojphbnk6YW55OmFueSIsImlhbS16ZW46Ojphbnk6YW55OmFueTphbnk6YW55OmFueSJdLCJpYXQiOjE2MzIyMDI1MzMsImV4cCI6MTYzMjIwMzQzM30.E553o_amTVDA11LU8akFEtS2aIscMF1MS9-Il7Y41XX2JO_OWdVZnHxTnCz2p5pVcvYJRI203R5BwOgAKdQOPP2Z_Vwdrj2XdrgFnKvzHcv-dSGbDxbqvCBXjJ9S8HkegVWCwc9ZiU9OUoko17dhNxQuf-zeRWmPoUvjI7V6cbXi_a4C6vUJu5j_m9espW7XogXDSgwM42mlH7xeW-Uxt941trJUcAZkGuBg8KVT2-zgSRwm973iO3BLflwcA55NZkaALJo6EzVM0L3gqLHYF_ZYz73gSsILg-uLkEilfgGCwdFJbKQZ3Alk9WMNYesTvwWjY_kxaaHOk2Uf0bfxq5v1fxiwJgzdPjsuAczYgXIWnaMdypBKqW9MY01B8p1nXUAPp-JCmB5ATrZwv-tJsWxb3xr-H7B3U54EvgDWgNPpXNvf7wmopkLbCpO4H4ZoCBFHOaHR4H3HHL9dijCRal7JcQ6ziOibYbU-s0gj_zXVibIRewJ-OqcBRXvekYQVRcNGHxToiCR-Jf1easBYuF223GRQlh-1YKGNOudnRv3TurryxHhouo1fifRfb41rMVxEp2h5BLDxnrHbwzXy9-kviOm42v9PmpMXMvomOMcYYtZhD9L9hnon3bLPY3H__-rggwZmNupE0exZE_pILsmXIC6ilF3ZmEmTgC8fbvs',
          },
          params: {
            userId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: {
            ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'],
          },
        }),
      }),
    };
    expect(guard.canActivate(context as ExecutionContext)).toBe(true);
  });
});
