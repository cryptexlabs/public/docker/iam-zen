import {
  ConflictException,
  HttpStatus,
  Inject,
  Injectable,
} from '@nestjs/common';
import { UserService } from './user.service';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { UserCreateInterface } from './create/user.create.interface';
import { TokenPairInterface } from '@cryptexlabs/authf-data-model';
import { Config } from '../config';
import { UserPersistedInterface } from './persistence/user.persisted.interface';

@Injectable()
export class UserHttpService {
  constructor(
    private readonly userService: UserService,
    @Inject('CONFIG') private readonly config: Config,
  ) {}

  public async createUser(
    context: Context,
    userId: string | null,
    newUser: UserCreateInterface,
  ): Promise<{ token: TokenPairInterface; status: HttpStatus }> {
    const [token, user]: [TokenPairInterface, UserPersistedInterface] =
      await Promise.all([
        this.userService.getTokenForNewUser(context, userId),
        this.userService.createUser(context, userId, newUser, {
          returnNullOnConflict: true,
        }),
      ]);

    if (
      !user &&
      (!newUser.ignoreConflicts || !this.config.allowIgnoreCreateUserConflicts)
    ) {
      throw new ConflictException(
        `User with id '${userId}' or username ${newUser.username} already exists.`,
      );
    }

    return {
      token: token as TokenPairInterface,
      status: user ? HttpStatus.CREATED : HttpStatus.OK,
    };
  }
}
