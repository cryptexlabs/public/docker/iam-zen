export interface UserDeletedEventInterface {
  userId: string;
}
