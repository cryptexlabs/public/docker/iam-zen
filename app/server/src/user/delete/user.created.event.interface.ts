export interface UserCreatedEventInterface {
  userId: string;
}
