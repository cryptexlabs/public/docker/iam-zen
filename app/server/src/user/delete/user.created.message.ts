import {
  MessageInterface,
  MessageMetaInterface,
} from '@cryptexlabs/codex-data-model';
import { Context, MessageMeta } from '@cryptexlabs/codex-nodejs-common';
import { KafkaTopicsEnum } from '../../kafka.topics.enum';
import { UserCreatedEventInterface } from './user.created.event.interface';

export class UserCreatedMessage
  implements MessageInterface<UserCreatedEventInterface>
{
  public readonly meta: MessageMetaInterface;
  public readonly data: UserCreatedEventInterface;

  constructor(context: Context, data: UserCreatedEventInterface) {
    this.meta = new MessageMeta(
      KafkaTopicsEnum.USER_CREATED,
      context.locale,
      context.config,
      context.correlationId,
      context.started,
    );
    this.data = data;
  }
}
