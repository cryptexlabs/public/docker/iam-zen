import {
  MessageInterface,
  MessageMetaInterface,
} from '@cryptexlabs/codex-data-model';
import { UserDeletedEventInterface } from './user.deleted.event.interface';
import { Context, MessageMeta } from '@cryptexlabs/codex-nodejs-common';
import { KafkaTopicsEnum } from '../../kafka.topics.enum';

export class UserDeletedMessage
  implements MessageInterface<UserDeletedEventInterface>
{
  public readonly meta: MessageMetaInterface;
  public readonly data: UserDeletedEventInterface;

  constructor(context: Context, data: UserDeletedEventInterface) {
    this.meta = new MessageMeta(
      KafkaTopicsEnum.USER_DELETED,
      context.locale,
      context.config,
      context.correlationId,
      context.started,
    );
    this.data = data;
  }
}
