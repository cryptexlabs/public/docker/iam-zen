import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class UserDeleteListAuthzGuard implements CanActivate {
  public canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzGuardUtil(context);
    for (const userId of util.query.ids) {
      if (
        !util.isAuthorized(
          {
            object: 'iam-zen',
            objectId: '',
            action: '',
          },
          {
            object: 'user',
            objectId: userId,
            action: 'delete',
          },
        )
      ) {
        return false;
      }
    }
    return true;
  }
}
