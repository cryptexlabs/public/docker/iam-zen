import {
  AuthenticateTypeEnum,
  AuthenticationProviderTypeEnum,
  AuthfMetaTypeEnum,
  BasicAuthAuthenticateDataInterface,
  BasicAuthAuthenticateInterface,
  BasicAuthAuthenticationProviderDataInterface,
  BasicAuthAuthenticationProviderInterface,
  CreateTokenResponseDataInterface,
  EmailTypeEnum,
  ProfileInterface,
  RefreshAuthenticateInterface,
  SamsungAnyAuthenticateDataInterface,
  TokenInterface,
  TokenPairInterface,
  UserInterface,
  UtcTimezone,
} from '@cryptexlabs/authf-data-model';
import { UserCreateInterface } from './create/user.create.interface';
import {
  ContextBuilder,
  RestResponse,
  ServiceClient,
} from '@cryptexlabs/codex-nodejs-common';
import { HttpStatus } from '@nestjs/common';
import { appConfig, baseLogger } from '../setup';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import { i18nData } from '../locale/locales';
import { RefreshAuthenticateDataInterface } from '@cryptexlabs/authf-data-model/lib/0.1.0/authentication/refresh/refresh.authenticate.data.interface';

export const exampleUserId = '278c6ec0-777c-43ad-83db-18b7cdfe2314';
export const exampleUsername = 'johndoe';

export class ExampleBasicAuthentication
  implements BasicAuthAuthenticationProviderInterface
{
  data: BasicAuthAuthenticationProviderDataInterface = {
    username: exampleUsername,
    password:
      'b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3',
  };
  type: AuthenticationProviderTypeEnum.BASIC =
    AuthenticationProviderTypeEnum.BASIC;
}

export class ExampleBasicAuth implements BasicAuthAuthenticateInterface {
  data: BasicAuthAuthenticateDataInterface = {
    username: exampleUsername,
    password:
      'b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3',
  };
  type: AuthenticateTypeEnum.BASIC = AuthenticateTypeEnum.BASIC;
}

export class ExampleRefreshAuth implements RefreshAuthenticateInterface {
  data: RefreshAuthenticateDataInterface = {
    token:
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0ZWEyN2M2ZS01N2IxLTRjZjctODIyYi1mM2RiODE3MWE4YmQiLCJ0eXBlIjoiYWNjZXNzIiwic2NvcGVzIjpbInVzZXI6c2VsZjphbGwiXSwiaWF0IjoxNjMxMDQzMDAzfQ.0hlmsRyS0SMs6LY8_9mfmyCKYr2yYSsWeC6ULct1GmH-IPsdDJM_p5Gjesa24R59v2_Ut6lJG6bPJKA1vGN5pg9HnaA2aBF4zGQ6zQayKZmElZxD2tv2pEmrjvWn1TXyKEga9Td9it2q9gxIwzDATeCPtzj7amvcgi47KI3jw5A_67jwdRkvimz9nagn4Y_xdahpcuxQD2tSQD-iRo7bnRS4cfAPNU_yXr6CP0HEbS7dLndvE6LfTPg5-WG3zwB6xNVMDCTYz_a7WAbj_knXzr2q0SnGsjVl_c1mxMSP3pmOye4KAVRMMXdIONd9_JGpnryAiE00Dp1bdPr-V4n-crkdq6rWVy-bG0ycnfJD6rCZNJkafsCNx0QTWs8iFNeciZ3NkU0_EMymK9_kAH4oO5sHu4eDvDMXBgABAmK_KHg_cZcKCnELNEy13H-ozEM9PDGDjBfvNwul9WyETSmLRtYBNAXlo5QOtFLzKh9lsoDjN0l1zQEgtaoDXqwSs7Ow1WP_1u_SB-WeI7uldwRGAuQHRLJHBwHyb42_8amqpcJJJpNa_6onFqaFgNyefUwaA596NokbMdno8kIwhk4l7NkjyFhGw3L0nI6YIY92Ym0C3ZVm8HxDyoznAgzvq_PI-lgpdoV8KFYOAZWutLBndylep3iqZlcL0MmpJeyvAnI',
  };
  type: AuthenticateTypeEnum.REFRESH = AuthenticateTypeEnum.REFRESH;
}

export class ExampleSamsungAuth {
  data: SamsungAnyAuthenticateDataInterface = {
    clientId: 'dksd032030',
    accessToken: 'BC5QXqkBBoRbyql7iecoZ08s3',
    userId: 'jkdssasd7',
  };
  type: AuthenticateTypeEnum.SAMSUNG_ANY = AuthenticateTypeEnum.SAMSUNG_ANY;
}

export class ExampleDeviceAuth {
  data = {
    deviceId: '9d25f3b6ab8cfba5d2d68dc8d062988534a63e87',
  };
  type = 'device';
}

export const exampleNewBasicAuthUser: UserCreateInterface = {
  authentication: new ExampleBasicAuthentication(),
  username: exampleUsername,
  groups: ['cf24881f-0983-40e2-a6d1-2e560863e89e'],
};

export class ExampleUser implements UserInterface {
  userId = 'asdf1234';
  timezone: UtcTimezone = '+06:00';
  username = null;
  profile: ProfileInterface = {
    name: {
      first: 'Jason',
      middle: null,
      last: 'Bourne',
      display: 'J. Bourne',
    },
    email: {
      other: [],
      primary: {
        value: 'jason.bourne@samsung.com',
        type: EmailTypeEnum.UNKNOWN,
      },
    },
    phone: {
      other: [],
      primary: null,
    },
    photo: {
      other: [],
      primary: null,
    },
  };
}

export class ExampleTokenPair implements TokenPairInterface {
  access: TokenInterface = {
    token:
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0ZWEyN2M2ZS01N2IxLTRjZjctODIyYi1mM2RiODE3MWE4YmQiLCJ0eXBlIjoiYWNjZXNzIiwic2NvcGVzIjpbInVzZXI6c2VsZjphbGwiXSwiaWF0IjoxNjMxMDQzMDAzfQ.0hlmsRyS0SMs6LY8_9mfmyCKYr2yYSsWeC6ULct1GmH-IPsdDJM_p5Gjesa24R59v2_Ut6lJG6bPJKA1vGN5pg9HnaA2aBF4zGQ6zQayKZmElZxD2tv2pEmrjvWn1TXyKEga9Td9it2q9gxIwzDATeCPtzj7amvcgi47KI3jw5A_67jwdRkvimz9nagn4Y_xdahpcuxQD2tSQD-iRo7bnRS4cfAPNU_yXr6CP0HEbS7dLndvE6LfTPg5-WG3zwB6xNVMDCTYz_a7WAbj_knXzr2q0SnGsjVl_c1mxMSP3pmOye4KAVRMMXdIONd9_JGpnryAiE00Dp1bdPr-V4n-crkdq6rWVy-bG0ycnfJD6rCZNJkafsCNx0QTWs8iFNeciZ3NkU0_EMymK9_kAH4oO5sHu4eDvDMXBgABAmK_KHg_cZcKCnELNEy13H-ozEM9PDGDjBfvNwul9WyETSmLRtYBNAXlo5QOtFLzKh9lsoDjN0l1zQEgtaoDXqwSs7Ow1WP_1u_SB-WeI7uldwRGAuQHRLJHBwHyb42_8amqpcJJJpNa_6onFqaFgNyefUwaA596NokbMdno8kIwhk4l7NkjyFhGw3L0nI6YIY92Ym0C3ZVm8HxDyoznAgzvq_PI-lgpdoV8KFYOAZWutLBndylep3iqZlcL0MmpJeyvAnI',
    expiration: new Date(new Date().getTime() + 15 * 60 * 1000),
  };
  refresh: TokenInterface = {
    token:
      'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0ZWEyN2M2ZS01N2IxLTRjZjctODIyYi1mM2RiODE3MWE4YmQiLCJ0eXBlIjoicmVmcmVzaCIsImlhdCI6MTYzMTA0MzAwM30.R9nIpPBZdcc6pW89TdLhpIyEdQUOBAQXw2qB8PQyjLx6P9iVPO_o9yLdjC0bdGxy7Vl8ETIYCJPcn0IBVgtqaMvzPx9mtFOrPh9orMKNbwXOF9F4Kl5R4mBHvP-sstpXiu64BIsGCTDXv7IQv1L0TlqzwrogOy4Wg5-GV3MaCZF0wJ9jehqD1hpqLrmBu3QtytdyNrJqkzrWU3T-Hvewxy-UtjeTcyljBY-R-q82toGEO2vHeO5E2SqVCdDVWozS6tok66zlelQdhMm4CPIkF43MRinUWey9OLENvKqUZ_ANzIO5ABBcuxouBTEigxV7Zz7oGjC2sGxc8oFKsW7lGMqqZdiiyD_Q0qRbQIe9lQbl2xgV83C-UGGJ0uyPVFKruTEoJ_8rFozAsKT-LFB3M0Zdx64ekjFOD7K1qZEOrLa9s5Qo22_gh9YA3zsk84_IXRBN1nk6mLeSC8EE6hJXFOwmFCc8r3jRT1BS1VNDFXAu74smyj5gK2eQ9tJv-aQ50AA8d7lm8Duo12OsNXH0HdS9ZKRefsC7r97N3wt8ggigK_qL-F6BVwtctK1kJXF9XjH64iW9qqVbgh7XvhmzMFkihwdLP6_QTUiK7MsDaxZNtYCaV9BTFNNAnUpIRWQ4tB7BjnDkpSd6jT4aJ31uvEBGrzqsPH2pNWfyfTv9nHo',
    expiration: new Date(new Date().getTime() + 30 * 24 * 60 * 60 * 1000),
  };
}

const responseData: CreateTokenResponseDataInterface = {
  token: new ExampleTokenPair(),
  user: new ExampleUser(),
};

const exampleContext = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext('default', null),
)
  .setI18nData(i18nData)
  .build()
  .getResult();

export class ExampleAuthTokenPairResponse extends RestResponse<CreateTokenResponseDataInterface> {
  constructor() {
    super(
      exampleContext,
      HttpStatus.CREATED,
      AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
      responseData,
    );
  }
}

export const exampleUser = {
  groups: [
    {
      id: 'cf24881f-0983-40e2-a6d1-2e560863e89e',
      name: 'admin',
    },
  ],
  immutable: true,
  id: '278c6ec0-777c-43ad-83db-18b7cdfe2314',
  username: 'admin',
};

export const exampleUsers = [
  {
    immutable: false,
    id: 'f938d896-d3ac-4533-a32b-0190eb42fe81',
    groupCount: 1,
    username: null,
  },
  {
    immutable: true,
    id: '278c6ec0-777c-43ad-83db-18b7cdfe2314',
    groupCount: 1,
    username: 'admin',
  },
];

export const exampleUpdateUser = {
  username: 'janedoe',
  groups: ['613b6c12-d9c1-47cf-b401-0b5b03ef33d0'],
};

export const examplePatchUser = {
  username: 'janedoe',
};
