import { UserPersistedInterface } from './persistence/user.persisted.interface';
import { UserGroupInterface } from './group/user.group.interface';
import { UserPolicyInterface } from './policy/user.policy.interface';
import { UserPermissionsInterface } from './permission/user.permissions.interface';
import { IdentityLinkInterface } from './identity-link/identity-link.interface';

export class UserDto {
  public readonly id: string;
  public readonly username: string;
  public readonly blacklisted: boolean;

  constructor(
    user: UserPersistedInterface,
    public readonly groups: UserGroupInterface[],
    public readonly policies: UserPolicyInterface[],
    public readonly permissions: UserPermissionsInterface[],
    public readonly immutable: boolean,
    public readonly identityLinks: IdentityLinkInterface[],
  ) {
    this.id = user.id;
    this.username = user.username;
    this.blacklisted = user.blacklisted;
  }
}
