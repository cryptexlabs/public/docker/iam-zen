export interface UserUpdateInterface {
  username: string;
  groups: string[];
}
