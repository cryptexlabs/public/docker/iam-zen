export interface UserUpdateBlacklistedStatusRequestInterface {
  blacklisted: boolean;
}
