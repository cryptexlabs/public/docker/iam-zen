import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { UserPatchInterface } from './user.patch.interface';

@Injectable()
export class UserPatchValidationPipe implements PipeTransform {
  private _schema: Joi.ObjectSchema<UserPatchInterface>;
  constructor() {
    this._schema = Joi.object({
      username: Joi.string().alphanum().optional(),
    });
  }

  public transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this._schema.validate(value.data);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
