import {
  CanActivate,
  ExecutionContext,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../../constants';
import { Config } from '../../config';

@Injectable()
export class UserCreateAuthzGuard implements CanActivate {
  constructor(@Inject('CONFIG') private readonly config: Config) {}
  public canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    if (this.config.userRegistrationEnabled) {
      return true;
    }

    const util = new HttpAuthzGuardUtil(context);

    const isAuthorizedToCreateUser = util.isAuthorized(AuthzNamespace, {
      object: 'user',
      objectId: '',
      action: 'create',
    });
    if (!isAuthorizedToCreateUser) {
      return false;
    }

    if (util.body.groups) {
      for (const id of util.body.groups as string[]) {
        if (
          !util.isAuthorized(
            AuthzNamespace,
            {
              action: '',
              object: 'user',
              objectId: util.params.userId,
            },
            {
              action: 'create',
              object: 'group',
              objectId: id,
            },
          )
        ) {
          return false;
        }
      }
    }

    return true;
  }
}
