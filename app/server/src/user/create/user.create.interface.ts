import {
  BasicAuthAuthenticationProviderInterface,
  SamsungAuthenticationProviderInterface,
  SamsungLegacyAuthenticationProviderInterface,
} from '@cryptexlabs/authf-data-model';

export type UserCreateAuthenticationType =
  | BasicAuthAuthenticationProviderInterface
  | SamsungLegacyAuthenticationProviderInterface
  | SamsungAuthenticationProviderInterface;

export interface UserCreateInterface {
  authentication?: UserCreateAuthenticationType;
  username?: string;
  groups?: string[];
  ignoreConflicts?: boolean;
}
