export interface UserDeviceAuthenticateInterface {
  type: 'device';
  data: {
    deviceId: string;
  };
}
