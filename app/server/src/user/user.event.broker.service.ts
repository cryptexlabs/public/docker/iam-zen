import { Context, KafkaService } from '@cryptexlabs/codex-nodejs-common';
import { UserDeletedMessage } from './delete/user.deleted.message';
import { Inject } from '@nestjs/common';
import { Config } from '../config';
import { UserCreatedMessage } from './delete/user.created.message';

export class UserEventBrokerService {
  constructor(
    @Inject('KAFKA_SERVICE') private readonly kafkaService: KafkaService,
    @Inject('CONFIG') private readonly config: Config,
  ) {}

  public async userDeleted(context: Context, userId: string): Promise<void> {
    const message = new UserDeletedMessage(context, {
      userId,
    });

    await this.kafkaService.publish(message.meta.type, message);
  }

  public async userCreated(context: Context, userId: string): Promise<void> {
    const message = new UserCreatedMessage(context, {
      userId,
    });

    await this.kafkaService.publish(message.meta.type, message);
  }
}
