import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { Inject } from '@nestjs/common';
import { QueryOptionsInterface } from 'src/query.options.interface';
import { UserPatchInterface } from '../update/user.patch.interface';
import { UserUpdateInterface } from '../update/user.update.interface';
import { UserInterface } from '../user.interface';
import { UserDataServiceInterface } from './user.data.service.interface';
import { UserPersistedInterface } from './user.persisted.interface';
import * as NodeCache from 'node-cache';
import { Config } from '../../config';

export class UserProductivePersistenceCacheService
  implements UserDataServiceInterface
{
  private _nodeCache: NodeCache;
  private readonly PREFIX_WITH_USER_ID = 'wuid';
  private readonly PREFIX_WITH_USERNAME = 'wun';
  private readonly TTL = 5;

  constructor(
    private readonly persistenceService: UserDataServiceInterface,
    @Inject('CONFIG') private readonly config: Config,
  ) {
    this._nodeCache = new NodeCache();
  }

  private _getWithUserIdKey(userId: string) {
    return `${this.PREFIX_WITH_USER_ID}:${userId}`;
  }

  private _getWithUsernameKey(username: string) {
    return `${this.PREFIX_WITH_USERNAME}:${username}`;
  }

  public async getUserWithId(userId: string): Promise<UserPersistedInterface> {
    const key = this._getWithUserIdKey(userId);

    const cachedUser: UserPersistedInterface = this._nodeCache.get(key);
    if (cachedUser) {
      return cachedUser;
    }

    const user = await this.persistenceService.getUserWithId(userId);

    if (user) {
      this._nodeCache.set(key, user, this.TTL);
    }

    return user;
  }

  public async getUsersWithIds(
    userIds: string[],
  ): Promise<UserPersistedInterface[]> {
    return await this.persistenceService.getUsersWithIds(userIds);
  }

  public async getUserWithUsername(
    username: string,
  ): Promise<UserPersistedInterface> {
    const key = this._getWithUsernameKey(username);
    const cachedUser: UserPersistedInterface = this._nodeCache.get(key);
    if (cachedUser) {
      return cachedUser;
    }
    const user = await this.persistenceService.getUserWithUsername(username);

    if (user) {
      this._nodeCache.set(key, user, this.TTL);
    }

    return user;
  }

  public async getUserWithIdOrName(
    userId: string,
    username: string,
  ): Promise<UserPersistedInterface> {
    const userIdKey = this._getWithUserIdKey(userId);
    const usernameKey = this._getWithUsernameKey(username);

    let cachedUser: UserPersistedInterface = this._nodeCache.get(userIdKey);
    if (cachedUser) {
      return cachedUser;
    }

    cachedUser = this._nodeCache.get(usernameKey);
    if (cachedUser) {
      return cachedUser;
    }

    const user = this.persistenceService.getUserWithIdOrName(userId, username);

    if (user) {
      this._nodeCache.set(userIdKey, user);
      this._nodeCache.set(usernameKey, user);
    }

    return user;
  }

  public async createUser(
    context: Context,
    user: UserInterface,
  ): Promise<UserInterface & { usernameText: string }> {
    const userIdKey = this._getWithUserIdKey(user.id);
    const usernameKey = this._getWithUsernameKey(user.username);

    const persistedUser = await this.persistenceService.createUser(
      context,
      user,
    );

    this._nodeCache.set(userIdKey, user);
    this._nodeCache.set(usernameKey, user);

    /*
     If redis is not enabled and this is not a local setup then we need to wait for the persistence
     so that subsequent login requests will not fail on other workers in a distributed cluster.
     If redis is enabled then user will be cached in redis
     If this is a local setup the user will be cached on this server
     */
    if (
      !this.config.redis.enabled &&
      !['localhost', 'docker'].includes(this.config.environmentName)
    ) {
      let checkedUser: UserPersistedInterface;
      do {
        checkedUser = await this.persistenceService.getUserWithId(user.id);
        if (!checkedUser) {
          context.logger.verbose(`Waiting for user to be persisted`);
          await new Promise((resolve) => setTimeout(resolve, 100));
        }
      } while (!checkedUser);
    }

    return persistedUser;
  }

  public async updateTokenHashes(
    context: Context,
    userId: string,
    permissionsHash: string,
    expirationPolicyHash: string,
    extraHash: string,
  ): Promise<void> {
    return await this.persistenceService.updateTokenHashes(
      context,
      userId,
      permissionsHash,
      expirationPolicyHash,
      extraHash,
    );
  }

  public async getUsers(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    return await this.persistenceService.getUsers(
      context,
      page,
      pageSize,
      options,
    );
  }

  public async getUserCount(context: Context): Promise<number> {
    return await this.persistenceService.getUserCount(context);
  }

  public async getUsersForGroups(
    context: Context,
    groupIds: string[],
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    return await this.persistenceService.getUsersForGroups(
      context,
      groupIds,
      page,
      pageSize,
      options,
    );
  }

  public async saveUsersFromConfig(
    users: UserPersistedInterface[],
  ): Promise<UserPersistedInterface[]> {
    return await this.persistenceService.saveUsersFromConfig(users);
  }

  public async deleteUser(context: Context, userId: string): Promise<boolean> {
    const userIdKey = this._getWithUserIdKey(userId);

    const cachedUser: UserPersistedInterface = this._nodeCache.get(userIdKey);
    if (cachedUser) {
      const usernameKey = this._getWithUsernameKey(cachedUser.username);
      this._nodeCache.del(userIdKey);
      this._nodeCache.del(usernameKey);
    }

    return await this.persistenceService.deleteUser(context, userId);
  }

  public async deleteUsers(
    context: Context,
    userIds: string[],
  ): Promise<boolean> {
    return await this.persistenceService.deleteUsers(context, userIds);
  }

  public async updateUser(
    context: Context,
    userId: string,
    userUpdate: UserUpdateInterface,
  ): Promise<boolean> {
    return await this.persistenceService.updateUser(
      context,
      userId,
      userUpdate,
    );
  }

  public async patchUser(
    context: Context,
    userId: string,
    userPatch: UserPatchInterface,
  ): Promise<boolean> {
    return await this.persistenceService.patchUser(context, userId, userPatch);
  }

  public async addGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    return await this.persistenceService.addGroups(context, userId, groupIds);
  }

  public async removeGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    return await this.persistenceService.removeGroups(
      context,
      userId,
      groupIds,
    );
  }

  public async updateBlacklistedStatus(
    context: Context,
    userId: string,
    blacklisted: boolean,
  ): Promise<void> {
    return await this.persistenceService.updateBlacklistedStatus(
      context,
      userId,
      blacklisted,
    );
  }

  public async createUsers(
    context: Context,
    users: UserInterface[],
  ): Promise<void> {
    // Call the persistence service's createUsers method
    await this.persistenceService.createUsers(context, users);

    // Create persisted users with the usernameText field
    const persistedUsers = users.map((user) => ({
      ...user,
      usernameText: user.username,
    }));

    // Cache the persisted users
    persistedUsers.forEach((user) => {
      const userIdKey = this._getWithUserIdKey(user.id);
      const usernameKey = this._getWithUsernameKey(user.username);

      this._nodeCache.set(userIdKey, user);
      this._nodeCache.set(usernameKey, user);
    });

    // Perform the Redis and environment checks
    if (
      !this.config.redis.enabled &&
      !['localhost', 'docker'].includes(this.config.environmentName)
    ) {
      for (const user of users) {
        let checkedUser: UserPersistedInterface;
        do {
          checkedUser = await this.persistenceService.getUserWithId(user.id);
          if (!checkedUser) {
            context.logger.verbose(`Waiting for user to be persisted`);
            await new Promise((resolve) => setTimeout(resolve, 100));
          }
        } while (!checkedUser);
      }
    }
  }
}
