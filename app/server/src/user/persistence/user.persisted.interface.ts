import { UserInterface } from '../user.interface';

export interface UserPersistedInterface extends UserInterface {
  hash: {
    permissions: string;
    tokenExpirationPolicy: string;
    preConfiguredAuthentication?: string;
    extra?: string;
  };
}
