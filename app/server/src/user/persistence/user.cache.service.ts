import { Context, PaginatedResults } from '@cryptexlabs/codex-nodejs-common';
import { Inject, Injectable } from '@nestjs/common';
import { QueryOptionsInterface } from 'src/query.options.interface';
import { IdentityLinkInterface } from '../identity-link/identity-link.interface';
import { UserPatchInterface } from '../update/user.patch.interface';
import { UserUpdateInterface } from '../update/user.update.interface';
import { UserInterface } from '../user.interface';
import { UserDataServiceInterface } from './user.data.service.interface';
import { UserPersistedInterface } from './user.persisted.interface';
import { RedisClientType } from 'redis';
import { Config } from '../../config';

@Injectable()
export class UserCacheService implements UserDataServiceInterface {
  private readonly PREFIX_BY_ID = 'iam-zen:user:by-id';
  private readonly PREFIX_BY_USERNAME = 'iam-zen:user:by-username';
  constructor(
    @Inject('REDIS') private readonly redis: RedisClientType,
    @Inject('CONFIG') private readonly config: Config,
  ) {}

  public async getUserWithId(userId: string): Promise<UserPersistedInterface> {
    const json = await this.redis.get(`${this.PREFIX_BY_ID}:${userId}`);
    if (json) {
      return JSON.parse(json);
    }
    return null;
  }
  public async getUsersWithIds(
    userIds: string[],
  ): Promise<UserPersistedInterface[]> {
    throw new Error('Method not implemented.');
  }
  public async getUserWithUsername(
    username: string,
  ): Promise<UserPersistedInterface> {
    const json = await this.redis.get(`${this.PREFIX_BY_USERNAME}:${username}`);
    if (json) {
      return JSON.parse(json);
    }
    return null;
  }
  public async getUserWithIdOrName(
    userId: string,
    username: string,
  ): Promise<UserPersistedInterface> {
    const userById = await this.getUserWithId(userId);
    if (userById) {
      return userById;
    }

    return await this.getUserWithUsername(username);
  }
  public async createUser(
    context: Context,
    user: UserInterface,
  ): Promise<UserInterface & { usernameText: string }> {
    const finalUser = { ...user, usernameText: user.username };
    const finalUserJson = JSON.stringify(finalUser);
    await Promise.all([
      this.redis.set(`${this.PREFIX_BY_ID}:${user.id}`, finalUserJson, {
        EX: this.config.userCacheTtl,
      }),
      this.redis.set(
        `${this.PREFIX_BY_USERNAME}:${user.username}`,
        finalUserJson,
        { EX: this.config.userCacheTtl },
      ),
    ]);
    return finalUser;
  }
  public async updateTokenHashes(
    context: Context,
    userId: string,
    permissionsHash: string,
    expirationPolicyHash: string,
    extraHash: string,
  ): Promise<void> {
    throw new Error('Method not implemented.');
  }
  public async getUsers(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    throw new Error('Method not implemented.');
  }
  public async getUserCount(context: Context): Promise<number> {
    throw new Error('Method not implemented.');
  }
  public async getUsersForGroups(
    context: Context,
    groupIds: string[],
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    throw new Error('Method not implemented.');
  }
  public async saveUsersFromConfig(
    users: UserPersistedInterface[],
  ): Promise<UserPersistedInterface[]> {
    throw new Error('Method not implemented.');
  }

  public async deleteUser(context: Context, userId: string): Promise<boolean> {
    const user = await this.getUserWithId(userId);
    if (user) {
      await Promise.all([
        this.redis.del(`${this.PREFIX_BY_USERNAME}:${user.username}`),
        this.redis.del(`${this.PREFIX_BY_ID}:${userId}`),
      ]);
    } else {
      return false;
    }
  }
  public async deleteUsers(
    context: Context,
    userIds: string[],
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async updateUser(
    context: Context,
    userId: string,
    userUpdate: UserUpdateInterface,
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async patchUser(
    context: Context,
    userId: string,
    userPatch: UserPatchInterface,
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async addGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }
  public async removeGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    throw new Error('Method not implemented.');
  }

  public async updateBlacklistedStatus(
    context: Context,
    userId: string,
    blacklisted: boolean,
  ): Promise<void> {
    const user = await this.getUserWithId(userId);
    if (user) {
      const newUser = {
        ...user,
        blacklisted,
      };
      const newUserJson = JSON.stringify(newUser);
      await this.redis.set(`${this.PREFIX_BY_ID}:${userId}`, newUserJson, {
        EX: this.config.userCacheTtl,
      });
      await this.redis.set(
        `${this.PREFIX_BY_USERNAME}:${newUser.username}`,
        newUserJson,
        {
          EX: this.config.userCacheTtl,
        },
      );
    }
  }

  createUsers(context: Context, user: UserInterface[]): Promise<void> {
    throw new Error('Method not implemented.');
  }
}
