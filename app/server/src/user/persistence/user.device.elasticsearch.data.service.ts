import { Inject, Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import {
  CustomLogger,
  ElasticsearchInitializer,
} from '@cryptexlabs/codex-nodejs-common';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';
import { UserDeviceDataServiceInterface } from './user.device.data.service.interface';

@Injectable()
export class UserDeviceElasticsearchDataService
  implements UserDeviceDataServiceInterface
{
  private static get DEFAULT_INDEX() {
    return 'user_device';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
    elasticsearchPinger: ElasticsearchPinger,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      UserDeviceElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        userId: {
          type: 'keyword',
        },
        deviceId: {
          type: 'keyword',
        },
      },
      elasticsearchPinger,
    );
  }

  public async getUserIdWithDeviceId(deviceId: string): Promise<string | null> {
    const response = await this.elasticsearchService.search({
      index: UserDeviceElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  deviceId,
                },
              },
            ],
          },
        },
      },
    });

    if ((response.hits.total as any).value === 0) {
      return null;
    }

    const result = {
      ...(response.hits.hits[0] as any)._source,
    };
    return result.userId;
  }

  public async saveUserDevice(userId: string, deviceId: string): Promise<void> {
    await this.elasticsearchService.index({
      index: UserDeviceElasticsearchDataService.DEFAULT_INDEX,
      body: {
        userId: userId,
        deviceId: deviceId,
      },
    });
  }

  async initializeIndex() {
    const exists = await this.elasticsearchService.indices.exists({
      index: UserDeviceElasticsearchDataService.DEFAULT_INDEX,
    });

    if (!exists) {
      await this.elasticsearchService.indices.create({
        index: UserDeviceElasticsearchDataService.DEFAULT_INDEX,
        body: {
          settings: {
            analysis: {
              normalizer: {
                lowercase_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase'],
                },
              },
            },
          },
        },
      });
    }

    await this.elasticsearchInitializer.initializeIndex();
  }
}
