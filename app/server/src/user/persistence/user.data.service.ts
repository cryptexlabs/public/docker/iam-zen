import {
  Context,
  ContextBuilder,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { Inject, Injectable } from '@nestjs/common';
import { QueryOptionsInterface } from 'src/query.options.interface';
import { UserPatchInterface } from '../update/user.patch.interface';
import { UserUpdateInterface } from '../update/user.update.interface';
import { UserInterface } from '../user.interface';
import { UserDataServiceInterface } from './user.data.service.interface';
import { UserPersistedInterface } from './user.persisted.interface';
import { UserCacheService } from './user.cache.service';
import { Config } from '../../config';

@Injectable()
export class UserDataService implements UserDataServiceInterface {
  private _batchCreateUsers: UserInterface[] = [];

  constructor(
    @Inject('USER_PERSISTENCE_SERVICE')
    private readonly userPersistenceService: UserDataServiceInterface,
    private readonly userCacheService: UserCacheService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
    @Inject('CONFIG') private readonly config: Config,
  ) {
    this._startBatchProcessor().then();
  }

  private async _startBatchProcessor() {
    const context = this.contextBuilder.build().getResult();

    setInterval(async () => {
      try {
        if (this._batchCreateUsers.length > 0) {
          context.logger.debug(
            `Creating ${this._batchCreateUsers.length} users`,
            this._batchCreateUsers.map((a) => a.id),
          );
          const users = [...this._batchCreateUsers];
          this._batchCreateUsers = [];
          await this.userPersistenceService.createUsers(context, users);
        }
      } catch (e) {
        context.logger.error(`Failed to create users: ${e.message}`);
      }
    }, this.config.createUserInterval);
  }

  public async getUserWithId(userId: string): Promise<UserPersistedInterface> {
    const cachedUser = await this.userCacheService.getUserWithId(userId);
    if (cachedUser) {
      return cachedUser;
    }

    return await this.userPersistenceService.getUserWithId(userId);
  }
  public async getUsersWithIds(
    userIds: string[],
  ): Promise<UserPersistedInterface[]> {
    return this.userPersistenceService.getUsersWithIds(userIds);
  }
  public async getUserWithUsername(
    username: string,
  ): Promise<UserPersistedInterface> {
    const cachedUser = await this.userCacheService.getUserWithUsername(
      username,
    );
    if (cachedUser) {
      return cachedUser;
    }
    return this.userPersistenceService.getUserWithUsername(username);
  }
  public async getUserWithIdOrName(
    userId: string,
    username: string,
  ): Promise<UserPersistedInterface> {
    const cachedUser = await this.userCacheService.getUserWithIdOrName(
      userId,
      username,
    );
    if (cachedUser) {
      return cachedUser;
    }
    return this.userPersistenceService.getUserWithIdOrName(userId, username);
  }
  public async createUser(
    context: Context,
    user: UserInterface,
  ): Promise<UserInterface & { usernameText: string }> {
    this._batchCreateUsers.push(user);
    const finalUser = { ...user, usernameText: user.username };
    await this.userCacheService.createUser(context, finalUser);
    return finalUser;
  }
  public async updateTokenHashes(
    context: Context,
    userId: string,
    permissionsHash: string,
    expirationPolicyHash: string,
    extraHash: string,
  ): Promise<void> {
    return this.userPersistenceService.updateTokenHashes(
      context,
      userId,
      permissionsHash,
      expirationPolicyHash,
      extraHash,
    );
  }
  public async getUsers(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    return this.userPersistenceService.getUsers(
      context,
      page,
      pageSize,
      options,
    );
  }
  public async getUserCount(context: Context): Promise<number> {
    return this.userPersistenceService.getUserCount(context);
  }
  public async getUsersForGroups(
    context: Context,
    groupIds: string[],
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    return this.userPersistenceService.getUsersForGroups(
      context,
      groupIds,
      page,
      pageSize,
      options,
    );
  }
  public async saveUsersFromConfig(
    users: UserPersistedInterface[],
  ): Promise<UserPersistedInterface[]> {
    return this.userPersistenceService.saveUsersFromConfig(users);
  }

  public async deleteUser(context: Context, userId: string): Promise<boolean> {
    await this.userCacheService.deleteUser(context, userId);
    return await this.userPersistenceService.deleteUser(context, userId);
  }
  public async deleteUsers(
    context: Context,
    userIds: string[],
  ): Promise<boolean> {
    return this.userPersistenceService.deleteUsers(context, userIds);
  }
  public async updateUser(
    context: Context,
    userId: string,
    userUpdate: UserUpdateInterface,
  ): Promise<boolean> {
    return this.userPersistenceService.updateUser(context, userId, userUpdate);
  }
  public async patchUser(
    context: Context,
    userId: string,
    userPatch: UserPatchInterface,
  ): Promise<boolean> {
    return this.userPersistenceService.patchUser(context, userId, userPatch);
  }
  public async addGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    return this.userPersistenceService.addGroups(context, userId, groupIds);
  }
  public async removeGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    return this.userPersistenceService.removeGroups(context, userId, groupIds);
  }

  public async updateBlacklistedStatus(
    context: Context,
    userId: string,
    blacklisted: boolean,
  ): Promise<void> {
    const cachedUser = await this.userCacheService.getUserWithId(userId);
    if (!cachedUser) {
      const persistedUser = await this.userPersistenceService.getUserWithId(
        userId,
      );
      await this.userCacheService.createUser(context, persistedUser);
    }

    await Promise.all([
      this.userPersistenceService.updateBlacklistedStatus(
        context,
        userId,
        blacklisted,
      ),
      this.userCacheService.updateBlacklistedStatus(
        context,
        userId,
        blacklisted,
      ),
    ]);
  }

  public async createUsers(
    context: Context,
    user: UserInterface[],
  ): Promise<void> {
    throw new Error('Method not implemented.');
  }
}
