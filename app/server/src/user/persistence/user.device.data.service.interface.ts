export interface UserDeviceDataServiceInterface {
  getUserIdWithDeviceId(deviceId: string): Promise<string | null>;
  saveUserDevice(userId: string, deviceId: string): Promise<void>;
}
