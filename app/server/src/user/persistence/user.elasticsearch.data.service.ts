import { UserDataServiceInterface } from './user.data.service.interface';
import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
} from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import {
  Context,
  CustomLogger,
  ElasticsearchInitializer,
  PaginatedResults,
} from '@cryptexlabs/codex-nodejs-common';
import { UserPersistedInterface } from './user.persisted.interface';
import { IdentityLinkInterface } from '../identity-link/identity-link.interface';
import { UserPatchInterface } from '../update/user.patch.interface';
import { UserUpdateInterface } from '../update/user.update.interface';
import { QueryOptionsInterface } from '../../query.options.interface';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';
import { UserInterface } from '../user.interface';

@Injectable()
export class UserElasticsearchDataService implements UserDataServiceInterface {
  private static get DEFAULT_INDEX() {
    return 'user';
  }

  private readonly elasticsearchInitializer: ElasticsearchInitializer;

  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    private readonly elasticsearchService: ElasticsearchService,
    elasticsearchPinger: ElasticsearchPinger,
  ) {
    this.elasticsearchInitializer = new ElasticsearchInitializer(
      logger,
      UserElasticsearchDataService.DEFAULT_INDEX,
      elasticsearchService,
      {
        id: {
          type: 'keyword',
        },
        groups: {
          type: 'keyword',
        },
        username: {
          type: 'keyword',
          normalizer: 'lowercase_normalizer',
        },
        usernameText: {
          type: 'text',
        },
        blacklisted: {
          type: 'boolean',
        },
      },
      elasticsearchPinger,
    );
  }

  async initializeIndex() {
    const exists = await this.elasticsearchService.indices.exists({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
    });

    if (!exists) {
      await this.elasticsearchService.indices.create({
        index: UserElasticsearchDataService.DEFAULT_INDEX,
        body: {
          settings: {
            analysis: {
              normalizer: {
                lowercase_normalizer: {
                  type: 'custom',
                  char_filter: [],
                  filter: ['lowercase'],
                },
              },
            },
          },
        },
      });
    }

    await this.elasticsearchInitializer.initializeIndex();
  }

  public async getUserWithUsername(
    username: string,
  ): Promise<UserPersistedInterface | null> {
    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      routing: username.toLowerCase(),
      body: {
        query: {
          bool: {
            filter: [
              {
                term: {
                  username,
                },
              },
            ],
          },
        },
      },
    });

    if ((response.hits.total as any).value === 0) {
      return null;
    }

    const result = {
      ...(response.hits.hits[0] as any)._source,
    };
    delete result.usernameText;
    return result;
  }

  public async getUserWithId(userId: string): Promise<UserPersistedInterface> {
    if (!userId) {
      throw new BadRequestException(`userId is null`);
    }
    try {
      const response = await this.elasticsearchService.search({
        index: UserElasticsearchDataService.DEFAULT_INDEX,
        body: {
          query: {
            term: {
              id: userId,
            },
          },
        },
      });

      // Check if any documents were found
      if ((response.hits.total as any).value > 0) {
        const result = response.hits.hits[0]._source as any;

        // Removing unwanted fields if necessary
        delete result?.usernameText;

        return {
          ...result,
          blacklisted: !!result?.blacklisted,
          groups: result?.groups || [],
          identityLinks: result?.identityLinks || [],
        } as UserPersistedInterface;
      }

      return null;
    } catch (e) {
      // Handle document not found error (404)
      if (e.meta && e.meta.statusCode === 404) {
        return null; // Document not found
      }

      throw e;
    }
  }

  public async getUserWithIdOrName(
    userId: string,
    username: string,
  ): Promise<UserPersistedInterface | null> {
    const [userWithId, userWithName] = await Promise.all([
      this.getUserWithId(userId),
      this.getUserWithUsername(username),
    ]);

    if (userWithId) {
      return userWithId;
    }
    if (userWithName) {
      return userWithName;
    }
    return null;
  }

  public async createUser(
    context: Context,
    user: UserPersistedInterface,
  ): Promise<UserInterface & { usernameText: string }> {
    const finalUser = {
      ...user,
      usernameText: user.username,
    };
    await this.elasticsearchService.index({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      id: user.id,
      routing: user.username.toLowerCase(),
      body: finalUser,
    });

    return finalUser;
  }

  public async updateTokenHashes(
    context: Context,
    userId: string,
    permissionsHash: string,
    expirationPolicyHash: string,
    extraHash: string,
  ): Promise<void> {
    await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: 'ctx._source.hash = params.hash',
          params: {
            hash: {
              permissions: permissionsHash,
              tokenExpirationPolicy: expirationPolicyHash,
              extra: extraHash,
            },
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
  }

  public async getUsers(
    context: Context,
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    let query: { query?: any; sort?: any } = {};

    if (!options?.search && options?.searchName) {
      query = {
        query: {
          wildcard: {
            username: {
              value: `*${options?.searchName}*`,
              boost: 1.0,
              rewrite: 'constant_score',
            },
          },
        },
      };
    }

    if (options?.search) {
      query = {
        query: {
          query_string: {
            query: `*${options?.search}*`,
            fields: ['usernameText', 'username', 'id'],
          },
        },
      };
    }

    const sortMap = {
      username: [{ username: options?.orderDirection || 'asc' }, '_score'],
      groupCount: {
        _script: {
          type: 'number',
          script: {
            lang: 'painless',
            source: 'doc.groups.size()',
          },
          order: options?.orderDirection || 'asc',
        },
      },
    };

    if (options?.orderBy) {
      const sort = sortMap[options?.orderBy];
      query = {
        ...query,
        sort,
      };
    }

    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: {
        from: (page - 1) * pageSize,
        size: pageSize,
        ...query,
      },
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        const user = {
          ...message._source,
        };
        delete user.usernameText;
        return user;
      }),
    };
  }

  public async getUsersForGroups(
    context: Context,
    groupIds: string[],
    page: number,
    pageSize: number,
    options?: QueryOptionsInterface,
  ): Promise<PaginatedResults<UserPersistedInterface>> {
    const baseQuery: { query?: any; sort?: any } = {};

    // Handling the search options
    if (options?.search) {
      baseQuery.query = {
        query_string: {
          query: `*${options.search}*`,
          fields: ['usernameText', 'id'],
        },
      };
    } else if (options?.searchName) {
      baseQuery.query = {
        wildcard: {
          username: {
            value: `*${options.searchName}*`,
            boost: 1.0,
            rewrite: 'constant_score',
          },
        },
      };
    }

    const sortMap = {
      username: [{ username: options?.orderDirection || 'asc' }, '_score'],
      groupCount: {
        _script: {
          type: 'number',
          script: {
            lang: 'painless',
            source: 'doc.groups.size()',
          },
          order: options?.orderDirection || 'asc',
        },
      },
    };

    // Adding filter on groupIds and ensuring 'must' condition is added under 'bool'
    const query = {
      bool: {
        must: [
          ...((baseQuery.query && [baseQuery.query]) || []),
          {
            terms: {
              groups: groupIds,
            },
          },
        ],
      },
    };

    // Handling sorting options
    if (options?.orderBy) {
      const sort = sortMap[options.orderBy];
      baseQuery.sort = sort;
    }

    // Constructing the final query body
    const queryBody = {
      from: (page - 1) * pageSize,
      size: pageSize,
      ...baseQuery,
      query, // Ensuring baseQuery and the new query bool are combined
    };

    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      track_total_hits: true,
      body: queryBody,
    });

    return {
      total: (response.hits.total as any).value as number,
      results: response.hits.hits.map((message: { _source?: any }) => {
        const user = {
          ...message._source,
        };
        delete user.usernameText;
        return user;
      }),
    };
  }

  public async saveUsersFromConfig(
    users: UserPersistedInterface[],
  ): Promise<UserPersistedInterface[]> {
    if (users.length === 0) {
      return [];
    }

    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: users.map((user) => user.id),
                },
              },
            ],
          },
        },
      },
    });

    const existingUsers = response.hits.hits;

    const existingUserIDs = existingUsers.map(
      (existingUser) => (existingUser._source as any).id,
    );

    const newUsers = users.filter((user) => !existingUserIDs.includes(user.id));
    const newUsersBulk = [];
    for (const newUser of newUsers) {
      newUsersBulk.push({
        index: {
          _index: UserElasticsearchDataService.DEFAULT_INDEX,
          routing: newUser.username.toLowerCase(),
          _id: newUser.id,
        },
      });
      newUsersBulk.push({
        ...newUser,
        identityLinks: newUser.identityLinks.map((link) => ({
          ...link,
          linkId: link.linkId || (link.data as any)?.authenticationId, // Set linkId to authenticationId if not already set
        })),
        usernameText: newUser.username,
      });
    }

    const existingUsersBulk = [];
    for (const existingUserDoc of existingUsers) {
      const updatedUser = users.find(
        (user) => user.id === (existingUserDoc._source as any).id,
      );

      existingUsersBulk.push({
        index: {
          _index: UserElasticsearchDataService.DEFAULT_INDEX,
          routing: (existingUserDoc._source as any).username.toLowerCase(),
          _id: existingUserDoc._id,
        },
      });

      existingUsersBulk.push({
        ...(existingUserDoc._source as any),
        username: updatedUser.username,
        usernameText: updatedUser.username,
        groups: updatedUser.groups,
        hash: {
          ...(existingUserDoc._source as any).hash,
          preConfiguredAuthentication:
            updatedUser.hash.preConfiguredAuthentication,
        },
      });
    }

    const bulk = [...newUsersBulk, ...existingUsersBulk];

    await this.elasticsearchService.bulk({ body: bulk });

    return newUsers;
  }

  public async getUsersWithIds(
    userIds: string[],
  ): Promise<UserPersistedInterface[]> {
    const response = await this.elasticsearchService.search({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: userIds,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map((message: { _source?: any }) => {
      const user = {
        ...message._source,
      };
      delete user.usernameText;
      return user;
    });
  }

  public async deleteUser(context: Context, userId: string): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      conflicts: 'proceed',
      body: {
        query: {
          bool: {
            filter: [
              {
                term: {
                  id: userId,
                },
              },
            ],
          },
        },
      },
    });
    return response.total === 1;
  }

  public async deleteUsers(
    context: Context,
    userIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                terms: {
                  id: userIds,
                },
              },
            ],
          },
        },
      },
    });
    return response.total > 0;
  }

  public async patchUser(
    context: Context,
    userId: string,
    userPatch: UserPatchInterface,
  ): Promise<boolean> {
    await this._validateUsernameNotTaken(context, userId, userPatch.username);
    const response = await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             if (params.username != null) {
               ctx._source.username = params.username
             }
            `,
          params: {
            username: userPatch.username,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async updateUser(
    context: Context,
    userId: string,
    userUpdate: UserUpdateInterface,
  ): Promise<boolean> {
    await this._validateUsernameNotTaken(context, userId, userUpdate.username);
    const response = await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
             ctx._source.username = params.username;
             ctx._source.groups = params.groups;
            `,
          params: {
            username: userUpdate.username,
            groups: userUpdate.groups,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
    return response.total === 1;
  }

  private async _validateUsernameNotTaken(
    context: Context,
    userId: string,
    username: string,
  ) {
    if (username) {
      const existingUser = await this.getUserWithUsername(username);
      if (existingUser && existingUser.id !== userId) {
        throw new HttpException(
          `This username is in use by another user`,
          HttpStatus.CONFLICT,
        );
      }
    }
  }

  public async addGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
           if (!(ctx._source.groups instanceof List)) {
             ctx._source.groups = new ArrayList();
           }
           ctx._source.groups.addAll(params.groups);
           ctx._source.groups = ctx._source.groups.stream().distinct().sorted().collect(Collectors.toList());
          `,
          params: {
            groups: groupIds,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
    return response.total === 1;
  }

  public async removeGroups(
    context: Context,
    userId: string,
    groupIds: string[],
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      body: {
        script: {
          lang: 'painless',
          source: `
            if (!(ctx._source.groups instanceof List)) {
              ctx._source.groups = new ArrayList();
            }
            ctx._source.groups.removeAll(params.groups);
          `,
          params: {
            groups: groupIds,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });

    return response.total === 1;
  }

  public async getUserCount(context: Context): Promise<number> {
    const { count } = await this.elasticsearchService.count({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
    });

    return count;
  }

  public async updateBlacklistedStatus(
    context: Context,
    userId: string,
    blacklisted: boolean,
  ): Promise<void> {
    await this.elasticsearchService.updateByQuery({
      index: UserElasticsearchDataService.DEFAULT_INDEX,
      conflicts: 'proceed',
      body: {
        script: {
          lang: 'painless',
          source: 'ctx._source.blacklisted = params.blacklisted',
          params: {
            blacklisted,
          },
        },
        query: {
          match: {
            id: userId,
          },
        },
      },
    });
  }

  public async createUsers(
    context: Context,
    users: UserInterface[],
  ): Promise<void> {
    const bulkOperations = [];

    users.forEach((user) => {
      const finalUser = {
        ...user,
        usernameText: user.username,
      };

      bulkOperations.push({
        index: {
          _index: UserElasticsearchDataService.DEFAULT_INDEX,
          routing: user.username.toLowerCase(),
          _id: user.id,
        },
      });
      bulkOperations.push(finalUser);
    });

    if (bulkOperations.length > 0) {
      await this.elasticsearchService.bulk({
        body: bulkOperations,
        refresh: 'wait_for',
      });
    }
  }
}
