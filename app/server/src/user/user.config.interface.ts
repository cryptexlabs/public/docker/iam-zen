import { UserCreateAuthenticationType } from './create/user.create.interface';

export interface UserConfigInterface {
  username: string;
  id: string;
  groups: string[];
  authentication: UserCreateAuthenticationType;
}
