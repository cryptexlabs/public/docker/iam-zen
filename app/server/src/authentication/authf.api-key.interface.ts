import { ApiKeyInterface } from '../api-key/api-key.interface';
import { ApiKeyTypeEnum } from '../api-key/api-key.type.enum';

export interface AuthfApiKeyInterface extends ApiKeyInterface {
  type: ApiKeyTypeEnum.AUTHF;
  data: {
    authenticationId: string;
  };
}
