import { UserCreateInterface } from '../user/create/user.create.interface';
import { IdentityLinkInterface } from '../user/identity-link/identity-link.interface';
import { UserInterface } from '../user/user.interface';
import {
  ApiAuthAuthenticateInterface,
  ApiAuthAuthenticationProviderInterface,
  AuthenticateStatelessResponseDataInterface,
  AuthenticateTypes,
  AuthenticationInterface,
  AuthenticationTypes,
  CreateTokenResponseDataInterface,
  RefreshAuthenticateInterface,
  StatelessAuthenticateTypes,
  TokenPairExpirationPolicyInterface,
} from '@cryptexlabs/authf-data-model';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { PermissionInterface } from '../permission/permission.interface';
import { AppInterface } from '../app/app.interface';
import { ApiKeyInterface } from '../api-key/api-key.interface';
import { CreateAppInterface } from '../app/create-app.interface';
import { ApiKeyTypeEnum } from '../api-key/api-key.type.enum';
import { IdentityLinkTypeEnum } from '../user/identity-link/identity-link-type.enum';
import { NewApiKeyInterface } from '../api-key/new-api-key.interface';

export interface AuthenticationServiceInterface {
  getToken(
    context: Context,
    payload: AuthenticationInterface,
    userLogin: AuthenticateTypes | null,
    authenticationId: string,
  ): Promise<CreateTokenResponseDataInterface>;

  createUser(
    context: Context,
    userId: string,
    identityLink: IdentityLinkInterface,
    user: UserCreateInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<void>;

  authenticationExists(
    context: Context,
    authenticationId: string,
  ): Promise<boolean>;

  updateUserAuthentication(
    context: Context,
    userLogin: AuthenticateTypes | null,
    identityLink: IdentityLinkInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    userId: string,
    authentication?: AuthenticationTypes,
  ): Promise<void | CreateTokenResponseDataInterface>;

  updateBlacklistStatus(
    context: Context,
    blacklisted: boolean,
    identityLink: IdentityLinkInterface,
  ): Promise<void>;

  createApp(
    context: Context,
    apiKeys: ApiKeyInterface[],
    id: string,
    app: CreateAppInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<void>;

  loginUser(
    context: Context,
    user: UserInterface,
    loginIdentityLink: IdentityLinkInterface,
    userLogin: AuthenticateTypes,
  ): Promise<any>;

  loginApp(
    context: Context,
    app: AppInterface,
    loginApiKey: ApiKeyInterface,
    appLogin: ApiAuthAuthenticateInterface | RefreshAuthenticateInterface,
  ): Promise<any>;

  /**
   * May be unimplemented depending on whether the authentication service supports ReAuth
   *
   * @param {Context} context
   * @param {string} token
   * @param {IdentityLinkInterface} identityLink
   */
  validateReAuthAuthorizeToken(
    context: Context,
    token: string,
    identityLink: IdentityLinkInterface,
  ): Promise<void>;

  validateAuthentication(
    context: Context,
    authenticate: StatelessAuthenticateTypes,
  ): Promise<AuthenticateStatelessResponseDataInterface>;

  updateTokenPayload(
    context: Context,
    userLogin: AuthenticateTypes | null,
    userId: string,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    authenticationLinks: any[],
    getLogin: boolean,
  ): Promise<void | CreateTokenResponseDataInterface>;

  getApiKeyType(): ApiKeyTypeEnum;

  getIdentityLinkType(): IdentityLinkTypeEnum;

  getIdentityLinkId(identityLink: IdentityLinkInterface): string;

  getApiKeysFromApiAuthenticationConfig(
    authenticationProviders: ApiAuthAuthenticationProviderInterface[],
  ): ApiKeyInterface[];

  updateApiKeyAuthentications(
    context: Context,
    existingApiKeys: ApiKeyInterface[],
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    appId: string,
    authentications: ApiAuthAuthenticationProviderInterface[],
  ): Promise<void>;

  getApiKeyForRefreshTokenSub(
    apiKeys: ApiKeyInterface[],
    sub: string,
  ): ApiKeyInterface;

  getIdentityLinkForRefreshTokenSub(
    identityLinks: IdentityLinkInterface[],
    sub: string,
  ): IdentityLinkInterface;

  loginUserWithRefreshToken(
    context: Context,
    authenticate: RefreshAuthenticateInterface,
  ): Promise<any>;

  createApiKeyForApp(
    context: Context,
    app: AppInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<NewApiKeyInterface>;

  createApiKeys(
    context: Context,
    apiKeys: ApiKeyInterface[],
    subject: string,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<NewApiKeyInterface>;

  deleteApiKeys(context: Context, apiKeys: ApiKeyInterface[]): Promise<void>;

  deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<void>;
}
