import { AuthfService } from './authf.service';
import { AuthenticationServiceInterface } from './authentication.service.interface';
import { IdentityLinkTypeEnum } from '../user/identity-link/identity-link-type.enum';
import { Injectable } from '@nestjs/common';

@Injectable()
export class AuthenticationServiceFactory {
  constructor(private readonly authfService: AuthfService) {}

  public getAuthenticationServiceForLoginIdentityLinkType(
    type: IdentityLinkTypeEnum,
  ): AuthenticationServiceInterface {
    if (type === IdentityLinkTypeEnum.AUTHF) {
      return this.authfService;
    }

    throw new Error(`Unsupported login identity link type: ${type}`);
  }
}
