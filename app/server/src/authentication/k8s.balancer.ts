import { Injectable } from '@nestjs/common';
import * as k8s from '@kubernetes/client-node';
import { CustomLogger } from '@cryptexlabs/codex-nodejs-common';

const kc = new k8s.KubeConfig();
kc.loadFromDefault();
const k8sApi = kc.makeApiClient(k8s.CoreV1Api);
const metricsClient = new k8s.Metrics(kc);

@Injectable()
export class K8sBalancer {
  private readonly _serviceName: string;
  private readonly _namespace: string;
  private _currentPod: string;
  private _nextEndpoint: string;
  private readonly REBALANCE_THRESHOLD = process.env.REBALANCE_THRESHOLD
    ? parseInt(process.env.REBALANCE_THRESHOLD, 10)
    : 0.25;
  private readonly INITIAL_MILLICORE_THRESHOLD = 20;

  constructor(
    private readonly endpoint: string,
    private readonly logger: CustomLogger,
    private readonly onRebalance: () => void,
  ) {
    this._nextEndpoint = endpoint;
    // if (endpoint?.includes('svc.cluster.local')) {
    //   const url = new URL(endpoint);
    //   const hostnameParts = url.hostname.split('.');
    //   this._serviceName = hostnameParts[0];
    //   this._namespace = hostnameParts[1];
    //   this._startWatcher().then();
    // }
  }

  private async _startWatcher() {
    setInterval(async () => {
      await this.updateNextEndpoint();
    }, (30 + (Math.floor(Math.random() * (90 - 30 + 1)) + 30)) * 1000);
  }

  public async updateNextEndpoint(): Promise<void> {
    if (!this._serviceName) {
      return;
    }
    try {
      const namespacedServicesResponse = await k8sApi.listNamespacedService(
        this._namespace,
      );

      const services = namespacedServicesResponse.body.items;

      const service = services.find(
        (svc) => svc.metadata.name === this._serviceName,
      );

      if (!service) {
        throw new Error(
          `Could not find service ${this._serviceName} in namespace ${this._namespace}`,
        );
      }
      const namespacedPodsResponse = await k8sApi.listNamespacedPod(
        this._namespace,
      );

      // Filter to include only Pods that are Ready
      const readyPods = namespacedPodsResponse.body.items.filter((pod) => {
        const isPodReadyCondition = pod.status.conditions.find(
          (condition) => condition.type === 'Ready',
        );
        if (isPodReadyCondition.status !== 'True') {
          this.logger.verbose(`Pod: ${pod.metadata.name} is not ready`);
        }
        return isPodReadyCondition && isPodReadyCondition.status === 'True';
      });
      const readyPodNames = readyPods.map((pod) => pod.metadata.name);
      this.logger.verbose(`Found ${readyPods.length} ready pods`);

      const labelSelectors = Object.keys(service.spec.selector)
        .map((key) => `${key}=${service.spec.selector[key]}`)
        .join(',');

      const podMetricsResponse = await metricsClient.getPodMetrics({
        labelSelector: labelSelectors,
      });

      const podMetrics = [...podMetricsResponse.items];
      podMetrics.sort((a, b) => {
        const cpuUsageA = Number(a.containers[0].usage.cpu.replace('n', ''));
        const cpuUsageB = Number(b.containers[0].usage.cpu.replace('n', ''));

        return cpuUsageA - cpuUsageB;
      });

      const readyPodMetrics = podMetrics.filter((podMetric) =>
        readyPods.some(
          (readyPod) =>
            readyPod.metadata.name === podMetric.metadata.name &&
            readyPod.metadata.namespace === podMetric.metadata.namespace,
        ),
      );
      this.logger.verbose(`Found ${readyPodMetrics.length} ready pod metrics`);

      const lowestPodMetric =
        readyPodMetrics.length > 0 ? readyPodMetrics[0] : null;
      const highestPodMetric =
        readyPodMetrics.length > 0
          ? readyPodMetrics[readyPodMetrics.length - 1]
          : null;
      let lowestPodCpu = 0;
      let highestPodCpu = 0;
      if (lowestPodMetric) {
        lowestPodCpu = Number(
          lowestPodMetric.containers[0].usage.cpu.replace('n', ''),
        );
        this.logger.verbose(
          `Lowest pod metric is ${lowestPodMetric.metadata.name} ${
            lowestPodCpu / 1000000
          }m`,
        );
      }
      if (highestPodMetric) {
        highestPodCpu = Number(
          highestPodMetric.containers[0].usage.cpu.replace('n', ''),
        );
        this.logger.verbose(
          `Highest pod metric is ${lowestPodMetric.metadata.name} ${
            highestPodCpu / 1000000
          }m`,
        );
      }
      const currentPodMetric = readyPodMetrics.find(
        (pod) => pod.metadata.name === this._currentPod,
      );
      if (currentPodMetric) {
        this.logger.verbose(
          `Current pod metric is ${currentPodMetric.metadata.name}`,
        );
      }
      const spread = highestPodCpu - lowestPodCpu;
      this.logger.verbose(`Spread is ${spread / 1000000}m`);
      const lowerQuartile = lowestPodCpu + spread * 0.25;
      this.logger.verbose(`Lower quartile is ${lowerQuartile / 1000000}m`);

      const lowerPodMetrics = readyPodMetrics.filter(
        (pod) =>
          Number(pod.containers[0].usage.cpu.replace('n', '')) < lowerQuartile,
      );
      this.logger.verbose(
        `Found ${lowerPodMetrics.length} pods in lower quartile`,
      );

      let nextPodMetric =
        lowerPodMetrics.length > 0
          ? lowerPodMetrics[Math.floor(Math.random() * lowerPodMetrics.length)]
          : null;
      this.logger.verbose(
        `Randomly selected ${nextPodMetric?.metadata.name} from lower quartile`,
      );

      // initial load balancing
      if (!this._currentPod) {
        this.logger.verbose(`Initial load balancing started`);
        const lowUsagePods = readyPodMetrics.filter((pod) => {
          const milliCores =
            Number(pod.containers[0].usage.cpu.replace('n', '')) / 1000000;
          return milliCores < this.INITIAL_MILLICORE_THRESHOLD;
        });
        this.logger.verbose(`Found ${lowUsagePods.length} low usage pods`);
        if (lowUsagePods.length > 0) {
          // Select random one from list of low usage pods
          nextPodMetric =
            lowUsagePods[Math.floor(Math.random() * lowUsagePods.length)];
          this.logger.verbose(
            `Randomly selected ${nextPodMetric.metadata.name} as next pod`,
          );
        }
      } else if (this._currentPod && !currentPodMetric) {
        this.logger.verbose(
          `Current pod ${this._currentPod} no longer exists. Re-balancing`,
        );
        this._currentPod = null;
      } else {
        this.logger.verbose(`Already completed initial load balancing`);
      }

      if (nextPodMetric) {
        if (currentPodMetric) {
          this.logger.verbose(
            `Comparing metrics of current pod with lowest cpu pod`,
          );
          const currentPodCpu = Number(
            currentPodMetric.containers[0].usage.cpu.replace('n', ''),
          );
          this.logger.verbose(
            `Current pod ${currentPodMetric.metadata.name} cpu is ${
              currentPodCpu / 1000000
            }m`,
          );
          const lowestPodCpu = Number(
            nextPodMetric.containers[0].usage.cpu.replace('n', ''),
          );
          this.logger.verbose(
            `Lowest pod ${lowestPodMetric.metadata.name} cpu is ${
              lowestPodCpu / 1000000
            }m`,
          );
          if (lowestPodCpu >= currentPodCpu * (1 - this.REBALANCE_THRESHOLD)) {
            this.logger.verbose(
              `Current pod ${nextPodMetric.metadata.name} is not lower than 25% of current pod ${currentPodMetric.metadata.name} cpu. Skipping update`,
            );
            // The next pod cpu is less than 25% lower than the current pod cpu. No need to switch
            return;
          } else if (currentPodCpu / 1000000 < this.REBALANCE_THRESHOLD) {
            this.logger.verbose(
              `Current pod ${nextPodMetric.metadata.name} cpu ${
                currentPodCpu / 1000000
              }m is already less than ${
                this.REBALANCE_THRESHOLD
              }m. Skipping update`,
            );
            return;
          } else {
            this.logger.verbose(
              `Lowest pod is more than 25% lower than current pod. Re-balancing`,
            );
          }
        } else {
          this.logger.verbose(`No current pod. Skipping comparison`);
        }

        const fqdnPartsWithScheme = this.endpoint.split('://');
        const fqdnParts = fqdnPartsWithScheme[1].split('.');
        const nextPod = namespacedPodsResponse.body.items.find(
          (pod) => pod.metadata.name === nextPodMetric.metadata.name,
        );
        const podRawIp = nextPod.status.podIP;
        const podDnsIp = podRawIp.replace(/\./g, '-');
        this.onRebalance();
        this._nextEndpoint = `${
          fqdnPartsWithScheme[0]
        }://${podDnsIp}.${fqdnParts.join('.')}`;
        this._currentPod = nextPodMetric.metadata.name;
        this.logger.verbose(
          `Next k8s load balanced endpoint is ${this._nextEndpoint}`,
        );
      } else {
        this.logger.verbose(`Next pod is not set`);
      }
    } catch (e) {
      this.logger.error(
        `${e.message}: ${
          e.response?.body ? JSON.stringify(e.response.body) : ''
        }`,
      );
    }
  }

  public getNextEndpoint(): string {
    return this._nextEndpoint;
  }
}
