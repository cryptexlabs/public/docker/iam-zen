import {
  BadRequestException,
  HttpException,
  HttpStatus,
  Inject,
  Injectable,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';
import { UserCreateInterface } from '../user/create/user.create.interface';
import { AuthenticationServiceInterface } from './authentication.service.interface';
import { Config } from '../config';
import { AuthfIdentityLinkInterface } from './authf.identity-link.interface';
import {
  ApiAuthAuthenticateInterface,
  ApiAuthAuthenticationProviderInterface,
  AuthenticateStatelessResponseDataInterface,
  AuthenticateTypeEnum,
  AuthenticateTypes,
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTokenDataInterface,
  AuthenticationTypes,
  CognitoExtraInputInterface,
  CreateTokenResponseDataInterface,
  CreateTokenWithAuthorizedAuthenticationInterface,
  ExtraTypeEnum,
  RefreshAuthenticateInterface,
  StatelessAuthenticateTypes,
  TokenPairExpirationPolicyInterface,
} from '@cryptexlabs/authf-data-model';
import { UserInterface } from '../user/user.interface';
import {
  Context,
  ContextBuilder,
  CustomLogger,
  StringUtil,
} from '@cryptexlabs/codex-nodejs-common';
import * as jwt from 'jsonwebtoken';
import { PermissionInterface } from '../permission/permission.interface';
import { AppInterface } from '../app/app.interface';
import { AuthfApiKeyInterface } from './authf.api-key.interface';
import { ApiKeyTypeEnum } from '../api-key/api-key.type.enum';
import { IdentityLinkTypeEnum } from '../user/identity-link/identity-link-type.enum';
import { CreateAppInterface } from '../app/create-app.interface';
import { v4 as uuidv4 } from 'uuid';
import { ApiKeyInterface } from '../api-key/api-key.interface';
import { IdentityLinkInterface } from '../user/identity-link/identity-link.interface';
import { ApiKeyUtil } from '../api-key/api-key.util';
import { NewApiKeyInterface } from '../api-key/new-api-key.interface';
import { IdentityLinkUsageEnum } from '../user/identity-link/identity-link-usage.enum';
import { ApiAuthAuthenticationProviderConfigInterface } from '../app/app.config.interface';
import * as http2 from 'http2-wrapper';
import { AuthfAuthenticationService } from './authf/authentication/authf.authentication.service';

@Injectable()
export class AuthfService implements AuthenticationServiceInterface {
  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    @Inject('CONFIG') private readonly config: Config,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
    private readonly authfAuthenticationService: AuthfAuthenticationService,
  ) {}

  public async authenticationExists(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    return this.authfAuthenticationService.authenticationExists(
      context,
      authenticationId,
    );
  }

  public async validateAuthentication(
    context: Context,
    authenticate: StatelessAuthenticateTypes,
  ): Promise<AuthenticateStatelessResponseDataInterface> {
    context.logger.debug(`Validating authentication`);

    const emptyIshStrings = ['null', 'undefined'];

    if (authenticate.type === AuthenticateTypeEnum.SAMSUNG_ANY) {
      if (!authenticate.data.userId) {
        throw new BadRequestException(`Missing property: data.userId`);
      }

      if (!authenticate.data.clientId) {
        throw new BadRequestException(`Missing property: data.clientId`);
      }

      if (!authenticate.data.accessToken) {
        throw new BadRequestException(`Missing property: data.accessToken`);
      }

      if (emptyIshStrings.includes(authenticate.data.userId)) {
        throw new BadRequestException(
          `Invalid property: data.userId="${authenticate.data.userId}"`,
        );
      }

      if (emptyIshStrings.includes(authenticate.data.clientId)) {
        throw new BadRequestException(
          `Invalid property: data.clientId="${authenticate.data.clientId}"`,
        );
      }

      if (emptyIshStrings.includes(authenticate.data.accessToken)) {
        throw new BadRequestException(
          `Invalid property: data.accessToken="${authenticate.data.accessToken}"`,
        );
      }

      if (StringUtil.isValidUUIDv4(authenticate.data.userId)) {
        throw new BadRequestException(
          `Invalid property: data.userId is a v4 uuid but should be the samsung user ID`,
        );
      }
    }

    return await this.authfAuthenticationService.createStatelessVerification(
      context,
      authenticate,
    );
  }

  public async createUser(
    context: Context,
    userId: string,
    identityLink: AuthfIdentityLinkInterface,
    user: UserCreateInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ) {
    const providers = [];
    if (user.authentication) {
      if (user.authentication.type === AuthenticationProviderTypeEnum.BASIC) {
        providers.push({
          type: user.authentication.type,
          data: {
            ...user.authentication.data,
            username: user.authentication.data.username.toLowerCase(),
          },
        });
      } else {
        providers.push({
          type: user.authentication.type,
          data: user.authentication.data,
        });
      }
    }

    const payload: AuthenticationInterface = {
      providers,
      token: {
        expirationPolicy: tokenExpirationPolicy,
        subject: userId,
        body: {
          scopes: permissions.map((permission) => permission.value),
        },
      },
    };

    const finalPayload = this._getExtra(payload);

    await this._createAuthentication(
      context,
      finalPayload,
      identityLink.data.authenticationId,
    );
  }

  private _getExtra(payload) {
    const extra = {};

    if (this.config.cognitoPoolId) {
      // noinspection UnnecessaryLocalVariableJS
      const cognitoExtra: CognitoExtraInputInterface = {
        developerProviderName: this.config.cognitoDeveloperProviderName,
        poolId: this.config.cognitoPoolId,
        region: this.config.cognitoPoolRegion,
        label: '',
      };
      extra[ExtraTypeEnum.COGNITO] = cognitoExtra;
    }

    const finalExtra = Object.keys(extra).length > 0 ? extra : undefined;

    return {
      ...payload,
      extra: finalExtra,
    };
  }

  public async updateUserAuthentication(
    context: Context,
    userLogin: AuthenticateTypes | null,
    identityLink: AuthfIdentityLinkInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    userId: string,
    authentication?: AuthenticationTypes,
  ): Promise<void | CreateTokenResponseDataInterface> {
    const providers = [];
    if (authentication) {
      if (authentication.type === AuthenticationProviderTypeEnum.BASIC) {
        providers.push({
          ...authentication,
          data: {
            ...authentication.data,
            username: authentication.data.username.toLowerCase(),
          },
        });
      } else {
        providers.push(authentication);
      }
    }

    const payload: AuthenticationInterface = {
      providers,
      token: {
        expirationPolicy: tokenExpirationPolicy,
        subject: userId,
        body: {
          scopes: permissions.map((permission) => permission.value),
        },
      },
    };

    const finalPayload = this._getExtra(payload);

    await this._patchAuthentication(
      context,
      finalPayload,
      identityLink.data.authenticationId,
    );

    return this.getToken(
      context,
      payload,
      userLogin,
      identityLink.data.authenticationId,
    );
  }

  public async getToken(
    context: Context,
    payload: AuthenticationInterface,
    userLogin: AuthenticateTypes | null,
    authenticationId: string,
  ): Promise<CreateTokenResponseDataInterface> {
    const finalAuthentication = this._getExtra(payload);
    const createTokenPayload: CreateTokenWithAuthorizedAuthenticationInterface =
      {
        authentication: finalAuthentication,
        authenticate: userLogin,
      };

    if (userLogin) {
      return await this.authfAuthenticationService.createTokenPairForUnverified(
        context,
        authenticationId,
        userLogin,
      );
    } else {
      return await this.authfAuthenticationService.createTokenPairForVerifiedWithAuthorizedAuthenticationData(
        context,
        authenticationId,
        createTokenPayload.authentication,
      );
    }
  }

  public async createApp(
    context: Context,
    apiKeys: AuthfApiKeyInterface[],
    id: string,
    app: CreateAppInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ) {
    if (app.authentications.length > 0) {
      const payload: AuthenticationInterface = {
        providers: app.authentications.map((p) => ({
          type: p.type,
          data: p.data,
        })),
        token: {
          expirationPolicy: tokenExpirationPolicy,
          subject: id,
          body: {
            scopes: permissions.map((permission) => permission.value),
          },
        },
      };

      await this._patchAuthentication(
        context,
        payload,
        apiKeys[0].data.authenticationId,
      );
    }
  }

  public async loginUser(
    context: Context,
    user: UserInterface,
    loginIdentityLink: AuthfIdentityLinkInterface,
    userLogin: AuthenticateTypes,
  ): Promise<CreateTokenResponseDataInterface> {
    if (
      await this.authfAuthenticationService.isBlacklisted(
        context,
        loginIdentityLink.data.authenticationId,
      )
    ) {
      throw new UnauthorizedException();
    }
    context.logger.debug(
      `Login user ${user.id} with authentication Id: ${loginIdentityLink.data.authenticationId}`,
      userLogin,
    );

    return await this.authfAuthenticationService.createTokenPairForUnverified(
      context,
      loginIdentityLink.data.authenticationId,
      userLogin,
    );
  }

  public async loginApp(
    context: Context,
    app: AppInterface,
    loginApiKey: AuthfApiKeyInterface,
    appLogin: ApiAuthAuthenticateInterface,
  ): Promise<CreateTokenResponseDataInterface> {
    if (
      await this.authfAuthenticationService.isBlacklisted(
        context,
        loginApiKey.data.authenticationId,
      )
    ) {
      throw new UnauthorizedException();
    }
    context.logger.debug(
      `Login app ${app.id} with authentication Id: ${loginApiKey.data.authenticationId}`,
      appLogin,
    );

    return await this.authfAuthenticationService.createTokenPairForUnverified(
      context,
      loginApiKey.data.authenticationId,
      appLogin,
    );
  }

  public async updateTokenPayload(
    context: Context,
    userLogin: AuthenticateTypes | null,
    userId: string,
    permissions: PermissionInterface[],
    expirationPolicy: TokenPairExpirationPolicyInterface,
    identityLinks: AuthfIdentityLinkInterface[],
    getLogin,
  ): Promise<void | CreateTokenResponseDataInterface> {
    if (identityLinks.length === 0) {
      throw new HttpException(
        'Missing identity links',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
    const payload: { token: AuthenticationTokenDataInterface } = {
      token: {
        expirationPolicy: expirationPolicy,
        subject: userId,
        body: {
          scopes: permissions.map((permission) => permission.value),
        },
      },
    };

    const authenticationIds = identityLinks.map(
      (identityLink) => identityLink.data.authenticationId,
    );

    const uniqueAuthenticationIds = authenticationIds.filter(
      (v, i, a) => a.indexOf(v) === i,
    );

    for (const authenticationId of uniqueAuthenticationIds) {
      await this._patchAuthentication(context, payload, authenticationId);
    }

    if (getLogin) {
      const loginIdentityLink = identityLinks.find(
        (item) => item.usage === IdentityLinkUsageEnum.LOGIN,
      );
      return this.getToken(
        context,
        { ...payload, providers: [] },
        userLogin,
        loginIdentityLink.data.authenticationId,
      );
    }
  }

  private async _patchAuthentication(
    context: Context,
    payload: any,
    authenticationId: string,
  ) {
    context.logger.warn(
      `Patching authentication with id: ${authenticationId}`,
      payload,
    );

    await this.authfAuthenticationService.saveAuthentication(
      context,
      authenticationId,
      payload,
    );
  }

  private async _createAuthentication(
    context: Context,
    payload: any,
    authenticationId: string,
  ) {
    context.logger.debug(
      `Creating authentication with id: ${authenticationId}`,
    );

    await this.authfAuthenticationService.createAuthentication(
      context,
      authenticationId,
      payload,
    );
  }

  public async validateReAuthAuthorizeToken(
    context: Context,
    token: string,
    identityLink: AuthfIdentityLinkInterface,
  ): Promise<void> {
    await this.authfAuthenticationService.validateAccessToken(context, token);

    const decodedAccessToken = jwt.decode(token) as unknown as any;

    const blacklistedSubjects =
      await this.authfAuthenticationService.getBlacklistedSubjects(context);
    if (blacklistedSubjects.includes(decodedAccessToken.sub)) {
      throw new UnauthorizedException();
    }

    const scopes = decodedAccessToken.scopes;
    if (
      !scopes.includes(
        `authentication:${identityLink.data.authenticationId}:any`,
      )
    ) {
      throw new HttpException('Not allowed', HttpStatus.FORBIDDEN);
    }
  }

  public getApiKeyType(): ApiKeyTypeEnum {
    return ApiKeyTypeEnum.AUTHF;
  }

  public getIdentityLinkType(): IdentityLinkTypeEnum {
    return IdentityLinkTypeEnum.AUTHF;
  }

  public getIdentityLinkId(identityLink: AuthfIdentityLinkInterface): string {
    return identityLink.data.authenticationId;
  }

  public getApiKeysFromApiAuthenticationConfig(
    authenticationProviders: ApiAuthAuthenticationProviderConfigInterface[],
  ): AuthfApiKeyInterface[] {
    const authenticationId = uuidv4();
    return authenticationProviders.map((provider) => ({
      type: ApiKeyTypeEnum.AUTHF,
      id: provider.data.apiKey,
      description: provider.description,
      data: {
        authenticationId,
      },
    }));
  }

  public async updateApiKeyAuthentications(
    context: Context,
    apiKeys: AuthfApiKeyInterface[],
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
    subject: string,
    authentications: ApiAuthAuthenticationProviderInterface[],
  ): Promise<void> {
    if (authentications.length > 0) {
      const payload = {
        providers: authentications,
        token: {
          expirationPolicy: tokenExpirationPolicy,
          subject,
          body: {
            scopes: permissions.map((permission) => permission.value),
          },
        },
      };

      const authfApiKeys = apiKeys.filter(
        (apiKey) => apiKey.type === ApiKeyTypeEnum.AUTHF,
      );

      for (const apiKey of authfApiKeys) {
        await this._patchAuthentication(
          context,
          payload,
          apiKey.data.authenticationId,
        );
      }
    }
  }

  public getApiKeyForRefreshTokenSub(
    apiKeys: AuthfApiKeyInterface[],
    sub: string,
  ): ApiKeyInterface {
    return apiKeys.find(
      (findApiKey) => findApiKey.data.authenticationId === sub,
    );
  }

  public getIdentityLinkForRefreshTokenSub(
    identityLinks: AuthfIdentityLinkInterface[],
    sub: string,
  ): IdentityLinkInterface {
    return identityLinks.find(
      (findIdentityLink) => findIdentityLink.data.authenticationId === sub,
    );
  }

  public createApiKeyForApp(
    context: Context,
    app: AppInterface,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<NewApiKeyInterface> {
    return this.createApiKeys(
      context,
      app.apiKeys,
      app.id,
      permissions,
      tokenExpirationPolicy,
    );
  }
  public async createApiKeys(
    context: Context,
    apiKeys: ApiKeyInterface[],
    subject: string,
    permissions: PermissionInterface[],
    tokenExpirationPolicy: TokenPairExpirationPolicyInterface,
  ): Promise<NewApiKeyInterface> {
    const apiAuth = ApiKeyUtil.generateApiAuth();

    const authfApiKeys = apiKeys.filter(
      (apiKey) => apiKey.type === ApiKeyTypeEnum.AUTHF,
    );

    let useApiKey: any;
    if (authfApiKeys.length === 0) {
      useApiKey = {
        data: {
          authenticationId: uuidv4(),
        },
        id: apiAuth.apiKey,
        type: ApiKeyTypeEnum.AUTHF,
      };
    } else {
      useApiKey = {
        data: {
          authenticationId: (authfApiKeys[0] as AuthfApiKeyInterface).data
            .authenticationId,
        },
        id: apiAuth.apiKey,
        type: ApiKeyTypeEnum.AUTHF,
      };
    }

    const authentication: ApiAuthAuthenticationProviderInterface = {
      data: apiAuth,
      type: AuthenticationProviderTypeEnum.API,
    };

    await this.updateApiKeyAuthentications(
      context,
      [useApiKey],
      permissions,
      tokenExpirationPolicy,
      subject,
      [authentication],
    );

    return {
      apiKey: useApiKey,
      authenticationData: apiAuth,
    };
  }

  public async deleteApiKeys(
    context: Context,
    apiKeys: AuthfApiKeyInterface[],
  ): Promise<void> {
    const promises = [];
    for (const apiKey of apiKeys) {
      promises.push(
        this.authfAuthenticationService.deleteAuthenticationProvider(
          context,
          apiKey.data.authenticationId,
          AuthenticationProviderTypeEnum.API,
          apiKey.id,
          true,
        ),
      );
    }
    await Promise.all(promises);
  }

  public async loginUserWithRefreshToken(
    context: Context,
    authenticate: RefreshAuthenticateInterface,
  ) {
    const decodedRefreshToken = jwt.decode(
      authenticate.data.token as any,
    ) as unknown as any;
    const sub = decodedRefreshToken.sub;

    if (await this.authfAuthenticationService.isBlacklisted(context, sub)) {
      throw new UnauthorizedException();
    }
    context.logger.debug(
      `Login refresh token with authentication Id: ${sub}`,
      authenticate,
    );

    return await this.authfAuthenticationService.createTokenPairForUnverified(
      context,
      sub,
      authenticate,
    );
  }

  public async deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<void> {
    await this.authfAuthenticationService.deleteAuthentication(
      context,
      authenticationId,
    );
  }

  public async updateBlacklistStatus(
    context: Context,
    blacklisted: boolean,
    identityLink: AuthfIdentityLinkInterface,
  ): Promise<void> {
    await this.authfAuthenticationService.updateAuthenticationBlacklistedStatus(
      context,
      identityLink.data.authenticationId,
      blacklisted,
    );
  }
}
