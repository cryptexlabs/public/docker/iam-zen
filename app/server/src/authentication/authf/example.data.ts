import {
  ApiAuthAuthenticationProviderInterface,
  AuthenticateStatelessResponseDataInterface,
  AuthenticateTypeEnum,
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTokenDataInterface,
  AuthenticationTypes,
  AuthfMetaTypeEnum,
  BasicAuthAuthenticationProviderInterface,
  CreateTokenResponseDataInterface,
  EmailTypeEnum,
  ProfileInterface,
  RefreshAuthenticateInterface,
  SamsungAnyAuthenticateDataInterface,
  SamsungAnyAuthenticateInterface,
  SamsungAuthenticateDataInterface,
  SamsungAuthenticateInterface,
  SamsungLegacyAuthenticateDataInterface,
  SamsungLegacyAuthenticateInterface,
  TokenInterface,
  TokenPairInterface,
  UserInterface,
  UtcTimezone,
} from '@cryptexlabs/authf-data-model';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import {
  ContextBuilder,
  RestResponse,
  ServiceClient,
  SimpleHttpResponse,
} from '@cryptexlabs/codex-nodejs-common';
import { HttpStatus } from '@nestjs/common';
import { LocalesEnum } from '../../locale/enum';
import { i18nData } from '../../locale/locales';
import { appConfig, baseLogger } from './setup';
import * as jwt from 'jsonwebtoken';
import { ApiAuthAuthenticationProviderDataInterface } from '@cryptexlabs/authf-data-model/src/authentication/provider/api/api-auth.authentication-provider.data.interface';
import { RefreshAuthenticateDataInterface } from '@cryptexlabs/authf-data-model/src/authentication/refresh/refresh.authenticate.data.interface';
import { BasicAuthAuthenticationProviderDataInterface } from '@cryptexlabs/authf-data-model/src/authentication/provider/basic/basic-auth.authentication-provider.data.interface';

export const exampleAuthenticationId = 'a27a7ae1-7350-46e6-87ad-2a9acf0d458f';

export class ExampleBasicAuth
  implements BasicAuthAuthenticationProviderInterface
{
  data: BasicAuthAuthenticationProviderDataInterface = {
    username: 'johndoe',
    password:
      'b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3',
  };
  type: AuthenticationProviderTypeEnum.BASIC =
    AuthenticationProviderTypeEnum.BASIC;
}

export class ExampleApiAuth implements ApiAuthAuthenticationProviderInterface {
  data: ApiAuthAuthenticationProviderDataInterface = {
    apiKey: '2o38080dsfjaidfnndf',
    secret: 'b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3',
  };
  type: AuthenticationProviderTypeEnum.API = AuthenticationProviderTypeEnum.API;
}

export class ExampleRefreshAuth implements RefreshAuthenticateInterface {
  data: RefreshAuthenticateDataInterface = {
    token: jwt.sign(
      { sub: exampleAuthenticationId, type: 'refresh' },
      appConfig.jwtPrivateKey,
      { algorithm: appConfig.jwtAlgorithm },
    ),
  };
  type: AuthenticateTypeEnum.REFRESH = AuthenticateTypeEnum.REFRESH;
}

export class ExampleSamsungLegacyAuth
  implements SamsungLegacyAuthenticateInterface
{
  data: SamsungLegacyAuthenticateDataInterface = {
    accessToken: 'alksjdf93230k13013kds',
    apiServerUrl: 'api-server.asdf.com',
  };
  type: AuthenticateTypeEnum.SAMSUNG_LEGACY =
    AuthenticateTypeEnum.SAMSUNG_LEGACY;
}

export class ExampleSamsungAuth implements SamsungAuthenticateInterface {
  data: SamsungAuthenticateDataInterface = {
    accessToken: 'alksjdf93230k13013kds',
  };
  type: AuthenticateTypeEnum.SAMSUNG = AuthenticateTypeEnum.SAMSUNG;
}

export class ExampleSamsungAnyAuth implements SamsungAnyAuthenticateInterface {
  data: SamsungAnyAuthenticateDataInterface = {
    accessToken: 'alksjdf93230k13013kds',
    clientId: '238923',
    userId: 'u239230a',
  };
  type: AuthenticateTypeEnum.SAMSUNG_ANY = AuthenticateTypeEnum.SAMSUNG_ANY;
}

export class ExampleAuthentication implements AuthenticationInterface {
  providers: AuthenticationTypes[] = [new ExampleBasicAuth()];
  token: AuthenticationTokenDataInterface = {
    expirationPolicy: {
      access: {
        length: 15 * 60,
      },
      refresh: {
        length: 30 * 24 * 60 * 60,
      },
    },
    subject: '4ea27c6e-57b1-4cf7-822b-f3db8171a8bd',
    body: {
      scopes: ['user:self:all'],
    },
  };
}

const exampleContext = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext('default', null),
)
  .setI18nData(i18nData)
  .build()
  .getResult();

export class ExampleSimpleResponse extends SimpleHttpResponse {
  constructor() {
    super(exampleContext, HttpStatus.OK, LocalesEnum.SUCCESS);
  }
}

export class ExampleTokenPair implements TokenPairInterface {
  access: TokenInterface = {
    token: jwt.sign(
      {
        sub: '4ea27c6e-57b1-4cf7-822b-f3db8171a8bd',
        type: 'access',
        scopes: ['user:self:all'],
      },
      appConfig.jwtPrivateKey,
      { algorithm: appConfig.jwtAlgorithm },
    ),
    expiration: new Date(new Date().getTime() + 15 * 60 * 1000),
  };
  refresh: TokenInterface = {
    token: jwt.sign(
      { sub: '4ea27c6e-57b1-4cf7-822b-f3db8171a8bd', type: 'refresh' },
      appConfig.jwtPrivateKey,
      { algorithm: appConfig.jwtAlgorithm },
    ),
    expiration: new Date(new Date().getTime() + 30 * 24 * 60 * 60 * 1000),
  };
}

export class ExampleUser implements UserInterface {
  timezone: UtcTimezone = '+06:00';
  username = null;
  profile: ProfileInterface = {
    name: {
      first: 'Jason',
      middle: null,
      last: 'Bourne',
      display: 'J. Bourne',
    },
    email: {
      other: [],
      primary: {
        value: 'jason.bourne@samsung.com',
        type: EmailTypeEnum.UNKNOWN,
      },
    },
    phone: {
      other: [],
      primary: null,
    },
    photo: {
      other: [],
      primary: null,
    },
  };
}

export class ExampleTokenPairResponse extends RestResponse<{
  token: ExampleTokenPair;
}> {
  constructor() {
    super(
      exampleContext,
      HttpStatus.CREATED,
      AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
      {
        token: new ExampleTokenPair(),
      },
    );
  }
}

export const exampleResponseData: CreateTokenResponseDataInterface = {
  token: new ExampleTokenPair(),
  user: new ExampleUser(),
};

export class ExampleSamsungAuthTokenPairResponse extends RestResponse<CreateTokenResponseDataInterface> {
  constructor() {
    super(
      exampleContext,
      HttpStatus.CREATED,
      AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
      exampleResponseData,
    );
  }
}

export const exampleStatelessResponseData: AuthenticateStatelessResponseDataInterface =
  {
    user: new ExampleUser(),
  };

export class ExampleSamsungStatelessAuthenticateResponse extends RestResponse<AuthenticateStatelessResponseDataInterface> {
  constructor() {
    super(
      exampleContext,
      HttpStatus.CREATED,
      AuthfMetaTypeEnum.AUTHENTICATION_VERIFICATION,
      exampleStatelessResponseData,
    );
  }
}
