import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { AuthfConfig } from '../authf.config';
import {
  Context,
  ContextualHttpException,
} from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class TokenValidator {
  constructor(@Inject('CONFIG') private readonly config: AuthfConfig) {}

  private async _validateToken(
    context: Context,
    token: string,
    ignoreExpiration = false,
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      jwt.verify(
        token,
        this.config.jwtPublicKey,
        { algorithms: [this.config.jwtAlgorithm], ignoreExpiration },
        (error, decoded) => {
          if (error) {
            reject(
              new ContextualHttpException(
                context,
                'Invalid token',
                HttpStatus.UNAUTHORIZED,
              ),
            );
          } else {
            resolve(decoded);
          }
        },
      );
    });
  }

  public async validateAccessToken(
    context: Context,
    token: string,
    ignoreExpiration = false,
  ) {
    const decodedToken = await this._validateToken(
      context,
      token,
      ignoreExpiration,
    );
    if (decodedToken.type !== 'access') {
      throw new ContextualHttpException(
        context,
        'Invalid token type',
        HttpStatus.UNAUTHORIZED,
      );
    }
  }

  public async validateRefreshToken(
    context: Context,
    token: string,
    authenticationId: string,
  ) {
    const decodedToken = await this._validateToken(context, token);
    // We will only allow a token to be used as a refresh token if the token type is refresh
    if (decodedToken.type !== 'refresh') {
      throw new ContextualHttpException(
        context,
        'Not allowed',
        HttpStatus.FORBIDDEN,
      );
    }
    // The subject of the refresh token must be the authentication id
    if (decodedToken.sub !== authenticationId) {
      throw new ContextualHttpException(
        context,
        'Not allowed',
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
