import { TokenValidator } from './token-validator';
import { appConfig } from '../setup';
import * as jwt from 'jsonwebtoken';
import { HttpStatus } from '@nestjs/common';
import { mock } from 'ts-mockito';
import {
  Context,
  ContextualHttpException,
} from '@cryptexlabs/codex-nodejs-common';

describe('TokenValidator', () => {
  const tokenValidator = new TokenValidator(appConfig);

  it('Should validate an access token', async () => {
    const token = jwt.sign({ type: 'access' }, appConfig.jwtPrivateKey, {
      algorithm: appConfig.jwtAlgorithm,
      expiresIn: 60 * 15,
    });
    const context = mock<Context>(Context);

    await tokenValidator.validateAccessToken(context, token);
  });

  it('Should reject an invalid access token', async () => {
    const token =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJ0eXBlIjoicmVmcmVzaCJ9.rzQhdqE2VpiJ8mdD24FghBZ71D64CWMwQR5_c3_z-w0';

    const context = mock<Context>(Context);

    await expect(
      tokenValidator.validateAccessToken(context, token),
    ).rejects.toBeInstanceOf(ContextualHttpException);

    try {
      await tokenValidator.validateAccessToken(context, token);
    } catch (e) {
      expect(e.getStatus()).toEqual(HttpStatus.UNAUTHORIZED);
    }
  });

  it('Should validate a refresh token', async () => {
    const authenticationId = 'd0e93f89-e57e-4b75-aa61-faaa91823c27';

    const token = jwt.sign(
      { type: 'refresh', sub: authenticationId },
      appConfig.jwtPrivateKey,
      {
        algorithm: appConfig.jwtAlgorithm,
        expiresIn: 60 * 15,
      },
    );

    const context = mock<Context>(Context);

    await tokenValidator.validateRefreshToken(context, token, authenticationId);
  });

  it('Should invalidate an access token that is not of type access', async () => {
    const authenticationId = 'd0e93f89-e57e-4b75-aa61-faaa91823c27';

    const token = jwt.sign(
      { type: 'refresh', sub: authenticationId },
      appConfig.jwtPrivateKey,
      {
        algorithm: appConfig.jwtAlgorithm,
        expiresIn: 60 * 15,
      },
    );

    const context = mock<Context>(Context);

    await expect(
      tokenValidator.validateAccessToken(context, token),
    ).rejects.toBeInstanceOf(ContextualHttpException);

    try {
      await tokenValidator.validateAccessToken(context, token);
    } catch (e) {
      expect(e.getStatus()).toEqual(HttpStatus.UNAUTHORIZED);
    }
  });

  it('Should invalidate a refresh token that is not of type refresh', async () => {
    const authenticationId = 'd0e93f89-e57e-4b75-aa61-faaa91823c27';

    const token = jwt.sign(
      { type: 'access', sub: authenticationId },
      appConfig.jwtPrivateKey,
      {
        algorithm: appConfig.jwtAlgorithm,
        expiresIn: 60 * 15,
      },
    );
    const context = mock<Context>(Context);

    await expect(
      tokenValidator.validateRefreshToken(context, token, authenticationId),
    ).rejects.toBeInstanceOf(ContextualHttpException);

    try {
      await tokenValidator.validateRefreshToken(
        context,
        token,
        authenticationId,
      );
    } catch (e) {
      expect(e.getStatus()).toEqual(HttpStatus.FORBIDDEN);
    }
  });

  it('Should invalidate a refresh token that does not have a subject that matches the authentication id', async () => {
    const authenticationId = 'd0e93f89-e57e-4b75-aa61-faaa91823c27';

    const token = jwt.sign(
      { type: 'refresh', sub: authenticationId },
      appConfig.jwtPrivateKey,
      {
        algorithm: appConfig.jwtAlgorithm,
        expiresIn: 60 * 15,
      },
    );

    const context = mock<Context>(Context);

    await expect(
      tokenValidator.validateRefreshToken(
        context,
        token,
        'ecafa0f9-73b8-4277-a79e-bde7f791d2e9',
      ),
    ).rejects.toBeInstanceOf(ContextualHttpException);

    try {
      await tokenValidator.validateRefreshToken(
        context,
        token,
        'ecafa0f9-73b8-4277-a79e-bde7f791d2e9',
      );
    } catch (e) {
      expect(e.getStatus()).toEqual(HttpStatus.FORBIDDEN);
    }
  });
});
