import { TokenPairData } from './token-pair-data';

describe('TokenPairData', () => {
  it('Should get an access token expiration in seconds', () => {
    const tokenPairData = new TokenPairData({
      body: undefined,
      expirationPolicy: {
        access: {
          length: 15 * 60,
        },
        refresh: {
          length: 60 * 60 * 24 * 30,
        },
      },
      subject: '4b829df0-1222-4b56-ac84-58db014e3893',
    });

    expect(tokenPairData.getAccessTokenExpiresIn()).toEqual(15 * 60);
  });

  it('Should get an access token payload', () => {
    const tokenPairData = new TokenPairData({
      body: {
        hello: 'world',
      },
      expirationPolicy: {
        access: {
          length: 15 * 60,
        },
        refresh: {
          length: 60 * 60 * 24 * 30,
        },
      },
      subject: '4b829df0-1222-4b56-ac84-58db014e3893',
    });

    expect(tokenPairData.getAccessTokenPayload()).toMatchObject({
      sub: '4b829df0-1222-4b56-ac84-58db014e3893',
      hello: 'world',
      type: 'access',
    });
  });

  it('Should get a refresh token expiration in seconds', () => {
    const tokenPairData = new TokenPairData({
      body: undefined,
      expirationPolicy: {
        access: {
          length: 15 * 60,
        },
        refresh: {
          length: 60 * 60 * 24 * 30,
        },
      },
      subject: '4b829df0-1222-4b56-ac84-58db014e3893',
    });

    expect(tokenPairData.getRefreshTokenExpiresIn()).toEqual(60 * 60 * 24 * 30);
  });

  it('Should get a refresh token payload', () => {
    const tokenPairData = new TokenPairData({
      body: {
        hello: 'world',
      },
      expirationPolicy: {
        access: {
          length: 15 * 60,
        },
        refresh: {
          length: 60 * 60 * 24 * 30,
        },
      },
      subject: '4b829df0-1222-4b56-ac84-58db014e3893',
    });

    expect(
      tokenPairData.getRefreshTokenPayload(
        '270a20ab-69a9-49ee-bc30-28bed689efc7',
      ),
    ).toMatchObject({
      type: 'refresh',
      sub: '270a20ab-69a9-49ee-bc30-28bed689efc7',
    });
  });
});
