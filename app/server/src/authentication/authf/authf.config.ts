import {
  DefaultConfig,
  SqlConfigInterface,
} from '@cryptexlabs/codex-nodejs-common';
import * as fs from 'fs';
import { Algorithm } from 'jsonwebtoken';
import { appDir, baseDir, rootDir } from '../../root';
import { DatabaseTypeEnum } from './data/database-type.enum';
import {
  SamsungConfigInterface,
  SamsungOAuthClientConfigInterface,
} from './authf.server-config.interface';
import { dirname } from 'path';

export class AuthfConfig extends DefaultConfig {
  private _jwtPrivateKey: Buffer;
  private _jwtPublicKey: Buffer;

  public get samsung(): SamsungConfigInterface {
    return this.getServerConfig().samsung;
  }

  public get samsungOAuthClients(): SamsungOAuthClientConfigInterface[] {
    const config = this.getServerConfig();
    if (config && config.oAuth && config.oAuth.samsung) {
      return config.oAuth.samsung;
    } else {
      return [];
    }
  }

  public get jwtPrivateKey(): Buffer {
    if (!this._jwtPrivateKey) {
      let path = process.env.JWT_PRIVATE_KEY_PATH;
      if (!path) {
        if (
          ['test', 'localhost', 'ci'].includes(this.environmentName) ||
          !this.environmentName
        ) {
          if (fs.existsSync(`${appDir}/config/jwt-rs256.key`)) {
            path = `${appDir}/config/jwt-rs256.key`;
          } else {
            path = `${dirname(rootDir)}/config/jwt-rs256.key`;
          }
        } else {
          path = `/var/app/config/jwt-rs256.key`;
        }
      }
      this._jwtPrivateKey = fs.readFileSync(path);
    }
    return this._jwtPrivateKey;
  }

  public get jwtPublicKey(): Buffer {
    if (!this._jwtPublicKey) {
      let path = process.env.JWT_PUBLIC_KEY_PATH;
      if (!path) {
        if (
          ['test', 'localhost', 'ci'].includes(this.environmentName) ||
          !this.environmentName
        ) {
          if (fs.existsSync(`${appDir}/config/jwt-rs256.pub`)) {
            path = `${appDir}/config/jwt-rs256.pub`;
          } else {
            path = `${dirname(rootDir)}/config/jwt-rs256.pub`;
          }
        } else {
          path = `/var/app/config/jwt-rs256.pub`;
        }
      }

      this._jwtPublicKey = fs.readFileSync(path);
    }
    return this._jwtPublicKey;
  }

  public get jwtAlgorithm(): Algorithm {
    return (process.env.JWT_ALGORITHM as Algorithm) || 'RS256';
  }

  public get dbType(): DatabaseTypeEnum {
    return (
      (process.env.DATABASE_TYPE as DatabaseTypeEnum) ||
      DatabaseTypeEnum.ELASTICSEARCH
    );
  }

  public get adminUsername(): string {
    return process.env.ADMIN_USERNAME || 'admin';
  }

  public get adminPassword(): string {
    return process.env.ADMIN_PASSWORD || 'password';
  }

  public get httpSecure(): boolean {
    return process.env.HTTP_SECURE === 'true';
  }

  public get sql(): SqlConfigInterface {
    return {
      database: process.env.SQL_DATABASE,
      client: process.env.SQL_CLIENT,
      userName: process.env.SQL_USERNAME,
      password: process.env.SQL_PASSWORD,
      primary: {
        hostName: process.env.SQL_HOSTNAME,
        port: parseInt(process.env.SQL_PORT, 10),
        pool: {
          min: parseInt(process.env.SQL_POOL_MIN, 10),
          max: parseInt(process.env.SQL_POOL_MAX, 10),
        },
      },
      reader: {
        hostName: process.env.SQL_HOSTNAME_RO,
        port: parseInt(process.env.SQL_PORT_RO, 10),
        pool: {
          min: parseInt(process.env.SQL_POOL_RO_MIN, 10),
          max: parseInt(process.env.SQL_POOL_RO_MAX, 10),
        },
      },
    };
  }

  public get quietPinoReqLogger(): boolean {
    return process.env.LOGGER_PINO_QUIET_REQ_LOGGER === 'true';
  }

  public get componentHealthRefreshInterval(): number {
    return process.env.COMPONENT_HEALTH_REFRESH_INTERVAL_IN_SECONDS
      ? parseInt(process.env.COMPONENT_HEALTH_REFRESH_INTERVAL_IN_SECONDS, 10)
      : 60;
  }

  public get createAuthenticationInterval(): number {
    return process.env.CREATE_AUTHENTICATION_BATCH_INTERVAL
      ? parseInt(process.env.CREATE_AUTHENTICATION_BATCH_INTERVAL, 10)
      : 3000;
  }

  public get authenticationCacheTtl(): number {
    return process.env.AUTHENTICATION_CACHE_TTL
      ? parseInt(process.env.AUTHENTICATION_CACHE_TTL, 10)
      : 5;
  }
}
