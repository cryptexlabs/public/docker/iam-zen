import { DynamicModule } from '@nestjs/common';
import { appConfig } from '../setup';
import { KnexModule } from 'nestjs-knex';
import { SqlConstants } from './sql.constants';
import { DatabaseTypeEnum } from './database-type.enum';

let databaseModules: DynamicModule[] = [];
databaseModules = [];

if (appConfig.dbType === DatabaseTypeEnum.MYSQL) {
  databaseModules = databaseModules.concat([
    KnexModule.forRoot({
      config: {
        client: appConfig.sql.client,
        connection: {
          host: appConfig.sql.primary.hostName,
          port: appConfig.sql.primary.port,
          user: appConfig.sql.userName,
          password: appConfig.sql.password,
          database: appConfig.sql.database,
          pool: {
            min: appConfig.sql.primary.pool.min,
            max: appConfig.sql.primary.pool.max,
          },
        },
      },
    }),
    KnexModule.forRoot(
      {
        config: {
          client: appConfig.sql.client,
          connection: {
            host: appConfig.sql.reader.hostName,
            port: appConfig.sql.reader.port,
            user: appConfig.sql.userName,
            password: appConfig.sql.password,
            database: appConfig.sql.database,
            pool: {
              min: appConfig.sql.reader.pool.min,
              max: appConfig.sql.reader.pool.max,
            },
          },
        },
      },
      SqlConstants.READ_ONLY,
    ),
  ]);
}

export { databaseModules };
