import { Module } from '@nestjs/common';
import { AuthfAuthenticationService } from './authentication/authf.authentication.service';
import { appConfig, baseLogger } from './setup';
import { AuthfAuthenticationDataServiceFactory } from './authentication/authf.authentication-data-service.factory';
import { AuthfConfig } from './authf.config';
import { AuthfAuthenticationDataServiceInterface } from './authentication/authf.authentication.data.service.interface';
import { AuthfAuthenticationDataService } from './authentication/authf.authentication.data.service';
import { AuthenticatorFactory } from './authenticator/authenticator.factory';
import { TokenValidator } from './token';
import { ExtraService } from './extra/extra.service';
import { ProviderIdentityMysqlDataService } from './identity/provider-identity.mysql.data.service';
import { DatabaseTypeEnum } from './data/database-type.enum';
import { ExtraCognitoService } from './extra/extra.cognito.service';
import { AuthfAuthenticationCacheService } from './authentication/authf.authentication.cache.service';
import { RedisModule } from '../../redis.module';
import {
  ContextBuilder,
  ServiceClient,
} from '@cryptexlabs/codex-nodejs-common';
import { MessageContext } from '@cryptexlabs/codex-data-model';
import { i18nData } from '../../locale/locales';
import { AuthfController } from './authf.controller';
import { AdminAuthenticator } from './authenticator/admin.authenticator';

const providers = [];
if (appConfig.dbType === DatabaseTypeEnum.MYSQL) {
  providers.push({
    provide: 'MYSQL_PROVIDER_IDENTITY_DATA_SERVICE',
    useClass: ProviderIdentityMysqlDataService,
  });
} else {
  providers.push({
    provide: 'MYSQL_AUTHENTICATION_DATA_SERVICE',
    useValue: null,
  });
  providers.push({
    provide: 'MYSQL_PROVIDER_IDENTITY_DATA_SERVICE',
    useValue: null,
  });
}
@Module({
  imports: [RedisModule],
  controllers: [AuthfController],
  providers: [
    ...providers,
    AuthfAuthenticationService,
    AdminAuthenticator,
    {
      provide: 'CONFIG',
      useValue: appConfig,
    },
    {
      provide: 'LOGGER',
      useValue: baseLogger,
    },
    AuthfAuthenticationDataServiceFactory,
    AuthenticatorFactory,
    TokenValidator,
    ExtraService,
    ExtraCognitoService,
    AuthfAuthenticationDataService,
    AuthfAuthenticationCacheService,
    {
      provide: 'AUTHENTICATION_PERSISTENCE_SERVICE',
      useFactory: async (
        factory: AuthfAuthenticationDataServiceFactory,
        config: AuthfConfig,
      ) => {
        return await factory.getPersistenceService(config.dbType);
      },
      inject: [AuthfAuthenticationDataServiceFactory, 'CONFIG'],
    },
    {
      provide: 'AUTHENTICATION_DATA_SERVICE',
      useFactory: async (
        persistenceService: AuthfAuthenticationDataServiceInterface,
        config: AuthfConfig,
        dataService: AuthfAuthenticationDataService,
      ) => {
        if (config.redis.enabled) {
          return dataService;
        } else {
          return persistenceService;
        }
      },
      inject: [
        'AUTHENTICATION_PERSISTENCE_SERVICE',
        'CONFIG',
        AuthfAuthenticationDataService,
      ],
    },
    {
      provide: 'CONTEXT_BUILDER',
      useValue: new ContextBuilder(
        baseLogger,
        appConfig,
        new ServiceClient(appConfig),
        new MessageContext('default', null),
      ).setI18nData(i18nData),
    },
  ],
  exports: [AuthfAuthenticationService],
})
export class AuthfModule {}
