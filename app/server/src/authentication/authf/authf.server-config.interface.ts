export interface SamsungOAuthClientConfigInterface {
  clientId: string;
  clientSecret: string;
  redirectUrl: string;
  finalRedirectUrl: string;
}

export interface SamsungConfigInterface {
  authorizeAccessToken: boolean;
}

export interface AuthfServerConfigInterface {
  oAuth?: {
    samsung?: SamsungOAuthClientConfigInterface[];
  };
  samsung?: SamsungConfigInterface;
}
