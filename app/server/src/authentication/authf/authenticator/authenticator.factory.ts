import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import {
  AuthenticateTypeEnum,
  AuthenticateTypes,
} from '@cryptexlabs/authf-data-model';
import { AuthenticatorInterface } from './authenticator.interface';
import { BasicAuthenticator } from '../provider/basic/basic.authenticator';
import { SamsungLegacyAuthenticator } from '../provider/samsung-legacy/samsung-legacy.authenticator';
import { ApiAuthenticator } from '../provider/api/api.authenticator';
import { SamsungAuthenticator } from '../provider/samsung/samsung.authenticator';
import { SamsungAnyAuthenticator } from '../provider/samsung/samsung.any.authenticator';
import {
  Context,
  ContextualHttpException,
} from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class AuthenticatorFactory {
  constructor(@Inject('CONFIG') private readonly config) {}

  getAuthenticator(
    context: Context,
    authenticate: AuthenticateTypes,
  ): AuthenticatorInterface {
    const authenticatorMap = {
      [AuthenticateTypeEnum.BASIC]: BasicAuthenticator,
      [AuthenticateTypeEnum.API]: ApiAuthenticator,
      [AuthenticateTypeEnum.SAMSUNG_LEGACY]: SamsungLegacyAuthenticator,
      [AuthenticateTypeEnum.SAMSUNG]: SamsungAuthenticator,
      [AuthenticateTypeEnum.SAMSUNG_ANY]: SamsungAnyAuthenticator,
    };

    if (!authenticatorMap[authenticate.type]) {
      throw new ContextualHttpException(
        context,
        `Invalid authentication provider type: ${authenticate.type}`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const authenticatorClass = authenticatorMap[authenticate.type];
    // noinspection TypeScriptValidateTypes
    const authenticator = new authenticatorClass(
      authenticate.data,
      this.config,
    );
    return authenticator as AuthenticatorInterface;
  }
}
