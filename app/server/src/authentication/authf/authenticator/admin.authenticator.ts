import { HttpException, HttpStatus, Inject, Injectable } from '@nestjs/common';
import * as basicAuth from 'basic-auth';
import { AuthfConfig } from '../authf.config';
import {
  Context,
  ContextualHttpException,
} from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class AdminAuthenticator {
  constructor(@Inject('CONFIG') private readonly config: AuthfConfig) {}

  public validateBasicAuthenticationHeader(
    context: Context,
    authenticationHeader: string,
  ) {
    const authParts = basicAuth.parse(authenticationHeader);
    if (!authParts) {
      throw new ContextualHttpException(
        context,
        'Unauthorized',
        HttpStatus.UNAUTHORIZED,
      );
    }
    const { name, pass } = authParts;

    if (
      name !== this.config.adminUsername ||
      pass !== this.config.adminPassword
    ) {
      throw new ContextualHttpException(
        context,
        'Unauthorized',
        HttpStatus.UNAUTHORIZED,
      );
    }
  }
}
