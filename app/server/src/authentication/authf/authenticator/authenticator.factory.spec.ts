import { AuthenticatorFactory } from './authenticator.factory';
import {
  ApiAuthAuthenticationProviderInterface,
  AuthenticateTypes,
  AuthenticationProviderTypeEnum,
  BasicAuthAuthenticationProviderInterface,
} from '@cryptexlabs/authf-data-model';
import { BasicAuthenticator } from '../provider/basic/basic.authenticator';
import { HttpStatus } from '@nestjs/common';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { instance, mock, when } from 'ts-mockito';
import { ApiAuthenticator } from '../provider/api/api.authenticator';
import { AuthfConfig } from '../authf.config';

describe('AuthenticatorFactory', () => {
  let authenticatorFactory;
  let MockConfig: AuthfConfig;

  beforeEach(() => {
    MockConfig = mock(AuthfConfig);
    const config = instance(MockConfig);

    authenticatorFactory = new AuthenticatorFactory(config);
  });

  it('Should create a basic authenticator', async () => {
    const provider: BasicAuthAuthenticationProviderInterface = {
      type: AuthenticationProviderTypeEnum.BASIC,
      data: {
        username: 'johndoe',
        password: 'asdf',
      },
    };
    const MockContext = mock<Context>(Context);
    const context = instance(MockContext);
    when(MockContext.logger).thenReturn({ debug: () => {} } as any);

    const authenticator = authenticatorFactory.getAuthenticator(
      context,
      provider,
    );

    expect(authenticator).toBeInstanceOf(BasicAuthenticator);

    await authenticator.authenticate(context, {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.BASIC,
          data: {
            username: 'johndoe',
            password:
              'f0e4c2f76c58916ec258f246851bea091d14d4247a2fc3e18694461b1816e13b',
          },
        },
      ],
      token: undefined,
    });
  });

  it('Should create an api authenticator', async () => {
    const provider: ApiAuthAuthenticationProviderInterface = {
      type: AuthenticationProviderTypeEnum.API,
      data: {
        apiKey: 'alskdjflks93923',
        secret: 'asdf',
      },
    };
    const context = mock<Context>(Context);

    const authenticator = authenticatorFactory.getAuthenticator(
      context,
      provider,
    );

    expect(authenticator).toBeInstanceOf(ApiAuthenticator);

    await authenticator.authenticate(context, {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.API,
          data: {
            apiKey: 'alskdjflks93923',
            secret:
              'f0e4c2f76c58916ec258f246851bea091d14d4247a2fc3e18694461b1816e13b',
          },
        },
      ],
      token: undefined,
    });
  });

  it('Should throw an error when an invalid authenticator type is provided', () => {
    const invalidProvider = {
      type: 'asdf',
      data: null,
    } as unknown as AuthenticateTypes;

    const context = mock<Context>(Context);

    expect(() => {
      authenticatorFactory.getAuthenticator(context, invalidProvider);
    }).toThrowError();

    try {
      authenticatorFactory.getAuthenticator(context, invalidProvider);
    } catch (e) {
      expect(e.getStatus()).toEqual(HttpStatus.BAD_REQUEST);
    }
  });
});
