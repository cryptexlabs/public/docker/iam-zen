import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { AuthenticationInterface } from '@cryptexlabs/authf-data-model';
import { basicAuthAuthenticationProviderSchema } from '../provider/basic/basic-auth.authentication-provider.schema';
import { samsungLegacyAuthenticationProviderSchema } from '../provider/samsung-legacy/samsung-legacy.authentication-provider.schema';
import { apiAuthAuthenticationProviderSchema } from '../provider/api/api-auth.authentication-provider.schema';
import { samsungAuthenticationProviderSchema } from '../provider/samsung/samsung.authentication-provider.schema';

@Injectable()
export class AuthfAuthenticationValidationPipe implements PipeTransform {
  private schema: Joi.ObjectSchema<AuthenticationInterface>;

  constructor() {
    this.schema = Joi.object({
      token: Joi.object({
        expirationPolicy: Joi.object({
          access: Joi.object({
            length: Joi.number().required(),
          }).required(),
          refresh: Joi.object({
            length: Joi.number().required(),
          }).required(),
        }).required(),
        subject: Joi.string().required(),
        body: Joi.object().required(),
      }).required(),
      providers: Joi.array()
        .optional()
        .items(
          basicAuthAuthenticationProviderSchema,
          apiAuthAuthenticationProviderSchema,
          samsungLegacyAuthenticationProviderSchema,
          samsungAuthenticationProviderSchema,
        ),
      extra: Joi.object().optional(),
    });
  }

  transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this.schema.validate(value);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
