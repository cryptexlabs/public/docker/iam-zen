import {
  HttpStatus,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { AuthfConfig } from '../authf.config';
import { AuthfAuthenticationDataServiceInterface } from './authf.authentication.data.service.interface';
import {
  AuthenticateStatelessResponseDataInterface,
  AuthenticateTypeEnum,
  AuthenticateTypes,
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
  CreateTokenResponseDataInterface,
  StatelessAuthenticateTypes,
  UserInterface,
} from '@cryptexlabs/authf-data-model';
import { TokenPairData, TokenValidator } from '../token';
import { AuthenticatorFactory } from '../authenticator/authenticator.factory';
import * as crypto from 'crypto';
import {
  Context,
  ContextualHttpException,
  CustomLogger,
} from '@cryptexlabs/codex-nodejs-common';
import { ExtraService } from '../extra/extra.service';

@Injectable()
export class AuthfAuthenticationService {
  constructor(
    @Inject('CONFIG') private readonly config: AuthfConfig,
    @Inject('LOGGER') private readonly logger: CustomLogger,
    @Inject('AUTHENTICATION_DATA_SERVICE')
    private readonly authenticationDataService: AuthfAuthenticationDataServiceInterface,
    private readonly authenticatorFactory: AuthenticatorFactory,
    private readonly tokenValidator: TokenValidator,
    private readonly extraService: ExtraService,
  ) {}

  public async isBlacklisted(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    const blacklistedList =
      await this.authenticationDataService.getBlacklistedAuthenticationIds(
        context,
      );

    return blacklistedList.includes(authenticationId);
  }

  public async authenticationExists(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    return this.authenticationDataService.authenticationExists(
      context,
      authenticationId,
    );
  }

  public async getBlacklistedSubjects(context: Context): Promise<string[]> {
    return await this.authenticationDataService.getBlacklistedSubjects(context);
  }

  public async createTokenPairForUnverified(
    context: Context,
    authenticationId: string,
    authenticate: AuthenticateTypes,
  ): Promise<CreateTokenResponseDataInterface> {
    // We do not want to query the database for refresh token before validation. Doing so could cause db to crash during brute force attack

    if (authenticate.type === AuthenticateTypeEnum.REFRESH) {
      await this.tokenValidator.validateRefreshToken(
        context,
        authenticate.data.token,
        authenticationId,
      );
    }

    if (
      authenticate.type === AuthenticateTypeEnum.REFRESH &&
      authenticate.data.expiredAccessToken
    ) {
      await this.tokenValidator.validateAccessToken(
        context,
        authenticate.data.expiredAccessToken,
        true,
      );

      const decodedAccessToken = jwt.decode(
        authenticate.data.expiredAccessToken,
      ) as unknown as any;
      const accessTokenExpirationLength =
        decodedAccessToken.exp - decodedAccessToken.iat;
      delete decodedAccessToken.exp;
      delete decodedAccessToken.iat;

      const accessTokenExpiration = new Date(
        new Date().getTime() + accessTokenExpirationLength,
      );

      const decodedRefreshToken = jwt.decode(
        authenticate.data.token,
      ) as unknown as any;
      const refreshTokenExpirationLength =
        decodedRefreshToken.exp - decodedRefreshToken.iat;
      delete decodedRefreshToken.exp;
      delete decodedRefreshToken.iat;

      const refreshTokenExpiration = new Date(
        new Date().getTime() + refreshTokenExpirationLength,
      );

      const accessToken = jwt.sign(
        decodedAccessToken,
        this.config.jwtPrivateKey,
        {
          algorithm: this.config.jwtAlgorithm,
          expiresIn: accessTokenExpirationLength,
        },
      );

      const refreshToken = jwt.sign(
        decodedRefreshToken,
        this.config.jwtPrivateKey,
        {
          algorithm: this.config.jwtAlgorithm,
          expiresIn: refreshTokenExpirationLength,
        },
      );

      return {
        token: {
          access: {
            token: accessToken,
            expiration: accessTokenExpiration,
          },
          refresh: {
            token: refreshToken,
            expiration: refreshTokenExpiration,
          },
        },
      };
    } else {
      const authentication =
        await this.authenticationDataService.getAuthentication(
          context,
          authenticationId,
        );
      if (!authentication) {
        throw new NotFoundException(
          `Could not find authentication with id: ${authenticationId}`,
        );
      }

      let user;

      if (authenticate.type !== AuthenticateTypeEnum.REFRESH) {
        const authenticator = this.authenticatorFactory.getAuthenticator(
          context,
          authenticate as AuthenticateTypes,
        );

        user = await authenticator.authenticate(context, authentication);
      }

      return this.createTokenPairForVerified(
        context,
        authenticationId,
        authentication,
        user,
      );
    }
  }

  public async createStatelessVerification(
    context: Context,
    authenticate: StatelessAuthenticateTypes,
  ): Promise<AuthenticateStatelessResponseDataInterface> {
    const statelessAuthenticators = [AuthenticateTypeEnum.SAMSUNG_ANY];

    if (
      !statelessAuthenticators.includes(
        authenticate.type as AuthenticateTypeEnum,
      )
    ) {
      throw new ContextualHttpException(
        context,
        `Invalid type: ${authenticate.type}. Only stateless authentors allowed on this endpoint`,
        HttpStatus.BAD_REQUEST,
      );
    }

    const authenticator = this.authenticatorFactory.getAuthenticator(
      context,
      authenticate as AuthenticateTypes,
    );

    const user = await authenticator.authenticate(context);

    return {
      user,
    };
  }

  public async createTokenPairForVerifiedWithAuthorizedAuthenticationData(
    context: Context,
    authenticationId: string,
    authorizedAuthentication: AuthenticationInterface,
  ): Promise<CreateTokenResponseDataInterface> {
    return this.createTokenPairForVerified(
      context,
      authenticationId,
      authorizedAuthentication,
      undefined,
    );
  }

  public async createTokenPairForUnverifiedWithAuthorizedAuthenticationData(
    context: Context,
    authenticationId: string,
    authenticate: AuthenticateTypes,
    authorizedAuthentication: AuthenticationInterface,
  ): Promise<CreateTokenResponseDataInterface> {
    if (authenticate.type !== AuthenticateTypeEnum.REFRESH) {
      const authentication =
        await this.authenticationDataService.getAuthentication(
          context,
          authenticationId,
        );
      if (!authentication) {
        throw new NotFoundException(
          `Could not find authentication with id: ${authenticationId}`,
        );
      }

      const authenticator = this.authenticatorFactory.getAuthenticator(
        context,
        authenticate as AuthenticateTypes,
      );

      const user = await authenticator.authenticate(context, authentication);

      return this.createTokenPairForVerified(
        context,
        authenticationId,
        authorizedAuthentication,
        user,
      );
    } else {
      return this.createTokenPairForUnverified(
        context,
        authenticationId,
        authenticate,
      );
    }
  }

  public async createTokenPairForVerified(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
    user?: UserInterface,
  ): Promise<CreateTokenResponseDataInterface> {
    const tokenPair = new TokenPairData(authentication.token);

    const accessTokenExpiration = new Date(
      new Date().getTime() + tokenPair.getAccessTokenExpiresIn() * 1000,
    );
    const accessToken = jwt.sign(
      tokenPair.getAccessTokenPayload(),
      this.config.jwtPrivateKey,
      {
        algorithm: this.config.jwtAlgorithm,
        expiresIn: tokenPair.getAccessTokenExpiresIn(),
      },
    );
    const refreshTokenExpiration = new Date(
      new Date().getTime() + tokenPair.getRefreshTokenExpiresIn() * 1000,
    );
    const refreshToken = jwt.sign(
      tokenPair.getRefreshTokenPayload(authenticationId),
      this.config.jwtPrivateKey,
      {
        algorithm: this.config.jwtAlgorithm,
        expiresIn: tokenPair.getRefreshTokenExpiresIn(),
      },
    );
    this.logger.debug(
      `AuthenticationService.createTokenPairForVerified.authentication: ${JSON.stringify(
        authentication,
      )}`,
    );
    this.logger.debug(
      `AuthenticationService.createTokenPairForVerified.authenticationId: ${authenticationId}`,
    );

    const extra = await this.extraService.getExtras(
      context,
      authentication,
      authenticationId,
    );

    return {
      token: {
        access: {
          token: accessToken,
          expiration: accessTokenExpiration,
        },
        refresh: {
          token: refreshToken,
          expiration: refreshTokenExpiration,
        },
      },
      extra,
      user,
    };
  }

  public async saveAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ) {
    const providers = (authentication.providers || []).map((provider) => {
      if (provider.type === AuthenticationProviderTypeEnum.BASIC) {
        return {
          ...provider,
          data: {
            username: provider.data.username,
            password: crypto
              .createHash('sha256')
              .update(provider.data.password)
              .digest('hex'),
          },
        };
      }

      if (provider.type === AuthenticationProviderTypeEnum.API) {
        return {
          ...provider,
          data: {
            apiKey: provider.data.apiKey,
            secret: crypto
              .createHash('sha256')
              .update(provider.data.secret)
              .digest('hex'),
          },
        };
      }

      if (
        [
          AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          AuthenticationProviderTypeEnum.SAMSUNG,
        ].includes(provider.type)
      ) {
        return provider;
      }

      throw new Error(
        `Invalid authentication provider ${(provider as unknown as any).type}`,
      );
    });

    const authenticationToSave = { ...authentication, providers };

    await this.authenticationDataService.patchAuthentication(
      context,
      authenticationId,
      authenticationToSave,
    );

    // Wait for replication lag
    if (!this.config.redis.enabled) {
      let authenticationToGet;
      do {
        authenticationToGet =
          await this.authenticationDataService.getAuthentication(
            context,
            authenticationId,
          );
        if (!authenticationToGet) {
          context.logger.log(`Waiting for authentication #1`);
          await new Promise((resolve) => setTimeout(resolve, 100));
        }
      } while (!authenticationToGet);
    }
  }

  public async createAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ) {
    const providers = (authentication.providers || []).map((provider) => {
      if (provider.type === AuthenticationProviderTypeEnum.BASIC) {
        return {
          ...provider,
          data: {
            username: provider.data.username,
            password: crypto
              .createHash('sha256')
              .update(provider.data.password)
              .digest('hex'),
          },
        };
      }

      if (provider.type === AuthenticationProviderTypeEnum.API) {
        return {
          ...provider,
          data: {
            apiKey: provider.data.apiKey,
            secret: crypto
              .createHash('sha256')
              .update(provider.data.secret)
              .digest('hex'),
          },
        };
      }

      if (
        [
          AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          AuthenticationProviderTypeEnum.SAMSUNG,
        ].includes(provider.type)
      ) {
        return provider;
      }

      throw new Error(
        `Invalid authentication provider ${(provider as unknown as any).type}`,
      );
    });

    const authenticationToCreate = { ...authentication, providers };

    await this.authenticationDataService.createAuthentication(
      context,
      authenticationId,
      authenticationToCreate,
    );

    // Wait for replication lag
    if (!this.config.redis.enabled) {
      let authenticationToGet;
      do {
        authenticationToGet =
          await this.authenticationDataService.getAuthentication(
            context,
            authenticationId,
          );
        if (!authenticationToGet) {
          context.logger.log(`Waiting for authentication #4`);
          await new Promise((resolve) => setTimeout(resolve, 100));
        }
      } while (!authenticationToGet);
    }
  }

  public async getAuthentication(context: Context, authenticationId: string) {
    const authentication =
      await this.authenticationDataService.getAuthentication(
        context,
        authenticationId,
      );
    if (!authentication) {
      throw new NotFoundException(
        `Could not find authentication with id: ${authenticationId}`,
      );
    }
    return authentication;
  }

  public async patchAuthenticationProvider(
    context: Context,
    authenticationId: string,
    provider: AuthenticationTypes,
  ) {
    return this.authenticationDataService.patchAuthenticationProvider(
      context,
      authenticationId,
      provider,
    );
  }

  public async deleteAuthenticationProvider(
    context: Context,
    authenticationId: string,
    type: AuthenticationProviderTypeEnum,
    identifier: string,
    deleteAuthenticationIfProvidersEmpty: boolean,
  ): Promise<void> {
    const exists =
      await this.authenticationDataService.deleteAuthenticationProvider(
        context,
        authenticationId,
        type,
        identifier,
        deleteAuthenticationIfProvidersEmpty,
      );
    if (!exists) {
      throw new NotFoundException();
    }
  }

  public async deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<void> {
    const exists = await this.authenticationDataService.deleteAuthentication(
      context,
      authenticationId,
    );
    if (!exists) {
      throw new NotFoundException();
    }
  }

  public async updateAuthenticationBlacklistedStatus(
    context: Context,
    authenticationId: string,
    blacklisted: boolean,
  ): Promise<void> {
    const exists =
      await this.authenticationDataService.updateAuthenticationBlacklistedStatus(
        context,
        authenticationId,
        blacklisted,
      );
    if (!exists) {
      throw new NotFoundException();
    }
  }

  public async validateAccessToken(
    context: Context,
    token: string,
    ignoreExpiration = false,
  ): Promise<void> {
    return await this.tokenValidator.validateAccessToken(
      context,
      token,
      ignoreExpiration,
    );
  }

  public async validateRefreshToken(
    context: Context,
    token: string,
    authenticationId: string,
  ): Promise<void> {
    return await this.tokenValidator.validateRefreshToken(
      context,
      token,
      authenticationId,
    );
  }
}
