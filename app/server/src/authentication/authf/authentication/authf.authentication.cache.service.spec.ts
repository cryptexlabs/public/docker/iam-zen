import NodeCache from 'node-cache';

// Mock the node-cache module
jest.mock('node-cache', () => {
  return jest.fn().mockImplementation(() => {
    return {
      get: () => {},
      set: () => {},
      del: () => {},
    };
  });
});

import { AuthfAuthenticationCacheService } from './authf.authentication.cache.service';
import { RedisClientType } from 'redis';
import { instance, mock, when, verify, anything, deepEqual } from 'ts-mockito';
import { Context, NotImplementedError } from '@cryptexlabs/codex-nodejs-common';
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
} from '@cryptexlabs/authf-data-model';
import { AuthfConfig } from '../authf.config';

describe('AuthenticationCacheService', () => {
  let service: AuthfAuthenticationCacheService;
  let RedisMock: RedisClientType;
  let ContextMock: Context;
  let context: Context;
  let NodeCacheMock: NodeCache;
  let ConfigMock: AuthfConfig;

  beforeEach(() => {
    RedisMock = mock<RedisClientType>();
    ContextMock = mock(Context);
    context = instance(ContextMock);
    NodeCacheMock = mock(NodeCache);
    ConfigMock = mock(AuthfConfig);
    service = new AuthfAuthenticationCacheService(
      instance(RedisMock),
      instance(ConfigMock),
    );
    (service as any)._memoryCache = instance(NodeCacheMock);
  });

  describe('deleteAuthenticationProvider', () => {
    it('should throw error when trying to delete an authentication provider', async () => {
      await expect(async () => {
        await service.deleteAuthenticationProvider(
          context,
          '285c2df5-8204-471f-bafa-d98f33cb9eaa',
          AuthenticationProviderTypeEnum.API,
          'hello',
          false,
        );
      }).rejects.toBeInstanceOf(NotImplementedError);
    });
  });

  describe('getAuthentication', () => {
    it('should return authentication data when present in Redis', async () => {
      when(RedisMock.get('authf:authentication:authId')).thenResolve(
        JSON.stringify({ id: 'authId' }),
      );

      const result = await service.getAuthentication(context, 'authId');

      expect(result).toEqual({ id: 'authId' });
      verify(RedisMock.get('authf:authentication:authId')).once();
    });

    it('should return null when authentication data is not present in Redis', async () => {
      when(RedisMock.get('authf:authentication:authId')).thenResolve(null);

      const result = await service.getAuthentication(context, 'authId');

      expect(result).toBeNull();
      verify(RedisMock.get('authf:authentication:authId')).once();
    });
  });

  describe('patchAuthentication', () => {
    it('Should update authentication data in Redis', async () => {
      const expectedResult: AuthenticationInterface = {
        token: {
          expirationPolicy: {
            access: {
              length: 1000,
            },
            refresh: {
              length: 1000,
            },
          },
          subject: 'c9b55ef5-a97d-4c9b-8298-b151f8d8442a',
          body: {
            scopes: ['hello'],
          },
        },
        providers: [
          {
            data: {
              username: 'hello',
              password: '<PASSWORD>',
            },
            type: AuthenticationProviderTypeEnum.BASIC,
          },
        ],
      };
      when(
        RedisMock.get(
          'authf:authentication:18195c63-6fd1-4943-ad84-f317b42344aa',
        ),
      ).thenResolve(JSON.stringify(expectedResult));
      when(ConfigMock.authenticationCacheTtl).thenReturn(5);

      await service.patchAuthentication(
        context,
        '18195c63-6fd1-4943-ad84-f317b42344aa',
        expectedResult,
      );

      verify(
        RedisMock.set(
          'authf:authentication:18195c63-6fd1-4943-ad84-f317b42344aa',
          JSON.stringify(expectedResult),
          deepEqual({ EX: 5 }),
        ),
      ).once();
    });
  });

  describe('patchAuthenticationProvider', () => {
    it('should throw error when trying to patch an authentication provider', async () => {
      await expect(async () => {
        await service.patchAuthenticationProvider(
          context,
          '285c2df5-8204-471f-bafa-d98f33cb9eaa',
          {} as any,
        );
      }).rejects.toBeInstanceOf(NotImplementedError);
    });
  });

  describe('deleteAuthentication', () => {
    it('should throw error when trying to delete authentication', async () => {
      await expect(async () => {
        await service.deleteAuthentication(
          context,
          '285c2df5-8204-471f-bafa-d98f33cb9eaa',
        );
      }).rejects.toBeInstanceOf(NotImplementedError);
    });
  });

  describe('updateAuthenticationBlacklistedStatus', () => {
    it('should remove item from blacklist', async () => {
      when(NodeCacheMock.get('authf:blacklist:authentications')).thenResolve([
        '8e8e7c3c-04c9-4028-8599-ae7594167696',
        '95a73836-f0bf-4eb1-bc11-4bcb28f8beab',
      ] as any);

      await service.updateAuthenticationBlacklistedStatus(
        context,
        '95a73836-f0bf-4eb1-bc11-4bcb28f8beab',
        false,
      );

      verify(
        RedisMock.set(
          'authf:blacklist:authentications',
          JSON.stringify(['8e8e7c3c-04c9-4028-8599-ae7594167696']),
        ),
      ).once();
    });

    it('should add item to blacklist', async () => {
      when(NodeCacheMock.get('authf:blacklist:authentications')).thenResolve([
        '8e8e7c3c-04c9-4028-8599-ae7594167696',
      ] as any);

      await service.updateAuthenticationBlacklistedStatus(
        context,
        '95a73836-f0bf-4eb1-bc11-4bcb28f8beab',
        true,
      );

      verify(
        RedisMock.set(
          'authf:blacklist:authentications',
          JSON.stringify([
            '8e8e7c3c-04c9-4028-8599-ae7594167696',
            '95a73836-f0bf-4eb1-bc11-4bcb28f8beab',
          ]),
        ),
      ).once();
    });
  });

  describe('getBlacklistedAuthenticationIds', () => {
    it('should return blacklisted IDs from memory cache', async () => {
      when(NodeCacheMock.get('authf:blacklist:authentications')).thenReturn([
        'authId',
      ]);

      const result = await service.getBlacklistedAuthenticationIds(context);

      expect(result).toEqual(['authId']);
      verify(NodeCacheMock.get('authf:blacklist:authentications')).once();
    });

    it('should fetch blacklisted IDs from Redis if not in memory cache', async () => {
      when(NodeCacheMock.get('authf:blacklist:authentications')).thenReturn(
        null,
      );
      when(RedisMock.get('authf:blacklist:authentications')).thenResolve(
        JSON.stringify(['authId']),
      );

      const result = await service.getBlacklistedAuthenticationIds(context);

      expect(result).toEqual(['authId']);
      verify(NodeCacheMock.get('authf:blacklist:authentications')).once();
      verify(RedisMock.get('authf:blacklist:authentications')).once();
      verify(
        NodeCacheMock.set(
          'authf:blacklist:authentications',
          deepEqual(['authId']),
          5,
        ),
      ).once();
    });

    it('should return null if blacklisted IDs are not in Redis', async () => {
      when(NodeCacheMock.get('authf:blacklist:authentications')).thenReturn(
        null,
      );
      when(RedisMock.get('authf:blacklist:authentications')).thenResolve(null);

      const result = await service.getBlacklistedAuthenticationIds(context);

      expect(result).toBeNull();
    });
  });

  describe('getBlacklistedSubjects', () => {
    it('should return blacklisted subjects from memory cache', async () => {
      when(NodeCacheMock.get('authf:blacklist:subjects')).thenReturn([
        'subject',
      ]);

      const result = await service.getBlacklistedSubjects(context);

      expect(result).toEqual(['subject']);
      verify(NodeCacheMock.get('authf:blacklist:subjects')).once();
    });

    it('should fetch blacklisted subjects from Redis if not in memory cache', async () => {
      when(NodeCacheMock.get('authf:blacklist:subjects')).thenReturn(null);
      when(RedisMock.get('authf:blacklist:subjects')).thenResolve(
        JSON.stringify(['subject']),
      );

      const result = await service.getBlacklistedSubjects(context);

      expect(result).toEqual(['subject']);
      verify(NodeCacheMock.get('authf:blacklist:subjects')).once();
      verify(RedisMock.get('authf:blacklist:subjects')).once();
      verify(
        NodeCacheMock.set(
          'authf:blacklist:subjects',
          deepEqual(['subject']),
          anything(),
        ),
      ).once();
    });

    it('should return null if blacklisted subjects are not in Redis', async () => {
      when(NodeCacheMock.get('authf:blacklist:subjects')).thenReturn(null);
      when(RedisMock.get('authf:blacklist:subjects')).thenResolve(null);

      const result = await service.getBlacklistedSubjects(context);

      expect(result).toBeNull();
    });
  });

  describe('saveAuthenticationBlacklist', () => {
    it('should save blacklist to Redis and memory cache', async () => {
      const blacklist = ['authId'];

      await service.saveAuthenticationBlacklist(context, blacklist);

      verify(
        RedisMock.set(
          'authf:blacklist:authentications',
          JSON.stringify(blacklist),
          deepEqual({ EX: 5 }),
        ),
      ).once();
      verify(
        NodeCacheMock.set('authf:blacklist:authentications', blacklist),
      ).once();
    });
  });

  describe('saveSubjectBlacklist', () => {
    it('should save subjects blacklist to Redis and memory cache', async () => {
      const blacklist = ['subject'];

      await service.saveSubjectBlacklist(context, blacklist);

      verify(
        RedisMock.set(
          'authf:blacklist:subjects',
          JSON.stringify(blacklist),
          deepEqual({
            EX: 5,
          }),
        ),
      ).once();
      verify(NodeCacheMock.set('authf:blacklist:subjects', blacklist)).once();
    });
  });
});
