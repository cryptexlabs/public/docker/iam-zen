import {
  HttpException,
  HttpStatus,
  Inject,
  NotFoundException,
} from '@nestjs/common';
import { AuthfAuthenticationDataServiceInterface } from './authf.authentication.data.service.interface';
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
} from '@cryptexlabs/authf-data-model';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Context, CustomLogger } from '@cryptexlabs/codex-nodejs-common';
import { AuthfAuthenticationUtil } from './authf.authentication.util';
import { IdentifiedProviderFactoryUtil } from '../provider/identified-provider.factory.util';
import { SearchTotalHits } from '@elastic/elasticsearch/lib/api/types';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';
import { UserPersistedInterface } from '../../../user/persistence/user.persisted.interface';

export class AuthfAuthenticationElasticsearchDataService
  implements AuthfAuthenticationDataServiceInterface
{
  public static get DEFAULT_INDEX() {
    return 'authentication';
  }

  constructor(
    private readonly elasticsearchService: ElasticsearchService,
    private readonly elasticsearchPinger: ElasticsearchPinger,
    @Inject('LOGGER') private readonly logger: CustomLogger,
  ) {}

  public async getBlacklistedSubjects(context: Context): Promise<string[]> {
    const response = await this.elasticsearchService.search({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  blacklisted: true,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits
      .map((hit: any) => (hit._source as any).data?.token?.subject)
      .filter((item) => item !== undefined);
  }

  private async _createIndex() {
    await this.elasticsearchService.indices.create({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      body: {},
    });
  }

  private async _getIndexExists() {
    return await this.elasticsearchService.indices.exists({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
    });
  }

  async initializeIndex() {
    await this._waitForReady();

    const exists = await this._getIndexExists();
    if (!exists) {
      await this._createIndex();
    }

    await this.elasticsearchService.indices.putMapping({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      body: {
        properties: {
          authenticationId: {
            type: 'keyword',
          },
          blacklisted: {
            type: 'boolean',
          },
        },
      },
    });
  }

  async _waitForReady() {
    await this.elasticsearchPinger.waitForReady();
  }

  async getAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<AuthenticationInterface> {
    try {
      const response = await this.elasticsearchService.get({
        index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
        id: authenticationId,
        routing: authenticationId,
      });

      // Check if document was found
      if (response.found) {
        const result = response._source as any;

        return result.data as AuthenticationInterface;
      }

      return null;
    } catch (e) {
      // Handle document not found error (404)
      if (e.meta && e.meta.statusCode === 404) {
        return null; // Document not found
      }

      throw e;
    }
  }

  async patchAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void> {
    context.logger.debug(`Authentication: ${JSON.stringify(authentication)}`);

    // Define the Painless script
    const script = `
       // Create a map of new providers by type for quick lookup
        Map newProvidersMap = new HashMap();
        for (def newProvider : params.authentication.providers) {
          newProvidersMap.put(newProvider.type, newProvider);
        }
      
        // Create a new list of providers
        def newProvidersData = new ArrayList();
      
        // Add existing providers if they are not in the new providers map
        for (def existingProvider : ctx._source.data.providers) {
          if (!newProvidersMap.containsKey(existingProvider.type)) {
            newProvidersData.add(existingProvider);
          }
        }
      
        // Add all new providers from the map to the list
        newProvidersData.addAll(newProvidersMap.values());
      
        // Update the providers field
        ctx._source.data.providers = newProvidersData;
      
        // Update other fields
        Map authMap = params.authentication;
        authMap.remove("providers");
        ctx._source.data.putAll(authMap);
    `;

    // Use the 'update' operation with Painless script
    await this.elasticsearchService.update({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      id: authenticationId,
      body: {
        script: {
          source: script,
          lang: 'painless',
          params: {
            authenticationId,
            authentication,
          },
        },
        upsert: {
          authenticationId,
          data: authentication,
        },
      },
    });
  }

  public async createAuthentications(
    context: Context,
    items: {
      authenticationId: string;
      authentication: AuthenticationInterface;
    }[],
  ): Promise<void> {
    // Array to hold bulk operations
    const bulkOperations = [];

    for (const item of items) {
      const { authenticationId, authentication } = item;
      context.logger.debug(
        `Inserting authentication with id: ${authenticationId}`,
        authentication,
      );

      // Push each operation into bulk array
      bulkOperations.push({
        index: {
          _index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
          _id: authenticationId,
          routing: authenticationId,
        },
      });

      bulkOperations.push({
        authenticationId,
        data: authentication,
      });
    }

    // Perform bulk indexing
    if (bulkOperations.length > 0) {
      await this.elasticsearchService.bulk({
        body: bulkOperations,
      });
    }
  }

  public async createAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void> {
    context.logger.debug(`Authentication: ${JSON.stringify(authentication)}`);

    // Use the 'update' operation with Painless script
    await this.elasticsearchService.index({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      id: authenticationId,
      routing: authenticationId,
      body: {
        authenticationId,
        data: authentication,
      },
    });
  }

  public async patchAuthenticationProvider(
    context: Context,
    authenticationId: string,
    provider: AuthenticationTypes,
  ): Promise<void> {
    const authentication = await this.getAuthentication(
      context,
      authenticationId,
    );
    if (!authentication) {
      throw new NotFoundException(
        `Could not find authentication with id: ${authenticationId}`,
      );
    }

    const patchedAuthentication = AuthfAuthenticationUtil.patchProvider(
      authentication,
      provider,
    );

    await this.patchAuthentication(
      context,
      authenticationId,
      patchedAuthentication,
    );
  }

  public async deleteAuthenticationProvider(
    context: Context,
    authenticationId: string,
    type: AuthenticationProviderTypeEnum,
    identifier: string,
    deleteAuthenticationIfProvidersEmpty: boolean,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.search({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  authenticationId,
                },
              },
            ],
          },
        },
      },
    });

    if ((response.hits.total as SearchTotalHits).value === 0) {
      return false;
    }

    const doc = response.hits.hits[0];
    const authentication = (doc._source as any).data;

    const newProviders = authentication.providers.filter((provider) => {
      const identifiedProvider =
        IdentifiedProviderFactoryUtil.getIdentifiedProvider(provider);
      return identifiedProvider.getIdentifier() !== identifier;
    });

    const newAuthentication = {
      ...authentication,
      providers: newProviders,
    };

    if (newProviders.length === 0 && deleteAuthenticationIfProvidersEmpty) {
      await this.elasticsearchService.delete({
        index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
        id: doc._id,
      });
    } else {
      await this.elasticsearchService.update({
        index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
        id: doc._id,
        body: {
          doc: {
            authenticationId,
            data: newAuthentication,
          },
        },
      });
    }

    return true;
  }

  public async deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.deleteByQuery({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      query: {
        bool: {
          must: [
            {
              term: {
                authenticationId,
              },
            },
          ],
        },
      },
      max_docs: 1,
    });
    return response.deleted === 1;
  }

  public async updateAuthenticationBlacklistedStatus(
    context: Context,
    authenticationId: string,
    blacklisted: boolean,
  ): Promise<boolean> {
    const response = await this.elasticsearchService.updateByQuery({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      conflicts: 'proceed',
      body: {
        script: {
          lang: 'painless',
          source: 'ctx._source.blacklisted = params.blacklisted',
          params: {
            blacklisted,
          },
        },
        query: {
          match: {
            authenticationId,
          },
        },
        max_docs: 1,
      },
    });

    return (
      response.updated === 1 ||
      response.noops === 1 ||
      response.version_conflicts === 1
    );
  }

  public async getBlacklistedAuthenticationIds(
    context: Context,
  ): Promise<string[]> {
    const response = await this.elasticsearchService.search({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  blacklisted: true,
                },
              },
            ],
          },
        },
      },
    });

    return response.hits.hits.map(
      (hit: any) => (hit._source as any).authenticationId,
    );
  }

  public async authenticationExists(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    return await this.elasticsearchService.exists({
      index: AuthfAuthenticationElasticsearchDataService.DEFAULT_INDEX,
      id: authenticationId,
      routing: authenticationId,
    });
  }
}
