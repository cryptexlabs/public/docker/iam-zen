import { Inject, Injectable } from '@nestjs/common';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { AuthfAuthenticationDataServiceInterface } from './authf.authentication.data.service.interface';
import { AuthfAuthenticationElasticsearchDataService } from './authf.authentication.elasticsearch.data.service';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { AuthfConfig } from '../authf.config';
import { CustomLogger } from '@cryptexlabs/codex-nodejs-common';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';

@Injectable()
export class AuthfAuthenticationDataServiceFactory {
  constructor(
    @Inject('CONFIG') private readonly config: AuthfConfig,
    @Inject('LOGGER') private readonly logger: CustomLogger,
  ) {}

  public async getPersistenceService(
    databaseType: DatabaseTypeEnum,
  ): Promise<AuthfAuthenticationDataServiceInterface> {
    if (databaseType === DatabaseTypeEnum.ELASTICSEARCH) {
      return this._getElasticsearchDatabaseService();
    }

    throw new Error(
      `Invalid database type: ${databaseType} from env var 'DATABASE_TYPE'`,
    );
  }

  private async _getElasticsearchDatabaseService(): Promise<AuthfAuthenticationElasticsearchDataService> {
    const elasticsearchService = new ElasticsearchService({
      node: this.config.elasticsearch.url,
    });
    const elasticsearchPinger = new ElasticsearchPinger(
      elasticsearchService,
      this.logger,
    );

    const service = new AuthfAuthenticationElasticsearchDataService(
      elasticsearchService,
      elasticsearchPinger,
      this.logger,
    );

    await service.initializeIndex();

    return service;
  }
}
