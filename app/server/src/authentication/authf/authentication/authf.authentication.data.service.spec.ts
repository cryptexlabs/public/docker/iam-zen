import { AuthfAuthenticationCacheService } from './authf.authentication.cache.service';
import { AuthfAuthenticationDataService } from './authf.authentication.data.service';
import { AuthfAuthenticationDataServiceInterface } from './authf.authentication.data.service.interface';
import { Context, ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
} from '@cryptexlabs/authf-data-model';
import { anything, deepEqual, instance, mock, verify, when } from 'ts-mockito';
import { AuthfConfig } from '../authf.config';

describe('AuthenticationDataService', () => {
  let service: AuthfAuthenticationDataService;
  let PersistenceMock: AuthfAuthenticationDataServiceInterface;
  let CacheMock: AuthfAuthenticationCacheService;
  let ContextMock: Context;
  let context: Context;
  let ConfigMock: AuthfConfig;
  let ContextBuilderMock: ContextBuilder;

  beforeEach(() => {
    PersistenceMock = mock<AuthfAuthenticationDataServiceInterface>();
    CacheMock = mock(AuthfAuthenticationCacheService);
    ContextMock = mock(Context);
    ConfigMock = mock(AuthfConfig);
    ContextBuilderMock = mock(ContextBuilder);
    when(ContextBuilderMock.build()).thenReturn(instance(ContextBuilderMock));
    when(ContextBuilderMock.getResult()).thenReturn(instance(ContextMock));

    when(ContextMock.logger).thenReturn({
      debug: () => {},
      error: () => {},
    } as any);
    context = instance(ContextMock);
    service = new AuthfAuthenticationDataService(
      instance(PersistenceMock),
      instance(CacheMock),
      instance(ConfigMock),
      instance(ContextBuilderMock),
    );
  });

  describe('getAuthentication', () => {
    it('should return authentication data from cache if available', async () => {
      const mockedAuth = { id: 'authId' } as unknown as AuthenticationInterface;
      when(CacheMock.getAuthentication(context, 'authId')).thenResolve(
        mockedAuth,
      );

      const result = await service.getAuthentication(context, 'authId');

      expect(result).toEqual(mockedAuth);
      verify(CacheMock.getAuthentication(context, 'authId')).once();
      verify(PersistenceMock.getAuthentication(anything(), anything())).never();
    });

    it('should return authentication data from persistence if not in cache', async () => {
      const mockedAuth = { id: 'authId' } as unknown as AuthenticationInterface;
      when(CacheMock.getAuthentication(context, 'authId')).thenResolve(null);
      when(PersistenceMock.getAuthentication(context, 'authId')).thenResolve(
        mockedAuth,
      );

      const result = await service.getAuthentication(context, 'authId');

      expect(result).toEqual(mockedAuth);
      verify(CacheMock.getAuthentication(context, 'authId')).once();
      verify(PersistenceMock.getAuthentication(context, 'authId')).once();
    });
  });

  describe('patchAuthentication', () => {
    it('should call persistence and cache services to patch authentication', async () => {
      const authentication: AuthenticationInterface = { providers: [] } as any;

      when(
        PersistenceMock.patchAuthentication(anything(), anything(), anything()),
      ).thenResolve(new Promise((resolve) => {}) as any);

      await service.patchAuthentication(context, 'authId', authentication);

      verify(
        PersistenceMock.patchAuthentication(
          context,
          'authId',
          deepEqual(authentication),
        ),
      ).never();
      verify(
        CacheMock.patchAuthentication(
          context,
          'authId',
          deepEqual(authentication),
        ),
      ).once();
    });
  });

  describe('getBlacklistedAuthenticationIds', () => {
    it('should return blacklisted IDs from cache if available', async () => {
      const blacklistedIds = ['authId'];
      when(CacheMock.getBlacklistedAuthenticationIds(context)).thenResolve(
        blacklistedIds,
      );

      const result = await service.getBlacklistedAuthenticationIds(context);

      expect(result).toEqual(blacklistedIds);
      verify(CacheMock.getBlacklistedAuthenticationIds(context)).once();
      verify(
        PersistenceMock.getBlacklistedAuthenticationIds(anything()),
      ).never();
    });

    it('should fetch blacklisted IDs from persistence and save to cache if not in cache', async () => {
      const blacklistedIds = ['authId'];
      when(CacheMock.getBlacklistedAuthenticationIds(context)).thenResolve(
        null,
      );
      when(
        PersistenceMock.getBlacklistedAuthenticationIds(context),
      ).thenResolve(blacklistedIds);

      const result = await service.getBlacklistedAuthenticationIds(context);

      expect(result).toEqual(blacklistedIds);
      verify(CacheMock.getBlacklistedAuthenticationIds(context)).once();
      verify(PersistenceMock.getBlacklistedAuthenticationIds(context)).once();
      verify(
        CacheMock.saveAuthenticationBlacklist(context, blacklistedIds),
      ).once();
    });
  });

  describe('getBlacklistedSubjects', () => {
    it('should return blacklisted subjects from cache if available', async () => {
      const blacklistedSubjects = ['subject'];
      when(CacheMock.getBlacklistedSubjects(context)).thenResolve(
        blacklistedSubjects,
      );

      const result = await service.getBlacklistedSubjects(context);

      expect(result).toEqual(blacklistedSubjects);
      verify(CacheMock.getBlacklistedSubjects(context)).once();
      verify(PersistenceMock.getBlacklistedSubjects(anything())).never();
    });

    it('should fetch blacklisted subjects from persistence and save to cache if not in cache', async () => {
      const blacklistedSubjects = ['subject'];
      when(CacheMock.getBlacklistedSubjects(context)).thenResolve(null);
      when(PersistenceMock.getBlacklistedSubjects(context)).thenResolve(
        blacklistedSubjects,
      );

      const result = await service.getBlacklistedSubjects(context);

      expect(result).toEqual(blacklistedSubjects);
      verify(CacheMock.getBlacklistedSubjects(context)).once();
      verify(PersistenceMock.getBlacklistedSubjects(context)).once();
      verify(
        CacheMock.saveSubjectBlacklist(context, blacklistedSubjects),
      ).once();
    });
  });

  describe('updateAuthenticationBlacklistedStatus', () => {
    it('should call both cache and persistence services to update blacklisted status', async () => {
      await service.updateAuthenticationBlacklistedStatus(
        context,
        'authId',
        true,
      );

      verify(
        CacheMock.updateAuthenticationBlacklistedStatus(
          context,
          'authId',
          true,
        ),
      ).once();
      verify(
        PersistenceMock.updateAuthenticationBlacklistedStatus(
          context,
          'authId',
          true,
        ),
      ).once();
    });
  });

  describe('deleteAuthenticationProvider', () => {
    it('should call the persistence service to delete the authentication provider', async () => {
      await service.deleteAuthenticationProvider(
        context,
        'authId',
        AuthenticationProviderTypeEnum.API,
        'hello',
        false,
      );

      verify(
        PersistenceMock.deleteAuthenticationProvider(
          context,
          'authId',
          AuthenticationProviderTypeEnum.API,
          'hello',
          false,
        ),
      ).once();
    });
  });

  describe('patchAuthenticationProvider', () => {
    it('should call the persistence service to patch the authentication provider', async () => {
      const provider = {
        type: AuthenticationProviderTypeEnum.API,
        data: {
          apiKey: 'hello',
          secret: 'world',
        },
      };
      await service.patchAuthenticationProvider(
        context,
        'authId',
        provider as any,
      );

      verify(
        PersistenceMock.patchAuthenticationProvider(
          context,
          'authId',
          deepEqual(provider) as any,
        ),
      ).once();
    });
  });

  describe('deleteAuthentication', () => {
    it('should call the persistence service to delete the authentication', async () => {
      await service.deleteAuthentication(context, 'authId');

      verify(PersistenceMock.deleteAuthentication(context, 'authId')).once();
    });
  });
});
