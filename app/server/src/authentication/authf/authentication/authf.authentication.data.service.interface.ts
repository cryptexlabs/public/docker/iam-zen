import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
} from '@cryptexlabs/authf-data-model';
import { Context } from '@cryptexlabs/codex-nodejs-common';

export interface AuthfAuthenticationDataServiceInterface {
  patchAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void>;

  authenticationExists(
    context: Context,
    authenticationId: string,
  ): Promise<boolean>;

  createAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void>;

  createAuthentications(
    context: Context,
    items: {
      authenticationId: string;
      authentication: AuthenticationInterface;
    }[],
  ): Promise<void>;

  patchAuthenticationProvider(
    context: Context,
    authenticationId: string,
    provider: AuthenticationTypes,
  ): Promise<void>;

  getAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<AuthenticationInterface | null>;

  deleteAuthenticationProvider(
    context: Context,
    authenticationId: string,
    type: AuthenticationProviderTypeEnum,
    identifier: string,
    deleteAuthenticationIfProvidersEmpty: boolean,
  ): Promise<boolean>;

  deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<boolean>;

  updateAuthenticationBlacklistedStatus(
    context: Context,
    authenticationId: string,
    blacklisted: boolean,
  ): Promise<boolean>;

  getBlacklistedAuthenticationIds(context: Context): Promise<string[]>;

  getBlacklistedSubjects(context: Context): Promise<string[]>;
}
