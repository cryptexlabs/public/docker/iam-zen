import { Inject, Injectable } from '@nestjs/common';
import { AuthfAuthenticationDataServiceInterface } from './authf.authentication.data.service.interface';
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
} from '@cryptexlabs/authf-data-model';
import { Context, ContextBuilder } from '@cryptexlabs/codex-nodejs-common';
import { AuthfAuthenticationCacheService } from './authf.authentication.cache.service';
import { AuthfConfig } from '../authf.config';
import { AuthfAuthenticationUtil } from './authf.authentication.util';

@Injectable()
export class AuthfAuthenticationDataService
  implements AuthfAuthenticationDataServiceInterface
{
  private _batchCreateAuthentications: {
    authenticationId: string;
    authentication: AuthenticationInterface;
  }[] = [];

  constructor(
    @Inject('AUTHENTICATION_PERSISTENCE_SERVICE')
    private readonly persistenceService: AuthfAuthenticationDataServiceInterface,
    private readonly authenticationCacheService: AuthfAuthenticationCacheService,
    @Inject('CONFIG') private readonly config: AuthfConfig,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
  ) {
    if (
      config.environmentName &&
      !['test', 'ci', 'e2e-test'].includes(config.environmentName)
    ) {
      this._startBatchProcessor().then();
    }
  }

  private async _startBatchProcessor() {
    const context = this.contextBuilder.build().getResult();

    setInterval(async () => {
      try {
        if (this._batchCreateAuthentications.length > 0) {
          context.logger.debug(
            `Creating ${this._batchCreateAuthentications.length} authentications`,
            this._batchCreateAuthentications.map((a) => a.authenticationId),
          );
          const authentications = [...this._batchCreateAuthentications];
          this._batchCreateAuthentications = [];
          await this.persistenceService.createAuthentications(
            context,
            authentications,
          );
        }
      } catch (e) {
        context.logger.error(`Failed to create authentications: ${e.message}`);
      }
    }, this.config.createAuthenticationInterval);
  }

  public async deleteAuthenticationProvider(
    context: Context,
    authenticationId: string,
    type: AuthenticationProviderTypeEnum,
    identifier: string,
    deleteAuthenticationIfProvidersEmpty: boolean,
  ): Promise<boolean> {
    return await this.persistenceService.deleteAuthenticationProvider(
      context,
      authenticationId,
      type,
      identifier,
      deleteAuthenticationIfProvidersEmpty,
    );
  }

  public async getAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<AuthenticationInterface | null> {
    const cachedAuthentication =
      await this.authenticationCacheService.getAuthentication(
        context,
        authenticationId,
      );
    if (cachedAuthentication) {
      context.logger.debug(
        `Got authentication ${authenticationId} from cache`,
        cachedAuthentication,
      );
      return cachedAuthentication;
    }

    return await this.persistenceService.getAuthentication(
      context,
      authenticationId,
    );
  }

  public async patchAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void> {
    let useAuthentication = authentication;

    if (authentication.providers.length === 0) {
      // If the providers are empty we cannot just use given authentication
      // because there are flows that simply update the permissions only with this endpoint
      // And then subsequently request the same authentication which would then have no providers in the cache
      const existingAuthentication = await this.getAuthentication(
        context,
        authenticationId,
      );

      if (existingAuthentication) {
        useAuthentication = AuthfAuthenticationUtil.patchAuthentication(
          existingAuthentication,
          authentication,
        );
      }
    }

    setTimeout(async () => {
      await this.persistenceService
        .patchAuthentication(context, authenticationId, authentication)
        .then()
        .catch((e) => context.logger.error(e));
    });

    await this.authenticationCacheService.patchAuthentication(
      context,
      authenticationId,
      useAuthentication,
    );
  }

  public async patchAuthenticationProvider(
    context: Context,
    authenticationId: string,
    provider: AuthenticationTypes,
  ): Promise<void> {
    return await this.persistenceService.patchAuthenticationProvider(
      context,
      authenticationId,
      provider,
    );
  }

  public deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    return this.persistenceService.deleteAuthentication(
      context,
      authenticationId,
    );
  }

  public async updateAuthenticationBlacklistedStatus(
    context: Context,
    authenticationId: string,
    blacklisted: boolean,
  ): Promise<boolean> {
    await this.authenticationCacheService.updateAuthenticationBlacklistedStatus(
      context,
      authenticationId,
      blacklisted,
    );
    return await this.persistenceService.updateAuthenticationBlacklistedStatus(
      context,
      authenticationId,
      blacklisted,
    );
  }

  public async getBlacklistedAuthenticationIds(
    context: Context,
  ): Promise<string[]> {
    const cachedBlacklist =
      await this.authenticationCacheService.getBlacklistedAuthenticationIds(
        context,
      );
    if (cachedBlacklist !== null) {
      return cachedBlacklist;
    }

    const blacklist =
      await this.persistenceService.getBlacklistedAuthenticationIds(context);

    await this.authenticationCacheService.saveAuthenticationBlacklist(
      context,
      blacklist,
    );

    return blacklist;
  }

  public async getBlacklistedSubjects(context: Context): Promise<string[]> {
    const cachedBlacklist =
      await this.authenticationCacheService.getBlacklistedSubjects(context);
    if (cachedBlacklist !== null) {
      return cachedBlacklist;
    }

    const blacklist = await this.persistenceService.getBlacklistedSubjects(
      context,
    );

    await this.authenticationCacheService.saveSubjectBlacklist(
      context,
      blacklist,
    );

    return blacklist;
  }

  public async createAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void> {
    this._batchCreateAuthentications.push({ authenticationId, authentication });

    await this.authenticationCacheService.createAuthentication(
      context,
      authenticationId,
      authentication,
    );
  }

  public async createAuthentications(
    context: Context,
    items: {
      authenticationId: string;
      authentication: AuthenticationInterface;
    }[],
  ): Promise<void> {
    throw new Error(`Method not implemented`);
  }

  public async authenticationExists(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    return this.persistenceService.authenticationExists(
      context,
      authenticationId,
    );
  }
}
