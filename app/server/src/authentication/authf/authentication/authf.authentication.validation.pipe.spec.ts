import { AuthfAuthenticationValidationPipe } from './authf.authentication.validation.pipe';

describe('AuthenticationValidationPipe', () => {
  const authenticationValidationPipe = new AuthfAuthenticationValidationPipe();
  it('Should validate a basic authentication', () => {
    const json = `
            {
              "providers": [
                {
                  "data": {
                    "username": "johndoe",
                    "password": "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3"
                  },
                  "type": "basic"
                }
              ],
              "token": {
                "expirationPolicy": {
                  "access": {
                    "length": 900
                  },
                  "refresh": {
                    "length": 2592000
                  }
                },
                "subject": "4ea27c6e-57b1-4cf7-822b-f3db8171a8bd",
                "body": {
                  "scopes": [
                    "user:self:all"
                  ]
                }
              }
            }
        `;
    const body = JSON.parse(json);
    authenticationValidationPipe.transform(body, null);
  });

  it('Should validate an API authentication', () => {
    const json = `
            {
              "providers": [
                {
                  "data": {
                    "apiKey": "johndoe",
                    "secret": "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3"
                  },
                  "type": "api"
                }
              ],
              "token": {
                "expirationPolicy": {
                  "access": {
                    "length": 900
                  },
                  "refresh": {
                    "length": 2592000
                  }
                },
                "subject": "4ea27c6e-57b1-4cf7-822b-f3db8171a8bd",
                "body": {
                  "scopes": [
                    "user:self:all"
                  ]
                }
              }
            }
        `;
    const body = JSON.parse(json);
    authenticationValidationPipe.transform(body, null);
  });

  it('Should invalidate an invalid authentication', () => {
    const json = `
            {
              "providers": [
                {
                  "data": {
                    "username": "johndoe",
                    "password": "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3"
                  },
                  "type": "basic"
                }
              ],
              "token": {
                "expirationPolicy": {
                  "access": {
                    "length": 900
                  },
                  "refresh": {
                    "length": 2592000
                  }
                },
                "body": {
                  "scopes": [
                    "user:self:all"
                  ]
                }
              }
            }
        `;
    const body = JSON.parse(json);
    expect(() => {
      authenticationValidationPipe.transform(body, null);
    }).toThrow();
  });
});
