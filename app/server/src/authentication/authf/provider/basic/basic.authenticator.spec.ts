import { BasicAuthenticator } from './basic.authenticator';
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
} from '@cryptexlabs/authf-data-model';
import { HttpStatus } from '@nestjs/common';
import {
  Context,
  ContextualHttpException,
} from '@cryptexlabs/codex-nodejs-common';
import { instance, mock, when } from 'ts-mockito';
import { AuthfConfig } from '../../authf.config';

describe('BasicAuthenticator', () => {
  let ConfigMock: AuthfConfig;
  let config: AuthfConfig;

  beforeEach(() => {
    ConfigMock = mock(AuthfConfig);
    config = instance(ConfigMock);
  });

  it('Should match a username and password', async () => {
    const authenticator = new BasicAuthenticator(
      {
        username: 'johndoe',
        password:
          'b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3',
      },
      config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.BASIC,
          data: {
            username: 'asdf',
            password: '1234',
          },
        },
        {
          type: AuthenticationProviderTypeEnum.BASIC,
          data: {
            username: 'johndoe',
            password:
              '673654ebb29719a24810ec69eeaf5b2347bea9ef29648c02f6d214e31b5aad70',
          },
        },
      ],
      token: undefined,
    };

    const MockContext = mock<Context>(Context);
    when(MockContext.logger).thenReturn({ debug: () => {} } as any);

    await authenticator.authenticate(instance(MockContext), authentication);
  });

  it('Should throw an exception if a username and password is not matched', async () => {
    const authenticator = new BasicAuthenticator(
      {
        username: 'johndoe',
        password:
          'b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3',
      },
      config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.BASIC,
          data: {
            username: 'johndoe',
            password: 'asdf',
          },
        },
      ],
      token: undefined,
    };

    const MockContext = mock<Context>(Context);
    const context = instance(MockContext);
    when(MockContext.logger).thenReturn({ debug: () => {} } as any);

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        'Unauthorized',
        HttpStatus.UNAUTHORIZED,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.UNAUTHORIZED);
    }
  });
});
