import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  BasicAuthAuthenticateDataInterface,
  BasicAuthAuthenticationProviderInterface,
} from '@cryptexlabs/authf-data-model';
import { HttpException, HttpStatus } from '@nestjs/common';
import * as crypto from 'crypto';
import {
  Context,
  ContextualHttpException,
} from '@cryptexlabs/codex-nodejs-common';
import { AuthenticatorInterface } from '../../authenticator/authenticator.interface';
import { AuthfConfig } from '../../authf.config';

export class BasicAuthenticator implements AuthenticatorInterface {
  constructor(
    private readonly data: BasicAuthAuthenticateDataInterface,
    private readonly config: AuthfConfig,
  ) {}

  async authenticate(
    context: Context,
    authentication: AuthenticationInterface,
  ): Promise<null> {
    context.logger.debug(
      `Authentication for Basic Authentication`,
      authentication,
    );
    const providers: BasicAuthAuthenticationProviderInterface[] =
      authentication.providers.filter((provider) => {
        return provider.type === AuthenticationProviderTypeEnum.BASIC;
      }) as BasicAuthAuthenticationProviderInterface[];

    for (const provider of providers) {
      if (provider.data.username === this.data.username) {
        const expectedPassword = crypto
          .createHash('sha256')
          .update(this.data.password)
          .digest('hex');
        if (expectedPassword === provider.data.password) {
          return null;
        }
      }
    }
    throw new ContextualHttpException(
      context,
      'Unauthorized',
      HttpStatus.UNAUTHORIZED,
    );
  }
}
