import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  EmailTypeEnum,
  SamsungLegacyAuthenticateDataInterface,
  SamsungLegacyAuthenticationProviderDataInterface,
  SamsungLegacyAuthenticationProviderInterface,
  samsungLegacyTimezoneMap,
  UserInterface,
} from '@cryptexlabs/authf-data-model';
import { HttpStatus } from '@nestjs/common';
import { AuthenticatorInterface } from '../../authenticator/authenticator.interface';
import axios from 'axios';
import * as qs from 'querystring';
import * as _ from 'lodash';
import {
  Context,
  ContextualHttpException,
} from '@cryptexlabs/codex-nodejs-common';
import * as xmlParser from 'fast-xml-parser';
import { AuthfConfig } from '../../authf.config';

export class SamsungLegacyAuthenticator implements AuthenticatorInterface {
  constructor(
    private readonly data: SamsungLegacyAuthenticateDataInterface,
    private readonly config: AuthfConfig,
  ) {}

  async authenticate<T extends UserInterface | void = UserInterface>(
    context: Context,
    authentication: AuthenticationInterface,
  ): Promise<T> {
    const providers: SamsungLegacyAuthenticationProviderInterface[] =
      authentication.providers.filter((provider) => {
        return provider.type === AuthenticationProviderTypeEnum.SAMSUNG_LEGACY;
      }) as SamsungLegacyAuthenticationProviderInterface[];

    for (const provider of providers) {
      if (this.config.samsung.authorizeAccessToken) {
        await this._authorizeAccessToken(context, provider.data);
      }

      return (await this._getUser(context, provider.data)) as T;
    }

    throw new ContextualHttpException(
      context,
      'Unauthorized',
      HttpStatus.UNAUTHORIZED,
    );
  }

  async _getUser(
    context: Context,
    providerData: SamsungLegacyAuthenticationProviderDataInterface,
  ): Promise<UserInterface> {
    const requestUrl = `https://${this.data.apiServerUrl}/v2/profile/user/user/${providerData.userId}`;
    try {
      const response = await axios.get(requestUrl, {
        headers: {
          Authorization: `Bearer ${this.data.accessToken}`,
          'x-osp-appId': providerData.appId,
          'x-osp-userId': providerData.userId,
        },
        responseType: 'text',
      });

      if (xmlParser.validate(response.data) !== true) {
        throw new ContextualHttpException(
          context,
          'Invalid profile data from Samsung Account',
          HttpStatus.INTERNAL_SERVER_ERROR,
        );
      }

      const profileResults = xmlParser.parse(response.data);

      const userIdentificationVOs = profileResults.UserVO.userIdentificationVO
        .length
        ? profileResults.UserVO.userIdentificationVO
        : [profileResults.UserVO.userIdentificationVO];
      const emailIdentificationVO = userIdentificationVOs.find(
        (t) => t.loginIDTypeCode === 3,
      );
      const primaryEmail = emailIdentificationVO
        ? emailIdentificationVO.loginID
        : null;

      let username = _.get(profileResults, 'UserVO.userBaseVO.userName');
      if (!username) {
        username = primaryEmail;
      }

      return {
        username: `samsung:${username}`,
        profile: {
          name: {
            first: _.get(
              profileResults,
              'UserVO.userBaseVO.userBaseIndividualVO.givenName',
            ),
            middle: null,
            last: _.get(
              profileResults,
              'UserVO.userBaseVO.userBaseIndividualVO.familyName',
            ),
            display: _.get(profileResults, 'UserVO.userBaseVO.userDisplayName'),
          },
          email: {
            primary: {
              value: primaryEmail,
              type: EmailTypeEnum.UNKNOWN,
            },
            other: [],
          },
          phone: {
            primary: null,
            other: [],
          },
          photo: {
            primary: null,
            other: [],
          },
        },
        timezone: this._convertTimezone(
          _.get(profileResults, 'UserVO.userBaseVO.userLocalTimezoneCode'),
        ),
      };
    } catch (e) {
      context.logger.error('Error response');
      context.logger.error(e.response.data);
      if (
        e.response &&
        e.response.status &&
        (e.response.status === HttpStatus.UNAUTHORIZED ||
          e.response.status === HttpStatus.FORBIDDEN)
      ) {
        throw new ContextualHttpException(
          context,
          'Error getting samsung account user profile',
          HttpStatus.UNAUTHORIZED,
        );
      } else {
        throw e;
      }
    }
  }

  _convertTimezone(saTimezone) {
    return samsungLegacyTimezoneMap[saTimezone];
  }

  async _authorizeAccessToken(
    context: Context,
    providerData: SamsungLegacyAuthenticationProviderDataInterface,
  ): Promise<void> {
    const requestUrl = `https://${this.data.apiServerUrl}/v2/license/security/authorizeToken`;
    const basicAuthToken = Buffer.from(
      `${providerData.appId}:${providerData.appSecret}`,
    ).toString('base64');
    let response;

    try {
      response = await axios.post(
        requestUrl,
        qs.stringify({ authToken: this.data.accessToken }),
        {
          headers: {
            'content-type': 'application/x-www-form-urlencoded',
            'x-osp-appId': providerData.appId,
            Authorization: `Basic ${basicAuthToken}`,
          },
        },
      );
    } catch (e) {
      context.logger.error('Error response');
      if (e.response) {
        context.logger.error(e.response.status);
        context.logger.error(e.response.data);
        const results = xmlParser.parse(e.response.data);
        const errorCode = _.get(results, 'error.code');
        const errorMessage = _.get(results, 'error.message');
        if (e.response.status === HttpStatus.BAD_REQUEST) {
          if (errorCode === 'LIC_4102') {
            throw new ContextualHttpException(
              context,
              'Token invalid or expired',
              HttpStatus.UNAUTHORIZED,
            );
          } else {
            throw new ContextualHttpException(
              context,
              errorMessage,
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }
        }
        if (
          e.response.status === HttpStatus.UNAUTHORIZED ||
          e.response.status === HttpStatus.FORBIDDEN
        ) {
          let message = 'Error authorizing access token';
          if (errorCode === 'ACF_0403') {
            message = 'Invalid access token';
          }
          throw new ContextualHttpException(
            context,
            message,
            HttpStatus.UNAUTHORIZED,
          );
        }
      }
      if (e.code === 'ENOTFOUND') {
        throw new ContextualHttpException(
          context,
          'Incorrect Account information provided',
          HttpStatus.BAD_REQUEST,
        );
      }
      throw e;
    }

    context.logger.debug(`authorize token results: ${response.data}`);

    if (xmlParser.validate(response.data) !== true) {
      throw new ContextualHttpException(
        context,
        'Invalid authentication response data from Samsung Account.',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
