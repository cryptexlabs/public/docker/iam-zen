export interface SamsungLegacyTokenAuthorizationResultInterface {
  authorizationCode: string;
  userId: string;
}
