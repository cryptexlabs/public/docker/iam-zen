import * as Joi from 'joi';
import { AuthenticationProviderTypeEnum } from '@cryptexlabs/authf-data-model';

export const samsungLegacyAuthenticationProviderSchema = Joi.object({
  type: Joi.string()
    .valid(AuthenticationProviderTypeEnum.SAMSUNG_LEGACY)
    .required(),
  data: Joi.object({
    appId: Joi.string().required(),
    appSecret: Joi.string().required(),
    userId: Joi.string().required(),
  }).required(),
});
