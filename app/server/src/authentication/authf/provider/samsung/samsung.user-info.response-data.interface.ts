export interface SamsungUserInfoResponseDataInterface {
  sub: string;

  family_name?: string;

  given_name?: string;

  nickname?: string;

  /**
   * Format: 20050203
   */
  birthdate?: string;

  user_status_code: string;

  locale?: string;

  picture?: string;

  preferred_username?: string;

  email?: string;

  email_verified?: boolean;

  phone_number?: string;

  phone_number_verified?: boolean;

  country_code: string;
}
