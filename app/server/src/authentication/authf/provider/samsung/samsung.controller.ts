import {
  Controller,
  Get,
  Inject,
  Param,
  ParseIntPipe,
  Query,
  Request,
  Response,
} from '@nestjs/common';
import { ApiParam, ApiQuery, ApiTags } from '@nestjs/swagger';
import { SamsungService } from './samsung.service';
import { ContextBuilder, CustomLogger } from '@cryptexlabs/codex-nodejs-common';
import { AuthfConfig } from '../../authf.config';

@Controller(`oauth/v2/authentication`)
@ApiTags('samsung', 'oauth')
export class SamsungController {
  constructor(
    private readonly samsungService: SamsungService,
    @Inject('CONTEXT_BUILDER') private readonly contextBuilder: ContextBuilder,
    @Inject('LOGGER') private readonly logger: CustomLogger,
    @Inject('CONFIG') private readonly config: AuthfConfig,
  ) {}

  @Get('samsung/:clientId')
  @ApiQuery({
    name: 'code',
    example: 'j259asdl49KAsdk5Hd9asdf',
  })
  @ApiQuery({
    name: 'code_expires_in',
    example: '300',
  })
  @ApiQuery({
    name: 'state',
    example: 'asjf839ada',
  })
  @ApiParam({
    name: 'clientId',
    example: 'asjf839ada',
  })
  public async handleRedirect(
    @Query('code') code: string,
    @Query('code_expires_in', ParseIntPipe) codeExpiresIn: number,
    @Query('state') state: string,
    @Param('clientId') clientId: string,
    @Request() req: any,
    @Response() res: any,
  ) {
    const protocol = req.protocol + (this.config.httpSecure ? 's' : '');
    const redirectUri = protocol + '://' + req.headers.host + req.path;

    this.logger.debug(`computed redirect URI: ${redirectUri}`);

    const context = this.contextBuilder.build().getResult();

    await this.samsungService.handleOauth2Redirect(
      context,
      res,
      req,
      redirectUri,
      code,
      clientId,
      state,
    );
  }
}
