import { Context } from '@cryptexlabs/codex-nodejs-common';
import { AuthenticatorInterface } from '../../authenticator/authenticator.interface';
import {
  SamsungAnyAuthenticateDataInterface,
  UserInterface,
} from '@cryptexlabs/authf-data-model';
import { AuthfConfig } from '../../authf.config';
import { SamsungApiUtil } from './samsung.api.util';

export class SamsungAnyAuthenticator implements AuthenticatorInterface {
  constructor(
    private readonly data: SamsungAnyAuthenticateDataInterface,
    private readonly config: AuthfConfig,
  ) {}

  public async authenticate<T extends UserInterface | void = UserInterface>(
    context: Context,
  ): Promise<T> {
    return (await SamsungApiUtil.getUser(
      context,
      this.data.clientId,
      this.data.accessToken,
      this.data.userId,
      this.data.apiBaseUrl,
    )) as T;
  }
}
