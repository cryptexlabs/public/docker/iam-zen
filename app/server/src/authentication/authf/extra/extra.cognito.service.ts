import { Inject, Injectable } from '@nestjs/common';
import { AuthfConfig } from '../authf.config';
import {
  CognitoIdentityClient,
  GetOpenIdTokenForDeveloperIdentityCommand,
  GetOpenIdTokenForDeveloperIdentityResponse,
} from '@aws-sdk/client-cognito-identity';
import { Context, CustomLogger } from '@cryptexlabs/codex-nodejs-common';
import {
  AuthenticationInterface,
  CognitoExtraInputInterface,
  CognitoExtraResultInterface,
} from '@cryptexlabs/authf-data-model';

@Injectable()
export class ExtraCognitoService {
  constructor(
    @Inject('LOGGER') private readonly logger: CustomLogger,
    @Inject('CONFIG') private readonly config: AuthfConfig,
  ) {}

  public async getLoginExtra(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
    input: CognitoExtraInputInterface,
  ): Promise<CognitoExtraResultInterface | undefined> {
    this.logger.debug(
      `ExtraCognitoService.getLoginExtra.input: ${JSON.stringify(input)}`,
    );
    let principalTags: Record<string, any> = {
      sub: authentication.token.subject,
    };

    if (authentication.token.body.scopes) {
      principalTags = {
        ...principalTags,
        /**
         * Does not work
         * @see https://github.com/aws/aws-sdk-js-v3/issues/2831
         */
        // scopes: authentication.token.body.scopes,
      };
    }

    const command = new GetOpenIdTokenForDeveloperIdentityCommand({
      IdentityPoolId: input.poolId,
      PrincipalTags: principalTags,
      Logins: {
        [input.developerProviderName]: authenticationId,
      },
    });

    const client: CognitoIdentityClient = new CognitoIdentityClient({
      region: input.region,
    });

    const openIdTokenResponse: GetOpenIdTokenForDeveloperIdentityResponse =
      await client.send(command);
    this.logger.debug(
      `ExtraCognitoService.openIdTokenResponse.openIdTokenResponse: ${JSON.stringify(
        openIdTokenResponse,
      )}`,
    );

    return {
      identityId: openIdTokenResponse.IdentityId,
      token: openIdTokenResponse.Token,
      region: input.region,
      label: '',
    };
  }
}
