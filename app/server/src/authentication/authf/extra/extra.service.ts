import {
  AuthenticationInterface,
  ExtraResultType,
  ExtraTypeEnum,
} from '@cryptexlabs/authf-data-model';
import { Injectable } from '@nestjs/common';
import { ExtraCognitoService } from './extra.cognito.service';
import { Context } from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class ExtraService {
  constructor(private readonly cognitoExtraService: ExtraCognitoService) {}

  public async getExtras(
    context: Context,
    authentication: AuthenticationInterface,
    authenticationId: string,
  ): Promise<Record<ExtraTypeEnum, ExtraResultType> | undefined> {
    if (Object.keys(authentication.extra || {}).length === 0) {
      return;
    }

    const extraResults: Record<ExtraTypeEnum, ExtraResultType> = {} as Record<
      ExtraTypeEnum,
      ExtraResultType
    >;

    // tslint:disable-next-line:forin
    for (const extraType in authentication.extra) {
      const extraInput = authentication.extra[extraType];

      if (extraType === ExtraTypeEnum.COGNITO) {
        const loginExtra = await this.cognitoExtraService.getLoginExtra(
          context,
          authenticationId,
          authentication,
          extraInput,
        );

        extraResults[ExtraTypeEnum.COGNITO] = [loginExtra];
      }
    }

    return extraResults;
  }
}
