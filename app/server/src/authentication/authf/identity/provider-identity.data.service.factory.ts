import { Inject, Injectable } from '@nestjs/common';
import { ProviderIdentityElasticsearchDataService } from './provider-identity.elasticsearch.data.service';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { AuthfConfig } from '../authf.config';
import { CustomLogger } from '@cryptexlabs/codex-nodejs-common';
import { DatabaseTypeEnum } from '../data/database-type.enum';
import { ProviderIdentityMysqlDataService } from './provider-identity.mysql.data.service';
import { ProviderIdentityDataServiceInterface } from './provider-identity.data.service.interface';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';

@Injectable()
export class ProviderIdentityDataServiceFactory {
  constructor(
    private readonly config: AuthfConfig,
    @Inject('LOGGER') private readonly logger: CustomLogger,
    @Inject('MYSQL_PROVIDER_IDENTITY_DATA_SERVICE')
    private readonly mysqlDataService: ProviderIdentityMysqlDataService,
  ) {}

  public async getService(
    databaseType: DatabaseTypeEnum,
  ): Promise<ProviderIdentityDataServiceInterface> {
    if (databaseType === DatabaseTypeEnum.ELASTICSEARCH) {
      return this._getElasticsearchDatabaseService();
    }
    if (databaseType === DatabaseTypeEnum.MYSQL) {
      return this.mysqlDataService;
    }

    throw new Error(
      `Invalid database type: ${databaseType} from env var 'DATABASE_TYPE'`,
    );
  }

  private async _getElasticsearchDatabaseService(): Promise<ProviderIdentityElasticsearchDataService> {
    const elasticsearchService = new ElasticsearchService({
      node: this.config.elasticsearch.url,
    });
    const elasticsearchPinger = new ElasticsearchPinger(
      elasticsearchService,
      this.logger,
    );

    const service = new ProviderIdentityElasticsearchDataService(
      elasticsearchService,
      elasticsearchPinger,
      this.logger,
    );

    await service.initializeIndex();

    return service;
  }
}
