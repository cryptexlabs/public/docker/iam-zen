import { ProviderIdentityDataServiceInterface } from './provider-identity.data.service.interface';
import { AuthenticationProviderTypeEnum } from '@cryptexlabs/authf-data-model';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { Inject } from '@nestjs/common';
import { CustomLogger } from '@cryptexlabs/codex-nodejs-common';
import { SearchTotalHits } from '@elastic/elasticsearch/lib/api/types';
import { ElasticsearchPinger } from '@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger';

export class ProviderIdentityElasticsearchDataService
  implements ProviderIdentityDataServiceInterface
{
  public static get DEFAULT_INDEX() {
    return 'provider_identity';
  }

  constructor(
    private readonly elasticsearchService: ElasticsearchService,
    private readonly elasticsearchPinger: ElasticsearchPinger,
    @Inject('LOGGER') private readonly logger: CustomLogger,
  ) {}

  private async _createIndex() {
    await this.elasticsearchService.indices.create({
      index: ProviderIdentityElasticsearchDataService.DEFAULT_INDEX,
      body: {},
    });
  }

  private async _getIndexExists() {
    return await this.elasticsearchService.indices.exists({
      index: ProviderIdentityElasticsearchDataService.DEFAULT_INDEX,
    });
  }

  async initializeIndex() {
    await this._waitForReady();

    const exists = await this._getIndexExists();
    if (!exists) {
      await this._createIndex();
    }

    await this.elasticsearchService.indices.putMapping({
      index: ProviderIdentityElasticsearchDataService.DEFAULT_INDEX,
      body: {
        properties: {
          identityIdType: {
            type: 'keyword',
          },
          identityId: {
            type: 'keyword',
          },
          provider: {
            type: 'keyword',
          },
        },
      },
    });
  }

  async _waitForReady() {
    await this.elasticsearchPinger.waitForReady();
  }

  public async getAuthenticationIdForProvider(
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
  ): Promise<string | null> {
    const response = await this.elasticsearchService.search({
      index: ProviderIdentityElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  provider,
                },
              },
              {
                term: {
                  identityIdType,
                },
              },
              {
                term: {
                  identityId,
                },
              },
            ],
          },
        },
      },
    });

    if ((response.hits.total as SearchTotalHits).value === 0) {
      return null;
    }

    return (response.hits.hits[0]._source as any).authenticationId;
  }

  public async saveProviderIdentity(
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
    authenticationId: string,
  ): Promise<void> {
    const getAuthenticationResponse = await this.elasticsearchService.search({
      index: ProviderIdentityElasticsearchDataService.DEFAULT_INDEX,
      body: {
        query: {
          bool: {
            must: [
              {
                term: {
                  provider,
                },
              },
              {
                term: {
                  identityIdType,
                },
              },
              {
                term: {
                  identityId,
                },
              },
            ],
          },
        },
      },
    });

    if ((getAuthenticationResponse.hits.total as SearchTotalHits).value > 0) {
      this.logger.debug(getAuthenticationResponse.hits.hits[0]);
      const _id = getAuthenticationResponse.hits.hits[0]._id;
      await this.elasticsearchService.update({
        index: ProviderIdentityElasticsearchDataService.DEFAULT_INDEX,
        id: _id,
        body: {
          doc: {
            provider,
            identityIdType,
            identityId,
            authenticationId,
          },
        },
      });
    } else {
      await this.elasticsearchService.index({
        index: ProviderIdentityElasticsearchDataService.DEFAULT_INDEX,
        body: {
          provider,
          identityIdType,
          identityId,
          authenticationId,
        },
      });
    }
  }
}
