import { ProviderIdentityDataServiceInterface } from './provider-identity.data.service.interface';
import { AuthenticationProviderTypeEnum } from '@cryptexlabs/authf-data-model';
import { Inject } from '@nestjs/common';
import { InjectKnex } from 'nestjs-knex';
import { Knex } from 'knex';
import { CustomLogger } from '@cryptexlabs/codex-nodejs-common';
import { SqlConstants } from '../data/sql.constants';

export class ProviderIdentityMysqlDataService
  implements ProviderIdentityDataServiceInterface
{
  constructor(
    @InjectKnex(SqlConstants.READ_ONLY) private readonly knexRO: Knex,
    @InjectKnex() private readonly knex: Knex,
    @Inject('LOGGER') private readonly logger: CustomLogger,
  ) {}

  public async getAuthenticationIdForProvider(
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
  ): Promise<string | null> {
    const result = await this.knexRO('provider_identity')
      .where({
        provider,
        identity_id_type: identityIdType,
        identity_id: identityId,
      })
      .limit(1);

    if (result.length > 0) {
      return result[0].authentication_id;
    } else {
      return null;
    }
  }

  public async saveProviderIdentity(
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
    authenticationId: string,
  ): Promise<void> {
    await this.knex('authentication')
      .insert({
        provider,
        identity_id_type: identityIdType,
        identity_id: identityId,
        authentication_id: authenticationId,
      })
      .onConflict(['provider', 'identity_id_type', 'identity_id'])
      .ignore();
  }
}
