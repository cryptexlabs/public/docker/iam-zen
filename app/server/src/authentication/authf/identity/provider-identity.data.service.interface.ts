import { AuthenticationProviderTypeEnum } from '@cryptexlabs/authf-data-model';

export interface ProviderIdentityDataServiceInterface {
  getAuthenticationIdForProvider(
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
  ): Promise<string | null>;
  saveProviderIdentity(
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
    authenticationId: string,
  ): Promise<void>;
}
