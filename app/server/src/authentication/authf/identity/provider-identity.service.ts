import { Inject, Injectable } from '@nestjs/common';
import { AuthenticationProviderTypeEnum } from '@cryptexlabs/authf-data-model';
import { ProviderIdentityDataServiceInterface } from './provider-identity.data.service.interface';

@Injectable()
export class ProviderIdentityService {
  constructor(
    @Inject('PROVIDER_IDENTITY_DATA_SERVICE')
    private readonly providerIdentityDataService: ProviderIdentityDataServiceInterface,
  ) {}

  public getAuthenticationIdForProvider(
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
  ) {
    return this.providerIdentityDataService.getAuthenticationIdForProvider(
      provider,
      identityIdType,
      identityId,
    );
  }

  public saveProviderIdentity(
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
    authenticationId: string,
  ) {
    return this.providerIdentityDataService.saveProviderIdentity(
      provider,
      identityIdType,
      identityId,
      authenticationId,
    );
  }
}
