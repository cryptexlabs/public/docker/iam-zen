import {
  ConfigBooleanEnum,
  DefaultConfig,
} from '@cryptexlabs/codex-nodejs-common';
import { DatabaseTypeEnum } from './data/database-type.enum';
import { AuthenticationServiceTypeEnum } from './authentication/authentication-service.type.enum';
import { UrlInterface } from '@cryptexlabs/codex-nodejs-common/src/config/url.interface';
import { TokenPairExpirationPolicyInterface } from '@cryptexlabs/authf-data-model';
import {
  NewUserConfigInterface,
  ServerConfigInterface,
} from './server.config.interface';
import { AuthenticatedConfigInterface } from './authenticated-config.interface';
import { PolicyConfigInterface } from './policy/policy-config.interface';
import { GroupConfigInterface } from './group/group-config.interface';
import { UserConfigInterface } from './user/user.config.interface';
import { PermissionConfigInterface } from './permission/permission.config.interface';
import { AppConfigInterface } from './app/app.config.interface';
import { RoleConfigInterface } from './role/role-config.interface';
import * as process from 'process';

export interface RedirectUrlConfigInterface {
  redirectUrl: string;
}

export class Config extends DefaultConfig {
  public get dbType(): DatabaseTypeEnum {
    return (
      (process.env.DATABASE_TYPE as DatabaseTypeEnum) ||
      DatabaseTypeEnum.ELASTICSEARCH
    );
  }

  public get authenticationProviderType(): AuthenticationServiceTypeEnum {
    return (
      (process.env
        .AUTHENTICATION_SERVICE_TYPE as AuthenticationServiceTypeEnum) ||
      AuthenticationServiceTypeEnum.AUTHF
    );
  }

  public get uiPrefix(): string {
    return process.env.UI_PREFIX || '';
  }

  public get newUserDefaultAttributes(): NewUserConfigInterface {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (serverConfig && serverConfig.newUser) {
      return serverConfig.newUser;
    } else {
      return {};
    }
  }

  public get defaultUsers(): UserConfigInterface[] {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (serverConfig && serverConfig.users) {
      return serverConfig.users;
    } else {
      return [];
    }
  }

  public get defaultGroups(): GroupConfigInterface[] {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (serverConfig && serverConfig.groups) {
      return serverConfig.groups;
    } else {
      return [];
    }
  }

  public get defaultRoles(): RoleConfigInterface[] {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (serverConfig && serverConfig.roles) {
      return serverConfig.roles;
    } else {
      return [];
    }
  }

  public get defaultApps(): AppConfigInterface[] {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (serverConfig && serverConfig.apps) {
      return serverConfig.apps;
    } else {
      return [];
    }
  }

  public get defaultPermissions(): PermissionConfigInterface[] {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (serverConfig && serverConfig.permissions) {
      return serverConfig.permissions;
    } else {
      return [];
    }
  }

  public get defaultPolicies(): PolicyConfigInterface[] {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (serverConfig && serverConfig.policies) {
      return serverConfig.policies;
    } else {
      return [];
    }
  }

  public get tokenExpirationPolicy(): TokenPairExpirationPolicyInterface {
    return this.getServerConfig<ServerConfigInterface>().token.expirationPolicy;
  }

  public get cognitoPoolId(): string | null {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (
      serverConfig &&
      serverConfig.identityLink &&
      serverConfig.identityLink.cognito
    ) {
      if (!serverConfig.identityLink.cognito.poolId) {
        throw new Error("Invalid cognito config. Missing 'poolId'");
      }
      return serverConfig.identityLink.cognito.poolId;
    } else {
      return null;
    }
  }

  public get autoLoadConfig(): boolean {
    return process.env.AUTOLOAD_SERVER_CONFIG !== 'false';
  }

  public get cognitoDeveloperProviderName(): string | null {
    const serverConfig = this.getServerConfig<ServerConfigInterface>();
    if (
      serverConfig &&
      serverConfig.identityLink &&
      serverConfig.identityLink.cognito
    ) {
      if (!serverConfig.identityLink.cognito.developerProviderName) {
        throw new Error("Invalid cognito config. Missing 'poolId'");
      }
      return serverConfig.identityLink.cognito.developerProviderName;
    } else {
      return null;
    }
  }

  public get forgotPasswordEnabled() {
    return process.env.FORGOT_PASSWORD_DISABLED !== 'true';
  }

  public get cognitoPoolRegion(): string | null {
    const id = this.cognitoPoolId;
    if (!id) {
      return null;
    }

    const parts = id.split(':');
    if (parts.length !== 2) {
      throw new Error(`Invalid cognito pool id: ${id}`);
    }

    return parts[0];
  }

  public get userRegistrationEnabled(): boolean {
    return process.env.USER_REGISTRATION_DISABLED !== 'true';
  }

  public get kafkaExtraConfig(): any {
    return {
      connectionTimeout: process.env.KAFKA_DEFAULT_CONNECTION_TIMEOUT
        ? parseInt(process.env.KAFKA_DEFAULT_CONNECTION_TIMEOUT, 10)
        : 1000,
      authenticationTimeout: process.env.KAFKA_DEFAULT_AUTHENTICATION_TIMEOUT
        ? parseInt(process.env.KAFKA_DEFAULT_AUTHENTICATION_TIMEOUT, 10)
        : 1000,
      ssl: process.env.KAFKA_SSL_ENABLED === ConfigBooleanEnum.TRUE,
    };
  }

  public get maxUsers(): number {
    return this.getServerConfig<ServerConfigInterface>().maxUsers;
  }

  public get quietPinoReqLogger(): boolean {
    return process.env.LOGGER_PINO_QUIET_REQ_LOGGER === 'true';
  }

  public get loginWithDeviceEnabled(): boolean {
    return process.env.LOGIN_WITH_DEVICE_ENABLED === 'true';
  }

  public get allowIgnoreCreateUserConflicts(): boolean {
    return process.env.ALLOW_IGNORE_CREATE_USER_CONFLICTS === 'true';
  }

  public get downstreamMaxRetries(): number {
    return process.env.DOWNSTREAM_MAX_RETRIES
      ? parseInt(process.env.DOWNSTREAM_MAX_RETRIES, 10)
      : 0;
  }

  public get componentHealthRefreshInterval(): number {
    return process.env.COMPONENT_HEALTH_REFRESH_INTERVAL_IN_SECONDS
      ? parseInt(process.env.COMPONENT_HEALTH_REFRESH_INTERVAL_IN_SECONDS, 10)
      : 60;
  }

  public get createUserInterval(): number {
    return process.env.CREATE_USER_BATCH_INTERVAL
      ? parseInt(process.env.CREATE_USER_BATCH_INTERVAL, 10)
      : 3000;
  }

  public get createIdentityLinkInterval(): number {
    return process.env.CREATE_IDENTITY_LINK_INTERVAL
      ? parseInt(process.env.CREATE_IDENTITY_LINK_INTERVAL, 10)
      : 3000;
  }

  public get httpKeepAliveTimeout(): number {
    return process.env.HTTP_KEEP_ALIVE_TIMEOUT !== undefined
      ? parseInt(process.env.HTTP_KEEP_ALIVE_TIMEOUT, 10)
      : 5000;
  }

  public get httpRequestTimeout(): number {
    return process.env.HTTP_REQUEST_TIMEOUT
      ? parseInt(process.env.HTTP_REQUEST_TIMEOUT, 10)
      : 60000;
  }

  public get userCacheTtl(): number {
    return process.env.USER_CACHE_TTL
      ? parseInt(process.env.USER_CACHE_TTL, 10)
      : 5000;
  }
}
