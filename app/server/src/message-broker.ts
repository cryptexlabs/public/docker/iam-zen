import {
  KafkaService,
  KafkaStubService,
} from '@cryptexlabs/codex-nodejs-common';
import { appConfig, baseLogger } from './setup';
import { KafkaServiceInterface } from '@cryptexlabs/codex-nodejs-common';

let kafkaService: KafkaServiceInterface = new KafkaStubService();

if (appConfig.kafka.enabled) {
  kafkaService = new KafkaService(
    appConfig,
    baseLogger,
    appConfig.kafkaExtraConfig,
  );
}

const messageBrokerProviders = [
  {
    provide: 'KAFKA_SERVICE',
    useValue: kafkaService,
  },
  {
    provide: KafkaService,
    useValue: kafkaService,
  },
  {
    provide: 'CONSUMER_SERVICE_DELEGATE',
    useValue: kafkaService,
  },
];

export { messageBrokerProviders };
