export class AuthUtil {
  public static getBasicAuth(username: string, password: string): string {
    return Buffer.from(`${username}:${password}`).toString('base64');
  }
}
