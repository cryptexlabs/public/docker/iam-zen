import { Inject, Injectable } from '@nestjs/common';
import { UserDefaultLoaderService } from './user/persistence/user.default-loader.service';
import { RoleDefaultLoaderService } from './role/role.default-loader.service';
import { GroupDefaultLoaderService } from './group/group.default-loader.service';
import { PolicyDefaultLoaderService } from './policy/policy.default-loader.service';
import { PermissionDefaultLoaderService } from './permission/permission.default-loader.service';
import { Config } from './config';
import { AppDefaultLoaderService } from './app/app.default-loader.service';
import { CustomLogger } from '@cryptexlabs/codex-nodejs-common';

@Injectable()
export class LoadConfigService {
  constructor(
    private readonly userDefaultLoaderService: UserDefaultLoaderService,
    private readonly roleDefaultLoaderService: RoleDefaultLoaderService,
    private readonly groupDefaultLoaderService: GroupDefaultLoaderService,
    private readonly policyDefaultLoaderService: PolicyDefaultLoaderService,
    private readonly permissionDefaultLoaderService: PermissionDefaultLoaderService,
    private readonly appDefaultLoaderService: AppDefaultLoaderService,
    @Inject('CONFIG') config: Config,
    @Inject('LOGGER') private readonly logger: CustomLogger,
  ) {
    config.setConfigChangeLoaderEventHandler(async () => {
      if (config.autoLoadConfig) {
        await this.loadConfig();
      }
    });
  }

  public async loadConfig(): Promise<void> {
    await Promise.all([
      this.userDefaultLoaderService.load(),
      this.roleDefaultLoaderService.load(),
      this.groupDefaultLoaderService.load(),
      this.policyDefaultLoaderService.load(),
      this.permissionDefaultLoaderService.load(),
      this.appDefaultLoaderService.load(),
    ]);
    this.logger.info('Config Loaded');
  }
}
