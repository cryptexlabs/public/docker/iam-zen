import { AppPermissionSyncAllAuthzGuard } from './app.permission.sync-all.authz.guard';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';
import { ExecutionContext } from '@nestjs/common';

describe(AppPermissionSyncAllAuthzGuard.name, () => {
  it('It should allow a super admin to sync permissions for all apps', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::any:any:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAllAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('It should allow someone with permission to sync all app permissions to sync all app permissions', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:all::permission:all::updated::create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAllAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('It should not allow someone with permission to sync a specific app permissions to sync all app permissions', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:13b75180-0e05-472c-bbc9-8bf7c80f9c54::permission:all::updated::create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAllAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('It should not allow someone with permission to sync a specific permission for all apps to sync all app permissions', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:all::permission:13b75180-0e05-472c-bbc9-8bf7c80f9c54::updated::create`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAllAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('It should not allow someone without any permissions to sync all permissions for all apps', () => {
    const token = jwt.sign(
      {
        scopes: [],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppPermissionSyncAllAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
