import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { AppPatchInterface } from './app.patch.interface';

@Injectable()
export class AppPatchValidationPipe implements PipeTransform {
  private schema: Joi.ObjectSchema<AppPatchInterface>;

  constructor() {
    this.schema = Joi.object({
      name: Joi.string().optional(),
      description: Joi.string().optional(),
    });
  }

  transform(value: AppPatchInterface, metadata: ArgumentMetadata): any {
    const { error } = this.schema.validate(value);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
