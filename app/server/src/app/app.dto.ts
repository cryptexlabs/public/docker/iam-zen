import { AppPersistedInterface } from './app.persisted.interface';
import { AppRoleInterface } from './app.role.interface';
import { AppPermissionInterface } from './app.permission.interface';
import { AppPolicyInterface } from './app.policy.interface';
import { AppApiKeyInListDto } from './app.api-key.in-list.dto';

export class AppDto {
  public readonly id: string;
  public readonly name: string;
  public readonly description: string;

  constructor(
    app: AppPersistedInterface,
    public readonly roles: AppRoleInterface[],
    public readonly policies: AppPolicyInterface[],
    public readonly permissions: AppPermissionInterface[],
    public readonly apiKeys: AppApiKeyInListDto[],
    public readonly immutable: boolean,
  ) {
    this.id = app.id;
    this.name = app.name;
    this.description = app.description;
  }
}
