import {
  AuthenticationProviderInterface,
  AuthenticationProviderTypeEnum,
  ApiAuthAuthenticationProviderDataInterface,
} from '@cryptexlabs/authf-data-model';

export interface ApiAuthAuthenticationProviderConfigInterface
  extends AuthenticationProviderInterface {
  data: ApiAuthAuthenticationProviderDataInterface;
  description: string;
  type: AuthenticationProviderTypeEnum.API;
}

export interface AppConfigInterface {
  name: string;
  description?: string;
  id: string;
  roles?: string[];
  authentications?: ApiAuthAuthenticationProviderConfigInterface[];
}
