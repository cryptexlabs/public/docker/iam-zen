import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzListSubObjectsGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class AppRoleListAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzListSubObjectsGuardUtil(context);
    return util.isAuthorized(
      'app',
      util.params.appId,
      'role',
      AuthzNamespace.object,
    );
  }
}
