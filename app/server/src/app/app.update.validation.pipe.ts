import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import * as Joi from 'joi';
import { AppUpdateInterface } from './app.update.interface';

@Injectable()
export class AppUpdateValidationPipe implements PipeTransform {
  private schema: Joi.ObjectSchema<AppUpdateInterface>;

  constructor() {
    this.schema = Joi.object({
      name: Joi.string().required(),
      description: Joi.string().optional(),
      roles: Joi.array().items(Joi.string()).optional(),
    });
  }

  transform(value: AppUpdateInterface, metadata: ArgumentMetadata): any {
    const { error } = this.schema.validate(value);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
