export interface AppUpdateInterface {
  name: string;
  description?: string;
  roles: string[];
}
