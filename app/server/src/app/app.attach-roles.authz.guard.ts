import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { HttpAuthzAttachObjectsGuardUtil } from '@cryptexlabs/codex-nodejs-common';
import { AuthzNamespace } from '../constants';

@Injectable()
export class AppAttachRolesAuthzGuard implements CanActivate {
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const util = new HttpAuthzAttachObjectsGuardUtil(context);
    return util.isAuthorized(
      'app',
      util.params.appId,
      'role',
      util.body,
      AuthzNamespace.object,
    );
  }
}
