import { AppInterface } from './app.interface';
import { Context } from '@cryptexlabs/codex-nodejs-common';
import { AppPolicyWithPermissionsInterface } from './app.policy.interface';

export interface AppPoliciesProviderInterface {
  getPoliciesForApp(
    context: Context,
    app: AppInterface,
  ): Promise<AppPolicyWithPermissionsInterface[]>;
}
