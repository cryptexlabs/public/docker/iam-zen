import { ApiAuthAuthenticationProviderInterface } from '@cryptexlabs/authf-data-model';

export interface CreateAppInterface {
  name: string;
  description?: string;
  authentications?: ApiAuthAuthenticationProviderInterface[];
}
