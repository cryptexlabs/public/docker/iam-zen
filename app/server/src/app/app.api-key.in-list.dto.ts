import { ApiKeyInterface } from '../api-key/api-key.interface';

export class AppApiKeyInListDto {
  public readonly id: string;
  public readonly description: string;

  constructor(apiKey: ApiKeyInterface, public readonly immutable: boolean) {
    this.id = apiKey.id;
    this.description = apiKey.description;
  }
}
