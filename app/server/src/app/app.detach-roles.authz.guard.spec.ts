import { AppDetachRolesAuthzGuard } from './app.detach-roles.authz.guard';
import { ExecutionContext } from '@nestjs/common';
import * as jwt from 'jsonwebtoken';
import { AuthzNamespace } from '../constants';

describe(AppDetachRolesAuthzGuard.name, () => {
  it('Should allow super admin to detach a role from an app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::any:any:any:any:any:any:any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to detach any role from an app to detach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::role:any:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any permission on an app to detach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::role:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to do anything to any sub object for an app to detach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should allow someone with permission to detach a specific role from an app to detach the role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::role:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(true);
  });

  it('Should not allow someone with permission to detach any role to a different app to detach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::role:any:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to a different app to detach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::role:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to do anything to any sub object for a different app to detach a role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::any:any:any`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to detach a specific role to a different app to detach the role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::role:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '001d4f53-798b-4a0b-8ef7-330a7bf72147',
          },
          query: { ids: ['680dddec-f0b9-4a01-b8b5-be725f946935'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });

  it('Should not allow someone with permission to detach a different specific permission from an app to detach the role to the app', () => {
    const token = jwt.sign(
      {
        scopes: [
          `${AuthzNamespace.object}:::app:4d2114ca-24e2-43e5-bddb-d9a6688b8340::role:680dddec-f0b9-4a01-b8b5-be725f946935:delete`,
        ],
      },
      'hello',
    );

    const context = {
      switchToHttp: () => ({
        getRequest: () => ({
          headers: {
            authorization: `Bearer ${token}`,
          },
          params: {
            appId: '4d2114ca-24e2-43e5-bddb-d9a6688b8340',
          },
          query: { ids: ['5be3176f-c066-4418-b682-18e16fd07b84'] },
        }),
      }),
    } as ExecutionContext;

    const guard = new AppDetachRolesAuthzGuard();

    expect(guard.canActivate(context)).toBe(false);
  });
});
