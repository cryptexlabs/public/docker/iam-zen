import React, { Suspense } from 'react';
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import AppLayout from 'layout/AppLayout';
import { uiBasePath } from '../../constants/defaultValues';

const Dashboards = React.lazy(() =>
  import(/* webpackChunkName: "home" */ './home')
);

const User = React.lazy(() =>
  import(/* webpackChunkName: "pages-product" */ './pages/user')
);

const UserDetail = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/user/user.detail')
);

const Group = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/group')
);

const GroupDetail = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/group/group.detail')
);

const Role = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/role')
);

const RoleDetail = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/role/role.detail')
);

const Permission = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/permission')
);

const PermissionDetail = React.lazy(() =>
  import(
    /* webpackChunkName: "ui-forms" */ './pages/permission/permission.detail'
  )
);

const App = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/app')
);

const AppDetail = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/app/app.detail')
);

const Policy = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/policy')
);

const PolicyDetail = React.lazy(() =>
  import(/* webpackChunkName: "ui-forms" */ './pages/policy/policy.detail')
);

const Ui = React.lazy(() => import(/* webpackChunkName: "ui" */ './ui'));
const Menu = React.lazy(() => import(/* webpackChunkName: "menu" */ './menu'));

const TheApp = ({ match }) => {
  return (
    <AppLayout>
      <div className="dashboard-wrapper">
        <Suspense fallback={<div className="loading" />}>
          <Switch>
            <Redirect exact from={`${match.url}`} to={`${match.url}/user`} />

            <Route
              path={`${match.url}/dashboards`}
              render={(props) => <Dashboards {...props} />}
            />
            <Redirect
              exact
              from={`${match.url}/user/:userId`}
              to={`${match.url}/user/:userId/groups`}
            />
            <Route
              path={`${match.url}/user/:userId/:tab`}
              render={(props) => <UserDetail {...props} />}
            />
            <Route
              path={`${match.url}/user`}
              render={(props) => <User {...props} />}
            />
            <Redirect
              exact
              from={`${match.url}/group/:groupId`}
              to={`${match.url}/group/:groupId/users`}
            />
            <Route
              path={`${match.url}/group/:groupId/:tab`}
              render={(props) => <GroupDetail {...props} />}
            />
            <Route
              path={`${match.url}/group`}
              render={(props) => <Group {...props} />}
            />
            <Redirect
              exact
              from={`${match.url}/role/:roleId`}
              to={`${match.url}/role/:roleId/apps`}
            />
            <Route
              path={`${match.url}/role/:roleId/:tab`}
              render={(props) => <RoleDetail {...props} />}
            />
            <Route
              path={`${match.url}/role`}
              render={(props) => <Role {...props} />}
            />
            <Redirect
              exact
              from={`${match.url}/app/:appId`}
              to={`${match.url}/app/:appId/roles`}
            />
            <Route
              path={`${match.url}/app/:appId/:tab`}
              render={(props) => <AppDetail {...props} />}
            />
            <Route
              path={`${match.url}/app`}
              render={(props) => <App {...props} />}
            />
            <Redirect
              exact
              from={`${match.url}/permission/:permissionId`}
              to={`${match.url}/permission/:permissionId/policies`}
            />
            <Route
              path={`${match.url}/permission/:permissionId/:tab`}
              render={(props) => <PermissionDetail {...props} />}
            />
            <Route
              path={`${match.url}/permission`}
              render={(props) => <Permission {...props} />}
            />
            <Redirect
              exact
              from={`${match.url}/policy/:policyId`}
              to={`${match.url}/policy/:policyId/groups`}
            />
            <Route
              path={`${match.url}/policy/:policyId/:tab`}
              render={(props) => <PolicyDetail {...props} />}
            />
            <Route
              path={`${match.url}/policy`}
              render={(props) => <Policy {...props} />}
            />
            <Redirect from={`${match.url}//`} to={`${match.url}/user`} />
            <Redirect to={`${uiBasePath}/error`} />
          </Switch>
        </Suspense>
      </div>
    </AppLayout>
  );
};

const mapStateToProps = ({ menu }) => {
  const { containerClassnames } = menu;
  return { containerClassnames };
};

export default withRouter(connect(mapStateToProps, {})(TheApp));
