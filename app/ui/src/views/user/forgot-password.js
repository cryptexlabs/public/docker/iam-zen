import React, { useEffect, useState } from 'react';
import { Button, Card, CardTitle, FormGroup, Label, Row } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import { Field, Form, Formik } from 'formik';
import { connect } from 'react-redux';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import { forgotPassword } from 'redux/actions';
import { NotificationManager } from 'components/common/react-notifications';

const validateEmail = (value) => {
  let error;
  if (!value) {
    error = 'Please enter your email address';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
    error = 'Invalid email address';
  }
  return error;
};

const ForgotPassword = ({
  history,
  forgotUserMail,
  loading,
  error,
  forgotPasswordAction,
}) => {
  const [email] = useState('demo@coloredstrategies.com');

  const onForgotPassword = (values) => {
    if (!loading) {
      if (values.email !== '') {
        forgotPasswordAction(values, history);
      }
    }
  };

  useEffect(() => {
    if (error) {
      NotificationManager.warning(
        error,
        'Forgot Password Error',
        3000,
        null,
        null,
        ''
      );
    } else if (!loading && forgotUserMail === 'success')
      NotificationManager.success(
        'Please check your email.',
        'Forgot Password Success',
        3000,
        null,
        null,
        ''
      );
  }, [error, forgotUserMail, loading]);

  const initialValues = { email };

  let forgotPasswordHtml;
  let forgotPasswordText;
  let registrationText;
  if (window.ENVIRONMENT.userRegistrationEnabled) {
    registrationText = (
      <p>
        If you are not a member, please{' '}
        <NavLink to="/user/register" className="text-primary">
          register
        </NavLink>
        .
      </p>
    );
  } else {
    registrationText = (
      <div>
        If you are not a member, please request access from your system
        administrator.
      </div>
    );
  }
  if (window.ENVIRONMENT.forgotPasswordEnabled) {
    forgotPasswordHtml = (
      <div className="form-side">
        <NavLink to="/" className="white">
          <span className="logo-single" />
        </NavLink>
        <CardTitle className="mb-4">
          <IntlMessages id="user.forgot-password" />
        </CardTitle>

        <Formik initialValues={initialValues} onSubmit={onForgotPassword}>
          {({ errors, touched }) => (
            <Form className="av-tooltip tooltip-label-bottom">
              <FormGroup className="form-group has-float-label">
                <Label>
                  <IntlMessages id="user.email" />
                </Label>
                <Field
                  className="form-control"
                  name="email"
                  validate={validateEmail}
                />
                {errors.email && touched.email && (
                  <div className="invalid-feedback d-block">{errors.email}</div>
                )}
              </FormGroup>

              <div className="d-flex justify-content-between align-items-center">
                <NavLink to="/user/forgot-password">
                  <IntlMessages id="user.forgot-password-question" />
                </NavLink>
                <Button
                  color="primary"
                  className={`btn-shadow btn-multiple-state ${
                    loading ? 'show-spinner' : ''
                  }`}
                  size="lg"
                >
                  <span className="spinner d-inline-block">
                    <span className="bounce1" />
                    <span className="bounce2" />
                    <span className="bounce3" />
                  </span>
                  <span className="label">
                    <IntlMessages id="user.reset-password-button" />
                  </span>
                </Button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    );
    forgotPasswordText = (
      <p className="authorization-text mb-0">
        Please use your e-mail to reset your password. <br />
        {registrationText}
      </p>
    );
  } else {
    forgotPasswordText = (
      <p className="authorization-text mb-0">
        Please ask your system administrator to reset your password. <br />
        {registrationText}
      </p>
    );
  }

  return (
    <Row className="h-100">
      <Colxx xxs="12" md="10" className="mx-auto my-auto">
        <Card className="auth-card">
          <div className="position-relative image-side ">
            <p className="authorization-text h2">
              ACCESS MANAGEMENT SIMPLIFIED
            </p>
            {forgotPasswordText}
          </div>
          {forgotPasswordHtml}
        </Card>
      </Colxx>
    </Row>
  );
};

const mapStateToProps = ({ authUser }) => {
  const { forgotUserMail, loading, error } = authUser;
  return { forgotUserMail, loading, error };
};

export default connect(mapStateToProps, {
  forgotPasswordAction: forgotPassword,
})(ForgotPassword);
