import axios from 'axios';
import ApiUtil from '../redux/api-util';

export default class UserApi {
  static async createUser(userId, username, password, groupIds) {
    let payload = {
      username,
    };
    payload = {
      ...payload,
      groups: groupIds,
    };
    if (password && password.trim()) {
      payload = {
        ...payload,
        authentication: {
          data: {
            username,
            password,
          },
          type: 'basic',
        },
      };
    }
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.user.created'
    );
    await axios
      .post(`/user/${userId}`, payload, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
      });
  }

  static async getUsers(searchUsername) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios
      .get('/user', {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          page: 1,
          pageSize: 10,
          searchName: searchUsername,
        },
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: [] } };
      });

    return response.data.data;
  }

  static async exportUsers(all, userIds) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios
      .post(
        '/user/any/export',
        {
          all,
          userIds,
        },
        {
          headers,
          baseURL: ApiUtil.apiBasePath,
        }
      )
      .catch((e) => {
        // All error handling failed
        return { data: { data: [] } };
      });

    return response.data;
  }

  static async userExists(username) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    const response = await axios
      .get('/user/any/username-exists', {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          username,
        },
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: true } };
      });
    return response.data.data;
  }

  static async addGroupsToUsers(userIds, groupIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.user.groups.added'
    );
    for (const userId of userIds) {
      await axios
        .post(`/user/${userId}/group`, groupIds, {
          headers,
          baseURL: ApiUtil.apiBasePath,
        })
        .catch((e) => {
          // All error handling failed
        });
    }
  }

  static async removeGroupsFromUsers(userIds, groupIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.user.groups.added'
    );
    for (const userId of userIds) {
      await axios
        .delete(`/user/${userId}/group`, {
          headers,
          params: {
            ids: groupIds,
          },
          baseURL: ApiUtil.apiBasePath,
        })
        .catch((e) => {
          // All error handling failed
        });
    }
  }

  static async getUser(id) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios
      .get(`/user/${id}`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: null } };
      });

    return response.data.data;
  }

  static async updateUser(userId, username, password) {
    const payload = {
      username,
      password,
    };
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.user.updated'
    );
    await axios
      .patch(`/user/${userId}`, payload, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
      });
  }

  static async deleteUser(userId) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    await axios
      .delete(`/user/${userId}`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
      });
  }

  static async updateBlacklistedStatus(userId, blacklisted) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    await axios
      .put(
        `/user/${userId}/blacklisted`,
        { blacklisted },
        {
          headers,
          baseURL: ApiUtil.apiBasePath,
        }
      )
      .catch((e) => {
        // All error handling failed
      });
  }
}
