import axios from 'axios';
import { adminRoot, uiBasePath } from '../constants/defaultValues';

export default class AuthApi {
  static history;

  static async getRefreshedToken(headers, basePath) {
    const currentUserRaw = localStorage.getItem('current_user');
    if (!currentUserRaw) {
      return undefined;
    }
    const currentUser = JSON.parse(currentUserRaw);
    const userId = currentUser.uid;
    const refreshToken = localStorage.getItem('refreshToken');

    const payload = {
      data: {
        token: refreshToken,
      },
      type: 'refresh',
    };
    try {
      const response = await axios
        .post(`/user/${userId}/token`, payload, {
          headers,
          baseURL: basePath,
        })
        .catch((e) => {
          // All error handling failed
        });

      if (response?.status !== 201) {
        return undefined;
      }

      const newAccessToken = response.data.data.token.access.token;
      const newRefreshToken = response.data.data.token.refresh.token;
      localStorage.setItem('accessToken', newAccessToken);
      localStorage.setItem('refreshToken', newRefreshToken);

      return newAccessToken;
    } catch (e) {
      if (!e) {
        return undefined;
      }
      if (e?.response?.status === 401) {
        return undefined;
      }
      throw e;
    }
  }

  static logout() {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('refreshToken');
    localStorage.removeItem('current_user');
    const currentPath = window.location.href;
    window.location.href = `${uiBasePath}/user/login?redirect=${currentPath}`;
  }
}
