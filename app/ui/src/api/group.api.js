import axios from 'axios';
import ApiUtil from '../redux/api-util';

export default class GroupApi {
  static async groupExists(groupName) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios
      .get('/group/any/exists', {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          name: groupName,
        },
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: true } };
      });

    return response.data.data;
  }

  static async getGroups(searchGroupName) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios
      .get('/group', {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          page: 1,
          pageSize: 10,
          searchName: searchGroupName,
        },
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: [] } };
      });

    return response.data.data;
  }

  static async deleteGroups(groupIds) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    await axios
      .delete(`/group`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          ids: groupIds,
        },
      })
      .catch((e) => {
        // All error handling failed
      });
  }

  static async saveGroup(groupId, name, description, policiesIds) {
    const payload = {
      name,
      description,
      policies: policiesIds,
    };

    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.group.created'
    );

    await axios
      .put(`/group/${groupId}`, payload, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
      });
  }

  static async getGroup(id) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios
      .get(`/group/${id}`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: null } };
      });

    return response.data.data;
  }

  static async addPoliciesToGroups(groupIds, policyIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.group.policies.added'
    );
    for (const groupId of groupIds) {
      await axios
        .post(`/group/${groupId}/policy`, policyIds, {
          headers,
          baseURL: ApiUtil.apiBasePath,
        })
        .catch((e) => {
          // All error handling failed
        });
    }
  }

  static async removePoliciesFromGroups(groupIds, policyIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.group.policies.added'
    );
    for (const groupId of groupIds) {
      await axios
        .delete(`/group/${groupId}/policy`, {
          headers,
          params: {
            ids: policyIds,
          },
          baseURL: ApiUtil.apiBasePath,
        })
        .catch((e) => {
          // All error handling failed
        });
    }
  }

  static async updateGroup(groupId, name, description) {
    const payload = {
      name,
      description,
    };
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.group.updated'
    );
    await axios
      .patch(`/group/${groupId}`, payload, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
      });
  }
}
