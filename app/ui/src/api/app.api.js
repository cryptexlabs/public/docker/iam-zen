import axios from 'axios';
import ApiUtil from '../redux/api-util';

export default class AppApi {
  static async getPaginatedApps(
    search,
    page,
    pageSize,
    orderBy,
    orderDirection
  ) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios
      .get(`/app`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          pageSize,
          page,
          orderBy,
          orderDirection,
          search,
        },
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: [] } };
      });

    return response.data;
  }

  static async getApps(searchAppName) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    const response = await axios
      .get('/app', {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          page: 1,
          pageSize: 10,
          searchName: searchAppName,
        },
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: [] } };
      });

    return response.data.data;
  }

  static async createApp(appId, appName, appDescription, roles) {
    let payload = {
      name: appName,
      description: appDescription,
    };
    const roleIds = roles.map((role) => role.id);
    if (roleIds.length > 0) {
      payload = {
        ...payload,
        roles: roleIds,
      };
    }
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.app.created'
    );
    await axios
      .put(`/app/${appId}`, payload, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
      });
  }

  static async createApiKey(appId, description) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();
    const response = await axios
      .post(
        `/app/${appId}/api-key`,
        {
          description,
        },
        {
          headers,
          baseURL: ApiUtil.apiBasePath,
        }
      )
      .catch((e) => {
        // All error handling failed
        return { data: { data: null } };
      });
    return response.data.data;
  }

  static async deleteApp(ids) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    await axios
      .delete(`/app`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          ids,
        },
      })
      .catch((e) => {
        // All error handling failed
      });
  }

  static async deleteApiKey(appId, apiKey) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    await axios
      .delete(`/app/${appId}/api-key/${apiKey}`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
      });
  }

  static async getApp(id) {
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    const response = await axios
      .get(`/app/${id}`, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
        return { data: { data: null } };
      });

    return response.data.data;
  }

  static async addRolesToApps(appIds, rolesIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.app.roles.added'
    );
    for (const appId of appIds) {
      await axios
        .post(`/app/${appId}/role`, rolesIds, {
          headers,
          baseURL: ApiUtil.apiBasePath,
        })
        .catch((e) => {
          // All error handling failed
        });
    }
  }

  static async removeRolesFromApps(appIds, roleIds) {
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.app.roles.added'
    );
    for (const userId of appIds) {
      await axios
        .delete(`/app/${userId}/role`, {
          headers,
          params: {
            ids: roleIds,
          },
          baseURL: ApiUtil.apiBasePath,
        })
        .catch((e) => {
          // All error handling failed
        });
    }
  }

  static async updateApp(appId, name, description) {
    const payload = {
      name,
      description,
    };
    const headers = await ApiUtil.getAuthorizedPayloadApiHeaders(
      'cryptexlabs.iam-zen.app.updated'
    );
    await axios
      .patch(`/app/${appId}`, payload, {
        headers,
        baseURL: ApiUtil.apiBasePath,
      })
      .catch((e) => {
        // All error handling failed
      });
  }
}
