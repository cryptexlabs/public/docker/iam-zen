import React, { Suspense, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Redirect,
  Route,
  Switch,
} from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import { enLang } from './lang';
import { NotificationContainer } from './components/common/react-notifications';
import { adminRoot, uiBasePath, UserRole } from './constants/defaultValues';
import { getDirection } from './helpers/Utils';
import { ProtectedRoute } from './helpers/authHelper';

const ViewHome = React.lazy(() =>
  import(/* webpackChunkName: "views" */ './views/home')
);
const ViewApp = React.lazy(() =>
  import(/* webpackChunkName: "views-app" */ './views/app')
);
const ViewUser = React.lazy(() =>
  import(/* webpackChunkName: "views-user" */ './views/user')
);
const ViewError = React.lazy(() =>
  import(/* webpackChunkName: "views-error" */ './views/error')
);
const ViewUnauthorized = React.lazy(() =>
  import(/* webpackChunkName: "views-error" */ './views/unauthorized')
);

const App = () => {
  const direction = getDirection();
  const currentAppLocale = enLang;
  useEffect(() => {
    if (direction.isRtl) {
      document.body.classList.add('rtl');
      document.body.classList.remove('ltr');
    } else {
      document.body.classList.add('ltr');
      document.body.classList.remove('rtl');
    }
  }, [direction]);

  return (
    <div className="h-100">
      <IntlProvider
        locale={currentAppLocale.locale}
        messages={currentAppLocale.messages}
      >
        <NotificationContainer />
        <Suspense fallback={<div className="loading" />}>
          <Router>
            <Switch>
              <ProtectedRoute
                path={adminRoot}
                component={ViewApp}
                roles={[UserRole.Admin, UserRole.Editor]}
              />
              <Route
                path={`${uiBasePath}/user`}
                render={(props) => <ViewUser {...props} />}
              />
              <Route
                path={`${uiBasePath}/error`}
                exact
                render={(props) => <ViewError {...props} />}
              />
              <Route
                path={`${uiBasePath}/unauthorized`}
                exact
                render={(props) => <ViewUnauthorized {...props} />}
              />

              <Redirect from={`${uiBasePath}//`} to={adminRoot} />
              <Redirect from="//" to={adminRoot} />
              <Redirect exact from={`${uiBasePath}`} to={adminRoot} />

              <Redirect to={`${uiBasePath}/error`} />
            </Switch>
          </Router>
        </Suspense>
      </IntlProvider>
    </div>
  );
};

console.log('ENVIRONMENT', window.ENVIRONMENT);

const mapStateToProps = ({ authUser, settings }) => {
  const { currentUser } = authUser;
  const { locale } = settings;
  return { currentUser, locale };
};
const mapActionsToProps = {};

export default connect(mapStateToProps, mapActionsToProps)(App);
