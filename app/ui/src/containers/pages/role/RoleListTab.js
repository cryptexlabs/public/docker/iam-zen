import React, { useState } from 'react';
import {
  Button,
  ButtonDropdown,
  CustomInput,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
} from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import RoleListView from './RoleListView';
import ArrayUtil from '../../../util/array.util';
import AppApi from '../../../api/app.api';
import AddRolesToAppModal from '../app/AddRolesToAppModal';
import AddGroupsToPolicyModal from '../policy/AddGroupsToPolicyModal';
import AddRolesToPolicyModal from '../policy/AddRolesToPolicyModal';
import RoleApi from '../../../api/role.api';

const RoleListTab = ({ roles, appId, policyId, intl, onChange }) => {
  const [lastCheckedRole, setLastCheckedRole] = useState(null);
  const [dropdownSplitOpen, setDropdownSplitOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [addRolesToAppModalOpen, setAddRolesToAppModalOpen] = useState(false);
  const [addRolesToPolicyModalOpen, setAddRolesToPolicyModalOpen] =
    useState(false);

  const onRoleSelected = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastCheckedRole === null) {
      setLastCheckedRole(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...roles];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastCheckedRole, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= roles.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(roles.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onRemoveClicked = async () => {
    if (appId) {
      await AppApi.removeRolesFromApps([appId], selectedItems);
    }
    if (policyId) {
      await RoleApi.removePoliciesFromRoles(selectedItems, [policyId]);
    }
    await onChange();
  };

  return (
    <>
      {(appId || policyId) && (
        <Row>
          <Colxx xxs="12" className="mb-4">
            <div className="text-zero top-right-button-container">
              <Button
                color="primary"
                size="lg"
                className="top-right-button mr-1"
                onClick={() => {
                  if (appId) {
                    setAddRolesToAppModalOpen(!addRolesToAppModalOpen);
                  }
                  if (policyId) {
                    setAddRolesToPolicyModalOpen(!addRolesToPolicyModalOpen);
                  }
                }}
              >
                <IntlMessages id="pages.add" />
              </Button>
              <ButtonDropdown
                className="btn-group"
                isOpen={dropdownSplitOpen}
                toggle={() => setDropdownSplitOpen(!dropdownSplitOpen)}
              >
                <div className="btn btn-primary btn-lg pl-4 pr-0 check-button check-all">
                  <CustomInput
                    className="custom-checkbox mb-0 d-inline-block"
                    type="checkbox"
                    id="checkAll"
                    checked={selectedItems.length >= roles.length}
                    onChange={() => handleChangeSelectAll(true)}
                    label={
                      <span
                        className={`custom-control-label ${
                          selectedItems.length > 0 &&
                          selectedItems.length < roles.length
                            ? 'indeterminate'
                            : ''
                        }`}
                      />
                    }
                  />
                </div>
                <DropdownToggle
                  caret
                  color="primary"
                  className="dropdown-toggle-split btn-lg"
                />
                <DropdownMenu right>
                  <DropdownItem onClick={onRemoveClicked}>
                    <IntlMessages id="pages.remove" />
                  </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </Colxx>
        </Row>
      )}

      <Row>
        {roles.map((role) => {
          return (
            <RoleListView
              showDetails={false}
              isSelect={selectedItems.includes(role.id)}
              onCheckItem={onRoleSelected}
              intl={intl}
              key={role.id}
              role={role}
            />
          );
        })}
      </Row>
      {appId && (
        <AddRolesToAppModal
          appId={appId}
          intl={intl}
          modalOpen={addRolesToAppModalOpen}
          toggleModal={() => setAddRolesToAppModalOpen(!addRolesToAppModalOpen)}
          onAppUpdated={onChange}
          existingRoleIds={roles.map((role) => role.id)}
        />
      )}
      {policyId && (
        <AddRolesToPolicyModal
          policyId={policyId}
          intl={intl}
          modalOpen={addRolesToPolicyModalOpen}
          toggleModal={() =>
            setAddRolesToPolicyModalOpen(!addRolesToPolicyModalOpen)
          }
          onPolicyUpdated={onChange}
          existingRoleIds={roles.map((role) => role.id)}
        />
      )}
    </>
  );
};

export default RoleListTab;
