import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import RoleApi from '../../../api/role.api';
import PolicyApi from '../../../api/policy.api';

const AddRolesToPolicyModal = ({
  modalOpen,
  toggleModal,
  intl,
  onPolicyUpdated,
  policyId,
  existingRoleIds,
}) => {
  const [searchRoleName, setSearchRoleName] = useState('');
  const [roles, setRoles] = useState([]);
  const [rolesSelected, setRolesSelected] = useState([]);

  const resetState = () => {
    setSearchRoleName('');
    setRoles([]);
    setRolesSelected([]);
  };

  useEffect(async () => {
    if (modalOpen) {
      const apiRoles = await RoleApi.getRoles(searchRoleName);
      setRoles(
        apiRoles
          .map((item) => ({
            id: item.id,
            name: item.name,
          }))
          .filter((role) => !existingRoleIds.includes(role.id))
      );
    }
  }, [modalOpen, searchRoleName]);

  const onAddButtonClicked = async () => {
    await RoleApi.addPoliciesToRoles(
      rolesSelected.map((role) => role.id),
      [policyId]
    );
    await resetState();
    onPolicyUpdated();
    toggleModal();
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-roles-to-policy-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label className="mt-4">
          <IntlMessages id="policy.roles" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={roles}
          selectedValues={rolesSelected}
          onSearch={setSearchRoleName}
          onSelect={setRolesSelected}
          onRemove={setRolesSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onAddButtonClicked}>
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddRolesToPolicyModal);
