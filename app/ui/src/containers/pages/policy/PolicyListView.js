import React from 'react';
import { Card, CustomInput } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import classnames from 'classnames';
import { ContextMenuTrigger } from 'react-contextmenu';
import { Colxx } from 'components/common/CustomBootstrap';
import TooltipIcon from '../../../components/common/TooltipIcon';

const PolicyListView = ({
  policy,
  isSelect,
  collect,
  onCheckItem,
  canEdit = true,
  showDetails = true,
  intl,
}) => {
  let immutabilityComponent;
  if (policy.immutable) {
    immutabilityComponent = (
      <TooltipIcon
        icon="simple-icon-lock"
        item={{
          body: intl.formatMessage({ id: 'tip.immutable.policy' }),
          placement: 'top',
        }}
        id={`policy_${policy.id}`}
      />
    );
  }
  return (
    <Colxx xxs="12" className="mb-3">
      <ContextMenuTrigger id="menu_id" data={policy.id} collect={collect}>
        <Card
          onClick={(event) => onCheckItem(event, policy.id)}
          className={classnames('d-flex flex-row', {
            active: isSelect && canEdit,
          })}
        >
          <div
            className={classnames('pl-2 d-flex flex-grow-1 min-width-zero', {
              row: !canEdit,
            })}
          >
            <div
              className={classnames(
                'card-body align-self-center d-flex flex-column flex-lg-row justify-content-between min-width-zero align-items-lg-center',
                {
                  'col-lg-7': !canEdit,
                  'col-md-5': !canEdit,
                  'col-sm-12': !canEdit,
                }
              )}
            >
              <NavLink
                to={`/iam/ui/policy/${policy.id}`}
                className={classnames(undefined, {
                  'w-30': canEdit,
                  'w-50': !canEdit,
                  'w-sm-100': !canEdit,
                })}
              >
                <p className="list-item-heading mb-1 truncate">{policy.name}</p>
              </NavLink>
              {showDetails && (
                <>
                  <p className="mb-1 text-muted text-small w-15 w-sm-100">
                    Permissions: {policy.permissionCount}
                  </p>
                  <div className="w-15 w-sm-100">{immutabilityComponent}</div>
                </>
              )}
            </div>
            {policy.fromGroups && (
              <div className="mt-2 mr-3 col-lg-2 col-md-3 col-sm-12">
                <span>From Groups: </span>
                <ul className="mr-3">
                  {policy.fromGroups.map((group) => {
                    return (
                      <li key={group.id}>
                        <NavLink
                          to={`/iam/ui/group/${group.id}`}
                          className="w-10 w-sm-100"
                        >
                          {group.name}
                        </NavLink>
                      </li>
                    );
                  })}
                </ul>
              </div>
            )}
            {policy.fromRoles && (
              <div className="mt-2 mr-3 col-lg-2 col-md-3 col-sm-12">
                <span>From Roles: </span>
                <ul className="mr-3">
                  {policy.fromRoles.map((role) => {
                    return (
                      <li key={role.id}>
                        <NavLink
                          to={`/iam/ui/role/${role.id}`}
                          className="w-10 w-sm-100"
                        >
                          {role.name}
                        </NavLink>
                      </li>
                    );
                  })}
                </ul>
              </div>
            )}
            {canEdit && (
              <div className="custom-control custom-checkbox pl-1 align-self-center pr-4">
                <CustomInput
                  className="item-check mb-0"
                  type="checkbox"
                  id={`check_${policy.id}`}
                  checked={isSelect}
                  onChange={() => {}}
                  label=""
                />
              </div>
            )}
          </div>
        </Card>
      </ContextMenuTrigger>
    </Colxx>
  );
};

/* React.memo detail : https://reactjs.org/docs/react-api.html#reactpurecomponent  */
export default React.memo(PolicyListView);
