import React, { useEffect, useState } from 'react';
import {
  Button,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import ApiUtil from '../../../redux/api-util';
import GroupApi from '../../../api/group.api';
import PermissionApi from '../../../api/permission.api';
import RoleApi from '../../../api/role.api';
import PolicyApi from '../../../api/policy.api';

const AddNewPolicyModal = ({ modalOpen, toggleModal, onCreatePolicy }) => {
  const [policyName, setPolicyName] = useState('');
  const [policyDescription, setPolicyDescription] = useState('');
  const [searchGroupName, setSearchGroupName] = useState('');
  const [searchPermissionName, setSearchPermissionName] = useState('');
  const [searchRoleName, setSearchRoleName] = useState('');

  const iconSize = 22;
  const initialState = {
    policyName: '',
    groupsSelected: [],
    permissionsSelected: [],
    rolesSelected: [],
  };
  const [state, setState] = useState(initialState);
  const [groups, setGroups] = useState([]);
  const [permissions, setPermissions] = useState([]);
  const [roles, setRoles] = useState([]);

  const resetState = () => {
    setPolicyName('');

    let initialStateToUse = {
      ...initialState,
    };

    if (searchGroupName || searchGroupName.trim() === '') {
      initialStateToUse = {
        ...initialStateToUse,
        groups,
      };
    }

    if (searchPermissionName || searchPermissionName.trim() === '') {
      initialStateToUse = {
        ...initialStateToUse,
        permissions,
      };
    }

    if (searchRoleName || searchRoleName.trim() === '') {
      initialStateToUse = {
        ...initialStateToUse,
        roles,
      };
    }

    setSearchGroupName('');
    setSearchPermissionName('');
    setSearchRoleName('');
    setState(initialStateToUse);
  };

  useEffect(() => {
    const fetchGroups = async () => {
      const apiGroups = await GroupApi.getGroups(searchGroupName);

      setGroups(
        apiGroups.map((item) => ({
          id: item.id,
          name: item.name,
        }))
      );
    };
    fetchGroups().then();
  }, [searchGroupName]);

  useEffect(() => {
    const fetchPermissions = async () => {
      const apiPermissions = await PermissionApi.getPermissions(
        searchPermissionName
      );

      setPermissions(
        apiPermissions.map((item) => ({
          id: item.id,
          name: item.name,
        }))
      );
    };
    fetchPermissions().then();
  }, [searchPermissionName]);

  useEffect(async () => {
    const apiRoles = await RoleApi.getRoles(searchRoleName);

    setRoles(
      apiRoles.map((item) => ({
        id: item.id,
        name: item.name,
      }))
    );
  }, [searchRoleName]);

  const createPolicy = async (policyId) => {
    await PolicyApi.savePolicy(
      policyId,
      policyName,
      policyDescription,
      state.groupsSelected.map((group) => group.id),
      state.permissionsSelected.map((permission) => permission.id),
      state.rolesSelected.map((role) => role.id)
    );
  };

  const onCreateButtonClicked = async () => {
    const policyId = uuidv4();
    await createPolicy(policyId);
    await toggleModal();
    await onCreatePolicy();
    await resetState();
  };

  const setGroupsSelected = (groupsSelected) => {
    setState({
      ...state,
      groupsSelected,
    });
  };

  const setPermissionsSelected = (permissionsSelected) => {
    setState({
      ...state,
      permissionsSelected,
    });
  };

  const setRolesSelected = (rolesSelected) => {
    setState({
      ...state,
      rolesSelected,
    });
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-new-policy-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label>
          <IntlMessages id="pages.policy-name" />
        </Label>
        <Input
          value={policyName}
          onChange={(event) => {
            setPolicyName(event.target.value);
          }}
        />

        <Label className="mt-4">
          <IntlMessages id="pages.description" />
        </Label>
        <Input
          type="textarea"
          onChange={(event) => {
            setPolicyDescription(event.target.value);
          }}
        />

        <Label className="mt-4">
          <IntlMessages id="dashboards.groups" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={groups}
          selectedValues={state.groupsSelected}
          onSearch={setSearchGroupName}
          onSelect={setGroupsSelected}
          onRemove={setGroupsSelected}
          displayValue="name"
        />

        <Label className="mt-4">
          <IntlMessages id="menu.permissions" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={permissions}
          selectedValues={state.permissionsSelected}
          onSearch={setSearchPermissionName}
          onSelect={setPermissionsSelected}
          onRemove={setPermissionsSelected}
          displayValue="name"
        />

        <Label className="mt-4">
          <IntlMessages id="menu.roles" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={roles}
          selectedValues={state.rolesSelected}
          onSearch={setSearchRoleName}
          onSelect={setRolesSelected}
          onRemove={setRolesSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onCreateButtonClicked}>
          <IntlMessages id="pages.submit" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddNewPolicyModal);
