import React, { useState } from 'react';
import {
  Button,
  ButtonDropdown,
  CustomInput,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
} from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import PolicyListView from './PolicyListView';
import ArrayUtil from '../../../util/array.util';
import GroupApi from '../../../api/group.api';
import AddPoliciesToGroupModal from '../group/AddPoliciesToGroupModal';
import AddPoliciesToRoleModal from '../role/AddPoliciesToRoleModal';
import RoleApi from '../../../api/role.api';
import PolicyApi from '../../../api/policy.api';
import AddPoliciesToPermissionModal from '../permission/AddPoliciesToPermissionModal';

const PolicyListTab = ({
  policies,
  groupId,
  roleId,
  permissionId,
  intl,
  onChange,
}) => {
  const [lastCheckedPolicy, setLastCheckedPolicy] = useState(null);
  const [dropdownSplitOpen, setDropdownSplitOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [addPoliciesToGroupModalOpen, setAddPoliciesToGroupModalOpen] =
    useState(false);
  const [addPoliciesToRoleModalOpen, setAddPoliciesToRoleModalOpen] =
    useState(false);
  const [
    addPoliciesToPermissionModalOpen,
    setAddPoliciesToPermissionModalOpen,
  ] = useState(false);

  const onPolicySelected = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastCheckedPolicy === null) {
      setLastCheckedPolicy(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...policies];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastCheckedPolicy, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= policies.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(policies.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onRemoveClicked = async () => {
    if (groupId) {
      await GroupApi.removePoliciesFromGroups([groupId], selectedItems);
    }
    if (roleId) {
      await RoleApi.removePoliciesFromRoles([roleId], selectedItems);
    }
    if (permissionId) {
      await PolicyApi.removePermissionsFromPolicies(selectedItems, [
        permissionId,
      ]);
    }

    await onChange();
  };

  return (
    <>
      {(groupId || roleId || permissionId) && (
        <Row>
          <Colxx xxs="12" className="mb-4">
            <div className="text-zero top-right-button-container">
              <Button
                color="primary"
                size="lg"
                className="top-right-button mr-1"
                onClick={() => {
                  if (groupId) {
                    setAddPoliciesToGroupModalOpen(
                      !addPoliciesToGroupModalOpen
                    );
                  }
                  if (roleId) {
                    setAddPoliciesToRoleModalOpen(!addPoliciesToRoleModalOpen);
                  }
                  if (permissionId) {
                    setAddPoliciesToPermissionModalOpen(
                      !addPoliciesToPermissionModalOpen
                    );
                  }
                }}
              >
                <IntlMessages id="pages.add" />
              </Button>
              <ButtonDropdown
                className="btn-group"
                isOpen={dropdownSplitOpen}
                toggle={() => setDropdownSplitOpen(!dropdownSplitOpen)}
              >
                <div className="btn btn-primary btn-lg pl-4 pr-0 check-button check-all">
                  <CustomInput
                    className="custom-checkbox mb-0 d-inline-block"
                    type="checkbox"
                    id="checkAll"
                    checked={selectedItems.length >= policies.length}
                    onChange={() => handleChangeSelectAll(true)}
                    label={
                      <span
                        className={`custom-control-label ${
                          selectedItems.length > 0 &&
                          selectedItems.length < policies.length
                            ? 'indeterminate'
                            : ''
                        }`}
                      />
                    }
                  />
                </div>
                <DropdownToggle
                  caret
                  color="primary"
                  className="dropdown-toggle-split btn-lg"
                />
                <DropdownMenu right>
                  <DropdownItem onClick={onRemoveClicked}>
                    <IntlMessages id="pages.remove" />
                  </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </Colxx>
        </Row>
      )}

      <Row>
        {policies.map((policy) => {
          return (
            <PolicyListView
              canEdit={!!(groupId || roleId || permissionId)}
              showDetails={false}
              isSelect={selectedItems.includes(policy.id)}
              onCheckItem={onPolicySelected}
              intl={intl}
              key={policy.id}
              policy={policy}
            />
          );
        })}
      </Row>
      {groupId && (
        <AddPoliciesToGroupModal
          groupId={groupId}
          intl={intl}
          modalOpen={addPoliciesToGroupModalOpen}
          toggleModal={() =>
            setAddPoliciesToGroupModalOpen(!addPoliciesToGroupModalOpen)
          }
          onGroupUpdated={onChange}
          existingPolicyIds={policies.map((policy) => policy.id)}
        />
      )}
      {roleId && (
        <AddPoliciesToRoleModal
          roleId={roleId}
          intl={intl}
          modalOpen={addPoliciesToRoleModalOpen}
          toggleModal={() =>
            setAddPoliciesToRoleModalOpen(!addPoliciesToRoleModalOpen)
          }
          onRoleUpdated={onChange}
          existingPolicyIds={policies.map((policy) => policy.id)}
        />
      )}
      {permissionId && (
        <AddPoliciesToPermissionModal
          permissionId={permissionId}
          intl={intl}
          modalOpen={addPoliciesToPermissionModalOpen}
          toggleModal={() =>
            setAddPoliciesToPermissionModalOpen(
              !addPoliciesToPermissionModalOpen
            )
          }
          onPermissionUpdated={onChange}
          existingPolicyIds={policies.map((policy) => policy.id)}
        />
      )}
    </>
  );
};

export default PolicyListTab;
