import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import PermissionApi from '../../../api/permission.api';
import PolicyApi from '../../../api/policy.api';

const AddPermissionsToPolicyModal = ({
  modalOpen,
  toggleModal,
  intl,
  onPolicyUpdated,
  policyId,
  existingPermissionIds,
}) => {
  const [searchPermissionName, setSearchPermissionName] = useState('');
  const [permissions, setPermissions] = useState([]);
  const [permissionsSelected, setPermissionsSelected] = useState([]);

  const resetState = () => {
    setSearchPermissionName('');
    setPermissions([]);
    setPermissionsSelected([]);
  };

  useEffect(async () => {
    if (modalOpen) {
      const apiPermissions = await PermissionApi.getPermissions(
        searchPermissionName
      );
      setPermissions(
        apiPermissions
          .map((item) => ({
            id: item.id,
            name: item.name,
          }))
          .filter(
            (permission) => !existingPermissionIds.includes(permission.id)
          )
      );
    }
  }, [modalOpen, searchPermissionName]);

  const onAddButtonClicked = async () => {
    await PolicyApi.addPermissionsToPolicies(
      [policyId],
      permissionsSelected.map((permission) => permission.id)
    );
    await resetState();
    onPolicyUpdated();
    toggleModal();
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-permissions-to-policy-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label className="mt-4">
          <IntlMessages id="policy.permissions" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={permissions}
          selectedValues={permissionsSelected}
          onSearch={setSearchPermissionName}
          onSelect={setPermissionsSelected}
          onRemove={setPermissionsSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onAddButtonClicked}>
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddPermissionsToPolicyModal);
