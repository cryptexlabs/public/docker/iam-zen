/* eslint-disable react/no-array-index-key */
import React, { useState } from 'react';
import { Button, Input, Row } from 'reactstrap';
import { injectIntl } from 'react-intl';
import classnames from 'classnames';
import TextareaAutosize from 'react-textarea-autosize';

import { Colxx, Separator } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';

import Breadcrumb from '../../navs/Breadcrumb';
import AppApi from '../../../api/app.api';
import TooltipIcon from '../../../components/common/TooltipIcon';

const AppDetailPageHeading = ({
  intl,
  match,
  immutable,
  appName,
  appDescription,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const [newAppName, setNewAppName] = useState(appName);
  const [newAppDescription, setNewAppDescription] = useState(appDescription);

  const onSaveClicked = async () => {
    await AppApi.updateApp(match.params.appId, newAppName, newAppDescription);
    setIsEditing(false);
  };

  const onCancelClicked = () => {
    setNewAppName(appName);
    setNewAppDescription(appDescription);
    setIsEditing(false);
  };

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <div className="mb-2">
            {!isEditing && (
              <h1>
                {!newAppName && <IntlMessages id="app.app" />}
                {newAppName}
              </h1>
            )}
            {isEditing && (
              <Input
                className="col-sm-4 d-sm-inline mb-lg-0 mb-2"
                value={newAppName}
                onChange={(event) => {
                  setNewAppName(event.target.value);
                }}
              />
            )}

            <div
              className={classnames('text-zero top-right-button-container', {
                'd-inline': immutable,
              })}
            >
              {immutable && (
                <div style={{ marginTop: 10 }} className="float-right">
                  <TooltipIcon
                    icon="simple-icon-lock"
                    item={{
                      body: intl.formatMessage({
                        id: 'tip.immutable.permission',
                      }),
                      placement: 'top',
                    }}
                    id="app-immutable-icon"
                  />
                </div>
              )}

              {!immutable && (
                <>
                  {!isEditing && (
                    <Button
                      onClick={() => setIsEditing(true)}
                      color="primary"
                      size="lg"
                      className="top-right-button"
                    >
                      <IntlMessages id="pages.edit" />
                    </Button>
                  )}
                  {isEditing && (
                    <>
                      <Button
                        onClick={onSaveClicked}
                        color="primary"
                        size="lg"
                        className="mr-1 top-right-button"
                      >
                        <IntlMessages id="pages.save" />
                      </Button>
                      <Button
                        onClick={onCancelClicked}
                        outline
                        color="secondary"
                        size="lg"
                        className="top-right-button"
                      >
                        <IntlMessages id="pages.cancel" />
                      </Button>
                    </>
                  )}
                </>
              )}
            </div>

            <div className="col-sm-4 d-sm-inline">
              <Breadcrumb match={match} />
            </div>
          </div>

          <Separator className="mb-3" />
        </Colxx>
      </Row>

      <Row style={{ marginBottom: 20 }}>
        <Colxx xxs="12">
          {!isEditing && <>{newAppDescription}</>}
          {isEditing && (
            <TextareaAutosize
              style={{ width: '100%' }}
              className="form-control"
              value={newAppDescription}
              onChange={(event) => {
                setNewAppDescription(event.target.value);
              }}
            />
          )}
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(AppDetailPageHeading);
