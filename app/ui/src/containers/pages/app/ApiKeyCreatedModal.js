import { v4 as uuidv4 } from 'uuid';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import axios from 'axios';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import ApiUtil from '../../../redux/api-util';
import AppApi from '../../../api/app.api';
import RoleApi from '../../../api/role.api';

const ApiKeyCreatedModal = ({
  apiKey,
  secret,
  modalOpen,
  toggleModal,
  intl,
}) => {
  return (
    <Modal isOpen={modalOpen} toggle={toggleModal}>
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.api-key-created-modal-title" />
      </ModalHeader>
      <ModalBody>
        Please copy the secret as you will not be able to access after this
        modal closes
        <p />
        <p className="mb-1 text-small w-15 w-sm-100">Key: {apiKey}</p>
        <p />
        <p className="mb-1 text-small w-15 w-sm-100">Secret: {secret}</p>
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.close" />
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(ApiKeyCreatedModal);
