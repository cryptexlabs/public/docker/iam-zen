import React, { useState } from 'react';
import {
  Button,
  ButtonDropdown,
  CustomInput,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
} from 'reactstrap';
import { Colxx } from 'components/common/CustomBootstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import ApiKeyListView from './ApiKeyListView';
import ArrayUtil from '../../../util/array.util';
import AppApi from '../../../api/app.api';
import CreateApiKeyModal from './CreateApiKeyModal';
import ApiKeyCreatedModal from './ApiKeyCreatedModal';

const ApiKeyListTab = ({ apiKeys, appId, intl, onChange }) => {
  const [lastCheckedApiKey, setLastCheckedApiKey] = useState(null);
  const [dropdownSplitOpen, setDropdownSplitOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [createApiModalOpen, setCreateApiModalOpen] = useState(false);
  const [apiKeyCreatedModalOpen, setApiKeyCreatedModalOpen] = useState(false);
  const [createdApiKey, setCreatedApiKey] = useState({
    apiKey: null,
    secret: null,
  });

  const onApiKeySelected = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastCheckedApiKey === null) {
      setLastCheckedApiKey(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...apiKeys];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastCheckedApiKey, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= apiKeys.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(apiKeys.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onRemoveClicked = async () => {
    if (appId) {
      await AppApi.deleteApiKey(appId, selectedItems);
    }

    await onChange();
  };

  return (
    <>
      {appId && (
        <Row>
          <Colxx xxs="12" className="mb-4">
            <div className="text-zero top-right-button-container">
              <Button
                color="primary"
                size="lg"
                className="top-right-button mr-1"
                onClick={() => {
                  if (appId) {
                    setCreateApiModalOpen(!createApiModalOpen);
                  }
                }}
              >
                <IntlMessages id="pages.add" />
              </Button>
              <ButtonDropdown
                className="btn-apiKey"
                isOpen={dropdownSplitOpen}
                toggle={() => setDropdownSplitOpen(!dropdownSplitOpen)}
              >
                <div className="btn btn-primary btn-lg pl-4 pr-0 check-button check-all">
                  <CustomInput
                    className="custom-checkbox mb-0 d-inline-block"
                    type="checkbox"
                    id="checkAll"
                    checked={selectedItems.length >= apiKeys.length}
                    onChange={() => handleChangeSelectAll(true)}
                    label={
                      <span
                        className={`custom-control-label ${
                          selectedItems.length > 0 &&
                          selectedItems.length < apiKeys.length
                            ? 'indeterminate'
                            : ''
                        }`}
                      />
                    }
                  />
                </div>
                <DropdownToggle
                  caret
                  color="primary"
                  className="dropdown-toggle-split btn-lg"
                />
                <DropdownMenu right>
                  <DropdownItem onClick={onRemoveClicked}>
                    <IntlMessages id="pages.remove" />
                  </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </Colxx>
        </Row>
      )}

      <Row>
        {apiKeys.map((apiKey) => {
          return (
            <ApiKeyListView
              showDetails={false}
              isSelect={selectedItems.includes(apiKey.id)}
              onCheckItem={onApiKeySelected}
              intl={intl}
              key={apiKey.id}
              apiKey={apiKey}
            />
          );
        })}
      </Row>
      {appId && (
        <CreateApiKeyModal
          appId={appId}
          intl={intl}
          modalOpen={createApiModalOpen}
          toggleModal={() => setCreateApiModalOpen(!createApiModalOpen)}
          onCreateApiKey={(apiKey) => {
            setCreatedApiKey(apiKey);
            setApiKeyCreatedModalOpen(true);
            onChange();
          }}
        />
      )}
      {appId && (
        <ApiKeyCreatedModal
          apiKey={createdApiKey.apiKey}
          secret={createdApiKey.secret}
          intl={intl}
          modalOpen={apiKeyCreatedModalOpen}
          toggleModal={() => setApiKeyCreatedModalOpen(!apiKeyCreatedModalOpen)}
        />
      )}
    </>
  );
};

export default ApiKeyListTab;
