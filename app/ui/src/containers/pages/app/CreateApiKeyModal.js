import { v4 as uuidv4 } from 'uuid';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import axios from 'axios';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import ApiUtil from '../../../redux/api-util';
import AppApi from '../../../api/app.api';
import RoleApi from '../../../api/role.api';

const CreateApiKeyModal = ({
  appId,
  modalOpen,
  toggleModal,
  intl,
  onCreateApiKey,
}) => {
  const [description, setDescription] = useState('');

  const resetState = () => {
    setDescription('');
  };

  const createApiKey = async () => {
    const response = await AppApi.createApiKey(appId, description);
    resetState();
    return response;
  };

  const onCreateButtonClicked = async () => {
    const apiKeyData = await createApiKey();
    await toggleModal();
    await onCreateApiKey(apiKeyData);
    await resetState();
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-new-api-key-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label>
          <IntlMessages id="app.api-key.description" />
        </Label>
        <Input
          value={description}
          onChange={(event) => {
            setDescription(event.target.value);
          }}
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onCreateButtonClicked}>
          <IntlMessages id="pages.submit" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(CreateApiKeyModal);
