/* eslint-disable react/no-array-index-key */
import React, { useState } from 'react';
import { Button, Input, Row } from 'reactstrap';
import { injectIntl } from 'react-intl';
import classnames from 'classnames';
import TextareaAutosize from 'react-textarea-autosize';

import { Colxx, Separator } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';

import Breadcrumb from '../../navs/Breadcrumb';
import GroupApi from '../../../api/group.api';
import TooltipIcon from '../../../components/common/TooltipIcon';

const GroupDetailPageHeading = ({
  intl,
  match,
  immutable,
  groupName,
  groupDescription,
}) => {
  const [isEditing, setIsEditing] = useState(false);
  const [newGroupName, setNewGroupName] = useState(groupName);
  const [newGroupDescription, setNewGroupDescription] =
    useState(groupDescription);

  const onSaveClicked = async () => {
    await GroupApi.updateGroup(
      match.params.groupId,
      newGroupName,
      newGroupDescription
    );
    setIsEditing(false);
  };

  const onCancelClicked = () => {
    setNewGroupName(groupName);
    setNewGroupDescription(groupDescription);
    setIsEditing(false);
  };

  return (
    <>
      <Row>
        <Colxx xxs="12">
          <div className="mb-2">
            {!isEditing && (
              <h1>
                {!newGroupName && <IntlMessages id="group.group" />}
                {newGroupName}
              </h1>
            )}
            {isEditing && (
              <Input
                className="col-sm-4 d-sm-inline mb-lg-0 mb-2"
                value={newGroupName}
                onChange={(event) => {
                  setNewGroupName(event.target.value);
                }}
              />
            )}

            <div
              className={classnames('text-zero top-right-button-container', {
                'd-inline': immutable,
              })}
            >
              {immutable && (
                <div style={{ marginTop: 10 }} className="float-right">
                  <TooltipIcon
                    icon="simple-icon-lock"
                    item={{
                      body: intl.formatMessage({
                        id: 'tip.immutable.permission',
                      }),
                      placement: 'top',
                    }}
                    id="group-immutable-icon"
                  />
                </div>
              )}

              {!immutable && (
                <>
                  {!isEditing && (
                    <Button
                      onClick={() => setIsEditing(true)}
                      color="primary"
                      size="lg"
                      className="top-right-button"
                    >
                      <IntlMessages id="pages.edit" />
                    </Button>
                  )}
                  {isEditing && (
                    <>
                      <Button
                        onClick={onSaveClicked}
                        color="primary"
                        size="lg"
                        className="mr-1 top-right-button"
                      >
                        <IntlMessages id="pages.save" />
                      </Button>
                      <Button
                        onClick={onCancelClicked}
                        outline
                        color="secondary"
                        size="lg"
                        className="top-right-button"
                      >
                        <IntlMessages id="pages.cancel" />
                      </Button>
                    </>
                  )}
                </>
              )}
            </div>

            <div className="col-sm-4 d-sm-inline">
              <Breadcrumb match={match} />
            </div>
          </div>

          <Separator className="mb-3" />
        </Colxx>
      </Row>

      <Row style={{ marginBottom: 20 }}>
        <Colxx xxs="12">
          {!isEditing && <>{newGroupDescription}</>}
          {isEditing && (
            <TextareaAutosize
              style={{ width: '100%' }}
              className="form-control"
              value={newGroupDescription}
              onChange={(event) => {
                setNewGroupDescription(event.target.value);
              }}
            />
          )}
        </Colxx>
      </Row>
    </>
  );
};

export default injectIntl(GroupDetailPageHeading);
