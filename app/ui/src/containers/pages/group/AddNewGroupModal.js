import { v4 as uuidv4 } from 'uuid';
import React, { useEffect, useState } from 'react';
import {
  Button,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  Tooltip,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import { FcCheckmark } from 'react-icons/fc';
import { injectIntl } from 'react-intl';
import Multiselect from 'multiselect-react-dropdown';
import PolicyApi from '../../../api/policy.api';
import GroupApi from '../../../api/group.api';
import WarnTooltipIcon from '../../../components/common/WarnTooltipIcon';

const AddNewGroupModal = ({ modalOpen, toggleModal, onCreateGroup }) => {
  const [groupName, setGroupName] = useState('');
  const [groupDescription, setGroupDescription] = useState('');
  const [groupExists, setGroupExists] = useState('unknown');
  const [policiesSelected, setPoliciesSelected] = useState([]);
  const [policies, setPolicies] = useState([]);
  const [searchPolicy, setSearchPolicy] = useState('');

  const resetState = () => {
    setGroupName('');
    setGroupDescription('');
    setSearchPolicy('');
    setPolicies([]);
    setPoliciesSelected([]);
  };

  useEffect(async () => {
    if (groupName) {
      const exists = await GroupApi.groupExists(groupName);

      setGroupExists(exists ? 'exists' : 'not-exists');
    } else {
      setGroupExists('unknown');
    }
  }, [groupName]);

  useEffect(async () => {
    const apiPolicies = await PolicyApi.getPolicies(searchPolicy);

    setPolicies(
      apiPolicies.map((item) => ({
        id: item.id,
        name: item.name,
      }))
    );
  }, [searchPolicy]);

  const iconSize = 22;

  const createGroup = async (groupId) => {
    await GroupApi.saveGroup(
      groupId,
      groupName,
      groupDescription,
      policiesSelected.map((policy) => policy.id)
    );
  };

  const onCreateButtonClicked = async () => {
    const groupId = uuidv4();
    await createGroup(groupId);
    await toggleModal();
    await onCreateGroup();
    await resetState();
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-new-group-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label>
          <IntlMessages id="pages.group-name" />
        </Label>

        <div className="modal-checkbox">
          <Input
            value={groupName}
            onChange={(event) => setGroupName(event.target.value)}
          />
          <div className="modal-checkbox-container">
            <div className="modal-checkbox-wrapper">
              {groupExists === 'exists' && (
                <WarnTooltipIcon
                  iconSize={iconSize}
                  id="group-exists-tooltip-icon"
                />
              )}
              {groupExists === 'not-exists' && (
                <FcCheckmark size={iconSize} className="modal-checkbox-icon" />
              )}
              {groupExists === 'unknown' && (
                <div
                  style={{ width: iconSize }}
                  className="modal-checkbox-icon"
                >
                  &#8203;
                </div>
              )}
            </div>
          </div>
        </div>
        <div>
          <Label className="mt-4">
            <IntlMessages id="pages.description" />
          </Label>
          <Input
            value={groupDescription}
            type="textarea"
            name="textValue"
            onChange={(event) => setGroupDescription(event.target.value)}
          />
        </div>
        <Label className="mt-4">
          <IntlMessages id="menu.policies" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={policies}
          selectedValues={policiesSelected}
          onSearch={setSearchPolicy}
          onSelect={setPoliciesSelected}
          onRemove={setPoliciesSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button
          color="primary"
          disabled={groupExists === 'exists'}
          onClick={onCreateButtonClicked}
        >
          <IntlMessages id="pages.submit" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};
export default injectIntl(AddNewGroupModal);
