import React, { useEffect, useRef, useState } from 'react';
import {
  Button,
  Input,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';

import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import { FcCancel, FcCheckmark } from 'react-icons/fc';
import { injectIntl } from 'react-intl';
import ApiUtil from '../../../redux/api-util';
import PermissionApi from '../../../api/permission.api';
import PolicyApi from '../../../api/policy.api';

const AddNewPermissionModal = ({
  modalOpen,
  toggleModal,
  intl,
  onCreatePermission,
}) => {
  const [permissionName, setPermissionName] = useState('');
  const [searchPolicyName, setSearchPolicyName] = useState('');
  const [permissionValue, setPermissionValue] = useState('');
  const descriptionInput = useRef();

  const initialState = {
    permissionName: '',
    permissionValue: '',
    policiesSelected: [],
  };

  const [state, setState] = useState(initialState);

  const resetState = () => {
    setPermissionName('');
    setPermissionValue('');

    let initialStateToUse = {
      ...initialState,
    };

    if (searchPolicyName || searchPolicyName.trim() === '') {
      initialStateToUse = {
        ...initialStateToUse,
        policies: state.policies,
      };
    }

    setSearchPolicyName('');
    setState(initialStateToUse);
  };

  useEffect(async () => {
    if (!permissionName || !permissionName.trim()) {
      setState({
        ...state,
        permissionExists: 'unknown',
      });
      return;
    }

    const permissionExists = await PermissionApi.permissionExists(
      permissionValue
    );
    setState({
      ...state,
      permissionExists: permissionExists === true ? 'exists' : 'not-exists',
    });
  }, [permissionValue]);

  useEffect(() => {
    const fetchPolicies = async () => {
      const apiPolicies = await PolicyApi.getPolicies(searchPolicyName);

      setState({
        ...state,
        policies: apiPolicies.map((item) => ({
          id: item.id,
          name: item.name,
        })),
      });
    };
    fetchPolicies().then();
  }, [searchPolicyName]);

  const iconSize = 22;

  const createPermission = async (permissionId) => {
    await PermissionApi.savePermission(
      permissionId,
      permissionName,
      descriptionInput.current.value,
      permissionValue,
      state.policiesSelected.map((policy) => policy.id)
    );
  };

  const onCreateButtonClicked = async () => {
    const permissionId = uuidv4();
    await createPermission(permissionId);
    await toggleModal();
    await onCreatePermission();
    await resetState();
  };

  const setPoliciesSelected = (policiesSelected) => {
    setState({
      ...state,
      policiesSelected,
    });
  };

  return (
    <Modal
      isOpen={modalOpen}
      toggle={toggleModal}
      wrapClassName="modal-right"
      backdrop="static"
    >
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-new-permission-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label>
          <IntlMessages id="pages.permission-name" />
        </Label>
        <Input
          value={permissionName}
          onChange={(event) => setPermissionName(event.target.value)}
        />

        <div>
          <Label className="mt-4">
            <IntlMessages id="pages.permission-value" />
          </Label>
          <div className="modal-checkbox">
            <Input
              value={permissionValue}
              onChange={(event) => setPermissionValue(event.target.value)}
            />
            <div className="modal-checkbox-container">
              <div className="modal-checkbox-wrapper">
                {state.permissionExists === 'exists' && (
                  <FcCancel size={iconSize} className="modal-checkbox-icon" />
                )}
                {state.permissionExists === 'not-exists' && (
                  <FcCheckmark
                    size={iconSize}
                    className="modal-checkbox-icon"
                  />
                )}
                {state.permissionExists === 'unknown' && (
                  <div
                    style={{ width: iconSize }}
                    className="modal-checkbox-icon"
                  >
                    &#8203;
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
        <div>
          <Label className="mt-4">
            <IntlMessages id="pages.description" />
          </Label>
          <Input type="textarea" name="text" innerRef={descriptionInput} />
        </div>
        <Label className="mt-4">
          <IntlMessages id="menu.policy" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={state.policies}
          selectedValues={state.policiesSelected}
          onSearch={setSearchPolicyName}
          onSelect={setPoliciesSelected}
          onRemove={setPoliciesSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button
          color="primary"
          disabled={state.permissionExists === 'exists'}
          onClick={onCreateButtonClicked}
        >
          <IntlMessages id="pages.submit" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AddNewPermissionModal);
