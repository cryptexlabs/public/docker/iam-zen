import React, { useState } from 'react';
import {
  Button,
  ButtonDropdown,
  CustomInput,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
} from 'reactstrap';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import IntlMessages from '../../../helpers/IntlMessages';
import PermissionListView from './PermissionListView';
import ArrayUtil from '../../../util/array.util';
import PolicyApi from '../../../api/policy.api';
import AddPermissionsToPolicyModal from '../policy/AddPermissionsToPolicyModal';

const PermissionListTab = ({ permissions, policyId, intl, onChange }) => {
  const [lastCheckedPermission, setLastCheckedPermission] = useState(null);
  const [dropdownSplitOpen, setDropdownSplitOpen] = useState(false);
  const [selectedItems, setSelectedItems] = useState([]);
  const [addPermissionsToPolicyModalOpen, setAddPermissionsToPolicyModalOpen] =
    useState(false);

  const onPermissionSelected = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastCheckedPermission === null) {
      setLastCheckedPermission(id);
    }

    let selectedList = [...selectedItems];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedItems(selectedList);

    if (event.shiftKey) {
      let newItems = [...permissions];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastCheckedPermission, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedItems.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedItems));
      setSelectedItems(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedItems.length >= permissions.length) {
      if (isToggle) {
        setSelectedItems([]);
      }
    } else {
      setSelectedItems(permissions.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onRemoveClicked = async () => {
    await PolicyApi.removePermissionsFromPolicies([policyId], selectedItems);
    await onChange();
  };

  return (
    <>
      {policyId && (
        <Row>
          <Colxx xxs="12" className="mb-4">
            <div className="text-zero top-right-button-container">
              <Button
                color="primary"
                size="lg"
                className="top-right-button mr-1"
                onClick={() =>
                  setAddPermissionsToPolicyModalOpen(
                    !addPermissionsToPolicyModalOpen
                  )
                }
              >
                <IntlMessages id="pages.add" />
              </Button>
              <ButtonDropdown
                className="btn-group"
                isOpen={dropdownSplitOpen}
                toggle={() => setDropdownSplitOpen(!dropdownSplitOpen)}
              >
                <div className="btn btn-primary btn-lg pl-4 pr-0 check-button check-all">
                  <CustomInput
                    className="custom-checkbox mb-0 d-inline-block"
                    type="checkbox"
                    id="checkAll"
                    checked={selectedItems.length >= permissions.length}
                    onChange={() => handleChangeSelectAll(true)}
                    label={
                      <span
                        className={`custom-control-label ${
                          selectedItems.length > 0 &&
                          selectedItems.length < permissions.length
                            ? 'indeterminate'
                            : ''
                        }`}
                      />
                    }
                  />
                </div>
                <DropdownToggle
                  caret
                  color="primary"
                  className="dropdown-toggle-split btn-lg"
                />
                <DropdownMenu right>
                  <DropdownItem onClick={onRemoveClicked}>
                    <IntlMessages id="pages.remove" />
                  </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </Colxx>
        </Row>
      )}

      <Row>
        {permissions.map((permission) => {
          return (
            <PermissionListView
              canEdit={!!policyId}
              showDetails={false}
              isSelect={selectedItems.includes(permission.id)}
              onCheckItem={onPermissionSelected}
              intl={intl}
              key={permission.id}
              permission={permission}
            />
          );
        })}
      </Row>
      {policyId && (
        <AddPermissionsToPolicyModal
          policyId={policyId}
          intl={intl}
          modalOpen={addPermissionsToPolicyModalOpen}
          toggleModal={() =>
            setAddPermissionsToPolicyModalOpen(!addPermissionsToPolicyModalOpen)
          }
          onPolicyUpdated={onChange}
          existingPermissionIds={permissions.map((permission) => permission.id)}
        />
      )}
    </>
  );
};

export default PermissionListTab;
