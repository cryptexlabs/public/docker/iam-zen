import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import GroupApi from '../../../api/group.api';
import UserApi from '../../../api/user.api';

const DeleteUserConfirmModal = ({
  modalOpen,
  toggleModal,
  onUserDeleted,
  userId,
  userName,
}) => {
  const onDeleteButtonClicked = async () => {
    await UserApi.deleteUser(userId);
    onUserDeleted();
    toggleModal();
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.delete-user-modal-title" />
      </ModalHeader>
      <ModalBody>
        <p>Are you sure you want to delete this user?</p>
        <p>Username: {userName}</p>
        <p>User ID: {userId}</p>
      </ModalBody>
      <ModalFooter>
        <Button color="primary" onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="danger" onClick={onDeleteButtonClicked}>
          <IntlMessages id="pages.delete" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(DeleteUserConfirmModal);
