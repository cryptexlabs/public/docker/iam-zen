import React from 'react';
import { Row } from 'reactstrap';
import IdentityLinkListView from './IdentityLinkListView';

const IdentityLinkListTab = ({ identityLinks, intl }) => {
  return (
    <>
      <Row>
        {identityLinks.map((identityLink) => {
          return (
            <IdentityLinkListView
              showDetails={false}
              intl={intl}
              key={`${identityLink.usage}-${identityLink.type}-${identityLink.linkId}`}
              identityLink={identityLink}
            />
          );
        })}
      </Row>
    </>
  );
};

export default IdentityLinkListTab;
