// noinspection JSSuspiciousEqPlus

import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import { useHistory, useLocation } from 'react-router-dom';
import UserListPageListing from './UserListPageListing';
import ArrayUtil from '../../../util/array.util';
import ApiUtil from '../../../redux/api-util';
import UserListPageHeading from './UserListPageHeading';
import AddUsersToGroupModal from './AddUsersToGroupModal';

const orderOptions = [
  { column: 'username', label: 'Username' },
  { column: 'groupCount', label: 'Group Count' },
];
const pageSizes = [4, 8, 12, 20];

const UserListTab = ({ intl, groupId, policyId, permissionId }) => {
  const [displayMode, setDisplayMode] = useState('list');
  const [selectedUserIds, setSelectedUserIds] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPage, setTotalPage] = useState(1);
  const [dataIsFetching, setDataIsFetching] = useState(true);
  const [lastChecked, setLastChecked] = useState(null);
  const [items, setItems] = useState([]);
  const [selectedPageSize, setSelectedPageSize] = useState(8);
  const [selectedOrderOption, setSelectedOrderOption] = useState({
    column: 'username',
    label: 'Username',
  });
  const [selectedOrderDirection, setSelectedOrderDirection] = useState('asc');
  const [totalItemCount, setTotalItemCount] = useState(0);
  const [isLoaded, setIsLoaded] = useState(false);
  const [addUsersToGroupsModalOpen, setAddUsersToGroupsModalOpen] =
    useState(false);

  const history = useHistory();
  const location = useLocation();

  const searchParams = new URLSearchParams(location.search);
  const search = searchParams.get('search') || '';

  const updateQueryParameter = (key, value) => {
    searchParams.set(key, value);

    const newSearch = searchParams.toString();

    history.replace({
      pathname: location.pathname,
      search: newSearch,
    });
  };

  useEffect(() => {
    setCurrentPage(1);
  }, [selectedPageSize, selectedOrderOption]);

  let fetchUrl;
  if (groupId) {
    fetchUrl = `/group/${groupId}/user`;
  }
  if (policyId) {
    fetchUrl = `/policy/${policyId}/user`;
  }
  if (permissionId) {
    fetchUrl = `/permission/${permissionId}/user`;
  }

  const fetchData = useCallback(async () => {
    setDataIsFetching(true);
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    axios
      .get(fetchUrl, {
        headers,
        baseURL: ApiUtil.apiBasePath,
        params: {
          pageSize: selectedPageSize,
          page: currentPage,
          orderBy: selectedOrderOption.column,
          orderDirection: selectedOrderDirection,
          search,
        },
      })
      .then((res) => {
        return res.data;
      })
      .then((message) => {
        console.log(message);
        setTotalPage(
          Math.ceil(message.meta.totalRecords / (selectedPageSize || 1))
        );
        setItems(
          message.data.map((x) => {
            return { ...x };
          })
        );
        setSelectedUserIds([]);
        setTotalItemCount(message.meta.totalRecords);
        setIsLoaded(true);
        setDataIsFetching(false);
        if (message.data.length === 0 && currentPage > 1) {
          setCurrentPage(currentPage - 1);
        }
      });
  }, [
    currentPage,
    selectedPageSize,
    selectedOrderOption,
    selectedOrderDirection,
    search,
    fetchUrl,
  ]);

  useEffect(() => {
    fetchData().then();
  }, [
    selectedPageSize,
    currentPage,
    selectedOrderOption,
    selectedOrderDirection,
    search,
    fetchData,
  ]);

  const onContextMenu = (e, data) => {
    const clickedProductId = data.data;
    if (!selectedUserIds.includes(clickedProductId)) {
      setSelectedUserIds([clickedProductId]);
    }

    return true;
  };

  const onContextMenuClick = (e, data) => {
    console.log('onContextMenuClick - selected items', selectedUserIds);
    console.log('onContextMenuClick - action : ', data.action);
  };

  const onCheckItem = (event, id) => {
    if (
      event.target.tagName === 'A' ||
      (event.target.parentElement && event.target.parentElement.tagName === 'A')
    ) {
      return true;
    }
    if (lastChecked === null) {
      setLastChecked(id);
    }

    let selectedList = [...selectedUserIds];
    if (selectedList.includes(id)) {
      selectedList = selectedList.filter((x) => x !== id);
    } else {
      selectedList.push(id);
    }
    setSelectedUserIds(selectedList);

    if (event.shiftKey) {
      let newItems = [...items];
      const start = ArrayUtil.getIndex(id, newItems, 'id');
      const end = ArrayUtil.getIndex(lastChecked, newItems, 'id');
      newItems = newItems.slice(Math.min(start, end), Math.max(start, end) + 1);
      selectedUserIds.push(
        ...newItems.map((item) => {
          return item.id;
        })
      );
      selectedList = Array.from(new Set(selectedUserIds));
      setSelectedUserIds(selectedList);
    }
    document.activeElement.blur();
    return false;
  };

  const handleChangeSelectAll = (isToggle) => {
    if (selectedUserIds.length >= items.length) {
      if (isToggle) {
        setSelectedUserIds([]);
      }
    } else {
      setSelectedUserIds(items.map((x) => x.id));
    }
    document.activeElement.blur();
    return false;
  };

  const onDeleteClicked = async () => {
    setDataIsFetching(true);
    const headers = await ApiUtil.getAuthorizedApiHeaders();

    for (const userId of selectedUserIds) {
      await axios
        .delete(`/user/${userId}/group`, {
          headers,
          baseURL: ApiUtil.apiBasePath,
          params: {
            ids: [groupId],
          },
        })
        .catch();
    }

    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  const onDataChanged = async () => {
    setDataIsFetching(true);
    await new Promise((resolve) => setTimeout(resolve, 1000));
    fetchData().then();
  };

  const startIndex = (currentPage - 1) * selectedPageSize;
  const endIndex = currentPage * selectedPageSize;

  return (
    <>
      <UserListPageHeading
        displayMode={displayMode}
        changeDisplayMode={setDisplayMode}
        handleChangeSelectAll={handleChangeSelectAll}
        changeOrderBy={(column) => {
          setSelectedOrderOption(orderOptions.find((x) => x.column === column));
        }}
        changeOrderDirection={(direction) => {
          setSelectedOrderDirection(direction);
        }}
        selectedOrderDirection={selectedOrderDirection}
        changePageSize={setSelectedPageSize}
        selectedPageSize={selectedPageSize}
        totalItemCount={totalItemCount}
        selectedOrderOption={selectedOrderOption}
        startIndex={startIndex}
        endIndex={endIndex}
        selectedItemsLength={selectedUserIds ? selectedUserIds.length : 0}
        itemsLength={items ? items.length : 0}
        onSearchKey={(e) => {
          updateQueryParameter('search', e.target.value.toLowerCase());
        }}
        searchValue={search}
        orderOptions={orderOptions}
        pageSizes={pageSizes}
        onDeleteClicked={onDeleteClicked}
        toggleModal={() =>
          setAddUsersToGroupsModalOpen(!addUsersToGroupsModalOpen)
        }
        groupId={groupId}
        showListOptions={!!groupId}
      />
      <AddUsersToGroupModal
        groupId={groupId}
        modalOpen={addUsersToGroupsModalOpen}
        toggleModal={() =>
          setAddUsersToGroupsModalOpen(!addUsersToGroupsModalOpen)
        }
        selectedUserIds={selectedUserIds}
        onGroupsUpdated={onDataChanged}
      />
      <UserListPageListing
        items={items}
        displayMode={displayMode}
        selectedItems={selectedUserIds}
        onCheckItem={onCheckItem}
        currentPage={currentPage}
        totalPage={totalPage}
        onContextMenuClick={onContextMenuClick}
        onContextMenu={onContextMenu}
        onChangePage={setCurrentPage}
        isLoading={dataIsFetching && !isLoaded}
        intl={intl}
        canCheckItem={!!groupId}
      />
    </>
  );
};

export default UserListTab;
