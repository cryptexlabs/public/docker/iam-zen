/* eslint-disable react/no-array-index-key */
import React, { useEffect, useState } from 'react';
import {
  Button,
  ButtonDropdown,
  Collapse,
  CustomInput,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
  Tooltip,
  UncontrolledDropdown,
} from 'reactstrap';
import { injectIntl } from 'react-intl';
import classnames from 'classnames';
import { Colxx, Separator } from 'components/common/CustomBootstrap';
import IntlMessages from 'helpers/IntlMessages';
import Breadcrumb from '../../navs/Breadcrumb';

const UserListPageHeading = ({
  intl,
  handleChangeSelectAll,
  changeOrderBy,
  changePageSize,
  selectedPageSize,
  totalItemCount,
  selectedOrderOption,
  changeOrderDirection,
  selectedOrderDirection,
  match,
  startIndex,
  endIndex,
  selectedItemsLength,
  itemsLength,
  onSearchKey,
  orderOptions,
  pageSizes,
  toggleModal,
  heading,
  onDeleteClicked,
  onAddGroups,
  onExport,
  onRemoveGroups,
  groupId,
  showListOptions = true,
  searchValue,
}) => {
  const [dropdownSplitOpen, setDropdownSplitOpen] = useState(false);
  const [displayOptionsIsOpen, setDisplayOptionsIsOpen] = useState(false);
  const [dropdownTooltipOpen, setDropdownTooltipOpen] = useState(false);
  const [dropdownHovered, setDropdownHovered] = useState(false);
  const [dropdownTooltipText, setDropdownTooltipText] = useState(
    'Please select at least one user'
  );
  const { messages } = intl;

  useEffect(() => {
    setTimeout(() => {
      if (dropdownSplitOpen && selectedItemsLength === 0 && dropdownHovered) {
        setDropdownTooltipOpen(
          dropdownSplitOpen && selectedItemsLength === 0 && dropdownHovered
        );
      } else if (selectedItemsLength === 0) {
        // Make the tooltip behave
        setDropdownTooltipOpen(true);
        setDropdownTooltipOpen(false);
      } else {
        setDropdownTooltipOpen(false);
      }
    }, 0);
  }, [selectedItemsLength, dropdownSplitOpen, dropdownHovered]);

  return (
    <Row>
      <Colxx xxs="12">
        <div className="mb-2">
          {heading && (
            <h1>
              <IntlMessages id={heading} />
            </h1>
          )}

          {showListOptions && (
            <div className="text-zero top-right-button-container">
              <Button
                color="primary"
                size="lg"
                className="top-right-button"
                onClick={() => toggleModal()}
              >
                <IntlMessages id="pages.add-new" />
              </Button>
              {'  '}
              <ButtonDropdown
                isOpen={dropdownSplitOpen}
                toggle={() => setDropdownSplitOpen(!dropdownSplitOpen)}
              >
                <div className="btn btn-primary btn-lg pl-4 pr-0 check-button check-all">
                  <CustomInput
                    className="custom-checkbox mb-0 d-inline-block"
                    type="checkbox"
                    id="checkAll"
                    checked={selectedItemsLength >= itemsLength}
                    onChange={() => handleChangeSelectAll(true)}
                    label={
                      <span
                        className={`custom-control-label ${
                          selectedItemsLength > 0 &&
                          selectedItemsLength < itemsLength
                            ? 'indeterminate'
                            : ''
                        }`}
                      />
                    }
                  />
                </div>
                <DropdownToggle
                  caret
                  color="primary"
                  className="dropdown-toggle-split btn-lg"
                />
                <DropdownMenu
                  id="user-items-dropdown-menu"
                  right
                  className={classnames({
                    disabled: selectedItemsLength === 0,
                  })}
                  onMouseEnter={() => setDropdownHovered(true)}
                  onMouseLeave={() => setDropdownHovered(false)}
                >
                  <DropdownItem
                    onClick={onDeleteClicked}
                    disabled={selectedItemsLength === 0}
                  >
                    {!groupId && <IntlMessages id="pages.delete" />}
                    {groupId && <IntlMessages id="pages.remove" />}
                  </DropdownItem>
                  {onAddGroups && (
                    <DropdownItem
                      onClick={onAddGroups}
                      disabled={selectedItemsLength === 0}
                    >
                      <IntlMessages id="pages.add-users-to-groups" />
                    </DropdownItem>
                  )}
                  {onRemoveGroups && (
                    <DropdownItem
                      onClick={onRemoveGroups}
                      disabled={selectedItemsLength === 0}
                    >
                      <IntlMessages id="pages.remove-users-from-groups" />
                    </DropdownItem>
                  )}
                  {onExport && (
                    <DropdownItem
                      onClick={onExport}
                      disabled={selectedItemsLength === 0}
                    >
                      <IntlMessages id="pages.export" />
                    </DropdownItem>
                  )}
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          )}
          {match && <Breadcrumb match={match} />}
          {!match && showListOptions && <div style={{ height: '2.75rem' }} />}

          {showListOptions && (
            <Tooltip
              placement="left"
              isOpen={dropdownTooltipOpen}
              target="user-items-dropdown-menu"
              toggle={() => setDropdownTooltipOpen(!dropdownTooltipOpen)}
            >
              {dropdownTooltipText}
            </Tooltip>
          )}
        </div>

        <div className="mb-2">
          <Button
            color="empty"
            className="pt-0 pl-0 d-inline-block d-md-none"
            onClick={() => setDisplayOptionsIsOpen(!displayOptionsIsOpen)}
          >
            <IntlMessages id="pages.display-options" />{' '}
            <i className="simple-icon-arrow-down align-middle" />
          </Button>
          <Collapse
            isOpen={displayOptionsIsOpen}
            className="d-md-block"
            id="displayOptions"
          >
            <div className="d-block d-md-inline-block pt-1">
              <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                <DropdownToggle caret color="outline-dark" size="xs">
                  <IntlMessages id="pages.orderby" />
                  {selectedOrderOption.label}
                </DropdownToggle>
                <DropdownMenu>
                  {orderOptions.map((order, index) => {
                    return (
                      <DropdownItem
                        key={index}
                        onClick={() => changeOrderBy(order.column)}
                      >
                        {order.label}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
              <UncontrolledDropdown className="mr-1 float-md-left btn-group mb-1">
                <DropdownToggle caret color="outline-dark" size="xs">
                  <IntlMessages id="pages.order-direction" />
                  {selectedOrderDirection === 'asc'
                    ? 'Ascending'
                    : 'Descending'}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem
                    key={0}
                    onClick={() => changeOrderDirection('asc')}
                  >
                    Ascending
                  </DropdownItem>
                  <DropdownItem
                    key={1}
                    onClick={() => changeOrderDirection('desc')}
                  >
                    Descending
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <div className="search-sm d-inline-block float-md-left mr-1 mb-1 align-top">
                <input
                  type="text"
                  name="keyword"
                  id="search"
                  value={searchValue}
                  placeholder={messages['menu.search']}
                  onKeyDown={(e) => onSearchKey(e)}
                  onChange={(e) => onSearchKey(e)}
                />
              </div>
            </div>
            <div className="float-md-right pt-1">
              <span className="text-muted text-small mr-1">
                <IntlMessages
                  id="data-list.page-count-summary"
                  values={{
                    startItemNumber: startIndex + 1,
                    endItemNumber: Math.min(totalItemCount, endIndex),
                    totalItemCount,
                  }}
                />
              </span>
              <UncontrolledDropdown className="d-inline-block">
                <DropdownToggle caret color="outline-dark" size="xs">
                  {selectedPageSize}
                </DropdownToggle>
                <DropdownMenu right>
                  {pageSizes.map((size, index) => {
                    return (
                      <DropdownItem
                        key={index}
                        onClick={() => changePageSize(size)}
                      >
                        {size}
                      </DropdownItem>
                    );
                  })}
                </DropdownMenu>
              </UncontrolledDropdown>
            </div>
          </Collapse>
        </div>
        <Separator className="mb-5" />
      </Colxx>
    </Row>
  );
};

export default injectIntl(UserListPageHeading);
