import React, { useEffect, useState } from 'react';
import {
  Button,
  Label,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
} from 'reactstrap';
import IntlMessages from 'helpers/IntlMessages';
import Multiselect from 'multiselect-react-dropdown';
import { injectIntl } from 'react-intl';
import GroupApi from '../../../api/group.api';
import UserApi from '../../../api/user.api';

const AdGroupsToUserModal = ({
  modalOpen,
  toggleModal,
  intl,
  onUserUpdated,
  userId,
  existingGroupIds,
}) => {
  const [searchGroupName, setSearchGroupName] = useState('');
  const [groups, setGroups] = useState([]);
  const [groupsSelected, setGroupsSelected] = useState([]);

  const resetState = () => {
    setSearchGroupName('');
    setGroups([]);
    setGroupsSelected([]);
  };

  useEffect(async () => {
    if (modalOpen) {
      const apiGroups = await GroupApi.getGroups(searchGroupName);
      setGroups(
        apiGroups
          .map((item) => ({
            id: item.id,
            name: item.name,
          }))
          .filter((group) => !existingGroupIds.includes(group.id))
      );
    }
  }, [modalOpen, searchGroupName]);

  const onAddButtonClicked = async () => {
    await UserApi.addGroupsToUsers(
      [userId],
      groupsSelected.map((group) => group.id)
    );
    await resetState();
    onUserUpdated();
    toggleModal();
  };

  return (
    <Modal isOpen={modalOpen} toggle={toggleModal} backdrop="static">
      <ModalHeader toggle={toggleModal}>
        <IntlMessages id="pages.add-groups-to-user-modal-title" />
      </ModalHeader>
      <ModalBody>
        <Label className="mt-4">
          <IntlMessages id="user.groups" />
        </Label>
        <Multiselect
          style={{ color: 'black' }}
          avoidHighlightFirstOption={true}
          options={groups}
          selectedValues={groupsSelected}
          onSearch={setSearchGroupName}
          onSelect={setGroupsSelected}
          onRemove={setGroupsSelected}
          displayValue="name"
        />
      </ModalBody>
      <ModalFooter>
        <Button color="secondary" outline onClick={toggleModal}>
          <IntlMessages id="pages.cancel" />
        </Button>
        <Button color="primary" onClick={onAddButtonClicked}>
          <IntlMessages id="pages.add" />
        </Button>{' '}
      </ModalFooter>
    </Modal>
  );
};

export default injectIntl(AdGroupsToUserModal);
