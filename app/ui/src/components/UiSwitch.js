import React, { useEffect, useState } from 'react';
import Switch from 'rc-switch';
import 'rc-switch/assets/index.css';
import { Tooltip } from 'reactstrap';
import { getCurrentColor, setCurrentColor } from 'helpers/Utils';

const UiSwitch = ({
  defaultToggled,
  name,
  tooltipText,
  onToggle,
  switchClassname,
}) => {
  const [switchChecked, setSwitchChecked] = useState(defaultToggled);
  const [tooltipOpen, setTooltipOpen] = useState(false);

  const changeMode = () => {
    const newChecked = !switchChecked;
    setSwitchChecked(newChecked);
    onToggle(newChecked);
  };

  const useName = name.replace(/@/g, '_at_').replace(/[^A-Za-z0-9]/g, '_');

  return (
    <div className="d-none d-md-inline-block align-middle mr-3">
      <Switch
        id={`${useName}_tooltip_switch`}
        className={`custom-switch ${switchClassname} custom-switch-small`}
        checked={switchChecked}
        onChange={changeMode}
      />
      <Tooltip
        placement="left"
        isOpen={tooltipOpen}
        target={`${useName}_tooltip_switch`}
        toggle={() => setTooltipOpen(!tooltipOpen)}
      >
        {tooltipText}
      </Tooltip>
    </div>
  );
};
export default UiSwitch;
