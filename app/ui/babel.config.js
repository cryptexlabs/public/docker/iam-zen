module.exports = function (api) {
  api.cache.never();

  const presets = [];
  const plugins = [];

  if (process.env.NODE_ENV === 'test') {
    presets.push([
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
      },
    ]);
  }

  return {
    presets,
    plugins,
  };
};