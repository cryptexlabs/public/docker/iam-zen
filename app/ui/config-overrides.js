module.exports = function override(config, env) {
  config.resolve.fallback = {
    ...config.resolve.fallback,
    stream: require.resolve('stream-browserify'),
    crypto: require.resolve('crypto-browserify'),
    vm: require.resolve('vm-browserify'),
    buffer: require.resolve('buffer'),
    'react/jsx-runtime': require.resolve('./node_modules/react/jsx-runtime'),
    'react/jsx-dev-runtime': require.resolve('./node_modules/react/jsx-dev-runtime'),
  };

  return config;
};
