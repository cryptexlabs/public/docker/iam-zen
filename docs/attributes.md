# Supported attributes

## Key Objects
- Policy: A list of permission
- Permission: A colon separated string identifying an action a user can perform
- Group: A list of users
- Role: A temporary set of permissions assigned to a user
- Identity Link: A link to an external identity provider
- User: A user of a system
- Organization (TODO): A list of groups and/or organizations
- App

## Attributes

### Supported policy attributes
- name
- policy id
- permissions

### Supported user attributes
- username
- userid
- identity links
- groups
- policies (discouraged. prefer use of group policy. TODO)

### Supported group attributes
- group name
- identity links
- group id
- users
- policies
- organization

### Supported role attributes
- role name
- role id
- identity links
- policies

### Supported organization attributes

- groups
- organizations
- organization
- identity links

### Permissions

While permissions are just a list of strings it is recommended to organize them like so `{object}:{objectId}:{action}` and so forth
- group:any:delete
- authors:self:update
- users:any:create
- users:self:update
- organization:123458::users:any:invite

## Example of bad usage of permissions
Using roles as permission is a bad practice and should be avoided. Instead group permissions by policy and assign policies to users, groups, and roles.
- admin
- manager

## Will not support attributes

Permissions can only be attached to a policy. A permission cannot be attached directly to any entity. 

### Will not support user attributes
- email
- phone number
- first name, last name, middle name or surname
- favorite color
- permissions (permissions are inherited from groups)
- organization (organizations are inherited from groups)

### Will not support new user attributes
- policies (new users should inherit policies from a group rather than attached directly each time they are created)

### Will not support application attributes
- permissions (permissions are inherited from roles)

### Will not support group attributes
- permissions (permissions are inherited from policies)

### Will not support role attributes
- permissions (permissions are inherited from policies)

### Will not support organization attributes
- permissions (permissions are attached to policies)
- policies (policies are attached to roles, groups, and eventually users)
- roles (roles are attached to applications)
- users (users are inherited through groups)