# IAM Zen Authentication Flows

![App login with App ID flow](iam_zen_authf_app_login_with_app_id_flow.png)
![App no middleman refresh token flow](iam_zen_authf_app_no_middleman_refresh_flow.png)
![App refresh token with app ID flow](iam_zen_authf_app_refresh_with_app_id_flow.png)
![User basic authentication with user ID and username flow](iam_zen_authf_user_basic_login_with_user_id_and_username_flow.png)
![User basic authentication with username flow](iam_zen_authf_user_basic_login_with_username_flow.png)
![User refresh with no middleman flow](iam_zen_authf_user_no_middleman_refresh_flow.png)
![User refresh with token only flow](iam_zen_authf_user_refresh_with_token_only_flow.png)
![User refresh with user ID flow](iam_zen_authf_user_refresh_with_user_id_flow.png)